-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2020 at 08:11 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tradebook_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `aid` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ac_type` enum('1','2','3') NOT NULL,
  `ac_group` varchar(55) NOT NULL,
  `disp_order` varchar(6) NOT NULL,
  `closing_rate_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`aid`, `uid`, `code`, `mobile`, `username`, `email`, `ac_type`, `ac_group`, `disp_order`, `closing_rate_type_id`) VALUES
(1, NULL, 'rj', '9999999999', 'raj', 'rajrachchh@gmail.com', '3', '1', '1', 0),
(2, NULL, '1111', '1111111111', 'home', 'home@gmail.com', '2', '1', '1', 1),
(3, NULL, 'viv', '000000000', 'vivek', 'vivek@gmail.com', '3', '1', '1', 0),
(4, NULL, 'jignesh', '11', 'jignesh', 'jig@gmail.com', '3', '1', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `brokrage`
--

CREATE TABLE `brokrage` (
  `bid` int(11) NOT NULL,
  `aid` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `symbol_id` int(11) NOT NULL,
  `brokrage_type_id` int(11) NOT NULL,
  `base_on_id` int(11) NOT NULL,
  `intraday` varchar(255) NOT NULL,
  `side_id` int(11) NOT NULL,
  `nxt_day` varchar(255) NOT NULL,
  `minimum_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `buy_sell`
--

CREATE TABLE `buy_sell` (
  `buysell_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `setlement_id` int(11) NOT NULL,
  `tr_no` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `date1` date NOT NULL,
  `ltp` varchar(255) NOT NULL,
  `buy_sell_id` int(11) NOT NULL,
  `symbol_id` int(11) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `ex_date` varchar(255) NOT NULL COMMENT 'expiries_id to ex_date',
  `expiries_id` int(11) NOT NULL,
  `qty1` varchar(255) NOT NULL,
  `rate1` varchar(255) NOT NULL,
  `total_amt1` varchar(255) NOT NULL,
  `pid` int(11) NOT NULL,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `bid` int(255) NOT NULL,
  `brokrage_code` varchar(255) NOT NULL,
  `brokrage_name` varchar(255) NOT NULL,
  `re_no` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `curr_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `base_on_id` varchar(255) NOT NULL,
  `brokrage_id` int(255) NOT NULL COMMENT 'brokrage table id',
  `brokrage` varchar(255) NOT NULL COMMENT 'charges of Brokrage',
  `nxt_day` varchar(255) NOT NULL,
  `side_id` varchar(255) NOT NULL,
  `chk_one` int(11) NOT NULL,
  `chk_two` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buy_sell`
--

INSERT INTO `buy_sell` (`buysell_id`, `exchange_id`, `setlement_id`, `tr_no`, `type`, `date1`, `ltp`, `buy_sell_id`, `symbol_id`, `instument`, `ex_date`, `expiries_id`, `qty1`, `rate1`, `total_amt1`, `pid`, `party_code`, `party_name`, `bid`, `brokrage_code`, `brokrage_name`, `re_no`, `remark`, `curr_time`, `base_on_id`, `brokrage_id`, `brokrage`, `nxt_day`, `side_id`, `chk_one`, `chk_two`) VALUES
(68, 63, 2, '', '1', '2020-12-24', '', 1, 66, 'FUTSTK', '', 21, '100', '198', '19800', 3, 'viv', 'vivek', 2, '1111', 'home', '1', '', '2020-12-24 13:19:25', '0', 0, '0', '0', '0', 0, 0),
(69, 63, 2, '', '1', '2020-12-24', '', 2, 66, 'FUTSTK', '', 21, '-100', '202', '20200', 3, 'viv', 'vivek', 2, '1111', 'home', '2', '', '2020-12-24 13:19:43', '0', 0, '0', '0', '0', 0, 0),
(71, 63, 2, '', '1', '2020-12-24', '', 1, 66, 'FUTSTK', '', 21, '100', '201', '20100', 3, 'viv', 'vivek', 2, '1111', 'home', '3', '', '2020-12-24 13:23:10', '0', 0, '0', '0', '0', 0, 0),
(72, 63, 2, '', '1', '2020-12-24', '', 1, 108, 'FUTSTK', '', 21, '100', '1930', '193000', 1, 'rj', 'raj', 2, '1111', 'home', '3', '', '2020-12-24 13:39:08', '0', 0, '0', '0', '0', 0, 0),
(73, 63, 2, '', '1', '2020-12-24', '', 1, 108, 'FUTSTK', '', 21, '100', '1938', '193800', 1, 'rj', 'raj', 2, '1111', 'home', '3', '', '2020-12-24 13:39:21', '0', 0, '0', '0', '0', 0, 0),
(74, 63, 2, '', '1', '2020-12-24', '', 2, 108, 'FUTSTK', '', 21, '-100', '1945', '194500', 1, 'rj', 'raj', 2, '1111', 'home', '3', '', '2020-12-24 13:39:33', '0', 0, '0', '0', '0', 0, 0),
(75, 63, 2, '', '1', '2020-12-24', '', 1, 1, 'FUTSTK', '', 21, '100', '1560', '156000', 1, 'rj', 'raj', 2, '1111', 'home', '75', '', '2020-12-24 14:21:24', '0', 0, '0', '0', '0', 0, 0),
(76, 63, 2, '', '1', '2020-12-24', '', 2, 1, 'FUTSTK', '', 21, '-100', '1573', '157300', 1, 'rj', 'raj', 2, '1111', 'home', '76', '', '2020-12-24 14:21:59', '0', 0, '0', '0', '0', 0, 0),
(77, 63, 2, '', '1', '2020-12-24', '', 1, 1, 'FUTSTK', '', 21, '100', '1582', '158200', 1, 'rj', 'raj', 2, '1111', 'home', '77', '', '2020-12-24 14:47:58', '0', 0, '0', '0', '0', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `closing_forward`
--

CREATE TABLE `closing_forward` (
  `cf_id` int(11) NOT NULL,
  `buysell_id` int(255) NOT NULL,
  `pid` int(255) NOT NULL,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `qty1` varchar(255) NOT NULL,
  `rate1` varchar(255) NOT NULL,
  `total_amt1` varchar(255) NOT NULL,
  `exchange_id` int(255) NOT NULL,
  `symbol_id` int(255) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `expiries_id` int(255) NOT NULL,
  `ex_date` varchar(255) NOT NULL,
  `closing_rate_price` varchar(255) NOT NULL,
  `closing_rate_type_id` varchar(255) NOT NULL,
  `next_open_date_id` int(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE `exchange` (
  `exchange_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `decimal_points` tinyint(1) NOT NULL DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exchange`
--

INSERT INTO `exchange` (`exchange_id`, `name`, `decimal_points`) VALUES
(63, 'fo', 0),
(70, 'mcx', 0);

-- --------------------------------------------------------

--
-- Table structure for table `expiries`
--

CREATE TABLE `expiries` (
  `expiries_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `symbol_id` int(255) NOT NULL,
  `ex_date` varchar(255) NOT NULL,
  `ex_date_value` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `is_Default` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expiries`
--

INSERT INTO `expiries` (`expiries_id`, `exchange_id`, `symbol_id`, `ex_date`, `ex_date_value`, `status`, `is_Default`) VALUES
(21, 63, 0, '31-DEC-2020', '2020-12-31', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `forward`
--

CREATE TABLE `forward` (
  `forward_id` int(11) NOT NULL,
  `party_code` varchar(11) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `buy_sell_type` int(11) NOT NULL COMMENT 'rl,fw,cf,bf',
  `buy_sell_id` int(11) NOT NULL COMMENT 'buy,sell',
  `side_id` int(11) NOT NULL COMMENT 'Brokrage Side',
  `base_on_id` int(11) NOT NULL COMMENT 'Brokrage Base on',
  `qty1` int(11) NOT NULL COMMENT 'Buy / Sell Master',
  `rate1` int(11) NOT NULL COMMENT 'Buy / Sell Master',
  `total_amt1` int(11) NOT NULL COMMENT 'qty * rate',
  `brokrage` varchar(255) NOT NULL,
  `symbol_id` int(11) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `expiries_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `remiser_brokarge`
--

CREATE TABLE `remiser_brokarge` (
  `remiser_id` int(11) NOT NULL,
  `main_user_id` int(11) NOT NULL,
  `remiser_use_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `rmiser` tinyint(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setlement`
--

CREATE TABLE `setlement` (
  `setlement_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `setlement_no` int(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `ledger` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setlement`
--

INSERT INTO `setlement` (`setlement_id`, `exchange_id`, `setlement_no`, `start_date`, `end_date`, `description`, `ledger`, `status`) VALUES
(2, 63, 1, '2020-12-21', '2020-12-25', 'FO-1 21-12-2020 To 25-12-2020', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock_position_print`
--

CREATE TABLE `stock_position_print` (
  `print_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `a_id` int(11) NOT NULL,
  `symbol_id` int(11) NOT NULL,
  `expiries_id` int(11) NOT NULL,
  `setlement_id` int(11) NOT NULL,
  `stock_a` varchar(255) NOT NULL,
  `ltp_a` varchar(255) NOT NULL,
  `profit_loss_a` varchar(255) NOT NULL,
  `NET` varchar(255) NOT NULL,
  `M2M` varchar(255) NOT NULL,
  `diff_wb_a` varchar(255) NOT NULL,
  `avarage` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `symbol`
--

CREATE TABLE `symbol` (
  `symbol_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `symbol` varchar(255) NOT NULL,
  `sy_code` varchar(255) NOT NULL,
  `nsc_id` varchar(255) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `lot_size` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `symbol`
--

INSERT INTO `symbol` (`symbol_id`, `exchange_id`, `symbol`, `sy_code`, `nsc_id`, `instument`, `lot_size`) VALUES
(1, 63, 'ACC', 'ACC', 'ACC', 'FUTSTK', '1.00'),
(2, 63, 'ADANIENT', 'AE01', 'ADANIENT', 'FUTSTK', '1.00'),
(3, 63, 'ADANIPORTS', 'MPS', 'ADANIPORTS', 'FUTSTK', '1.00'),
(4, 63, 'AMARAJABAT', 'ARB', 'AMARAJABAT', 'FUTSTK', '1.00'),
(5, 63, 'AMBUJACEM', 'GAC', 'AMBUJACEM', 'FUTSTK', '1.00'),
(6, 63, 'APOLLOHOSP', 'AHE', 'APOLLOHOSP', 'FUTSTK', '1.00'),
(7, 63, 'APOLLOTYRE', 'AT', 'APOLLOTYRE', 'FUTSTK', '1.00'),
(8, 63, 'ASHOKLEY', 'AL', 'ASHOKLEY', 'FUTSTK', '1.00'),
(9, 63, 'ASIANPAINT', 'API', 'ASIANPAINT', 'FUTSTK', '1.00'),
(10, 63, 'AUROPHARMA', 'AP', 'AUROPHARMA', 'FUTSTK', '1.00'),
(11, 63, 'AXISBANK', 'UTI10', 'AXISBANK', 'FUTSTK', '1.00'),
(12, 63, 'BAJAJ-AUTO', 'BA06', 'BAJAJ-AUTO', 'FUTSTK', '1.00'),
(13, 63, 'BAJFINANCE', 'BAF', 'BAJFINANCE', 'FUTSTK', '1.00'),
(14, 63, 'BAJAJFINSV', 'BF04', 'BAJAJFINSV', 'FUTSTK', '1.00'),
(15, 63, 'BALKRISIND', 'BI03', 'BALKRISIND', 'FUTSTK', '1.00'),
(16, 63, 'BANDHANBNK', 'BB09', 'BANDHANBNK', 'FUTSTK', '1.00'),
(17, 63, 'BANKBARODA', 'BOB', 'BANKBARODA', 'FUTSTK', '1.00'),
(18, 63, 'BATAINDIA', 'BI01', 'BATAINDIA', 'FUTSTK', '1.00'),
(19, 63, 'BERGEPAINT', 'BPI02', 'BERGEPAINT', 'FUTSTK', '1.00'),
(20, 63, 'BEL', 'BE03', 'BEL', 'FUTSTK', '1.00'),
(21, 63, 'BHARATFORG', 'BFC', 'BHARATFORG', 'FUTSTK', '1.00'),
(22, 63, 'BHARTIARTL', 'BTV', 'BHARTIARTL', 'FUTSTK', '1.00'),
(23, 63, 'INDUSTOWER', 'BI26', 'INDUSTOWER', 'FUTSTK', '1.00'),
(24, 63, 'BHEL', 'BHE', 'BHEL', 'FUTSTK', '1.00'),
(25, 63, 'BIOCON', 'BL03', 'BIOCON', 'FUTSTK', '1.00'),
(26, 63, 'BOSCHLTD', 'MIC', 'BOSCHLTD', 'FUTSTK', '1.00'),
(27, 63, 'BPCL', 'BPC', 'BPCL', 'FUTSTK', '1.00'),
(28, 63, 'BRITANNIA', 'BI', 'BRITANNIA', 'FUTSTK', '1.00'),
(29, 63, 'CADILAHC', 'CHC', 'CADILAHC', 'FUTSTK', '1.00'),
(30, 63, 'CANBK', 'CB06', 'CANBK', 'FUTSTK', '1.00'),
(31, 63, 'CHOLAFIN', 'CIF01', 'CHOLAFIN', 'FUTSTK', '1.00'),
(32, 63, 'CIPLA', 'C', 'CIPLA', 'FUTSTK', '1.00'),
(33, 63, 'COALINDIA', 'CI29', 'COALINDIA', 'FUTSTK', '1.00'),
(34, 63, 'COFORGE', 'NII02', 'COFORGE', 'FUTSTK', '1.00'),
(35, 63, 'COLPAL', 'CPI', 'COLPAL', 'FUTSTK', '1.00'),
(36, 63, 'CONCOR', 'CCI', 'CONCOR', 'FUTSTK', '1.00'),
(37, 63, 'CUMMINSIND', 'CI02', 'CUMMINSIND', 'FUTSTK', '1.00'),
(38, 63, 'DABUR', 'DI', 'DABUR', 'FUTSTK', '1.00'),
(39, 63, 'DIVISLAB', 'DL03', 'DIVISLAB', 'FUTSTK', '1.00'),
(40, 63, 'DLF', 'D04', 'DLF', 'FUTSTK', '1.00'),
(41, 63, 'DRREDDY', 'DRL', 'DRREDDY', 'FUTSTK', '1.00'),
(42, 63, 'EICHERMOT', 'EM', 'EICHERMOT', 'FUTSTK', '1.00'),
(43, 63, 'ESCORTS', 'E', 'ESCORTS', 'FUTSTK', '1.00'),
(44, 63, 'EXIDEIND', 'EI', 'EXIDEIND', 'FUTSTK', '1.00'),
(45, 63, 'FEDERALBNK', 'FB', 'FEDERALBNK', 'FUTSTK', '1.00'),
(46, 63, 'GAIL', 'GAI', 'GAIL', 'FUTSTK', '1.00'),
(47, 63, 'GLENMARK', 'GP08', 'GLENMARK', 'FUTSTK', '1.00'),
(48, 63, 'GMRINFRA', 'GI27', 'GMRINFRA', 'FUTSTK', '1.00'),
(49, 63, 'GODREJCP', 'GCP', 'GODREJCP', 'FUTSTK', '1.00'),
(50, 63, 'GODREJPROP', 'GP11', 'GODREJPROP', 'FUTSTK', '1.00'),
(51, 63, 'GRASIM', 'GI01', 'GRASIM', 'FUTSTK', '1.00'),
(52, 63, 'HAVELLS', 'HI01', 'HAVELLS', 'FUTSTK', '1.00'),
(53, 63, 'HCLTECH', 'HCL02', 'HCLTECH', 'FUTSTK', '1.00'),
(54, 63, 'HDFC', 'HDF', 'HDFC', 'FUTSTK', '1.00'),
(55, 63, 'HDFCBANK', 'HDF01', 'HDFCBANK', 'FUTSTK', '1.00'),
(56, 63, 'HDFCLIFE', 'HSL01', 'HDFCLIFE', 'FUTSTK', '1.00'),
(57, 63, 'HEROMOTOCO', 'HHM', 'HEROMOTOCO', 'FUTSTK', '1.00'),
(58, 63, 'HINDALCO', 'H', 'HINDALCO', 'FUTSTK', '1.00'),
(59, 63, 'HINDPETRO', 'HPC', 'HINDPETRO', 'FUTSTK', '1.00'),
(60, 63, 'HINDUNILVR', 'HL', 'HINDUNILVR', 'FUTSTK', '1.00'),
(61, 63, 'ICICIBANK', 'ICI02', 'ICICIBANK', 'FUTSTK', '1.00'),
(62, 63, 'ICICIGI', 'ILG', 'ICICIGI', 'FUTSTK', '1.00'),
(63, 63, 'ICICIPRULI', 'IPL01', 'ICICIPRULI', 'FUTSTK', '1.00'),
(64, 63, 'IDFCFIRSTB', 'IDF01', 'IDFCFIRSTB', 'FUTSTK', '1.00'),
(65, 63, 'IGL', 'IG04', 'IGL', 'FUTSTK', '1.00'),
(66, 63, 'IBULHSGFIN', 'IHF01', 'IBULHSGFIN', 'FUTSTK', '1.00'),
(67, 63, 'INDUSINDBK', 'IIB', 'INDUSINDBK', 'FUTSTK', '1.00'),
(68, 63, 'NAUKRI', 'IEI01', 'NAUKRI', 'FUTSTK', '1.00'),
(69, 63, 'INFY', 'IT', 'INFY', 'FUTSTK', '1.00'),
(70, 63, 'INDIGO', 'IA05', 'INDIGO', 'FUTSTK', '1.00'),
(71, 63, 'IOC', 'IOC', 'IOC', 'FUTSTK', '1.00'),
(72, 63, 'ITC', 'ITC', 'ITC', 'FUTSTK', '1.00'),
(73, 63, 'JINDALSTEL', 'JSP', 'JINDALSTEL', 'FUTSTK', '1.00'),
(74, 63, 'JSWSTEEL', 'JVS', 'JSWSTEEL', 'FUTSTK', '1.00'),
(75, 63, 'JUBLFOOD', 'JF04', 'JUBLFOOD', 'FUTSTK', '1.00'),
(76, 63, 'KOTAKBANK', 'KMF', 'KOTAKBANK', 'FUTSTK', '1.00'),
(77, 63, 'L&TFH', 'LFH', 'L&TFH', 'FUTSTK', '1.00'),
(78, 63, 'LT', 'LT', 'LT', 'FUTSTK', '1.00'),
(79, 63, 'LICHSGFIN', 'LIC', 'LICHSGFIN', 'FUTSTK', '1.00'),
(80, 63, 'LUPIN', 'LC03', 'LUPIN', 'FUTSTK', '1.00'),
(81, 63, 'M&M', 'MM', 'M&M', 'FUTSTK', '1.00'),
(82, 63, 'M&MFIN', 'MMF04', 'M&MFIN', 'FUTSTK', '1.00'),
(83, 63, 'MGL', 'MG02', 'MGL', 'FUTSTK', '1.00'),
(84, 63, 'MANAPPURAM', 'MGF01', 'MANAPPURAM', 'FUTSTK', '1.00'),
(85, 63, 'MARICO', 'MI25', 'MARICO', 'FUTSTK', '1.00'),
(86, 63, 'MARUTI', 'MU01', 'MARUTI', 'FUTSTK', '1.00'),
(87, 63, 'MFSL', 'MI', 'MFSL', 'FUTSTK', '1.00'),
(88, 63, 'MINDTREE', 'MC20', 'MINDTREE', 'FUTSTK', '1.00'),
(89, 63, 'MOTHERSUMI', 'MSS01', 'MOTHERSUMI', 'FUTSTK', '1.00'),
(90, 63, 'MRF', 'MRF', 'MRF', 'FUTSTK', '1.00'),
(91, 63, 'MUTHOOTFIN', 'MF19', 'MUTHOOTFIN', 'FUTSTK', '1.00'),
(92, 63, 'NATIONALUM', 'NAC', 'NATIONALUM', 'FUTSTK', '1.00'),
(93, 63, 'NESTLEIND', 'NI', 'NESTLEIND', 'FUTSTK', '1.00'),
(94, 63, 'NMDC', 'NMD01', 'NMDC', 'FUTSTK', '1.00'),
(95, 63, 'NTPC', 'NTP', 'NTPC', 'FUTSTK', '1.00'),
(96, 63, 'ONGC', 'ONG', 'ONGC', 'FUTSTK', '1.00'),
(97, 63, 'PAGEIND', 'PI35', 'PAGEIND', 'FUTSTK', '1.00'),
(98, 63, 'PETRONET', 'PLN', 'PETRONET', 'FUTSTK', '1.00'),
(99, 63, 'PIDILITIND', 'PI11', 'PIDILITIND', 'FUTSTK', '1.00'),
(100, 63, 'PEL', 'NP07', 'PEL', 'FUTSTK', '1.00'),
(101, 63, 'PNB', 'PNB05', 'PNB', 'FUTSTK', '1.00'),
(102, 63, 'PFC', 'PFC02', 'PFC', 'FUTSTK', '1.00'),
(103, 63, 'POWERGRID', 'PGC', 'POWERGRID', 'FUTSTK', '1.00'),
(104, 63, 'PVR', 'PVR', 'PVR', 'FUTSTK', '1.00'),
(105, 63, 'RAMCOCEM', 'MC', 'RAMCOCEM', 'FUTSTK', '1.00'),
(106, 63, 'RBLBANK', 'RB02', 'RBLBANK', 'FUTSTK', '1.00'),
(107, 63, 'RECLTD', 'REC02', 'RECLTD', 'FUTSTK', '1.00'),
(108, 63, 'RELIANCE', 'RI', 'RELIANCE', 'FUTSTK', '1.00'),
(109, 63, 'SAIL', 'SAI', 'SAIL', 'FUTSTK', '1.00'),
(110, 63, 'SBIN', 'SBI', 'SBIN', 'FUTSTK', '1.00'),
(111, 63, 'SBILIFE', 'SLI03', 'SBILIFE', 'FUTSTK', '1.00'),
(112, 63, 'SHREECEM', 'SC12', 'SHREECEM', 'FUTSTK', '1.00'),
(113, 63, 'SRTRANSFIN', 'STF', 'SRTRANSFIN', 'FUTSTK', '1.00'),
(114, 63, 'SIEMENS', 'S', 'SIEMENS', 'FUTSTK', '1.00'),
(115, 63, 'SRF', 'SRF', 'SRF', 'FUTSTK', '1.00'),
(116, 63, 'SUNPHARMA', 'SPI', 'SUNPHARMA', 'FUTSTK', '1.00'),
(117, 63, 'SUNTV', 'STV01', 'SUNTV', 'FUTSTK', '1.00'),
(118, 63, 'TATACHEM', 'TC', 'TATACHEM', 'FUTSTK', '1.00'),
(119, 63, 'TATACONSUM', 'TT', 'TATACONSUM', 'FUTSTK', '1.00'),
(120, 63, 'TATAMOTORS', 'TEL', 'TATAMOTORS', 'FUTSTK', '1.00'),
(121, 63, 'TATAPOWER', 'TPC', 'TATAPOWER', 'FUTSTK', '1.00'),
(122, 63, 'TATASTEEL', 'TIS', 'TATASTEEL', 'FUTSTK', '1.00'),
(123, 63, 'TCS', 'TCS', 'TCS', 'FUTSTK', '1.00'),
(124, 63, 'TECHM', 'TM4', 'TECHM', 'FUTSTK', '1.00'),
(125, 63, 'TITAN', 'TI01', 'TITAN', 'FUTSTK', '1.00'),
(126, 63, 'TORNTPHARM', 'TP06', 'TORNTPHARM', 'FUTSTK', '1.00'),
(127, 63, 'TORNTPOWER', 'TP13', 'TORNTPOWER', 'FUTSTK', '1.00'),
(128, 63, 'TVSMOTOR', 'TVS', 'TVSMOTOR', 'FUTSTK', '1.00'),
(129, 63, 'ULTRACEMCO', 'UTC', 'ULTRACEMCO', 'FUTSTK', '1.00'),
(130, 63, 'UBL', 'UBB', 'UBL', 'FUTSTK', '1.00'),
(131, 63, 'MCDOWELL-N', 'MC08', 'MCDOWELL-N', 'FUTSTK', '1.00'),
(132, 63, 'UPL', 'SI10', 'UPL', 'FUTSTK', '1.00'),
(133, 63, 'VEDL', 'SG', 'VEDL', 'FUTSTK', '1.00'),
(134, 63, 'IDEA', 'IC8', 'IDEA', 'FUTSTK', '1.00'),
(135, 63, 'VOLTAS', 'V', 'VOLTAS', 'FUTSTK', '1.00'),
(136, 63, 'WIPRO', 'W', 'WIPRO', 'FUTSTK', '1.00'),
(137, 63, 'ZEEL', 'ZT', 'ZEEL', 'FUTSTK', '1.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$BnyluHe1aAxM.HYIL4ZVOu20V.vJ/fxzP14owCUn4qilt2YxYlZBW', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1608830056, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(4, '::1', 'rajrachchh', '$2y$10$nZf4N9a5nFxLi5JyOm2n4egDBf.Ar2/MOFRAvzM4SH6TsNBKemBDO', 'raj@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1590848806, NULL, 1, 'raja', 'rachchh', 'webcode', '9999999999'),
(5, '::1', 'vivekrachchh', '$2y$10$2HLe69.PbcjEalzp2N7K2.CVQhVvW6LA.O8QQTO0.MxUjMQDulhYS', 'vivek@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1590923577, NULL, 1, 'vivek', 'rachchh', '1', '123');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(6, 4, 1),
(7, 4, 2),
(8, 5, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `brokrage`
--
ALTER TABLE `brokrage`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `buy_sell`
--
ALTER TABLE `buy_sell`
  ADD PRIMARY KEY (`buysell_id`);

--
-- Indexes for table `closing_forward`
--
ALTER TABLE `closing_forward`
  ADD PRIMARY KEY (`cf_id`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
  ADD PRIMARY KEY (`exchange_id`);

--
-- Indexes for table `expiries`
--
ALTER TABLE `expiries`
  ADD PRIMARY KEY (`expiries_id`),
  ADD KEY `exchange_id` (`exchange_id`);

--
-- Indexes for table `forward`
--
ALTER TABLE `forward`
  ADD PRIMARY KEY (`forward_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remiser_brokarge`
--
ALTER TABLE `remiser_brokarge`
  ADD PRIMARY KEY (`remiser_id`);

--
-- Indexes for table `setlement`
--
ALTER TABLE `setlement`
  ADD PRIMARY KEY (`setlement_id`),
  ADD KEY `exchange_id` (`exchange_id`);

--
-- Indexes for table `stock_position_print`
--
ALTER TABLE `stock_position_print`
  ADD PRIMARY KEY (`print_id`);

--
-- Indexes for table `symbol`
--
ALTER TABLE `symbol`
  ADD PRIMARY KEY (`symbol_id`),
  ADD KEY `exchange_id` (`exchange_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `brokrage`
--
ALTER TABLE `brokrage`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `buy_sell`
--
ALTER TABLE `buy_sell`
  MODIFY `buysell_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `closing_forward`
--
ALTER TABLE `closing_forward`
  MODIFY `cf_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
  MODIFY `exchange_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `expiries`
--
ALTER TABLE `expiries`
  MODIFY `expiries_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `forward`
--
ALTER TABLE `forward`
  MODIFY `forward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `remiser_brokarge`
--
ALTER TABLE `remiser_brokarge`
  MODIFY `remiser_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setlement`
--
ALTER TABLE `setlement`
  MODIFY `setlement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock_position_print`
--
ALTER TABLE `stock_position_print`
  MODIFY `print_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `symbol`
--
ALTER TABLE `symbol`
  MODIFY `symbol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `expiries`
--
ALTER TABLE `expiries`
  ADD CONSTRAINT `expiries_ibfk_1` FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`exchange_id`);

--
-- Constraints for table `setlement`
--
ALTER TABLE `setlement`
  ADD CONSTRAINT `setlement_ibfk_1` FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`exchange_id`);

--
-- Constraints for table `symbol`
--
ALTER TABLE `symbol`
  ADD CONSTRAINT `symbol_ibfk_1` FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`exchange_id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
