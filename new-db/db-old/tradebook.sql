-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 03:54 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tradebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `aid` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ac_type` enum('1','2','3') NOT NULL,
  `ac_group` varchar(55) NOT NULL,
  `disp_order` varchar(6) NOT NULL,
  `closing_rate_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`aid`, `uid`, `code`, `mobile`, `username`, `email`, `ac_type`, `ac_group`, `disp_order`, `closing_rate_type_id`) VALUES
(118, NULL, '3', '9999999999', 'nick', 'nick@gmail.com', '3', '2', '1', 2),
(119, 1, '1', '99999999999', 'administrator', 'admin@admin.com', '1', '1', '1', 1),
(120, 4, '2', '88888888', 'rajrachchh', 'raj@gmail.com', '3', '1', '1', 1),
(128, NULL, 'u', '9999999999', 'u', 'u@gmail.com', '2', '0', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `brokrage`
--

CREATE TABLE `brokrage` (
  `bid` int(11) NOT NULL,
  `aid` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `symbol_id` int(11) NOT NULL,
  `brokrage_type_id` int(11) NOT NULL,
  `base_on_id` int(11) NOT NULL,
  `intraday` varchar(255) NOT NULL,
  `side_id` int(11) NOT NULL,
  `nxt_day` varchar(255) NOT NULL,
  `minimum_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `buy_sell`
--

CREATE TABLE `buy_sell` (
  `buysell_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `setlement_id` int(11) NOT NULL,
  `tr_no` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `date1` date NOT NULL,
  `buy_sell_id` int(11) NOT NULL,
  `symbol_id` int(11) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `ex_date` varchar(255) NOT NULL COMMENT 'expiries_id to ex_date',
  `qty1` varchar(255) NOT NULL,
  `rate1` varchar(255) NOT NULL,
  `total_amt1` varchar(255) NOT NULL,
  `pid` int(11) NOT NULL,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `bid` int(255) NOT NULL,
  `brokrage_code` varchar(255) NOT NULL,
  `brokrage_name` varchar(255) NOT NULL,
  `re_no` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `curr_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `base_on_id` varchar(255) NOT NULL,
  `brokrage_id` int(255) NOT NULL COMMENT 'brokrage table id',
  `brokrage` varchar(255) NOT NULL COMMENT 'charges of Brokrage',
  `nxt_day` varchar(255) NOT NULL,
  `side_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buy_sell`
--

INSERT INTO `buy_sell` (`buysell_id`, `exchange_id`, `setlement_id`, `tr_no`, `type`, `date1`, `buy_sell_id`, `symbol_id`, `instument`, `ex_date`, `qty1`, `rate1`, `total_amt1`, `pid`, `party_code`, `party_name`, `bid`, `brokrage_code`, `brokrage_name`, `re_no`, `remark`, `curr_time`, `base_on_id`, `brokrage_id`, `brokrage`, `nxt_day`, `side_id`) VALUES
(31, 63, 4, '', '1', '2020-09-14', 1, 171, 'FUTSTK', '24-Sep-2020', '2', '20', '40', 120, '2', 'rajrachchh', 128, 'u', 'u', '31', '', '2020-09-14 12:31:45', '0', 0, '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `closing_forward`
--

CREATE TABLE `closing_forward` (
  `cf_id` int(11) NOT NULL,
  `buysell_id` int(255) NOT NULL,
  `pid` int(255) NOT NULL,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `qty1` varchar(255) NOT NULL,
  `rate1` varchar(255) NOT NULL,
  `total_amt1` varchar(255) NOT NULL,
  `exchange_id` int(255) NOT NULL,
  `symbol_id` int(255) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `expiries_id` int(255) NOT NULL,
  `ex_date` varchar(255) NOT NULL,
  `closing_rate_price` varchar(255) NOT NULL,
  `closing_rate_type_id` varchar(255) NOT NULL,
  `next_open_date_id` int(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE `exchange` (
  `exchange_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `decimal_points` tinyint(1) NOT NULL DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exchange`
--

INSERT INTO `exchange` (`exchange_id`, `name`, `decimal_points`) VALUES
(63, 'fo', 0),
(70, 'mcx', 0),
(71, 'ncdex', 0);

-- --------------------------------------------------------

--
-- Table structure for table `expiries`
--

CREATE TABLE `expiries` (
  `expiries_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `symbol_id` varchar(11) NOT NULL,
  `ex_date` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `is_Default` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expiries`
--

INSERT INTO `expiries` (`expiries_id`, `exchange_id`, `symbol_id`, `ex_date`, `status`, `is_Default`) VALUES
(331, 63, '0', '27-Aug-2020', 1, 0),
(332, 63, '0', '24-Sep-2020', 1, 0),
(333, 63, '0', '29-Oct-2020', 1, 0),
(336, 71, '', '20-May-15', 1, 0),
(337, 71, '', '19-Jun-15', 1, 0),
(338, 71, '', '20-Jul-15', 1, 0),
(339, 71, '', '11-Feb-15', 1, 0),
(340, 71, '', '11-Mar-15', 1, 0),
(341, 71, '', '13-Apr-15', 1, 0),
(342, 71, '', '11-May-15', 1, 0),
(343, 71, '', '11-Jun-15', 1, 0),
(344, 71, '', '20-Feb-15', 1, 0),
(345, 71, '', '13-Jul-15', 1, 0),
(346, 71, '', '27-Feb-15', 1, 0),
(347, 71, '', '30-Apr-15', 1, 0),
(348, 71, '', '30-Jun-15', 1, 0),
(349, 71, '', '31-Mar-15', 1, 0),
(350, 71, '', '29-May-15', 1, 0),
(351, 71, '', '19-Feb-15', 1, 0),
(352, 71, '', '19-Mar-15', 1, 0),
(353, 71, '', '03-Apr-15', 1, 0),
(354, 71, '', '25-Feb-15', 1, 0),
(355, 71, '', '27-Mar-15', 1, 0),
(356, 71, '', '28-Apr-15', 1, 0),
(357, 71, '', '27-May-15', 1, 0),
(358, 71, '', '29-Jul-15', 1, 0),
(359, 71, '', '03-Mar-15', 1, 0),
(360, 71, '', '04-May-15', 1, 0),
(361, 71, '', '03-Jul-15', 1, 0),
(362, 71, '', '03-Sep-15', 1, 0),
(363, 71, '', '26-Jun-15', 1, 0),
(364, 71, '', '20-Oct-15', 1, 0),
(365, 71, '', '18-Dec-15', 1, 0),
(366, 71, '', '18-Mar-16', 1, 0),
(367, 71, '', '20-May-16', 1, 0),
(368, 71, '', '20-Jul-16', 1, 0),
(369, 71, '', '11-Aug-15', 1, 0),
(370, 71, '', '12-Oct-15', 1, 0),
(371, 71, '', '20-Aug-15', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `forward`
--

CREATE TABLE `forward` (
  `forward_id` int(11) NOT NULL,
  `party_code` varchar(11) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `buy_sell_type` int(11) NOT NULL COMMENT 'rl,fw,cf,bf',
  `buy_sell_id` int(11) NOT NULL COMMENT 'buy,sell',
  `side_id` int(11) NOT NULL COMMENT 'Brokrage Side',
  `base_on_id` int(11) NOT NULL COMMENT 'Brokrage Base on',
  `qty1` int(11) NOT NULL COMMENT 'Buy / Sell Master',
  `rate1` int(11) NOT NULL COMMENT 'Buy / Sell Master',
  `total_amt1` int(11) NOT NULL COMMENT 'qty * rate',
  `brokrage` varchar(255) NOT NULL,
  `symbol_id` int(11) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `expiries_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `forward`
--

INSERT INTO `forward` (`forward_id`, `party_code`, `party_name`, `buy_sell_type`, `buy_sell_id`, `side_id`, `base_on_id`, `qty1`, `rate1`, `total_amt1`, `brokrage`, `symbol_id`, `instument`, `expiries_id`) VALUES
(21, '786', 'nick', 1, 1, 0, 0, 10, 100, 1000, '', 358, 'OPTSTK', 66);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `remiser_brokarge`
--

CREATE TABLE `remiser_brokarge` (
  `remiser_id` int(11) NOT NULL,
  `main_user_id` int(11) NOT NULL,
  `remiser_use_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `rmiser` tinyint(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setlement`
--

CREATE TABLE `setlement` (
  `setlement_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `setlement_no` int(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `ledger` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setlement`
--

INSERT INTO `setlement` (`setlement_id`, `exchange_id`, `setlement_no`, `start_date`, `end_date`, `description`, `ledger`, `status`) VALUES
(1, 63, 1, '2020-09-11', '2020-09-15', 'FO-1 11-09-2020 To 15-09-2020', 0, 1),
(2, 63, 2, '2020-09-16', '2020-09-20', 'FO-2 16-09-2020 To 20-09-2020', 1, 1),
(3, 70, 3, '2020-09-12', '2020-09-16', 'MCX-3 12-09-2020 To 16-09-2020', 1, 1),
(4, 63, 4, '2020-09-14', '2020-09-18', 'FO-4 14-09-2020 To 18-09-2020', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `symbol`
--

CREATE TABLE `symbol` (
  `symbol_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `symbol` varchar(255) NOT NULL,
  `instument` varchar(255) NOT NULL,
  `lot_size` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `symbol`
--

INSERT INTO `symbol` (`symbol_id`, `exchange_id`, `symbol`, `instument`, `lot_size`) VALUES
(1, 70, 'ALUMINI      ', 'FUTCOM', '1.00'),
(2, 70, 'ALUMINIUM    ', 'FUTCOM', '1.00'),
(3, 70, 'BRCRUDEOIL   ', 'FUTCOM', '1.00'),
(4, 70, 'CARDAMOM     ', 'FUTCOM', '1.00'),
(5, 70, 'CASTORSEED   ', 'FUTCOM', '1.00'),
(6, 70, 'COPPER       ', 'FUTCOM', '1.00'),
(7, 70, 'COPPERM      ', 'FUTCOM', '1.00'),
(8, 70, 'COTTON       ', 'FUTCOM', '1.00'),
(9, 70, 'CPO          ', 'FUTCOM', '1.00'),
(10, 70, 'CRUDEOIL     ', 'FUTCOM', '1.00'),
(11, 70, 'CRUDEOILM    ', 'FUTCOM', '1.00'),
(12, 70, 'GOLD         ', 'FUTCOM', '1.00'),
(13, 70, 'GOLDGUINEA   ', 'FUTCOM', '1.00'),
(14, 70, 'GOLDM        ', 'FUTCOM', '1.00'),
(15, 70, 'GOLDPETAL    ', 'FUTCOM', '1.00'),
(16, 70, 'GOLDPTLDEL   ', 'FUTCOM', '1.00'),
(17, 70, 'KAPAS        ', 'FUTCOM', '1.00'),
(18, 70, 'LEAD         ', 'FUTCOM', '1.00'),
(19, 70, 'LEADMINI     ', 'FUTCOM', '1.00'),
(20, 70, 'MENTHAOIL    ', 'FUTCOM', '1.00'),
(21, 70, 'NATURALGAS   ', 'FUTCOM', '1.00'),
(22, 70, 'NICKEL       ', 'FUTCOM', '1.00'),
(23, 70, 'NICKELM      ', 'FUTCOM', '1.00'),
(24, 70, 'SILVER       ', 'FUTCOM', '1.00'),
(25, 70, 'SILVER1000   ', 'FUTCOM', '1.00'),
(26, 70, 'SILVERM      ', 'FUTCOM', '1.00'),
(27, 70, 'SILVERMIC    ', 'FUTCOM', '1.00'),
(28, 70, 'ZINC         ', 'FUTCOM', '1.00'),
(29, 70, 'ZINCMINI     ', 'FUTCOM', '1.00'),
(30, 63, 'BANKNIFTY', 'FUTIDX', '1.00'),
(31, 63, 'NIFTY', 'FUTIDX', '1.00'),
(33, 63, 'ADANIENT', 'FUTSTK', '1.00'),
(34, 63, 'ADANIPORTS', 'FUTSTK', '1.00'),
(35, 63, 'AMARAJABAT', 'FUTSTK', '1.00'),
(36, 63, 'AMBUJACEM', 'FUTSTK', '1.00'),
(37, 63, 'APOLLOHOSP', 'FUTSTK', '1.00'),
(38, 63, 'APOLLOTYRE', 'FUTSTK', '1.00'),
(39, 63, 'ASHOKLEY', 'FUTSTK', '1.00'),
(40, 63, 'ASIANPAINT', 'FUTSTK', '1.00'),
(41, 63, 'AUROPHARMA', 'FUTSTK', '1.00'),
(42, 63, 'AXISBANK', 'FUTSTK', '1.00'),
(43, 63, 'BAJAJ-AUTO', 'FUTSTK', '1.00'),
(44, 63, 'BAJAJFINSV', 'FUTSTK', '1.00'),
(45, 63, 'BAJFINANCE', 'FUTSTK', '1.00'),
(46, 63, 'BALKRISIND', 'FUTSTK', '1.00'),
(47, 63, 'BANDHANBNK', 'FUTSTK', '1.00'),
(48, 63, 'BANKBARODA', 'FUTSTK', '1.00'),
(49, 63, 'BATAINDIA', 'FUTSTK', '1.00'),
(50, 63, 'BEL', 'FUTSTK', '1.00'),
(51, 63, 'BERGEPAINT', 'FUTSTK', '1.00'),
(52, 63, 'BHARATFORG', 'FUTSTK', '1.00'),
(53, 63, 'BHARTIARTL', 'FUTSTK', '1.00'),
(54, 63, 'BHEL', 'FUTSTK', '1.00'),
(55, 63, 'BIOCON', 'FUTSTK', '1.00'),
(56, 63, 'BOSCHLTD', 'FUTSTK', '1.00'),
(57, 63, 'BPCL', 'FUTSTK', '1.00'),
(58, 63, 'BRITANNIA', 'FUTSTK', '1.00'),
(59, 63, 'CADILAHC', 'FUTSTK', '1.00'),
(60, 63, 'CANBK', 'FUTSTK', '1.00'),
(61, 63, 'CENTURYTEX', 'FUTSTK', '1.00'),
(62, 63, 'CHOLAFIN', 'FUTSTK', '1.00'),
(63, 63, 'CIPLA', 'FUTSTK', '1.00'),
(64, 63, 'COALINDIA', 'FUTSTK', '1.00'),
(65, 63, 'COFORGE', 'FUTSTK', '1.00'),
(66, 63, 'COLPAL', 'FUTSTK', '1.00'),
(67, 63, 'CONCOR', 'FUTSTK', '1.00'),
(68, 63, 'CUMMINSIND', 'FUTSTK', '1.00'),
(69, 63, 'DABUR', 'FUTSTK', '1.00'),
(70, 63, 'DIVISLAB', 'FUTSTK', '1.00'),
(71, 63, 'DLF', 'FUTSTK', '1.00'),
(72, 63, 'DRREDDY', 'FUTSTK', '1.00'),
(73, 63, 'EICHERMOT', 'FUTSTK', '1.00'),
(74, 63, 'EQUITAS', 'FUTSTK', '1.00'),
(75, 63, 'ESCORTS', 'FUTSTK', '1.00'),
(76, 63, 'EXIDEIND', 'FUTSTK', '1.00'),
(77, 63, 'FEDERALBNK', 'FUTSTK', '1.00'),
(78, 63, 'GAIL', 'FUTSTK', '1.00'),
(79, 63, 'GLENMARK', 'FUTSTK', '1.00'),
(80, 63, 'GMRINFRA', 'FUTSTK', '1.00'),
(81, 63, 'GODREJCP', 'FUTSTK', '1.00'),
(82, 63, 'GODREJPROP', 'FUTSTK', '1.00'),
(83, 63, 'GRASIM', 'FUTSTK', '1.00'),
(84, 63, 'HAVELLS', 'FUTSTK', '1.00'),
(85, 63, 'HCLTECH', 'FUTSTK', '1.00'),
(86, 63, 'HDFC', 'FUTSTK', '1.00'),
(87, 63, 'HDFCBANK', 'FUTSTK', '1.00'),
(88, 63, 'HDFCLIFE', 'FUTSTK', '1.00'),
(89, 63, 'HEROMOTOCO', 'FUTSTK', '1.00'),
(90, 63, 'HINDALCO', 'FUTSTK', '1.00'),
(91, 63, 'HINDPETRO', 'FUTSTK', '1.00'),
(92, 63, 'HINDUNILVR', 'FUTSTK', '1.00'),
(93, 63, 'IBULHSGFIN', 'FUTSTK', '1.00'),
(94, 63, 'ICICIBANK', 'FUTSTK', '1.00'),
(95, 63, 'ICICIPRULI', 'FUTSTK', '1.00'),
(96, 63, 'IDEA', 'FUTSTK', '1.00'),
(97, 63, 'IDFCFIRSTB', 'FUTSTK', '1.00'),
(98, 63, 'IGL', 'FUTSTK', '1.00'),
(99, 63, 'INDIGO', 'FUTSTK', '1.00'),
(100, 63, 'INDUSINDBK', 'FUTSTK', '1.00'),
(101, 63, 'INFRATEL', 'FUTSTK', '1.00'),
(102, 63, 'INFY', 'FUTSTK', '1.00'),
(103, 63, 'IOC', 'FUTSTK', '1.00'),
(104, 63, 'ITC', 'FUTSTK', '1.00'),
(105, 63, 'JINDALSTEL', 'FUTSTK', '1.00'),
(106, 63, 'JSWSTEEL', 'FUTSTK', '1.00'),
(107, 63, 'JUBLFOOD', 'FUTSTK', '1.00'),
(108, 63, 'KOTAKBANK', 'FUTSTK', '1.00'),
(109, 63, 'L&TFH', 'FUTSTK', '1.00'),
(110, 63, 'LICHSGFIN', 'FUTSTK', '1.00'),
(111, 63, 'LT', 'FUTSTK', '1.00'),
(112, 63, 'LUPIN', 'FUTSTK', '1.00'),
(113, 63, 'M&M', 'FUTSTK', '1.00'),
(114, 63, 'M&MFIN', 'FUTSTK', '1.00'),
(115, 63, 'MANAPPURAM', 'FUTSTK', '1.00'),
(116, 63, 'MARICO', 'FUTSTK', '1.00'),
(117, 63, 'MARUTI', 'FUTSTK', '1.00'),
(118, 63, 'MCDOWELL-N', 'FUTSTK', '1.00'),
(119, 63, 'MFSL', 'FUTSTK', '1.00'),
(120, 63, 'MGL', 'FUTSTK', '1.00'),
(121, 63, 'MINDTREE', 'FUTSTK', '1.00'),
(122, 63, 'MOTHERSUMI', 'FUTSTK', '1.00'),
(123, 63, 'MRF', 'FUTSTK', '1.00'),
(124, 63, 'MUTHOOTFIN', 'FUTSTK', '1.00'),
(125, 63, 'NATIONALUM', 'FUTSTK', '1.00'),
(126, 63, 'NAUKRI', 'FUTSTK', '1.00'),
(127, 63, 'NESTLEIND', 'FUTSTK', '1.00'),
(128, 63, 'NMDC', 'FUTSTK', '1.00'),
(129, 63, 'NTPC', 'FUTSTK', '1.00'),
(130, 63, 'ONGC', 'FUTSTK', '1.00'),
(131, 63, 'PAGEIND', 'FUTSTK', '1.00'),
(132, 63, 'PEL', 'FUTSTK', '1.00'),
(133, 63, 'PETRONET', 'FUTSTK', '1.00'),
(134, 63, 'PFC', 'FUTSTK', '1.00'),
(135, 63, 'PIDILITIND', 'FUTSTK', '1.00'),
(136, 63, 'PNB', 'FUTSTK', '1.00'),
(137, 63, 'POWERGRID', 'FUTSTK', '1.00'),
(138, 63, 'PVR', 'FUTSTK', '1.00'),
(139, 63, 'RAMCOCEM', 'FUTSTK', '1.00'),
(140, 63, 'RBLBANK', 'FUTSTK', '1.00'),
(141, 63, 'RECLTD', 'FUTSTK', '1.00'),
(142, 63, 'RELIANCE', 'FUTSTK', '1.00'),
(143, 63, 'SAIL', 'FUTSTK', '1.00'),
(144, 63, 'SBILIFE', 'FUTSTK', '1.00'),
(145, 63, 'SBIN', 'FUTSTK', '1.00'),
(146, 63, 'SHREECEM', 'FUTSTK', '1.00'),
(147, 63, 'SIEMENS', 'FUTSTK', '1.00'),
(148, 63, 'SRF', 'FUTSTK', '1.00'),
(149, 63, 'SRTRANSFIN', 'FUTSTK', '1.00'),
(150, 63, 'SUNPHARMA', 'FUTSTK', '1.00'),
(151, 63, 'SUNTV', 'FUTSTK', '1.00'),
(152, 63, 'TATACHEM', 'FUTSTK', '1.00'),
(153, 63, 'TATACONSUM', 'FUTSTK', '1.00'),
(154, 63, 'TATAMOTORS', 'FUTSTK', '1.00'),
(155, 63, 'TATAPOWER', 'FUTSTK', '1.00'),
(156, 63, 'TATASTEEL', 'FUTSTK', '1.00'),
(157, 63, 'TCS', 'FUTSTK', '1.00'),
(158, 63, 'TECHM', 'FUTSTK', '1.00'),
(159, 63, 'TITAN', 'FUTSTK', '1.00'),
(160, 63, 'TORNTPHARM', 'FUTSTK', '1.00'),
(161, 63, 'TORNTPOWER', 'FUTSTK', '1.00'),
(162, 63, 'TVSMOTOR', 'FUTSTK', '1.00'),
(163, 63, 'UBL', 'FUTSTK', '1.00'),
(164, 63, 'UJJIVAN', 'FUTSTK', '1.00'),
(165, 63, 'ULTRACEMCO', 'FUTSTK', '1.00'),
(166, 63, 'UPL', 'FUTSTK', '1.00'),
(167, 63, 'VEDL', 'FUTSTK', '1.00'),
(168, 63, 'VOLTAS', 'FUTSTK', '1.00'),
(169, 63, 'WIPRO', 'FUTSTK', '1.00'),
(170, 63, 'ZEEL', 'FUTSTK', '1.00'),
(171, 63, 'ACC', 'FUTSTK', '1.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$BnyluHe1aAxM.HYIL4ZVOu20V.vJ/fxzP14owCUn4qilt2YxYlZBW', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1600069111, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(4, '::1', 'rajrachchh', '$2y$10$nZf4N9a5nFxLi5JyOm2n4egDBf.Ar2/MOFRAvzM4SH6TsNBKemBDO', 'raj@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1590848806, NULL, 1, 'raja', 'rachchh', 'webcode', '9999999999'),
(5, '::1', 'vivekrachchh', '$2y$10$2HLe69.PbcjEalzp2N7K2.CVQhVvW6LA.O8QQTO0.MxUjMQDulhYS', 'vivek@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1590923577, NULL, 1, 'vivek', 'rachchh', '1', '123');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(6, 4, 1),
(7, 4, 2),
(8, 5, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `brokrage`
--
ALTER TABLE `brokrage`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `buy_sell`
--
ALTER TABLE `buy_sell`
  ADD PRIMARY KEY (`buysell_id`);

--
-- Indexes for table `closing_forward`
--
ALTER TABLE `closing_forward`
  ADD PRIMARY KEY (`cf_id`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
  ADD PRIMARY KEY (`exchange_id`);

--
-- Indexes for table `expiries`
--
ALTER TABLE `expiries`
  ADD PRIMARY KEY (`expiries_id`),
  ADD KEY `exchange_id` (`exchange_id`);

--
-- Indexes for table `forward`
--
ALTER TABLE `forward`
  ADD PRIMARY KEY (`forward_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remiser_brokarge`
--
ALTER TABLE `remiser_brokarge`
  ADD PRIMARY KEY (`remiser_id`);

--
-- Indexes for table `setlement`
--
ALTER TABLE `setlement`
  ADD PRIMARY KEY (`setlement_id`),
  ADD KEY `exchange_id` (`exchange_id`);

--
-- Indexes for table `symbol`
--
ALTER TABLE `symbol`
  ADD PRIMARY KEY (`symbol_id`),
  ADD KEY `exchange_id` (`exchange_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `brokrage`
--
ALTER TABLE `brokrage`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `buy_sell`
--
ALTER TABLE `buy_sell`
  MODIFY `buysell_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `closing_forward`
--
ALTER TABLE `closing_forward`
  MODIFY `cf_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
  MODIFY `exchange_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `expiries`
--
ALTER TABLE `expiries`
  MODIFY `expiries_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=372;

--
-- AUTO_INCREMENT for table `forward`
--
ALTER TABLE `forward`
  MODIFY `forward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `remiser_brokarge`
--
ALTER TABLE `remiser_brokarge`
  MODIFY `remiser_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setlement`
--
ALTER TABLE `setlement`
  MODIFY `setlement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `symbol`
--
ALTER TABLE `symbol`
  MODIFY `symbol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `expiries`
--
ALTER TABLE `expiries`
  ADD CONSTRAINT `expiries_ibfk_1` FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`exchange_id`);

--
-- Constraints for table `setlement`
--
ALTER TABLE `setlement`
  ADD CONSTRAINT `setlement_ibfk_1` FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`exchange_id`);

--
-- Constraints for table `symbol`
--
ALTER TABLE `symbol`
  ADD CONSTRAINT `symbol_ibfk_1` FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`exchange_id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
