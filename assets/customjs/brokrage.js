
		var ac_global;
		var ex_global;
		var code_global;
		var name_global;
	
	
		// Load Data Table Start
		// load_data();
		$('#exchange_id').on('change', function(event){ // change // exchange_id
		
			// var inputVal=$('#exchange_id').val(); // or var inputVal = $(this).val();
			ex_global= $('#exchange_id').val();
			name_global= $('#name').val();
			code_global= $('#code').val();
			load_data();
			
		});
		function load_data()
		{
			$('#exchange_id-error').hide();
			
			$.ajax({
				url: base_url+"master/brokrage/searchTable/",
				type: 'post',
				data: {term1: ex_global , term2: name_global , term3: code_global},
				success:function(data)
				{
				
					//$('#aid').val();
					
					$('#Response').html(data);
					$('.MyTable').DataTable({
						"paging": true,
						"lengthChange": true,
						"searching": true,
						"ordering": true,
						"info": true,
						"autoWidth": false,
						"responsive": true,
						"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
						"iDisplayLength": 5
					});
				}
				});
			}
		
		
		// Load Data Table End
		
		//****************************************************** ***************************************************//
			
		// Add Record Start
		$(document).ready(function()
		{
			$(document).on('submit', '#FormSubmit', function() 
			{ 
				 var aid = $("#aid").val();
				 var bid = $("#bid").val();
				
				var code = $("#code").val();
				var name = $("#name").val();
				var exchange_id = $("#exchange_id").val();
				var symbol_id = $("#symbol_id").val();

				var brokrage_type_id = $("#brokrage_type_id").val();
				var base_on_id = $("#base_on_id").val();
				var intraday = $("#intraday").val();
				var side_id = $("#side_id").val();
				var nxt_day = $("#nxt_day").val();
				var minimum_id = $("#minimum_id").val();
				
				// 'aid='+ aid + '&bid='+ bid +
				
				// var dataString =  'aid='+ aid + '&bid='+ bid + '&code='+ code + '&name='+ name + '&exchange_id='+ exchange_id + '&symbol_id='+ symbol_id + '&brokrage_type_id='+ brokrage_type_id + '&base_on_id='+ base_on_id + '&intraday='+ intraday + '&side_id='+ side_id + '&nxt_day='+ nxt_day + '&minimum_id='+ minimum_id ;
				
				
				// var dataset={'aid':aid,'bid':bid,'code':code,'name':name,'exchange_id':exchange_id,'symbol_id':symbol_id,'brokrage_type_id':brokrage_type_id,'base_on_id':base_on_id,'intraday':intraday,'side_id':side_id,'nxt_day':nxt_day,'minimum_id':minimum_id  };
				
				
				var dataset={'code':code,'name':name,'exchange_id':exchange_id,'symbol_id':symbol_id,'brokrage_type_id':brokrage_type_id,'base_on_id':base_on_id,'intraday':intraday,'side_id':side_id,'nxt_day':nxt_day,'minimum_id':minimum_id,'aid':aid,'bid':bid  };
				
					$.ajax({
					type: "POST",
					url: base_url+"master/Brokrage/Add",
					// data: dataString,
					data: dataset,
					cache: false,

					success: function(data)
					{
						// alert(data);
						
						  if (data == 1) 
						  {
							  ac_global=aid;
							  ex_global = exchange_id;
							  code_global = code;
							  name_global = name;
							  
							  
							 // $("#symbol_id").val(""); // Hidden Id Clear
							 
							 $("#bid").val(""); // Hidden Id Clear
							 $("#aid").val(""); // Hidden Id Clear
							  
							  load_data(); // Load Ajax	
							  $('#row2').hide();	
										
								//$("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down							
								// $("#FormSubmit")[0].reset();  // Form Clear after insert
								// alert(data);
						
								$(function()
								{
									const Toast = Swal.mixin({
									toast: true,
									position: 'top-end',
									showConfirmButton: false,
									timer: 3000
									});
									toastr.success('Brokrage Added Successfully.')
								});
							}
						}
					});
                return false;
             });
         });
         // Add Record End
		  
		
		
//****************************************************** ***************************************************//

	// Delete Record Start
	function Delete(id)
	{
		Swal.fire({
			title: "Are you sure to delete this Brokrage ?",
			text: "You will not be able to recover this Data !!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it !!",
			reverseButtons: true
			}).then((result) => {
	
				if (result.value)  // Brfore Delete Chk Result 
				{
				
					$.ajax({
						url: base_url+"master/brokrage/Delete/"+id,
						type : "POST",
						data: { id: id},
						success: function(data) 
						{
							//alert(data);
							Swal.fire("Deleted!", "Your Record has been deleted.", "success");
							$(function(){
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.error('Brokrage Delete Successfully.');
									location.reload();
							});
							// load_data();
						
						}
					});
				}
				else 
				{
					Swal.fire("Cancelled", "Your record is safe :)", "error");
				}
		
			})
		}
	// Delete Record End

//****************************************************** ***************************************************//	
			
	// Bulk Delete Start
	$(document).ready(function()
	{
		$('#bulk_delete_submit').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Delete Selected Brokrage",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
				}).then((result) => 
				{
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
								url: base_url+"master/brokrage/Delete_Bulk",
								type : "POST",
								//data : { id: id },
								data: {id: users_arr},
								success: function(data)
								{
									Swal.fire
									(
										'Deleted!',
										'Brokrage has been deleted.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Brokrage Delete Successfully.')
									});
									// load_data(); // Load AJAX
								}
							});
					}
					else 
					{
						Swal.fire("Cancelled", "Your record is safe :)", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to delete", "info");
			}
		});
	});
	// Bulk Delete End
	
	

	//****************************************************** ***************************************************//
	// Exchange On Change To Symbol
	
        $(document).ready(function(){
 
            $('#exchange_id').change(function(){  // cate
				//alert("a");
                var id=$(this).val();
                $.ajax({
					url: base_url+"master/expiry/Symbol",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
						html += '<option value="0">All</option>';
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].symbol_id+'>'+data[i].symbol;+'</option>';
                        }
                        $('#symbol_id').html(html);
 
                    }
                });
                return false;
            }); 
             
        });
		
		
		//****************************************************** ***************************************************//
		
		    $(document).ready(function(){
 
            $('#symbol_id').change(function(){  // cate
			
				$("#aid").val(""); 
				$("#bid").val(""); 
			
			
				$("#brokrage_type_id").val(""); 
				
				// $("#symbol_id").val(""); 
				$("#base_on_id").val(""); 
				$("#intraday").val(""); 
				$("#side_id").val(""); 
				$("#nxt_day").val(""); 
				$("#minimum_id").val(""); 
				$("#row2").hide(); 
			}); 
        });
		
		
		
    
	
	
	
	
		
$(function (){

    $('.select2').select2()
	$('.select2').select2({
      theme: 'bootstrap4'
    })
 })


$("#FormSubmit").validate({

	 rules:
	{
		code:
		{
			required: true,
		},
		
		name:
		{
			required: true,
		},
		exchange_id:
		{
			required: true,
		},
		symbol:
		{
			required: true,
		},
	},
	
	messages:
	{
		code:
		{
			 required: 'Please Enter Account Code.',
		},
		name:
		{
			required: 'Please Enter Name.',
		},
		exchange_id:
		{
			required: 'Please Select Exchange.',
		},
		symbol:
		{
			required: 'Please Enter Symbol.',
		},
	},
		 
		
});


	