			// Load Data Table Start
			load_data();
			function load_data()
			{
					$.ajax({
				
					url: base_url+"master/Account/Ajax_View_Table",
					method:"POST",
					success:function(data)
					{
						$('#Response').html(data);
						$('.MyTable').DataTable({
							"paging": true,
							"lengthChange": false,
							"searching": true,
							"ordering": true,
							"info": true,
							"autoWidth": false,
							"responsive": true,
						});
					}
				});
			}
		// Load Data Table End
		
		
		// Add Record Start
			$(document).ready(function()
           {
				$(document).on('submit', '#FormSubmit', function() 
				{ 
					//   alert('add');
					var aid = $("#aid").val();
					var uid = $("#uid").val();
					
					var code = $("#code").val();
                    var mobile = $("#mobile").val();
                    var username = $("#username").val();
                    var email = $("#email").val();
                    var ac_type = $("#ac_type").val();
                    var ac_group = $("#ac_group").val();
                    var disp_order = $("#disp_order").val();
                    var closing_rate_type_id = $("#closing_rate_type_id").val();
               
					 var dataString = 'aid='+ aid + '&uid='+ uid + '&code='+ code +'&mobile='+ mobile+'&username='+ username+'&email='+ email+'&ac_type='+ac_type+'&ac_group='+ ac_group+'&disp_order='+ disp_order+'&closing_rate_type_id='+ closing_rate_type_id;

                    
                     $.ajax({
							type: "POST",
							url: base_url+"master/Account/Add",
							data: dataString,
							cache: false,

							success: function(data)
							{
							  if (data == 1) 
							  {
								$("#FormSubmit")[0].reset();  // Form Clear after insert
								load_data(); // Load Ajax
								// alert(data);
								
								$(function() {
									const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
									});
									toastr.success('Account Added Successfully.')
									/* 
									Toast.fire({
									type: 'success',
									title: 'Account Added Successfully.'
									})
									*/
								});
							}
						  }
					});
					return false;
				});
			});
			 // Add Record End
			 
			 // Edit Record Start
			 function Edit(id)
			{
					$(document).ready(function()
					{
						$.ajax({
						data:  "id="+id,
						url: base_url+"master/Account/Edit/"+id,
						type: "POST",
						dataType: 'json',
					
						success: function(data)
						{
							$("#aid").val(data.aid);   
							$("#id").val(data.id);
							$("#uid").val(data.uid);

							$("#username").val(data.username);
							$("#code").val(data.code);
							$("#mobile").val(data.mobile);
							// $("#name").val(data.name);
							$("#email").val(data.email);
							$("#ac_type").val(data.ac_type);
							$("#ac_group").val(data.ac_group);
							$("#disp_order").val(data.disp_order);
							$("#closing_rate_type_id").val(data.closing_rate_type_id);

							$('#FormSubmit').attr('id','FormUpdate');  // Change ID
						}
					});
				});
			}
			// Edit Record End
			
			// Update Record Start
			$(document).on('submit',  '#FormUpdate', function() 
			{ 
				var aid = $("#aid").val();
				var uid = $("#uid").val();
				var code = $("#code").val();
				var mobile = $("#mobile").val();
				
				var username = $("#username").val();
				var email = $("#email").val();
				var ac_type = $("#ac_type").val();
				var ac_group = $("#ac_group").val();
				var disp_order = $("#disp_order").val();
				var closing_rate_type_id = $("#closing_rate_type_id").val();

  
				$.ajax({
				type:"POST", 
				url: base_url+"master/Account/Update",
				data: {aid:aid , uid:uid , code:code , mobile:mobile , username:username , email:email , ac_type:ac_type , ac_group:ac_group , disp_order:disp_order , closing_rate_type_id:closing_rate_type_id },
			   
			   
				success: function(response,data)
				{ 
					//    alert(response); 	//    alert(data);
					$("#FormUpdate")[0].reset();  // Form Clear after Update

					$(function(){
						const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
						});
						toastr.info('Account Update Successfully.')
					});
					load_data(); // Load AJAX
					$('#FormUpdate').attr('id','FormSubmit');
				}
			});
			return false;
		});
		// Update Record End
		
		// Delete Record Start
			function Delete(id)
			{
				Swal.fire({
				title: "Are you sure to delete this account ?",
				text: "You will not be able to recover this Data !!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it !!",
				reverseButtons: true
				}).then((result) => {
		
					if (result.value)  // Brfore Delete Chk Result 
					{
					
						$.ajax({
							url: base_url+"master/Account/Delete/"+id,	
							type : "POST",
							data: { id: id},
							success: function(data) 
							{
								//alert(data);
								Swal.fire("Deleted!", "Your Record has been deleted.", "success");
								load_data();
								toastr.error('Account Delete Successfully.')	
							}
						});
					}
					else 
					{
						Swal.fire("Cancelled", "Your record is safe :)", "error");
					}
			
				})
			}
			// Delete Record End
			
		// Bulk Delete Start	
		$(document).ready(function()
		{
		$('#bulk_delete_submit').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Delete Selected Account",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
				}).then((result) => 
				{
						
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
							//alert(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
							url: base_url+"master/Account/Delete_Bulk/",	
							type : "POST",
							//data : { id: id },
							data: {id: users_arr},
							success: function(data)
							{
								Swal.fire
								(
									'Deleted!',
									'Account has been deleted.',
									'success'
								)
								$(function(){
									const Toast = Swal.mixin({
									toast: true,
									position: 'top-end',
									showConfirmButton: false,
									timer: 3000
									});
									toastr.error('Account Delete Successfully.')
								});
								load_data(); // Load AJAX
							}
						});
						
					}
					else 
					{
						Swal.fire("Cancelled", "Your record is safe :)", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to delete", "info");
			}
		});
	});
	// Bulk Delete End	
	
	
	// Autocomplete Start
	  $(document).ready(function()
	  {
			$("#username").autocomplete({
			source: function( request, response )
			{
				$.ajax({
				//url:"<?php echo base_url(); ?>master/Account/Search_Account/",
				url: base_url+"master/Account/Search_Account/",
				type: 'post',
				dataType: "json",
				data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							$('#uid').val(""); // if Not Registred Name So Uid will Null 
						}
						else
						{
							response(data);
						}
					}
				});
			},
			select: function (event, ui) 
			{
				// Set selection
				  $('#username').val(ui.item.label); // display the selected text
				  $('#uid').val(ui.item.value); // display the selected text
				  $('#email').val(ui.item.email); // display the selected text
			  
				
				return false;
			}
		});
	});
	// Autocomplete End
	
	// Validation Start
		$("#FormSubmit").validate({
		rules:
		{
			username:
			{
				required: true,
			 
			},
			
			code:
			{
				required: true,
			 
			},
			 user_id:
			{
				required: true,
			   
			},
			
			ac_type:
			{
				required: true,
			   
			},
			ac_group:
			{
				required: false,
			   
			},
			
			closing_rate_type:
			{
				required: false,
			   
			},
			 mobile:
			{
				required: false,
				maxlength: 12
			},
			email:
			{
				//noSpace: true,
				required: false,
				email: true,
				//  remote:
			//	{
				//	url :"<?php  echo base_url(); ?>Employee/Match_Email",
				//	type: "POST",
					
			//	},
				
			},
			disp_order:
			{
				required: true,
			   
			},
		 },
		messages:
		{
			username:
			{
				 required: 'Please Enter username.',
			},
			
			code:
			{
				required: 'Please Enter Account Code.',
				
			},
			 user_id:
			{
				required: 'Please Enter User id.',
				
			},
			 ac_type:
			{
				required: 'Please Select Account Type.',
				
			},
			ac_group:
			{
				required: 'Please Enter Account Group.',
				
			},
			closing_rate_type:
			{
				required: 'Please Enter Closing Rate Type.',
				
			},
			mobile:
			 {
				required: 'Please Enter mobile.',
				
			},
			email:
			 {
				required: 'Please Enter Email.',
				email: "Please enter a valid email address!",
				//remote: "Email is already in use !",
			 },
			 
			disp_order:
			{
				required: 'Please Enter Last Name.',
			},
		},
	});
	// Validation End
			 
			 
			 /*
	$('input').on('blur keyup change', function() {
    if ($("#FormSubmit").valid()) {
        $('#AddRec').prop('disabled', false);  
    } else {
        $('#AddRec').prop('disabled', 'disabled');
    }
}); 
          */