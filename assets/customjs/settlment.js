	
			

		// Load Data Table Start
		load_data();
		function load_data()
		{
			$.ajax({
					
					url: base_url+"master/settlment/Ajax_View_Table",
					method:"POST",
					success:function(data)
					{
						$('#Response').html(data);
						$('.MyTable').DataTable({
							"paging": true,
							"lengthChange": true,
							"searching": true,
							"ordering": true,
							"info": true,
							"autoWidth": false,
							"responsive": true,
							//"pageLength": 5,
							//"sLengthMenu": "Display _MENU_ records",
							"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
							"iDisplayLength": 5
						});

					}
				});
		}
		// Load Data Table End
			

		// Add Record Start
		$(document).ready(function()
		{
			$(document).on('submit', '#FormSubmit', function() 
			{ 
				//   alert('add');
				
				var exchange_id = $("#exchange_id").val();
				var setlement_no = $("#setlement_no").val();
				var start_date = $("#start_date").val();
				var end_date = $("#end_date").val();
				var description = $("#description").val();
				// var ledger = $("#ledger").val();
				
				//var status = $("#status").val();
				// + '&status='+ status
				
				var dataString = 'exchange_id='+ exchange_id + '&setlement_no='+ setlement_no + '&start_date='+ start_date + '&end_date='+ end_date + '&description='+ description ; // + '&ledger='+ ledger;
				

                $.ajax({
					type: "POST",
					url: base_url+"master/settlment/Add",
					data: dataString,
					cache: false,

					success: function(data)
					{
						  if (data == 1) 
						  {
							$("#setlement_no").attr("value",parseInt(setlement_no) + 1);
							$("#FormSubmit")[0].reset();  // Form Clear after insert
						//	$("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down
							// alert(data);
						
							$(function() {
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.success('Settlment Added Successfully.')
							});
							 load_data();
						}
					}
				});
                return false;
             });
         });
          // Add Record End
		  
			
		// Edit Record Start
		function Edit(id)
        {
			$(document).ready(function(){
			$.ajax({
					data:  "id="+id,
					url: base_url+"master/settlment/Edit/"+id,
					type: "POST",
					dataType: 'json',
				
					success: function(data)
					{
						//$("#exchange_id").val(data.exchange_id);
						// $("#start_date").val(data.start_date);
						// $("#end_date").val(data.end_date);
						
						$("#setlement_id").val(data.setlement_id); 
						$("#setlement_no").val(data.setlement_no); 
						$('#exchange_id').val(data.exchange_id).trigger('change');
						
						var arr1 = data.start_date.split('-');
						$("#start_date").val(arr1[2] + "-" + arr1[1] + "-" + arr1[0]);	
						
						var arr2 = data.end_date.split('-');
						$("#end_date").val(arr2[2] + "-" + arr2[1] + "-" + arr2[0]);	
						
						
						$("#description").val(data.description);
						$("#ledger").val(data.ledger);
						$('#FormSubmit').attr('id','FormUpdate');  // Change ID
					}
				});
			});
		}
		// Edit Record End
		

		// Update Record Start
		$(document).on('submit',  '#FormUpdate', function() { 
			
			var exchange_id = $("#exchange_id").val();
			var setlement_no = $("#setlement_no").val();
			var start_date = $("#start_date").val();
			var end_date = $("#end_date").val();
			var description = $("#description").val();
			var ledger = $("#ledger").val();
			var setlement_id = $("#setlement_id").val();
		
			
			$.ajax({
			type:"POST", 
			url: base_url+"master/settlment/Update",
			data: {exchange_id:exchange_id , setlement_no:setlement_no ,start_date:start_date , end_date:end_date , description:description , ledger:ledger , setlement_id:setlement_id },
			   
			success: function(response,data)
            { 
				$("#FormUpdate")[0].reset();  // Form Clear after Update
				
					$(function(){
						const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
						});
						toastr.info('Settlment Update Successfully.')
					});
					load_data(); // Load AJAX
					// $("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down
					
					$('#FormUpdate').attr('id','FormSubmit');
			 }
		});
		return false;
   });
     // Update Record End
	


	// Delete Record Start
	function Delete(id)
	{
		Swal.fire({
			title: "Are you sure to delete this Settlment ?",
			text: "You will not be able to recover this Data !!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it !!",
			reverseButtons: true
			}).then((result) => {
	
				if (result.value)  // Brfore Delete Chk Result 
				{
				
					$.ajax({
						url: base_url+"master/settlment/Delete/"+id,
						type : "POST",
						data: { id: id},
						success: function(data) 
						{
							//alert(data);
									//Swal.fire("Deleted!", "Your Record has been deleted.", "success");
							
									Swal.fire
									(
										'Deleted!',
										'Settlment has been deleted.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Settlment Delete Successfully.')
									});
									
									
							load_data();
							
							/*
							var no = 1;
							var setlement_no = $("#setlement_no").val();
						    $("#setlement_no").val(setlement_no - no);
							*/
							
						}
					});
				}
				else 
				{
					Swal.fire("Cancelled", "Your record is safe :)", "error");
				}
		
			})
	}
	// Delete Record End

	
			
	// Bulk Delete Start
	$(document).ready(function()
	{
		$('#bulk_delete_submit').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Delete Selected Exchange",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
				}).then((result) => 
				{
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
								url: base_url+"master/settlment/Delete_Bulk",
								type : "POST",
								//data : { id: id },
								data: {id: users_arr},
								success: function(data)
								{
									Swal.fire
									(
										'Deleted!',
										'Settlment has been deleted.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Settlment Delete Successfully.')
									});
									load_data(); // Load AJAX
								}
							});
					}
					else 
					{
						Swal.fire("Cancelled", "Your record is safe :)", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to delete", "info");
			}
		});
	});
	// Bulk Delete End
	
	
	
	
	
	
	
	
	
	
	
	
	// Bulk ledger YES Start
	$(document).ready(function()
	{
		$('#Ledger_Yes').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Selected Ledger In Yes",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'Cancel',
				reverseButtons: true
				}).then((result) => 
				{
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
								url: base_url+"master/settlment/Bulk_Ledger_Yes",
								type : "POST",
								//data : { id: id },
								data: {id: users_arr},
								success: function(data)
								{
									Swal.fire
									(
										'Ledger!',
										'has been Update.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Ledger Update Successfully.');
									});
									load_data(); // Load AJAX
								}
							});
					}
					else 
					{
						Swal.fire("Cancelled", "Cancelled Update", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to Update", "info");
			}
		});
	});
	// Bulk ledger YES End
	
	
	
	
	// Bulk ledger No Start
	$(document).ready(function()
	{
		$('#Ledger_No').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Selected Ledger In No",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'Cancel',
				reverseButtons: true
				}).then((result) => 
				{
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
								url: base_url+"master/settlment/Bulk_Ledger_No",
								type : "POST",
								//data : { id: id },
								data: {id: users_arr},
								success: function(data)
								{
									Swal.fire
									(
										'Ledger!',
										'has been Update.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Ledger Update Successfully.');
									});
									load_data(); // Load AJAX
								}
							});
					}
					else 
					{
						Swal.fire("Cancelled", "Cancelled Update", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to Update", "info");
			}
		});
	});
	// Bulk ledger No End
		
// Description Auto Fill Value Start	
$(document).ready(function(){
		// $("#end_date").keyup(function(){  blur // .on( "click", "tr", function() { mouseleave
			
		$('.card-body').on('mouseout' , function(event){
		 // alert("aa");
		var a = $("#exchange_id option:selected").text();
		var b = $("#setlement_no").val();
		var c = $("#start_date").val();
		var d = $("#end_date").val();
		$("#description").val(a+"-"+b+" "+c+" "+"To "+d);
	});
});
// Description Auto Fill Value End	
		
		

$("#FormSubmit").validate({

	 rules:
	{
		exchange_id:
		{
			required: true,
		},
		setlement_no:
		{
			required: true,
		},
		start_date:
		{
			required: true,
		},
		end_date:
		{
			required: true,
		},
		ledger:
		{
			required: true,
		},
		description:
		{
			required: true,
		},
	},
	
	messages:
	{
		exchange_id:
		{
			 required: 'Please Enter Exchange Name.',
		},
		
		setlement_no:
		{
			 required: 'Please Enter Setlement No.',
		},
		
		start_date:
		{
			required: 'Please Enter Start date.',
		},
		end_date:
		{
			required: 'Please Enter End Date.',
		},
		ledger:
		{
			required: 'Please Enter Ledger.',
		},
		description:
		{
			required: 'Please Enter Description.',
		},
	},
		 
		
});



  $(document).ready(function() {
    $(".select2").select2();
	
	$('.select2').select2({
      theme: 'bootstrap4'
    })
});


$(function (){
	$("#start_date").datepicker({
			 firstDay: 1,
			 dateFormat: 'dd-mm-yy',
			 onSelect: function (selected) 
			 {
				$("#end_date").datepicker("option","minDate", selected)
				
				dt=selected.split("-");
				console.log(dt);
				myDate = new Date(new Date(dt[2],parseInt(dt[1])-1,dt[0],0,0,0,0).getTime()+(4*24*60*60*1000));
				console.log(myDate);
				 $( "#end_date" ).datepicker( "setDate",myDate);
			 }
		});

   
		$("#end_date").datepicker({
			firstDay: 1,
			dateFormat: 'dd-mm-yy',
			onSelect: function (selected) {
			$("#start_date").datepicker("option","maxDate", selected)
			}
		});
	});


		