$(document).ready(function()
{
		$(document).on('submit', '#FormSubmit', function() 
		{ 
		
			var start_date = $("#start_date").val();
			var end_date = $("#end_date").val();
			var party_code = $("#party_code").val();
			var party_name = $("#party_name").val();
			
			var exchange_id = $("#exchange_id").val();
			var setlement_id = $("#setlement_id").val();
			var type = $("#type").val();
			
			
			 $.ajax({
					type: "POST",
					url: base_url+"utility/Transaction_remove/Remove_buy_sell_data",
					data: {start_date:start_date , end_date:end_date , party_code:party_code, party_name:party_name , exchange_id:exchange_id , setlement_id:setlement_id , type:type},
					cache: false,

					success: function(data)
					{
						toastr.error('Record Delete Successfully.');
					}
			});
			return false;
		});
});		



$("#FormSubmit").validate({
	rules:
	{
		exchange_id:
		{
			required: true,
		},
		setlement_id:
		{
			required: true,
		},
		start_date:
		{
			required: true,
		},
		end_date:
		{
			required: true,
		},
		type:
		{
			required: true,
		},
		
	},
	messages:
	{
		exchange_id:
		{
			 required: 'Please Select Exchange Name.',
		},
		
		setlement_id:
		{
			 required: 'Please Select Setlement.',
		},
		
		start_date:
		{
			required: 'Please Enter Start date.',
		},
		end_date:
		{
			required: 'Please Enter End Date.',
		},
		type:
		{
			required: 'Please Select Type .',
		},
	},
});


