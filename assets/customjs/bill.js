
// Autocomplete Start For Account-Code Searching 
	  $(document).ready(function(){
		  
			$("#party_code").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"reports/Bill/Search_Account_Code/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							// If Wrong Data Type In Text Box Will null
							
							$('#party_code').val("");
							$('#party_name').val("");
							toastr.error('Please Enter Correct Acccount Code')
					
						}
						else
						{
							//$('#name-error').hide(); // label Hide
							//$('#name').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
							response(data); // This is Auto complete code  only if part is added
						}
					}
				});
			},
			select: function (event, ui) 
			{
				
				$('#party_code').val(ui.item.label);
				$('#party_name').val(ui.item.party_name);
				return false;
			}
		});
	});
	// Autocomplete End For Account-Code Searching 
  

	$(document).ready(function(){
		$(document).on('change focusout',  '#party_code', function(event) {
			
			var codeVal = $(this).val();
				if(codeVal.length){
						$.ajax({
							url: base_url+"reports/Bill/party_code_search/",
							type: 'post',
							data: {code: codeVal},
							dataType: "json",
							success:function(data)
							{
								// If Wrong Data Type In Text Box Will null 
								// toastr.error('Please Enter Correct Acccount Name')
								if (!$.trim(data))
								{   
									$('#party_name').val("");
									$('#party_code').val("");
								}
								else
								{
									$("#party_name").val(data.username);
									
								}
							}
						});
					}
				});
			});

	
	
	$(function () {
    $("#start_date").datepicker({
   
     dateFormat: 'dd-mm-yy',
	firstDay: 1,
       onSelect: function (selected) {
        $("#end_date").datepicker("option","minDate", selected)
      
        }
    });
	$("#end_date").datepicker({
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		onSelect: function (selected) {
        $("#start_date").datepicker("option","maxDate", selected)
        }
    });
});



	// Validation Start
		$("#FormSubmit").validate({
		rules:
		{
			exchange_id:
			{
				required: true,
			 
			},
			
			start_date:
			{
				required: true,
			 
			},
			 end_date:
			{
				required: true,
			   
			},
			setlement_id:
			{
				required: true,
			   
			},
			
		 },
		messages:
		{
			exchange_id:
			{
				 required: 'Please Select Exchange.',
			},
			
			start_date:
			{
				required: 'Please  Select Start Date.',
				
			},
			 end_date:
			{
				required: 'Please Select End Date.',
				
			},
			setlement_id:
			{
				required: 'Please Select Setlement .',
			   
			},
		},
	});
	// Validation End
	