	// Global Variable
	var symbol_id_global;	
	 $(document).ready(function(){
 
            $('#exchange_id').change(function(){
				symbol_function(); 
                return false;
            }); 
             
        });
		
		
	$(document).ready(function(){
			symbol_function(); 
			
	 });
	
	function symbol_function()
	{
		$(document).ready(function(){
			
			var id=$("#exchange_id").val();
            
                $.ajax({
					url: base_url+"master/expiry/Symbol",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data)
					{
                         
                        var html = '';
                        var i;
						html += '<option value="0">All</option>';
                        for(i=0; i<data.length; i++)
						{
							html += '<option value='+data[i].symbol_id+' '+((symbol_id_global==data[i].symbol_id)?"selected":"")+'>'+data[i].symbol+'</option>';
						}
						$('#symbol_id').html(html);
					}
                });
                return false;
           
             
        });
	}
	
	/*
		// Global Variable
		var symbol_id_global;
	
		 $(document).ready(function(){
			
			//$('#exchange_id').change(function(){  // 
			$('#exchange_id').on('change', function() {
			
			
			// alert('me');
			
                var id=$(this).val();
                $.ajax({
					url: base_url+"master/expiry/Symbol",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data)
					{
                         
                        var html = '';
                        var i;
						html += '<option value="0">All</option>';
                        for(i=0; i<data.length; i++)
						{
							html += '<option value='+data[i].symbol_id+' '+((symbol_id_global==data[i].symbol_id)?"selected":"")+'>'+data[i].symbol+'</option>';
						}
						$('#symbol_id').html(html);
					}
                });
                return false;
            }); 
             
        });
		
		*/

		// Load Data Table Start
		load_data();
		function load_data()
		{
			$.ajax({
					
					url: base_url+"master/expiry/Ajax_View_Table",
					method:"POST",
					success:function(data)
					{
						$('#Response').html(data);
						$('.MyTable').DataTable({
							"paging": true,
							"lengthChange": true,
							"searching": true,
							"ordering": true,
							"info": true,
							"autoWidth": false,
							"responsive": true,
							"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
							"iDisplayLength": 5
						});

					}
				});
		}
		// Load Data Table End
			

		// Add Record Start
		$(document).ready(function()
		{
			$(document).on('submit', '#FormSubmit', function() 
			{ 
				//   alert('add');
				
				var exchange_id = $("#exchange_id").val();
				var ex_date = $("#ex_date").val();
				var status = $("#status").val();
				var symbol_id = $("#symbol_id").val();
			
				var dataString = 'exchange_id='+ exchange_id + '&ex_date='+ ex_date + '&status='+ status + '&symbol_id='+ symbol_id;

                $.ajax({
					type: "POST",
					url: base_url+"master/expiry/Add",
					data: dataString,
					cache: false,

					success: function(data)
					{
						  if (data == 1) 
						  {
							$("#FormSubmit")[0].reset();  // Form Clear after insert
							load_data(); // Load Ajax
						//	$("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down
							
							// alert(data);
						
							$(function() {
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.success('Expiry Added Successfully.')
							});
						}
					}
				});
                return false;
             });
         });
          // Add Record End
		  
			
				
		

		// Update Record Start
		$(document).on('submit',  '#FormUpdate', function() { 
			

			
			var symbol_id = $("#symbol_id").val();

			var expiries_id = $("#expiries_id").val();
			var exchange_id = $("#exchange_id").val();
			var ex_date = $("#ex_date").val();
			var status = $("#status").val();
		
			$.ajax({
			type:"POST", 
			url: base_url+"master/expiry/Update",
			data: {symbol_id:symbol_id , expiries_id:expiries_id , exchange_id:exchange_id , ex_date:ex_date ,  status:status },
			   
			success: function(response,data)
            { 
				$("#FormUpdate")[0].reset();  // Form Clear after Update
				
					$(function(){
						const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
						});
						toastr.info('Expiry Update Successfully.')
					});
					load_data(); // Load AJAX
				//	$("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down
					
					$('#FormUpdate').attr('id','FormSubmit');
			 }
		});
		return false;
   });
     // Update Record End
	


	// Delete Record Start
	function Delete(id)
	{
		Swal.fire({
			title: "Are you sure to delete this Symbol ?",
			text: "You will not be able to recover this Data !!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it !!",
			reverseButtons: true
			}).then((result) => {
	
				if (result.value)  // Brfore Delete Chk Result 
				{
				
					$.ajax({
						url: base_url+"master/expiry/Delete/"+id,
						type : "POST",
						data: { id: id},
						success: function(data) 
						{
							//alert(data);
							Swal.fire("Deleted!", "Your Record has been deleted.", "success");
							load_data();
							toastr.error('Expiry Delete Successfully.')
						}
					});
				}
				else 
				{
					Swal.fire("Cancelled", "Your record is safe :)", "error");
				}
		
			})
	}
	// Delete Record End

	
			
	// Bulk Delete Start
	$(document).ready(function()
	{
		$('#bulk_delete_submit').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Delete Selected Exchange",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
				}).then((result) => 
				{
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
								url: base_url+"master/expiry/Delete_Bulk",
								type : "POST",
								//data : { id: id },
								data: {id: users_arr},
								success: function(data)
								{
									Swal.fire
									(
										'Deleted!',
										'Expiry has been deleted.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Expiry Delete Successfully.')
									});
									load_data(); // Load AJAX
								}
							});
					}
					else 
					{
						Swal.fire("Cancelled", "Your record is safe :)", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to delete", "info");
			}
		});
	});
	// Bulk Delete End
		
		
		function Edit_is_Default(id)
		{
			
			Swal.fire({
			title: "Default Chage",
			text: "Are you sure Do You Want To Chage Default ?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#007bff",
			confirmButtonText: "Yes, Chage",
			reverseButtons: true
			}).then((result) => {
	
				if (result.value)  // Brfore Delete Chk Result 
				{
				
					$.ajax({
						url: base_url+"master/Expiry/is_Default/"+id,	
						type : "POST",
						data: { id: id},
						success: function(data) 
						{
							//alert(data);
							Swal.fire("Update!", "Your Record has been Update", "success");
							load_data();
							toastr.error('Expiry Update Successfully.')	
						}
					});
				}
				else 
				{
					Swal.fire("Cancelled", "No Chages", "error");
				}
		
			})
		}
	
		
		

$("#FormSubmit").validate({

	 rules:
	{
		exchange_id:
		{
			required: true,
		},
		ex_date:
		{
			required: true,
		},
	},
	
	messages:
	{
		exchange_id:
		{
			 required: 'Please Enter Exchange Name.',
		},
		
		ex_date:
		{
			required: 'Please Enter Exchange Date.',
		},
	},
		 
		
});



  $(document).ready(function() {
    $(".select2").select2();
	
	$('.select2').select2({
      theme: 'bootstrap4'
    })
});

	$(function() {
			$('#ex_date').datepicker({
			firstDay: 1,	
            changeMonth: true,
            changeYear: true,
            // showButtonPanel: true,
            dateFormat: 'dd-M-yy',
        });
    });	


	