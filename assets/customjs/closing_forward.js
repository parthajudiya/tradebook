	$(document).ready(function(){
		var exchange_id_A = $("#exchange_id").val();
		$(".exchange_id").val(exchange_id_A);
	});

	$(document).ready(function(){
		$('#exchange_id').change(function(){
			var exchange_id_B = $("#exchange_id").val();
			$(".exchange_id").val(exchange_id_B);
		});
	});

	$(function() {
		$('#ex_date').datepicker({
		firstDay: 1,	
		changeMonth: true,
		changeYear: true,
		// showButtonPanel: true,
		dateFormat: 'dd-M-yy',
		});
	});	
	
	function load_data()
	{
		$('.MyTable').DataTable({
			"paging": true,
			"lengthChange": true,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"responsive": true,
			"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
			"iDisplayLength": 5
		});
	}
	
	// Load Data Table End
		
		
		
		

var start_date=[];
var end_date=[];
var next_open_date=[];
        $(document).ready(function(){
 
            $('#exchange_id').change(function(){  
				// settlement_function();
				closing_setlment_function();
                return false;
				/*
                var id=$(this).val();
                $.ajax({
					url: base_url+"process/closing_forward/Exchange_On_Change",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
						html += '<option value="" disabled selected>Select</option>';
					
                        for(i=0; i<data.length; i++){
							
							start_date[data[i].setlement_id]=data[i].start_date; 
							end_date[data[i].setlement_id]=data[i].end_date; 
						//	console.log(a);
							
                            html += '<option value='+data[i].setlement_id+'>'+data[i].description;+'</option>';
                        }
                        $('#closing_setlment_id').html(html);
						$('#opening_setlment_id').html(html); 
						
						// $('#setlement_id').html(html)		
						// $('#ex_date').html(html)		
					}
                });
				*/
                //return false;
            }); 
             
        });
		
		$(document).ready(function(){
			closing_setlment_function(); 
			// openning_setlment_function(); 
		});
   
			// settlement id
		$(document).on('change',  '#closing_setlment_id', function() { 
				datefunction();
		 });
		 
		function datefunction()
		{
			 var id=$("#closing_setlment_id").val();
			//var id=$(this).val();
			var arr1Dt = start_date[id].split('-');
			$("#start_date").val(arr1Dt[2] + "-" + arr1Dt[1] + "-" + arr1Dt[0]);	
			
			var arr1Dt2 = end_date[id].split('-');
			$("#end_date").val(arr1Dt2[2] + "-" + arr1Dt2[1] + "-" + arr1Dt2[0]);
			// $("#next_open_date").val(start_date[id]); //  next_open_date
		}
		
		function closing_setlment_function() 
		{
			 var id=$("#exchange_id").val();
			 $.ajax({
						// url: base_url+"reports/Stock_position/Exchange_On_Change",
						url: base_url+"global/Global_function/Exchange_On_Change",
						method : "POST",
						data : {id: id},
						async : true,
						dataType : 'json',
						success: function(data){
							
						var html = '';
						var i;
						
						for(i=0; i<data.length; i++)
						{
							start_date[data[i].setlement_id]=data[i].start_date; 
							end_date[data[i].setlement_id]=data[i].end_date; 
							html += '<option value='+data[i].setlement_id+'>'+data[i].description;+'</option>';
						}
						//$('#setlement_id').html(html); 
						   $('#closing_setlment_id').html(html);
						$('#opening_setlment_id').html(html); 
						datefunction();
					
						}
					});
				return false;
		}
	
	
		
		$(document).ready(function()
		{
			var opening_setlment_id=[];
			$('#opening_setlment_id').change(function()
			{  
				opening_setlment_id = $("#opening_setlment_id").val();
				 
				
				 var id=$("#exchange_id").val();
				 $.ajax({
					
						url: base_url+"global/Global_function/Exchange_On_Change",
						method : "POST",
						data : {id: id},
						async : true,
						dataType : 'json',
						success: function(data){
							
						var html = '';
						var i;
						
						for(i=0; i<data.length; i++)
						{
							next_open_date[data[i].setlement_id]=data[i].next_open_date; 
						}
						
						var id=$("#opening_setlment_id").val();
						console.log(id);
						
						var arr1Dt3 = end_date[id].split('-');
						$("#next_open_date").val(arr1Dt3[2] + "-" + arr1Dt3[1] + "-" + arr1Dt3[0]);
					
						}
					});
				return false;
				
				 
			});
		}); 
	
		

		
		
	/* Exchange to settlement and settlement to date */		

 
  
  	$(function () {
    $("#start_date").datepicker({
	firstDay: 1,
     dateFormat: 'dd-mm-yy',
   
       onSelect: function (selected) {
        $("#end_date").datepicker("option","minDate", selected)
      
        }
    });
	$("#end_date").datepicker({
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		onSelect: function (selected) {
        $("#start_date").datepicker("option","maxDate", selected)
        }
    });
	
	
	$("#next_open_date").datepicker({
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		onSelect: function (selected) {
        $("#next_open_date").datepicker()
        }
    });
	
	
});

			
			
			
			
			
			
			
			
			
			// Validation Start
		$("#FormSubmit").validate({
		rules:
		{
			exchange_id:
			{
				required: true,
			 
			},
			
			start_date:
			{
				required: true,
			 
			},
			 end_date:
			{
				required: true,
			   
			},
			closing_setlment_id:
			{
				required: true,
			   
			},
			opening_setlment_id:
			{
				required: true,
			   
			},
			
			
		 },
		messages:
		{
			exchange_id:
			{
				 required: 'Please Select Exchange.',
			},
			
			start_date:
			{
				required: 'Please  Select Start Date.',
				
			},
			 end_date:
			{
				required: 'Please Select End Date.',
				
			},
			closing_setlment_id:
			{
				required: 'Please Select Closing Setlement .',
			   
			},
			opening_setlment_id:
			{
				required: 'Please Select Opening Setlement .',
			   
			},
		},
	});
	// Validation End
			


	