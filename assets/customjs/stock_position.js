var start_date = [];
var end_date = [];
$(document).ready(function () {
	
	 $('#exchange_id').change(function () {
		var one = $("#exchange_id").val(); // hidden
		var exchange_id = $('.exchange_id').val(one); // hidden
		settlement_function();
        return false;
    });

});

$(document).ready(function (){
	
	var one = $("#exchange_id").val(); // hidden
	var exchange_id = $('.exchange_id').val(one); // hidden
	settlement_function();
});




$(document).on('change', '#setlement_id', function () {
	
	var two = $("#setlement_id").val(); // hidden
	var setlement_id = $('.setlement_id').val(two); // hidden
	
	datefunction();
});

function datefunction()
{
    var id = $("#setlement_id").val();
	
	var setlement_id = $('.setlement_id').val(id); // hidden

    var arr1Dt = start_date[id].split('-');
    $("#start_date").val(arr1Dt[2] + "-" + arr1Dt[1] + "-" + arr1Dt[0]);

    var arr1Dt2 = end_date[id].split('-');
    $("#end_date").val(arr1Dt2[2] + "-" + arr1Dt2[1] + "-" + arr1Dt2[0]);
	
	var arr1DtA = $('.arr1DtA').val(arr1Dt[2] + "-" + arr1Dt[1] + "-" + arr1Dt[0]); // hidden
	var arr1DtB = $('.arr1DtB').val(arr1Dt2[2] + "-" + arr1Dt2[1] + "-" + arr1Dt2[0]); // hidden
	
}

function settlement_function()
{
	
	var id = $("#exchange_id").val();
	
   $.ajax({
		url: base_url + "global/Global_function/Exchange_On_Change",
        method: "POST",
        data: {id: id},
        async: true,
        dataType: 'json',
        success: function (data) {

            var html = '';
            var i;

            for (i = 0; i < data.length; i++)
            {
                start_date[data[i].setlement_id] = data[i].start_date;
                end_date[data[i].setlement_id] = data[i].end_date;
                html += '<option value=' + data[i].setlement_id + '>' + data[i].description;
                +'</option>';
            }
            $('#setlement_id').html(html);
            datefunction();

        }
    });
    return false;
}



// Load  Second Table
    function load_second()
    {
        $.ajax({
            url: base_url + "reports/Stock_position/load_second",
            method: "POST",
            success: function (data) {
                $('#Response2').html(data);
				$('.MyTable2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
					// dom: 'Blfrtip',
                     // buttons: [{extend: 'excel',}],
                });
            }
        });
        return false;
    }




/*
    // Load  Three Table
    function load_four()
    {
        $.ajax({
            url: base_url + "reports/Stock_position/load_four",
            method: "POST",
            success: function (data) {
                $('#Response4').html(data);

                $('.MyTable4').DataTable({
					"paging": false,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                    "aLengthMenu": [[5, 15, 25, 50, 75, -1], [5, 15, 25, 50, 75, "All"]],
                    "iDisplayLength": 5
                });
            }
        });
        return false;
    }
	*/

	// Pass Symbol id
	// var Symbol_id_Global;
	/*
    function ViewTBL4(Symbol_id)
    {
		Symbol_id_Global=Symbol_id;
		$(document).ready(function ()
        {
            $.ajax({
                data: "id=" + Symbol_id,
                url: base_url + "reports/Stock_position/load_four/" +Symbol_id,
                type: "POST",
                success: function (data)
                {
                    $('#Response4').html(data);
                    $('.MyTable4').DataTable({
                       "paging": false,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                    });
                }
            });
        });
    }
	*/
	
	
		$(document).on('click', '#PrintBtn', function ()  
        {
				var exchange_id = $("#exchange_id").val();
				var setlement_id = $("#setlement_id").val();
				var arr1Dt = $("#start_date").val();
				var arr1Dt2 = $("#end_date").val();	
		
				
				  $.ajax({
                    url: base_url + "reports/Stock_position/Table1_Print/",
                    type: 'post',
                    data: {exchange_id:exchange_id, setlement_id:setlement_id ,arr1Dt:arr1Dt, arr1Dt2: arr1Dt2},
                    success: function (data)
                    {
						var openWin =  window.open(base_url + "reports/stock_position/Table1_Print" ,  "_blank");
						openWin.document.writeln(data);
						  
						 // var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
						//	WindowObject.document.writeln(data);
					}
                });
				return false;
		});
		
			/* pdf table 2
		$(document).on('click', '#PrintBtn2', function ()  
        {
				
			
				
				var exchange_id = $("#exchange_id").val();
				var setlement_id = $("#setlement_id").val();
				var arr1Dt = $("#start_date").val();
				var arr1Dt2 = $("#end_date").val();
				
				  $.ajax({
                    url: base_url + "reports/Stock_position/Table2_Print/",
                    type: 'post',
                    data: {exchange_id:exchange_id, setlement_id:setlement_id ,arr1Dt:arr1Dt, arr1Dt2: arr1Dt2},
                    success: function (data)
                    {
						var openWin =  window.open(data);
						openWin.document.writeln(data);
					}
                });
				return false;
		});
		*/
		
		$(document).on('click', '#PrintBtn3', function ()  
        {
			var exchange_id = $("#exchange_id").val();
			var setlement_id = $("#setlement_id").val();
			var arr1Dt = $("#start_date").val();
			var arr1Dt2 = $("#end_date").val();
			

			$.ajax({
				url: base_url + "reports/Stock_position/Table3_Print",
				method: "POST",
				data: {exchange_id: exchange_id, setlement_id: setlement_id, arr1Dt: arr1Dt, arr1Dt2: arr1Dt2},
				success: function (data) {
					var openWin =  window.open(data);
						openWin.document.writeln(data);
				}
			});
			return false;
		});
		
		
		$(document).on('click', '#Current_Symbol', function ()  
        {
			// alert(Symbol_id_Global);
			var exchange_id = $("#exchange_id").val();
			var setlement_id = $("#setlement_id").val();
			var arr1Dt = $("#start_date").val();
			var arr1Dt2 = $("#end_date").val();
			
			$.ajax({
				url: base_url + "reports/Stock_position/Current_Symbol",
				method: "POST",
				data: {exchange_id:exchange_id , setlement_id:setlement_id , arr1Dt:arr1Dt , arr1Dt2:arr1Dt2 , Symbol_id_Global:Symbol_id_Global},
				success: function (data) {
					var openWin =  window.open(data);
						openWin.document.writeln(data);
				}
			});
			return false;
		});
		
		// Symbol_id_Global:Symbol_id_Global
		$(document).on('click', '#Summary', function ()  
        {
			var exchange_id = $("#exchange_id").val();
			var arr1Dt = $("#start_date").val();
			var arr1Dt2 = $("#end_date").val();
			
			$.ajax({
				url: base_url + "reports/Stock_position/Summary",
				method: "POST",
				data: {exchange_id:exchange_id , arr1Dt:arr1Dt , arr1Dt2:arr1Dt2  },
				success: function (data) {
					 var openWin =  window.open(data);
						openWin.document.writeln(data);
				}
			});
			return false;
		});	
		
			
		$(document).on('click', '#Print_Table_2_Excel', function (){	
		
			var exchange_id = $("#exchange_id").val();
			var setlement_id = $("#setlement_id").val();
			var arr1Dt = $("#start_date").val();
			var arr1Dt2 = $("#end_date").val();
			
			$.ajax({
				url: base_url + "reports/Stock_position/Print_Table_2_Excel",
				method: "POST",
				data: {exchange_id:exchange_id , arr1Dt:arr1Dt , arr1Dt2:arr1Dt2 , setlement_id:setlement_id },
				success: function (data) {
					 var openWin =  window.open(data);
						openWin.document.writeln(data);
				}
			});
			return false;
		});
		
								
         


	// Validation Start
		$("#FormSubmit").validate({
		rules:
		{
			exchange_id:
			{
				required: true,
			 
			},
			
			start_date:
			{
				required: true,
			 
			},
			 end_date:
			{
				required: true,
			   
			},
			setlement_id:
			{
				required: true,
			   
			},
			
		 },
		messages:
		{
			exchange_id:
			{
				 required: 'Please Select Exchange.',
			},
			
			start_date:
			{
				required: 'Please  Select Start Date.',
				
			},
			 end_date:
			{
				required: 'Please Select End Date.',
				
			},
			setlement_id:
			{
				required: 'Please Select Setlement .',
			   
			},
		},
	});
	// Validation End
	