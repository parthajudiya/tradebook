$(function () {
    $("#start_date").datepicker({
	firstDay: 1,
     dateFormat: 'dd-mm-yy',
   
       onSelect: function (selected) {
        $("#end_date").datepicker("option","minDate", selected)
      
        }
    });
	$("#end_date").datepicker({
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		onSelect: function (selected) {
			$("#start_date").datepicker("option","maxDate", selected)
        }
    });
});

/*
<script>
    $(document).on('change', '#start_date', function () {
        $("#end_date").attr("min", $(this).val());
    });

    $(document).on('change', '#end_date', function () {
        $("#start_date").attr("min", $(this).val());
    });

</script>
*/