var start_date = [];
var end_date = [];
$(document).ready(function () {

    $('#exchange_id').change(function () {
		settlement_function();
        return false;
    });

});

$(document).ready(function () {
    settlement_function();

});


$(document).on('change', '#setlement_id', function () {
    datefunction();
});
function datefunction()
{
    var id = $("#setlement_id").val();

    var arr1Dt = start_date[id].split('-');
    $("#start_date").val(arr1Dt[2] + "-" + arr1Dt[1] + "-" + arr1Dt[0]);

    var arr1Dt2 = end_date[id].split('-');
    $("#end_date").val(arr1Dt2[2] + "-" + arr1Dt2[1] + "-" + arr1Dt2[0]);
}
function settlement_function()
{
 
    var id = $("#exchange_id").val();
    $.ajax({
        // url: base_url+"reports/Stock_position/Exchange_On_Change",
        url: base_url + "global/Global_function/Exchange_On_Change",
        method: "POST",
        data: {id: id},
        async: true,
        dataType: 'json',
        success: function (data) {

            var html = '';
            var i;

            for (i = 0; i < data.length; i++)
            {
                start_date[data[i].setlement_id] = data[i].start_date;
                end_date[data[i].setlement_id] = data[i].end_date;
                html += '<option value=' + data[i].setlement_id + '>' + data[i].description;
                +'</option>';
            }
            $('#setlement_id').html(html);
            datefunction();

        }
    });
    return false;
}