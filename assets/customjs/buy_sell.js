/* all main on changes start */

var symbol_id_global;
var setlement_id_global;
var instument = [];
var ex_date_global = [];

$(document).ready(function () {
    settlement_function();
    symbol_function();
    instrument_function();
    // expirey_function();
});

$(document).on('change', '#exchange_id', function () {
    settlement_function();
    symbol_function();
    // return false;
});

//$(document).ready(function () {

// $('#symbol_id').change(function () {
$(document).on('change', '#symbol_id', function () {
    expirey_function();
    //return false;
});
// });
//});
function settlement_function()
{
    var id = $("#exchange_id").val();
    $.ajax({

        url: base_url + "global/Global_function/Exchange_On_Change",
        method: "POST",
        data: {id: id},
        async: true,
        dataType: 'json',
        success: function (data) {

            var html = '';
            var i;
            for (i = 0; i < data.length; i++)
            {
                // html += '<option value='+data[i].setlement_id+'>'+data[i].description;+'</option>';
                html += '<option value=' + data[i].setlement_id + ' ' + ((setlement_id_global == data[i].setlement_id) ? "selected" : "") + '>' + data[i].description + '</option>';
            }
            $('#setlement_id').html(html);
        }
    });
    return false;
}

function symbol_function()
{
    var id = $("#exchange_id").val();
    $.ajax({
        url: base_url + "transaction/Buy_sell/Symbol_Search",
        method: "POST",
        data: {id: id},
        async: true,
        dataType: 'json',
        success: function (data)
        {
            var html = '';
            var i;
            // html += '<option value=""  >Select Symbol</option>'; // selected disabled
            for (i = 0; i < data.length; i++)
            {
                instument[data[i].symbol_id] = data[i].instument;
                //    html += '<option value='+data[i].symbol_id+'>'+data[i].symbol+'</option>';
                html += '<option value=' + data[i].symbol_id + ' ' + ((symbol_id_global == data[i].symbol_id) ? "selected" : "") + '>' + data[i].symbol + '</option>';
            }
            $('#symbol_id').html(html);
            instrument_function();
            expirey_function();
        }
    });
    return false;
}

function instrument_function()
{
    var id = $("#symbol_id").val();
    $("#instument").val(instument[id]);
}

function expirey_function()
{
    var id = $("#symbol_id").val();
    var exchange_id = $("#exchange_id").val();
    // alert(exchange_id);
    $.ajax({
        url: base_url + "transaction/Buy_sell/Symbol_OnChange",
        method: "POST",
        data: {id: id, exchange_id: exchange_id},
        async: true,
        dataType: 'json',
        success: function (data)
        {

            var html = '';
            var i;
            for (i = 0; i < data.length; i++)
            {
                // 15-dec
                // html += '<option value='+data[i].ex_date+' '+((ex_date_global==data[i].ex_date)?"selected":"")+'>'+data[i].ex_date+'</option>';

                html += '<option value=' + data[i].expiries_id + ' ' + ((ex_date_global == data[i].expiries_id) ? "selected" : "") + '>' + data[i].ex_date + '</option>';

                // html += '<option value='+data[i].expiries_id+'>'+data[i].ex_date+'</option>';

            }
            //$('#ex_date').html(html); // 15 dec
            $('#expiries_id').html(html); // 15 dec
        }
    });
    return false;
}
/*
 $(document).ready(function(){
 
 $('#exchange_id').change(function(){  // cate
 var id=$(this).val();
 $.ajax({
 url: base_url+"transaction/Buy_sell/setlement",
 method : "POST",
 data : {id: id},
 async : true,
 dataType : 'json',
 success: function(data){
 
 var html = '';
 var i;
 for(i=0; i<data.length; i++){
 
 html += '<option value='+data[i].setlement_id+' '+((setlement_id_global==data[i].setlement_id)?"selected":"")+'>'+data[i].description+'</option>';
 }
 $('#setlement_id').html(html);
 }
 });
 return false;
 }); 
 });
 */

/* all on changes main end */

// Load Data Table Start
// load_data();
function load_data()
{
    $.ajax({

        url: base_url + "transaction/Buy_sell/Ajax_View_Table",
        method: "POST",
        success: function (data)
        {
            $('#Response').html(data);
            $('.MyTable').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                //"info": true,
                "autoWidth": false,
                "responsive": true,
                // "aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
                //"iDisplayLength": 5 , 
                "scrollY": '80vh',
                "info": false,
            });

        }
    });
}
// Load Data Table End

// After Insert Single Record Display
function singlerecord()
{
    //alert('a');
    $.ajax({
        url: base_url + "transaction/Buy_sell/singlerecord",
        method: "POST",
        dataType: 'json',
        success: function (data)
        {

            $('#exchange_id').val(data.exchange_id).trigger('change');
            $('#setlement_id').val(setlement_id_global = Object.seal(data.setlement_id)).trigger('change');
            $('#type').val(data.type).trigger('change');
            $('#buy_sell_id').val(data.buy_sell_id).trigger('change');
            $('#symbol_id').val(symbol_id_global = Object.seal(data.symbol_id)).trigger('change');
            // $('#ex_date').val(data.ex_date).trigger('change'); 
            $('#expiries_id').val(data.expiries_id).trigger('change');
            $("#instument").val(data.instument);
            $("#qty1").focus();
        }
    });
    return false;
}
/* */

// Edit Record Start
function Edit(id)
{

    $(document).ready(function () {
        $.ajax({
            data: "id=" + id,
            url: base_url + "transaction/Buy_sell/Edit/" + id,
            type: "POST",
            dataType: 'json',

            success: function (data)
            {

                // $('#exchange_id').val(data.exchange_id).select2(); $("#exchange_id").val("").trigger('change'); 
                $("#buysell_id").val(data.buysell_id);
                $('#exchange_id').val(data.exchange_id).trigger('change');
                $('#setlement_id').val(setlement_id_global = Object.seal(data.setlement_id)).trigger('change');
                //$("#tr_no").val(data.tr_no);
                $('#type').val(data.type).trigger('change');

                var arr1 = data.date1.split('-');
                $("#date1").val(arr1[2] + "-" + arr1[1] + "-" + arr1[0]);

                $('#buy_sell_id').val(data.buy_sell_id).trigger('change');
                $("#instument").val(data.instument);

                $("#pid").val(data.pid);
                party();
                //$("#party_code").val(data.party_code);
                //$("#party_name").val(data.party_name);
                $("#bid").val(data.bid);
                broker();
                //$("#brokrage_code").val(data.brokrage_code);
                //$("#brokrage_name").val(data.brokrage_name);

                $("#re_no").val(data.re_no);
                // $("#remark").val(data.remark);
                $("#qty1").val(data.qty1);
                $("#rate1").val(data.rate1);
                $('#symbol_id').val(symbol_id_global = Object.seal(data.symbol_id)).trigger('change');
                $('#expiries_id').val(ex_date_global = Object.seal(data.expiries_id)).trigger('change');
                // $('#expiries_id').val(data.expiries_id).trigger('change'); // 
                // $('#ex_date').val(data.ex_date).trigger('change'); 
                $('#FormSubmit').attr('id', 'FormUpdate');

                //console.log(ab);
            }
        });
        return false;
    });
}
// Edit Record End
// Update Record Start
$(document).on('submit', '#FormUpdate', function () {

    //	alert("update");

    var buysell_id = $("#buysell_id").val(); // hidden
    var exchange_id = $("#exchange_id").val();
    var setlement_id = $("#setlement_id").val();
    //	var tr_no = $("#tr_no").val();
    var type = $("#type").val();
    var date1 = $("#date1").val();
    var buy_sell_id = $("#buy_sell_id").val(); // buy or sell
    var instument = $("#instument").val();
    var pid = $("#pid").val();
    var party_code = $("#party_code").val();
    var party_name = $("#party_name").val();
    var bid = $("#bid").val();
    var brokrage_code = $("#brokrage_code").val();
    var brokrage_name = $("#brokrage_name").val();
    var re_no = $("#re_no").val();
    // var remark = $("#remark").val();
    var qty1 = $("#qty1").val();
    var rate1 = $("#rate1").val();
    var symbol_id = $("#symbol_id").val();
    // var ex_date = $("#ex_date").val();
    var expiries_id = $("#expiries_id").val();

    // remark:remark , tr_no:tr_no // ex_date // ex_date
    $.ajax({

        type: "POST",
        url: base_url + "transaction/Buy_sell/Update",
        data: {buysell_id: buysell_id, exchange_id: exchange_id, setlement_id: setlement_id, type: type, date1: date1, buy_sell_id: buy_sell_id, instument: instument, pid: pid, party_code: party_code, party_name: party_name, bid: bid, brokrage_code: brokrage_code, brokrage_name: brokrage_name, re_no: re_no, qty1: qty1, rate1: rate1, symbol_id: symbol_id, expiries_id: expiries_id},

        success: function (response, data)
        {
            // $("#FormUpdate")[0].reset();  // Form Clear after Update

            $("#qty1").val("");
            $("#rate1").val("");
            $("#party_name").val("");
            $("#party_code").val("");
            $("#pid").val("");
            //$("#pid").focus("#qty1");
            $("#qty1").focus();

            $("#buy_sell_id").val(buy_sell_id); // 1  == buy 2 = sell
            // $("#buy_sell_id").val("1"); // 1  == buy 2 = sell
            $(function () {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                });
                toastr.info('Buy / Sell Update Successfully.')
            });
            load_data(); // Load AJAX

            //$("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down
            $('#FormUpdate').attr('id', 'FormSubmit');
        }
    });
    return false;
});
// Update Record End
// Delete Record Start
function Delete(id)
{
    Swal.fire({
        title: "Are you sure to delete this Record ?",
        text: "You will not be able to recover this Data !!",
        type: "warning",
        showCancelButton: true,
        //confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it !!",
        reverseButtons: true
    }).then((result) => {

        if (result.value)  // Brfore Delete Chk Result 
        {

            $.ajax({
                url: base_url + "transaction/Buy_sell/Delete/" + id,
                type: "POST",
                data: {id: id},
                success: function (data)
                {
                    //alert(data);
                    Swal.fire("Deleted!", "Your Record has been deleted.", "success");
                    load_data();
                }
            });
        } else
        {
            Swal.fire("Cancelled", "Your record is safe :)", "error");
        }

    })
}
// Delete Record End

// Bulk Delete Start
$(document).ready(function ()
{
    $('#bulk_delete_submit').on('click', function ()
    {
        if ($('.checkbox:checked').length > 0)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "Delete Selected Record",
                type: "warning",
                // icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) =>
            {
                if (result.value)  // Brfore Delete Chk Result 
                {
                    var users_arr = [];
                    $(".checkbox:checked").each(function ()
                    {
                        var id = $(this).val();
                        users_arr.push(id);
                    });
                    var length = users_arr.length;

                    $.ajax({
                        url: base_url + "transaction/Buy_sell/Delete_Bulk",
                        type: "POST",
                        //data : { id: id },
                        data: {id: users_arr},
                        success: function (data)
                        {
                            Swal.fire
                                    (
                                            'Deleted!',
                                            'Record has been deleted.',
                                            'success'
                                            )
                            $(function () {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000
                                });
                                toastr.error('Record Delete Successfully.')
                            });
                            load_data(); // Load AJAX
                        }
                    });
                } else
                {
                    Swal.fire("Cancelled", "Your record is safe :)", "error");
                }
                //return false;
            })
        } else
        {
            Swal.fire("Info", "Select at least 1 record to delete", "info");
        }
    });
});
// Bulk Delete End

// 
// $('#qty1').change
// $('#qty1').change(function(){  blur
// alert("ss");
// $('#qty1').color("red");
// $('#code').attr({class: 'form-control ui-autocomplete-input'});
// var  qty1 =  $('#qty1').val();

//$('#qty1').val("+").val();
// $('#qty1').val($('-').val())
//$('#qty1').val("-"+).val();
// $('#qty1').val($('+').val())
// var arr1 = data.ex_date.split('-');
// $("#ex_date").val(arr1[2] + "-" + arr1[1] + "-" + arr1[0]);	
//$('#qty1').val("+").val();

//********************************   Drop Down Change to Qty Color Change  -> Start  ****************************//	
$(document).ready(function ()
{
    function colorfunction()
    {
        var qty1 = parseInt($('#qty1').val());

        if (qty1 < 0)
        {
            // $('#qty1').attr({style: 'background-color:red; color:#FFF'});
            $('#topform').attr({style: 'background-color:red;'});
            // $("#qty1").val( "-" + qty1);
            //$("#qty1").val(-qty1);
            // -Math.abs(num) => Always negative		
        } else
        {
            // $('#qty1').attr({style: 'background-color:green; color:#FFF'});
            $('#topform').attr({style: 'background-color:blue;'});
            // var qty1 = $('#qty1').val();
            // $("#qty1").val(Math.abs(qty1));
            //$("#qty1").val( "+" + qty1);	

        }
    }

    // change
    $('#qty1').on('change', function (event)
    {
        colorfunction();
    });

    // keyup
    $('#qty1').on('keyup', function (event)
    {
        colorfunction();
    });

    // keydown
    $('#qty1').on('keydown', function (event)
    {
        colorfunction();
    });

});
// Buy Sell Drop Down To On Change Qty Text Box Color Change
$(document).ready(function ()
{
    $(document).on('change', '#buy_sell_id', function () {
        var qty1 = parseInt($('#qty1').val());

        if ($('#buy_sell_id').val() == "1")
        {
            $("#qty1").val(Math.abs(qty1));
            // $('#qty1').attr({style: 'background-color:green; color:#FFF'});
            $('#topform').attr({style: 'background-color:blue;'});
        } else
        {
            if (qty1 < 0)
            {
                // $('#qty1').attr({style: 'background-color:red; color:#FFF'});

                $('#topform').attr({style: 'background-color:red;'});
                // $("#qty1").val( "-" + qty1);
                $("#qty1").val(qty1);
                // -Math.abs(num) => Always negative		
            } else
            {
                // $('#qty1').attr({style: 'background-color:green; color:#FFF'});

                $('#topform').attr({style: 'background-color:blue;'});

                // var qty1 = $('#qty1').val();
                $("#qty1").val(Math.abs(qty1) * -1);
                //$("#qty1").val( "+" + qty1);	

            }

            //$('#qty1').attr({style: 'background-color:red; color:#FFF'});
            $('#topform').attr({style: 'background-color:red;'});
        }
    });
});
//********************************   Drop Down Change to Qty Color Change  -> End  ****************************//		
//********************************  Qty + - To Buy Sell Drop Down Change  -> Start  ****************************//	

// keyup
$(document).ready(function ()
{
    $(document).on('keyup', '#qty1', function ()
    {
        if ($('#qty1').val() < 1)
        {
            // sell 
            $('#buy_sell_id').val("2");
        } else
        {
            $('#buy_sell_id').val("1");
            // Buy
        }
    });
});

//  keydown
$(document).ready(function ()
{
    $(document).on('keydown', '#qty1', function ()
    {
        if ($('#qty1').val() < 1)
        {
            // sell 
            $('#buy_sell_id').val("2");
        } else
        {
            $('#buy_sell_id').val("1");
            // Buy
        }
    });
});

// change
$(document).ready(function ()
{
    $(document).on('change', '#qty1', function ()
    {
        if ($('#qty1').val() < 1)
        {
            // sell 
            $('#buy_sell_id').val("2");
        } else
        {
            $('#buy_sell_id').val("1");
            // Buy
        }
    });
});

//******************************** Qty + - To Buy Sell Drop Down Change  - End  ****************************//	

//******** For Party Code And Party_Name  && Broker Code And Broker Name IF Mathch same Value Display Validation Msg Code  - Start ********//

// brokrage_name  ,  party_name ,  party_code , brokrage_code

$(document).ready(function ()
{

    $('#party_code').on('focusout', function (event)
    {
        var party_code = $('#party_code').val();
        var brokrage_code = $('#brokrage_code').val();
        if (party_code == brokrage_code)
        {
            // alert("Brokrage Code And Party Code Both Are Same 1");
            toastr.error('Brokrage Code And Party Code Both Are Same');
        }
    });

    $('#brokrage_code').on('focusout', function (event)
    {
        var party_code = $('#party_code').val();
        var brokrage_code = $('#brokrage_code').val();
        if (party_code == brokrage_code)
        {
            // alert("Brokrage Code And Party Code Both Are Same 2");
            toastr.error('Brokrage Code And Party Code Both Are Same');
        }
    });

    $('#brokrage_name').on('focusout', function (event)
    {
        var brokrage_name = $('#brokrage_name').val();
        var party_name = $('#party_name').val();
        if (brokrage_name == party_name)
        {
            // alert("Brokrage Name And Party Name Both Are Same 1");
            toastr.error('Brokrage Name And Party Name Both Are Same.')
        }
    });

    $('#party_name').on('focusout', function (event)
    {
        var brokrage_name = $('#brokrage_name').val();
        var party_name = $('#party_name').val();
        if (brokrage_name == party_name)
        {
            // alert("Brokrage Name And Party Name Both Are Same 2");
            toastr.error('Brokrage Name And Party Name Both Are Same.')
        }
    });
});

//******** For Party Code And Party_Name  && Broker Code And Broker Name IF Mathch same Value Display Validation Msg Code - End ********//	

//******** For Window Load To Default Hide table - Start ****//
$(window).on("load", function () {
    $("#ViewTable").hide();
    // $("#ViewTable").css("display","none");
});

$(document).ready(function ()
{
    $('#viewbtn').on('click', function ()
    {
        load_data();
        $("#ViewTable").show();
        window.resizeTo("1300", "700"); // w h
    });

    $('#Close').on('click', function ()
    {
        //load_data();
        $("#ViewTable").hide();
        window.resizeTo("1300", "200"); // w h
    });

});
//******** For Window Load To Default Hide table - End ****//           
//******** Date Picker Start ***** //

$(function ()
{
    $('#date1').datepicker({
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
    });

    $('#expiries_id').datepicker({
        dateFormat: 'dd-mm-yy',
    });

    $('#From_Date').datepicker({
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
    });

    $('#To_Date').datepicker({
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
    });
});

$(function () {
    $('#date').datepicker({
        dateFormat: 'dd-mm-yy',
    });
});
//******** Date Picker End ***** //

//******** select2 Start ***** //
$(document).ready(function ()
{
    $(".select2").select2();

    $('.select2').select2({
        theme: 'bootstrap4'
    })
});
//******** Select2 End ***** //

//	

$("#FormSubmit").validate({

    /*
     var  = $("#buy_sell_id").val();
     var  = $("#symbol_id").val();
     var instument = $("#instument").val();
     var expiry = $("#expiry").val();
     
     var qty1 = $("#qty1").val();
     var rate1 = $("#rate1").val();
     var uid = $("#uid").val();
     var party_code = $("#party_code").val();
     var party_name = $("#party_name").val();
     var brokrage_code = $("#brokrage_code").val();
     var brokrage_name = $("#brokrage_name").val();
     var re_no = $("#re_no").val();
     var remark = $("#remark").val();
     */
    rules:
            {
                exchange_id:
                        {
                            required: true,
                        },

                setlement_id:
                        {
                            required: true,
                        },

                type:
                        {
                            required: true,
                        },

                date1:
                        {
                            required: true,
                        },

                buy_sell_id:
                        {
                            required: true,
                        },
                symbol_id:
                        {
                            required: true,
                        },

            },

    messages:
            {
                exchange_id:
                        {
                            required: 'Please Enter Exchange Name.',
                        },

                setlement_id:
                        {
                            required: 'Please Enter setlement.',
                        },

                type:
                        {
                            required: 'Please Enter  type.',
                        },
                date1:
                        {
                            required: 'Please Enter Date.',
                        },
                buy_sell_id:
                        {
                            required: 'Please Enter Date.',
                        },
                symbol_id:
                        {
                            required: 'Please Enter symbol.',
                        },

            },

});
//**************************************************************************//
// Hidden id Edit 

function broker() {
    var bid = $("#bid").val();
    if (bid.length > 0) {
        $.ajax({
            url: base_url + "transaction/Buy_sell/broker_id_search/",
            type: 'POST',
            data: {bid: bid},
            dataType: "json",
            success: function (data)
            {
                if (!$.trim(data))
                {
                    $('#brokrage_name').val("");
                    $('#brokrage_code').val("");
                    $('#bid').val("");
                } else
                {
                    $('#brokrage_name').val(data.username);
                    $('#brokrage_code').val(data.code);
                    $("#bid").val(data.aid);
                }
            }
        });
    }
}

function party()
{
    var pid = $("#pid").val();
    //console.log(pid)
    if (pid.length > 0) {
        $.ajax({
            url: base_url + "transaction/Buy_sell/party_id_search/",
            type: 'POST',
            data: {pid: pid},
            dataType: "json",
            success: function (data)
            {

                // If Wrong Data Type In Text Box Will null 
                // toastr.error('Please Enter Correct Acccount Name')
                if (!$.trim(data))
                {

                    $('#party_name').val("");
                    $('#party_code').val("");
                    $('#pid').val("");
                } else
                {
                    $('#party_name').val(data.username);
                    $('#party_code').val(data.code);
                    $("#pid").val(data.aid);
                }
            }
        });
    }
}

//**************************************************************************//
// 1-1-2020
/*
 $(document).ready(function () {
 $(document).on('change focusout', '#party_code', function (event) {
 var codeVal = $(this).val();
 if (codeVal.length) {
 $.ajax({
 url: base_url + "transaction/Buy_sell/party_code_search/",
 type: 'post',
 data: {code: codeVal},
 dataType: "json",
 success: function (data)
 {
 // If Wrong Data Type In Text Box Will null 
 // toastr.error('Please Enter Correct Acccount Name')
 if (!$.trim(data))
 {
 $('#party_name').val("");
 $('#pid').val("");
 $('#party_code').val("");
 } else
 {
 $("#party_name").val(data.username);
 $("#pid").val(data.aid);
 }
 }
 });
 }
 });
 });
 */

/*
 $(document).ready(function () {
 $(document).on('change focusout', '#brokrage_code', function (event) {
 var codeVal = $(this).val();
 if (codeVal.length) {
 $.ajax({
 url: base_url + "transaction/Buy_sell/brokrage_code_search/",
 type: 'post',
 data: {code: codeVal},
 dataType: "json",
 success: function (data)
 {
 // If Wrong Data Type In Text Box Will null 
 // toastr.error('Please Enter Correct Acccount Name')
 if (!$.trim(data))
 {
 $('#bid').val("");
 $('#brokrage_name').val("");
 $('#brokrage_code').val("");
 } else
 {
 $("#brokrage_name").val(data.username);
 $("#bid").val(data.aid);
 }
 }
 });
 }
 });
 });
 */

//****** For Party Code AND Party Name Start Search ****//
// Autocomplete Start For Account-Name // Party_Name Searching 
/*
 $(document).ready(function ()
 {
 $("#party_name").autocomplete({
 source: function (request, response)
 {
 $.ajax({
 url: base_url + "transaction/Buy_sell/Search_Account_Name/",
 type: 'post',
 dataType: "json",
 data: {search: request.term},
 success: function (data)
 {
 if (!$.trim(data))
 {
 // If Wrong Data Type In Text Box Will null 
 $('#party_name').val("");
 $('#pid').val("");
 $('#party_code').val("");
 toastr.error('Please Enter Correct Acccount Name')
 } else
 {
 //$('#code-error').hide(); // label Hide
 //$('#code').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
 response(data); // This is Auto complete code  only if part is added
 }
 }
 });
 },
 select: function (event, ui)
 {
 $('#pid').val(ui.item.value);
 $('#party_name').val(ui.item.label);
 $('#party_code').val(ui.item.party_code);
 return false;
 }
 });
 });
 */
// Autocomplete End For Account-Name  // party name Searching 

// Autocomplete Start For Account-Code Searching 
/*
 $(document).ready(function ()
 {
 $("#party_code").autocomplete({
 source: function (request, response)
 {
 $.ajax({
 url: base_url + "transaction/Buy_sell/Search_Account_Code/",
 type: 'post',
 dataType: "json",
 data: {search: request.term},
 success: function (data)
 {
 if (!$.trim(data))
 {
 // If Wrong Data Type In Text Box Will null
 $('#pid').val("");
 $('#party_code').val("");
 $('#party_name').val("");
 toastr.error('Please Enter Correct Acccount Code')
 
 } else
 {
 //$('#name-error').hide(); // label Hide
 //$('#name').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
 response(data); // This is Auto complete code  only if part is added
 }
 }
 });
 },
 select: function (event, ui)
 {
 $('#pid').val(ui.item.value);
 $('#party_code').val(ui.item.label);
 $('#party_name').val(ui.item.party_name);
 return false;
 }
 });
 });
 */
// Autocomplete End For Account-Code Searching 

/******
 For Broker Code AND Brokrage Name Start Search 
 ****/
// Autocomplete Start For Account-Name // Party_Name Searching  || brokrage_name
/*
 $(document).ready(function ()
 {
 $("#brokrage_name").autocomplete({
 source: function (request, response)
 {
 $.ajax({
 url: base_url + "transaction/Buy_sell/Search_Broker_Name/",
 type: 'post',
 dataType: "json",
 data: {search: request.term},
 success: function (data)
 {
 if (!$.trim(data))
 {
 // If Wrong Data Type In Text Box Will null 
 
 $('#brokrage_name').val("");
 $('#bid').val("");
 //	$('#uid').val("");
 $('#brokrage_code').val("");
 toastr.error('Please Enter Correct Broker Name')
 
 } else
 {
 //$('#code-error').hide(); // label Hide
 //$('#code').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
 response(data); // This is Auto complete code  only if part is added
 }
 }
 });
 },
 select: function (event, ui)
 {
 $('#bid').val(ui.item.value);
 $('#brokrage_name').val(ui.item.label);
 // $('#uid').val(ui.item.value);
 
 $('#brokrage_code').val(ui.item.brokrage_code); // party_code
 return false;
 }
 });
 });
 */
// Autocomplete End For Account-Name  || brokrage_name Searching 
/*  */
/*  */
// Autocomplete Start For Account-Code Searching 
/*
 $(document).ready(function () {
 $("#brokrage_code").autocomplete({
 source: function (request, response)
 {
 $.ajax({
 url: base_url + "transaction/Buy_sell/Search_Brokrage_Code/",
 type: 'post',
 dataType: "json",
 data: {search: request.term},
 success: function (data)
 {
 if (!$.trim(data))
 {
 // If Wrong Data Type In Text Box Will null
 //$('#uid').val("");  
 
 $('#brokrage_code').val("");
 $('#brokrage_name').val("");
 toastr.error('Please Enter Correct Brokrage  Code')
 
 } else
 {
 //$('#name-error').hide(); // label Hide
 //$('#name').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
 response(data); // This is Auto complete code  only if part is added
 }
 }
 });
 },
 select: function (event, ui)
 {
 $('#bid').val(ui.item.value);
 $('#brokrage_code').val(ui.item.label);
 //$('#uid').val(ui.item.value);
 $('#brokrage_name').val(ui.item.brokrage_name);
 return false;
 }
 });
 });
 */
// Autocomplete End For Account-Code Searching 
/*
 //symbol_id_global=Object.seal(data.symbol_id);	
 // ex_date_global=Object.seal(data.ex_date);
 // $('#symbol_id').val(data.symbol_id).trigger('change'); 
 // var brokrage_code = $("#brokrage_code").val();
 
 // ex_date
 
 $("#symbol_id").val(data.symbol_id); 
 
 $('#exchange_id').val(data.exchange_id).trigger('change');
 $("#symbol").val(data.symbol);
 $("#instument").val(data.instument);
 $("#lot_size").val(data.lot_size);
 $('#FormSubmit').attr('id','FormUpdate');  // Change ID
 
 
 */



var aid_global;
$(document).ready(function () {
    party_code();
    party_name();
});

$(document).on('change', '#party_code', function () {
    aid_global = $("#party_code").val();
    $("#pid").val(aid_global);
    party_name(aid_global);
});

$(document).on('change', '#party_name', function () {
    aid_global = $("#party_name").val();
    $("#pid").val(aid_global);
    party_code(aid_global);
});

// All Party Load In Droup Down
function party_code(aid_global = null)
{
    $.ajax({
        url: base_url + "global/Global_function/all_party",
        method: "POST",
        async: true,
        dataType: 'json',
        success: function (data)
        {
            var html = '';
            html += '<option value="">Select Party Code </option>';
            var i;
            for (i = 0; i < data.length; i++)
            {
                //html += '<option value=' + data[i].aid + '>' + data[i].code + '</option>';
                html += '<option value=' + data[i].aid + ' ' + ((aid_global == data[i].aid) ? "selected" : "") + '>' + data[i].code + '</option>';
            }
            $('#party_code').html(html);
        }
    });
    return false;
}


// All Party Load In Droup Down
function party_name(aid_global = null)
{
    $.ajax({
        url: base_url + "global/Global_function/all_party",
        method: "POST",
        data: {aid_global: aid_global},
        async: true,
        dataType: 'json',
        success: function (data)
        {
            var html = '';
            html += '<option value="">Select Party Name</option>';
            var i;
            for (i = 0; i < data.length; i++)
            {
                //html += '<option value=' + data[i].aid + '>' + data[i].username + '</option>';
                html += '<option value=' + data[i].aid + ' ' + ((aid_global == data[i].aid) ? "selected" : "") + '>' + data[i].username + '</option>';
            }
            $('#party_name').html(html);
        }
    });
}

