

		// Load Data Table Start
		load_data();
		function load_data()
		{
			$.ajax({
					
					url: base_url+"master/Exchange/Ajax_View_Table",
					method:"POST",
					success:function(data)
					{
						$('#Response').html(data);
						$('.MyTable').DataTable({
							"paging": true,
							"lengthChange": true,
							"searching": true,
							"ordering": true,
							"info": true,
							"autoWidth": false,
							"responsive": true,
							//"pageLength": 5,
							"sLengthMenu": "Display _MENU_ records",
							"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
							"iDisplayLength": 5
						});

					}
				});
		}
		// Load Data Table End
			
			
		// Add Record Start
		$(document).ready(function()
		{
			$(document).on('submit', '#FormSubmit', function() 
			{ 
				//   alert('add');
				
				var name = $("#name").val();
				var decimal_points = $("#decimal_points").val();
				var dataString = 'name='+ name + '&decimal_points='+ decimal_points;

                $.ajax({
					type: "POST",
					url: base_url+"master/Exchange/Add",
					data: dataString,
					cache: false,

					success: function(data)
					{
						  if (data == 1) 
						  {
							$("#FormSubmit")[0].reset();  // Form Clear after insert
							load_data(); // Load Ajax
							
							// alert(data);
						
							$(function() {
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.success('Exchange Added Successfully.')
							});
						}
					}
				});
                return false;
             });
         });
          // Add Record End
		  
		
		// Edit Record Start
		function Edit(id)
        {
			$(document).ready(function(){
			$.ajax({
					data:  "id="+id,
					url: base_url+"master/Exchange/Edit/"+id,
					type: "POST",
					dataType: 'json',
				
					success: function(data)
					{
						$("#exchange_id").val(data.exchange_id);   
						$("#name").val(data.name);
						$("#decimal_points").val(data.decimal_points);
						$('#FormSubmit').attr('id','FormUpdate');  // Change ID
					}
				});
			});
		}
		// Edit Record End
		
		
		// Update Record Start
		$(document).on('submit',  '#FormUpdate', function() { 
			
			var exchange_id = $("#exchange_id").val();
			var name = $("#name").val();
			var decimal_points = $("#decimal_points").val();
			
			$.ajax({

			type:"POST", 
			url: base_url+"master/Exchange/Update",
			data: {exchange_id:exchange_id , name:name , decimal_points:decimal_points },
			   
			success: function(response,data)
            { 
				$("#FormUpdate")[0].reset();  // Form Clear after Update
				
					$(function(){
						const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
						});
						toastr.info('Exchange Update Successfully.')
					});
					load_data(); // Load AJAX
					
					$('#FormUpdate').attr('id','FormSubmit');
			 }
		});
		return false;
   });
     // Update Record End



	// Delete Record Start
	function Delete(id)
	{
		Swal.fire({
			title: "Are you sure to delete this Exchange ?",
			text: "You will not be able to recover this Data !!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it !!",
			reverseButtons: true
			}).then((result) => {
	
				if (result.value)  // Brfore Delete Chk Result 
				{
				
					$.ajax({
						url: base_url+"master/Exchange/Delete/"+id,
						type : "POST",
						data: { id: id},
						success: function(data) 
						{
							//alert(data);
							Swal.fire("Deleted!", "Your Record has been deleted.", "success");
							$(function(){
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.error('Exchange Delete Successfully.')
							});
							load_data();
						}
					});
				}
				else 
				{
					Swal.fire("Cancelled", "Your record is safe :)", "error");
				}
		
			})
	}
	// Delete Record End

	
			
	// Bulk Delete Start
	$(document).ready(function()
	{
		$('#bulk_delete_submit').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Delete Selected Exchange",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
				}).then((result) => 
				{
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
								url: base_url+"master/Exchange/Delete_Bulk",
								type : "POST",
								//data : { id: id },
								data: {id: users_arr},
								success: function(data)
								{
									Swal.fire
									(
										'Deleted!',
										'Exchange has been deleted.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Exchange Delete Successfully.')
									});
									load_data(); // Load AJAX
								}
							});
					}
					else 
					{
						Swal.fire("Cancelled", "Your record is safe :)", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to delete", "info");
			}
		});
	});
	// Bulk Delete End
		
		
$(document).ready(function() {
  jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0 && value != ""; 
  }, "Space are not allowed");		

$("#FormSubmit").validate({

	 rules:
	{
		name:
		{
			required: true,
			noSpace: true
		},
		
		decimal_points:
		{
			required: true,
		},
	},
	
	messages:
	{
		name:
		{
			 required: 'Please Enter Exchange Names.',
			 noSpace: 'Not Allowed Space',
		},
		
		decimal_points:
		{
			required: 'Please Enter Decimal Points.',
		},
	},
		 
		
});

});
	