/*
	var start_date=[];
	var end_date=[];
        $(document).ready(function(){
 
            $('#exchange_id').change(function(){  
                var id=$(this).val();
                $.ajax({
					url: base_url+"reports/Stock_position/Exchange_On_Change",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
						html += '<option value="" disabled selected>Select</option>';
					
                        for(i=0; i<data.length; i++){
							
							start_date[data[i].setlement_id]=data[i].start_date; 
							end_date[data[i].setlement_id]=data[i].end_date; 
							//	console.log(a);
							
                            html += '<option value='+data[i].setlement_id+'>'+data[i].description;+'</option>';
                        }
                        $('#setlement_id').html(html); 
						
					}
                });
                return false;
            }); 
             
        });

		$(document).on('change',  '#setlement_id', function() { 
			var id=$(this).val();
			
			
			var arr1Dt = start_date[id].split('-');
			$("#start_date").val(arr1Dt[2] + "-" + arr1Dt[1] + "-" + arr1Dt[0]);	
			
			var arr1Dt2 = end_date[id].split('-');
			$("#end_date").val(arr1Dt2[2] + "-" + arr1Dt2[1] + "-" + arr1Dt2[0]);	
						
			
	  });
	  */
	  
	  
// Autocomplete Start For Account-Code Searching 
	  $(document).ready(function(){
		  
			$("#party_code").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"reports/Bill/Search_Account_Code/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							// If Wrong Data Type In Text Box Will null
							
							$('#party_code').val("");
							$('#party_name').val("");
							toastr.error('Please Enter Correct Acccount Code')
					
						}
						else
						{
							//$('#name-error').hide(); // label Hide
							//$('#name').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
							response(data); // This is Auto complete code  only if part is added
						}
					}
				});
			},
			select: function (event, ui) 
			{
				
				$('#party_code').val(ui.item.label);
				$('#party_name').val(ui.item.party_name);
				return false;
			}
		});
	});
	// Autocomplete End For Account-Code Searching 
	
	$(document).ready(function(){
		$(document).on('change focusout',  '#party_code', function(event) {
			
			var codeVal = $(this).val();
				if(codeVal.length){
						$.ajax({
							url: base_url+"reports/Bill/party_code_search/",
							type: 'post',
							data: {code: codeVal},
							dataType: "json",
							success:function(data)
							{
								// If Wrong Data Type In Text Box Will null 
								// toastr.error('Please Enter Correct Acccount Name')
								if (!$.trim(data))
								{   
									$('#party_name').val("");
									$('#party_code').val("");
								}
								else
								{
									$("#party_name").val(data.username);
									
								}
							}
						});
					}
				});
			});
			
			
	  
	  
	$(function () {
    $("#start_date").datepicker({
   
     dateFormat: 'dd-mm-yy',
		firstDay: 1,
       onSelect: function (selected) {
        $("#end_date").datepicker("option","minDate", selected)
      
        }
    });
	$("#end_date").datepicker({
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		onSelect: function (selected) {
        $("#start_date").datepicker("option","maxDate", selected)
        }
    });
});


$("#FormSubmit").validate({
		rules:
		{
			exchange_id:
			{
				required: true,
			 
			},
			
			start_date:
			{
				required: true,
			 
			},
			 end_date:
			{
				required: true,
			   
			},
			setlement_id:
			{
				required: true,
			   
			},
			
		 },
		messages:
		{
			exchange_id:
			{
				 required: 'Please Select Exchange.',
			},
			
			start_date:
			{
				required: 'Please  Select Start Date.',
				
			},
			 end_date:
			{
				required: 'Please Select End Date.',
				
			},
			setlement_id:
			{
				required: 'Please Select Setlement .',
			   
			},
		},
	});
