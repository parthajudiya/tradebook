$("#FormSubmit").validate({
	rules:
	{
		exchange_id:
		{
			required: true,
		},
		
		start_date:
		{
			required: true,
		},
		end_date:
		{
			required: true,
		},
		party_code:
		{
			required: true,
		},
		party_name:
		{
			required: true,
		},
	},
	
	messages:
	{
		exchange_id:
		{
			 required: 'Please Enter Exchange Name.',
		},
		
		start_date:
		{
			required: 'Please Enter Start Date.',
		},
		end_date:
		{
			required: 'Please Enter End Date.',
		},
		party_code:
		{
			required: 'Please Enter Party Code.',
		},
		party_name:
		{
			required: 'Please Enter Party Name.',
		},
	},
});

		$(document).ready(function()
		{	
			$(document).on('submit', '#FormSubmit', function()  // // $(document).on('click', '#Submit', function() 
			{ 
				var exchange_id = $("#exchange_id").val();
				var arr1Dt = $("#start_date").val();
				var arr1Dt2 = $("#end_date").val();
				var party_code = $("#party_code").val();
				var party_name = $("#party_name").val();
				
				$.ajax({ 
					url: base_url+"reports/Party_ledger/ResultTable", 
                    method : "POST",
                    data : {exchange_id:exchange_id,  arr1Dt:arr1Dt , arr1Dt2:arr1Dt2 , party_code:party_code , party_name:party_name},
                  
                    success: function(data)
					{
					
						$('#Response1').html(data);
						$('.MyTable1').DataTable({
							"paging": true, 
							"searching": true,
							"info": true,
							"responsive": true,
							//"scrollY":'20vh',
							//"scrollX": true,
						});
						
						// load_();
					}
                });  
				return false; 
            }); 
        });



	