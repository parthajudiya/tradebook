

// Autocomplete Start For Account-Code Searching 
$(document).ready(function ()
{
    $("#party_code").autocomplete({
        source: function (request, response)
        {
            $.ajax({
                url: base_url + "global/Global_function/Search_Account_Code/",
                type: 'post',
                dataType: "json",
                data: {search: request.term},
                success: function (data)
                {
                    if (!$.trim(data))
                    {
                        // If Wrong Data Type In Text Box Will null

                        $('#aid').val(""); // 28-dec

                        $('#party_code').val("");
                        $('#party_name').val("");
                        toastr.error('Please Enter Correct Acccount Code')

                    } else
                    {
                        //$('#name-error').hide(); // label Hide
                        //$('#name').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
                        response(data); // This is Auto complete code  only if part is added
                    }
                }
            });
        },
        select: function (event, ui)
        {

            $('#party_code').val(ui.item.label);
            $('#party_name').val(ui.item.party_name);
            $('#aid').val(ui.item.value); // 28-dec
            return false;
        }
    });
});
// Autocomplete End For Account-Code Searching 


$(document).ready(function () {
    $(document).on('change focusout', '#party_code', function (event) {

        var codeVal = $(this).val();
        if (codeVal.length) {
            $.ajax({
                url: base_url + "global/Global_function/party_code_search/",
                type: 'post',
                data: {code: codeVal},
                dataType: "json",
                success: function (data)
                {
                    // If Wrong Data Type In Text Box Will null 
                    // toastr.error('Please Enter Correct Acccount Name')
                    if (!$.trim(data))
                    {
                        $('#aid').val(""); // 28-dec
                        $('#party_name').val("");
                        $('#party_code').val("");
                    } else
                    {
                        $("#party_name").val(data.username);
                        $("#aid").val(data.aid); // 28-dec

                    }
                }
            });
        } else // 28-dec
        {
            $('#aid').val(""); // 28-dec
            $('#party_name').val(""); // 28-dec
            $('#party_code').val(""); // 28-dec
        }
    });
});
