

		// Load Data Table Start
		load_data();
		function load_data()
		{
			$.ajax({
					url: base_url+"master/remiser_brokrage/Ajax_View_Table",
					method:"POST",
					success:function(data)
					{
						$('#Response').html(data);
						$('.MyTable').DataTable({
							"paging": true,
							"lengthChange": true,
							"searching": true,
							"ordering": true,
							"info": true,
							"autoWidth": false,
							"responsive": true,
							//"pageLength": 5,
							//"sLengthMenu": "Display _MENU_ records",
							"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
							"iDisplayLength": 5
						});
					}
				});
		}
		// Load Data Table End
			
	/*	
		// Add Record Start
		$(document).ready(function()
		{
			$(document).on('submit', '#FormSubmit', function() 
			{ 
				//   alert('add');
				
				var exchange_id = $("#exchange_id").val();
				var symbol = $("#symbol").val();
				var instument = $("#instument").val();
				var lot_size = $("#lot_size").val();
				
				var dataString = 'exchange_id='+ exchange_id + '&symbol='+ symbol + '&instument='+ instument + '&lot_size='+ lot_size;

                $.ajax({
					type: "POST",
					url: base_url+"master/remiser_brokrage/Add",
					data: dataString,
					cache: false,

					success: function(data)
					{
						  if (data == 1) 
						  {
							$("#FormSubmit")[0].reset();  // Form Clear after insert
							load_data(); // Load Ajax
							
							// alert(data);
						
							$(function() {
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.success('Remiser Brokrage Added Successfully.')
							});
						}
					}
				});
                return false;
             });
         });
          // Add Record End
		  
		
		// Edit Record Start
		function Edit(id)
        {
			$(document).ready(function(){
			$.ajax({
					data:  "id="+id,
					url: base_url+"master/Symbol/Edit/"+id,
					type: "POST",
					dataType: 'json',
				
					success: function(data)
					{
						$("#symbol_id").val(data.symbol_id); 
						$("#exchange_id").val(data.exchange_id);
						$("#symbol").val(data.symbol);
						$("#instument").val(data.instument);
						$("#lot_size").val(data.lot_size);
						$('#FormSubmit').attr('id','FormUpdate');  // Change ID
					}
				});
			});
		}
		// Edit Record End
		

		// Update Record Start
		$(document).on('submit',  '#FormUpdate', function() { 
			
			var exchange_id = $("#exchange_id").val();
			var symbol = $("#symbol").val();
			var instument = $("#instument").val();
			var lot_size = $("#lot_size").val();
			
			var symbol_id = $("#symbol_id").val();
		
			
			$.ajax({

			type:"POST", 
			url: base_url+"master/Symbol/Update",
			data: {exchange_id:exchange_id , symbol:symbol , instument:instument , lot_size:lot_size , symbol_id:symbol_id },
			   
			success: function(response,data)
            { 
				$("#FormUpdate")[0].reset();  // Form Clear after Update
				
					$(function(){
						const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
						});
						toastr.info('Symbol Update Successfully.')
					});
					load_data(); // Load AJAX
					
					$('#FormUpdate').attr('id','FormSubmit');
			 }
		});
		return false;
   });
     // Update Record End
	*/


	// Delete Record Start
	function Delete(id)
	{
		Swal.fire({
			title: "Are you sure to delete this Remiser Brokrage ?",
			text: "You will not be able to recover this Data !!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it !!",
			reverseButtons: true
			}).then((result) => {
	
				if (result.value)  // Brfore Delete Chk Result 
				{
				
					$.ajax({
						url: base_url+"master/remiser_brokrage/Delete/"+id,
						type : "POST",
						data: { id: id},
						success: function(data) 
						{
							//alert(data);
							Swal.fire("Deleted!", "Your Record has been deleted.", "success");
							load_data();
						}
					});
				}
				else 
				{
					Swal.fire("Cancelled", "Your record is safe :)", "error");
				}
		
			})
	}
	// Delete Record End

	
			
	// Bulk Delete Start
	$(document).ready(function()
	{
		$('#bulk_delete_submit').on('click',function()
		{	
			if($('.checkbox:checked').length > 0)
			{
				Swal.fire({
				title: 'Are you sure?',
				text: "Delete Selected Remiser Brokrage",
				type: "warning",
				// icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
				}).then((result) => 
				{
					if (result.value)  // Brfore Delete Chk Result 
					{
						var users_arr = [];
						$(".checkbox:checked").each(function()
						{
							var id = $(this).val();
							users_arr.push(id);
						});
						var length = users_arr.length;  
				
							$.ajax({
								url: base_url+"master/remiser_brokrage/Delete_Bulk",
								type : "POST",
								//data : { id: id },
								data: {id: users_arr},
								success: function(data)
								{
									Swal.fire
									(
										'Deleted!',
										'Remiser Brokrage has been deleted.',
										'success'
									)
									$(function(){
										const Toast = Swal.mixin({
										toast: true,
										position: 'top-end',
										showConfirmButton: false,
										timer: 3000
										});
										toastr.error('Remiser Brokrage Delete Successfully.')
									});
									load_data(); // Load AJAX
								}
							});
					}
					else 
					{
						Swal.fire("Cancelled", "Your record is safe :)", "error");
					}
					//return false;
				}) 
			}
			else
			{
				Swal.fire("Info", "Select at least 1 record to delete", "info");
			}
		});
	});
	// Bulk Delete End
		
		
		

$("#FormSubmit").validate({

	 rules:
	{
		exchange_id:
		{
			required: true,
		},
		
		symbol:
		{
			required: true,
		},
		instument:
		{
			required: true,
		},
		lot_size:
		{
			required: true,
		},
	},
	
	messages:
	{
		exchange_id:
		{
			 required: 'Please Enter Exchange Name.',
		},
		
		symbol:
		{
			required: 'Please Enter Symbol.',
		},
		instument:
		{
			required: 'Please Enter Instument.',
		},
		lot_size:
		{
			required: 'Please Enter Lot size.',
		},
	},
		 
		
});



  $(document).ready(function() {
    $(".select2").select2();
	
	$('.select2').select2({
      theme: 'bootstrap4'
    })
});




	