<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->isLogin();
	
    }

    function isLogin() {
        $directory = $this->router->directory;
        $class = $this->router->class;
        $method = $this->router->method;
        if (!$this->ion_auth->logged_in()) {
            if ($class == "auth" && ($method == "login" || $method == "forgot_password")) {
                
            } else {
                redirect('auth/login', 'refresh');
            }
        } else {
            if ($method != "logout") {
                if ($class == "auth" && $method == "login") {
                    redirect('welcome', 'refresh');
                }
                if (!$this->ion_auth->is_admin()) {
                    redirect('welcome', 'refresh');
                } else {
                    
                }
            }
        }
    }

    public function display($view, $data = array()) {
        $directory = $this->router->directory;
        $class = $this->router->class;
        $method = $this->router->method;
        $dir_c = explode("/", $directory);
        //form help
        if ($method == "add" || $method == "edit") {
            $this->load->helper('form');
        }
        $this->load->view("include/header");
        //view
        $this->load->view($directory . "$class/" . $view, $data);

        $this->load->view("include/footer");
    }

}
