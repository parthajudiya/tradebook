<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";


class Brokrage_update extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('process/Brokrage_update_model');
	}
	
	public function index()
	{
		$data["Exchange"] = $this->Brokrage_update_model->Select_Exchange();
        $this->display("index", $data);
	}
	
	
	
	public function Update()
	{
		$data = array();
		$exchange_id = $this->input->post('exchange_id');
		$setlement_id = $this->input->post('setlement_id');
		
		$party_code = empty($this->input->post('party_code'))?null:$this->input->post('party_code');
		
		# SD 
		$start_date = explode('-', $_POST['start_date']);
		$start_date = "$start_date[2]-$start_date[1]-$start_date[0]";
		# ED
		$end_date =  explode('-', $_POST['end_date']);
		$end_date = "$end_date[2]-$end_date[1]-$end_date[0]";
		
		$data = $this->Brokrage_update_model->SelBsBro($exchange_id , $setlement_id , $start_date , $end_date, $party_code);
		
		foreach($data as $row)
		{
			$party_code = $row['party_code'];
			$buysell_id = $row['buysell_id'];
			$brokrage = $row['qty1']*$row['rate1']*$row['intraday']/100;
			$myarry = array(
				'brokrage' =>$brokrage,
				
			);
			$result = $this->Brokrage_update_model->update($myarry , $buysell_id , $party_code);
		}
	}
	
}
