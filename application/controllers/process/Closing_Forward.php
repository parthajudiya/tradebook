<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";
ini_set('max_execution_time', 5000);
// error_reporting(0);
class Closing_forward extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('process/Closing_forward_model');
	}
	
	
	//31-DEC-2020
	public function UploadDataExcel()
	{
		
		 
			$config['upload_path']   = './upload/'; 
			$config['allowed_types'] = 'csv'; 
			$config['file_name'] =  $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
			if (!$uploadData ) // if ($count == 0) {}  && $count == 0
			{
				echo "Upload Error 3 ";
			}
			else
			{ 
				$uploadData2 = $this->upload->data();
				$FileData = $uploadData2['file_name'];
				$data = file_get_contents(base_url("upload/$FileData"),"r");
				$csv = explode("\n", $data);
				
			
				$count = 0;
				$symbol=array();
				foreach ($csv as $csvData)
				{
					if ($count == 0) {} 
					else 
					{
						$row = explode(",", $csvData);
						// if($row[4]!="PE" && $row[4]!="CE"  && $row[1]!="" )  {
							
							$symbol[]=array(
							'instrument' =>trim($row[1]),
							'symbol' =>trim($row[2]),
							'Exdt' =>trim($row[3]),
							'closingrate' =>trim($row[4])
							);
						// }
					}
					$count++;
				}
				echo json_encode($symbol);
				
			}
	}
	
	/* GET LTP IN MONEY Controller  IN PUT DATA Table 2*/
	// LTP
	public function GetData_Online()
	{
				$data_array = array();
				$array = array();
				
				$array = array(
					'Symbol' => $this->input->post("syname_glob"),
					'Instrument' => $this->input->post("ins_glob"),
					'Expirey' =>  $this->input->post("exdt_glob")
				);	
		
				 # Count Arry post data and check how much types of loop
				 $syname_glob = count($this->input->post("syname_glob"));
			for ($i = 0; $i < $syname_glob; $i++) {
				 # echo $i;
		 
		 
				 $Symbol_Code = $array['Symbol'][$i];
				 $SY_TYPE = $array['Instrument'][$i];
				 $SY_EXP = $array['Expirey'][$i];
				 
				$url = "https://www.moneycontrol.com/mc/widget/fnoquote/updateFnOTable?classic=true&sc_did=$Symbol_Code&instType=$SY_TYPE&optiontype=&expDate=$SY_EXP&strikeprice=Select&pricecurr=1";

				$cURLConnection = curl_init();
				curl_setopt($cURLConnection, CURLOPT_URL, $url);
				curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
				$phoneList = curl_exec($cURLConnection);
				$error=curl_error($cURLConnection);
				curl_close($cURLConnection);
				print_r($error);
				$response = json_decode($phoneList);
				
				// print_r($response);
					
				$lastprice = $response->fno_list->item[0]->lastprice;
				$expiry_date = $response->fno_list->item[0]->expiry_date;
				$fno_symbol = $response->fno_list->item[0]->fno_symbol;
				$fno_instrument = $response->fno_list->item[0]->fno_instrument;
					
				$data_array[] = array(
					'Passing_Symbol_Code'=>$Symbol_Code,
					'instrument' =>$fno_instrument,
					'symbol' =>$fno_symbol,
					'expiry_date' =>$expiry_date,
					'lastprice' =>$lastprice
				);
		 }
		 
		 echo json_encode($data_array);
		 
	}
	
	//************************** first table Load *********************************//
	# ABS Function For negative Value Convert In To Positive

	# select first table 	
	public function Check_Closing_Forward_Base_On_Buy_Sell() 
	{
		
		#POST DATA
		$exchange_id = $this->input->post('exchange_id');
		$closing_setlment_id = $this->input->post('closing_setlment_id');
		
		# Closing Rate Type LTP OR BHAV COPY Account
		 $closing_rate_type_id = $this->input->post('closing_rate_type_id');
		// exit;
		
		# SD
		$start_date = explode('-', $_POST['start_date']);
		$start_date = "$start_date[2]-$start_date[1]-$start_date[0]";
		# ED
		$end_date =  explode('-', $_POST['end_date']);
		$end_date = "$end_date[2]-$end_date[1]-$end_date[0]";
		
		
		
		
	
		$data = $this->Closing_forward_model->Select_Buy_Sell($exchange_id , $closing_setlment_id , $start_date , $end_date ); //  $ex_date ,
		echo json_encode($data);
		
		
		
		
		#$this->load->view('process/closing_forward/Ajax_Display_Table' , $data); 
		#$dataa = $this->Closing_forward_model->insert_bs_cf_data($data);	
		#echo "<pre>";
		#print_r($data);
		#echo "</pre>";
		//	$next_open_date = $this->input->post('next_open_date');
		//$closing_rate_type_id = $this->input->post('closing_rate_type_id');
		// echo $ex_date = $this->input->post('ex_date');
		// $data['rec'] = $this->Closing_forward_model->Select_Buy_Sell($exchange_id , $closing_setlment_id , $ex_date);
		
	}
	
	
	
	
	/************************** Second_table *********************************/
	# Second Table Display symbol
	public function Second_table()
	{
		/* 07 - 09 - 2020 */
		$exchange_id = $this->input->post('exchange_id');
		$closing_setlment_id = $this->input->post('closing_setlment_id');
		$opening_setlment_id = $this->input->post('opening_setlment_id');
		
		$start_date = explode('-', $_POST['start_date']);
		$start_date = "$start_date[2]-$start_date[1]-$start_date[0]";
		
		$end_date =  explode('-', $_POST['end_date']);
		$end_date = "$end_date[2]-$end_date[1]-$end_date[0]";
		
		 $data['rec'] = $this->Closing_forward_model->Second_table($exchange_id, $closing_setlment_id, $start_date, $end_date );
		/* 07 - 09 - 2020 */
		
		
		
		// $data['rec'] = $this->Closing_forward_model->Second_table();
		$this->load->view('process/closing_forward/Ajax_Display_Table2' , $data); 
	}

	
	//////////// End Done ///////////////////////
		 
		 
	public function Forward_Remove()
	{
		$next_open_date = explode('-', $_POST['next_open_date']);
		$next_open_date = "$next_open_date[2]-$next_open_date[1]-$next_open_date[0]";
		$data = $this->Closing_forward_model->Forward_Remove($next_open_date);
		echo json_encode($data);
		
	}
	
	
	
	
	
	//************************** insert *********************************//

	#  Insert
	public function Closing_Forward_Key_Chk()
	{
		$exchange_id = $this->input->post('exchange_id');
	    $expiries_id = $this->input->post('expiries_id');
	
		
		if($exchange_id == "63") {
			
	
		$setlement_id =  $this->input->post('opening_setlment_id'); # Settlement id
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
	
		$next_open_date = explode('-', $_POST['next_open_date']);
		 $next_open_date = "$next_open_date[2]-$next_open_date[1]-$next_open_date[0]";
		
	
		$closing_rate_type_id = $this->input->post('closing_rate_type_id');
		// $ex_date = $this->input->post('ex_date');
	
		
		$rate1=array();
		$qty1=array();
		$total=array();

		$rate1 = $this->input->post('rate1[]'); 
		$qty1 = $this->input->post('qty1[]'); 
		 
		$keychk = array();
		
	
		foreach ($rate1 as $key=>$rate11) 
		{	
			
			$total = $rate11 * $qty1[$key];
			
			
			if($total < 0)
			{
				 $buy_sell_id = "1"; // Buy
			}
			else
			{
				  $buy_sell_id = "2";  // sell
			}		
			#die();
			
			// print_r($key);
			$keychk = $this->Closing_forward_model->keychk($key);
			
				foreach($keychk as $row) 
			   {
					 # Selling	
						$keychk = array(	
							 array(
								"expiries_id" => $expiries_id, // 27 dec
								"exchange_id" =>  $row['exchange_id'],
								"setlement_id" =>  $row['setlement_id'],
								"tr_no" => $row['tr_no'],
								"type" => 3, // CF
								"date1" =>  $next_open_date, //$row['date1'],
								"buy_sell_id" =>  $buy_sell_id,  //   sell == 2
								"symbol_id" => $row['symbol_id'],
							//	"instument" => $row['instument'],   // 27-dec
							//	"ex_date" => $row['ex_date'],   // 27-dec
								"qty1" => ($qty1[$key]*-1),
								"rate1" => $rate11,
								"total_amt1" =>($total*-1),
								"pid" => $row['pid'], //  27-dec
							//	"party_code" => $row['party_code'],  27-dec
							//	"party_name" => $row['party_name'],  27-dec
								"bid" => $row['bid'],
							//	"brokrage_code" => $row['brokrage_code'], 27-dec
							//	"brokrage_name" => $row['brokrage_name'], 27-dec
								"re_no" => $row['re_no'],
							//	"remark" => $row['remark'],  27-dec
								"curr_time" => date("Y-m-d H:i:s"),
								"base_on_id" => $row['base_on_id'],
								// "brokrage" => $row['brokrage'],
								 "brokrage" => 0,
								"nxt_day" => $row['nxt_day'],
								"side_id" => $row['side_id'],
							), 
							
							# Buying
							array( 
								"expiries_id" => $expiries_id, // 27 dec
								"exchange_id" =>  $row['exchange_id'],
								"setlement_id" =>  $setlement_id,
								"tr_no" => $row['tr_no'],
								"type" =>  2, // fw
								"date1" => $next_open_date,
								"buy_sell_id" =>$row['buy_sell_id'], // Buy  == 1
								// "buy_sell_id" =>$row['buy_sell_id'], // Buy  == 1
								"symbol_id" => $row['symbol_id'],
							//	"instument" => $row['instument'],  // 27-dec
							//	"ex_date" => $row['ex_date'],  27-dec
								"qty1" => $row['qty1'],
								"rate1" => $rate11,
								"total_amt1" =>$total,					
								"pid" => $row['pid'], //  27-dec
							//	"party_code" => $row['party_code'], 27-dec
							//	"party_name" => $row['party_name'],  27-dec
								"bid" => $row['bid'],
							//	"brokrage_code" => $row['brokrage_code'], 27-dec
							//	"brokrage_name" => $row['brokrage_name'], 27-dec
								"re_no" => $row['re_no'],
							//	"remark" => $row['remark'],   27-dec
								"curr_time" => date("Y-m-d H:i:s"),
								"base_on_id" => $row['base_on_id'],
								 "brokrage" => $row['qty1']*$row['nxt_day']*$rate11/100, 
								// "brokrage" => $row['brokrage'], 
								 "nxt_day" => $row['nxt_day'],
								"side_id" => $row['side_id'], 
							),
						);
					$query_B = $this->db->insert_batch('buy_sell', $keychk );
								 
				}	 
			
			   #print_r($keychk);
			   // $qty1*$rate1*$Intraday/100;	
		}
	}
	else
	{
		echo "else";
		exit;
	}
	
	}
	
	
	public function index()
	{
		$data["Exchange"] = $this->Closing_forward_model->Select_Exchange();
		$data["Expiries"] = $this->Closing_forward_model->Select_Expiries();
        $this->display("index", $data);
	}
	
	
	
	
	public function expiries_date()
	{
		$id = $this->input->post('id');
		$data = $this->Closing_forward_model->expiries_date($id)->result();
		echo json_encode($data);
	}
	
}
		
		
		
		
		
		
		
		
	// added global 
	// public function Exchange_On_Change()
	// {
		// $id = $this->input->post('id');
		// $data = $this->Closing_forward_model->Select_Exchange_On_Change($id)->result();
		// echo json_encode($data);
	// }

/*
	public function UploadDataExcel()
	{
		
		 
			$config['upload_path']   = './upload/'; 
			$config['allowed_types'] = 'csv'; 
			$config['file_name'] =  $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
			if (!$uploadData ) // if ($count == 0) {}  && $count == 0
			{
				echo "Upload Error 3 ";
			}
			else
			{ 
				$uploadData2 = $this->upload->data();
				$FileData = $uploadData2['file_name'];
				$data = file_get_contents(base_url("upload/$FileData"),"r");
				$csv = explode("\n", $data);
				
			
				$count = 0;
				$symbol=array();
				foreach ($csv as $csvData)
				{
					if ($count == 0) {} // 'exchange_id' =>$exchange_id,
					else 
					{
						$row = explode(",", $csvData);
						if($row[4]!="PE" && $row[4]!="CE"  && $row[1]!="" )  // !is_null($row[1]) || $row[1]!="" && 
						{
							// $symbol[$row[1]]=array(
							$symbol[]=array(
							'instrument' =>trim($row[0]),
							'symbol' =>trim($row[1]),
							'Exdt' =>trim($row[2]),
							'closingrate' =>trim($row[8])
							);
						}
					}
					$count++;
				}
				// print_r($symbol);
				echo json_encode($symbol);
				
			}
	}
	*/
	
	
	/* Code Working Excel Upload
	public function UploadDataExcel()
	{
		
		 
			$config['upload_path']   = './upload/'; 
			$config['allowed_types'] = 'csv'; 
			$config['file_name'] =  $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
			if (!$uploadData ) // if ($count == 0) {}  && $count == 0
			{
				echo "Upload Error 3 ";
			}
			else
			{ 
				$uploadData2 = $this->upload->data();
				$FileData = $uploadData2['file_name'];
				$data = file_get_contents(base_url("upload/$FileData"),"r");
				$csv = explode("\n", $data);
				
			
				$count = 0;
				$symbol=array();
				foreach ($csv as $csvData)
				{
					if ($count == 0) {} 
					else 
					{
						$row = explode(",", $csvData);
						if($row[4]!="PE" && $row[4]!="CE"  && $row[1]!="" )  // !is_null($row[1]) || $row[1]!="" && 
						{
							// $symbol[$row[1]]=array(
							$symbol[]=array(
							'instrument' =>trim($row[0]),
							'symbol' =>trim($row[1]),
							'Exdt' =>trim($row[2]),
							'closingrate' =>trim($row[8])
							);
						}
					}
					$count++;
				}
				echo json_encode($symbol);
				
			}
	}
	*/
	
	
	
	
	
	
	
	


