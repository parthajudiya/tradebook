<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";
ini_set('max_input_vars','6000');
 // error_reporting(0);
class Import_excel_mt extends Controller 
{
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('process/Import_Excel_Mt_model');
		
		$this->load->model('global/Global_function_model'); 
	}
	
	public function index()
	{
		$data["Exchange"] = $this->Import_Excel_Mt_model->Select_Exchange();
        $this->display("index", $data);
	}
	
	public function UploadData()
	{
		# POST Data
		$exchange_id = $this->input->post('exchange_id');
		$setlement_id = $this->input->post('setlement_id');
		$type = $this->input->post('type');  # FW CF
		
		  
		######################## Match Data ##############################
			# Qry Match Data 
			
			# Party
			$party_query_id =array();
			$party_query_code =array();
			$party_query_name =array();
			$party_query =  $this->Global_function_model->Sel_Users();
			foreach ($party_query as $key=>$val)
			{
				# First is Data base Var	
				$party_query_id[$val["code"]] = trim($val["aid"]);
				$party_query_code[$val["code"]]= trim($val["code"]); # User code
				$party_query_name[$val["code"]]= trim($val["username"]); # User Name
			}
			
			# Symbol
			$symbol_qry_id =array();
			#$symbol_qry_instrument =array();
			$symbol_qry = $this->Global_function_model->Sel_Symbol();
			foreach ($symbol_qry as $key=>$val)
			{
				$symbol_qry_id[$val["symbol"]]= trim($val["symbol_id"]);
				
			}	
			
		######################## Match Data ##############################
		
			$config['upload_path']   = './assets/upload/importformexcel/'; 
			$config['allowed_types'] = 'csv|xlsx'; 
			$config['file_name'] = $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
		
			if($exchange_id == "63")
			{	
				if (!$uploadData ) {
					echo "Upload Error 5 ";
				}
				else{
						$uploadData2 = $this->upload->data();
						$FileData = $uploadData2['file_name'];
						$data = file_get_contents(base_url("assets/upload/importformexcel/$FileData"),"r");
						$csv = explode("\n", $data);
						$count = 0;
						// $excel_array=array();
						foreach ($csv as $csvData)
						{
							$row = explode(",", $csvData);
							if(!isset($row[5])){  //!=="" - var |      !empty = array
							//print_r($row);
							continue;
							}
							$dmy_time_excel = explode(" ", trim($row[5]));
							$dt = explode(".", $dmy_time_excel[0]);
						
							$script_excel = explode("-", trim($row[8]));
							
							
							// if($num>1000) { }
							# Buy Or Sell
							$Buy_Sell = $row[6];
							if($Buy_Sell == "sell")
							{
								$buy_sell_id = "2";  // sell
							}
							else
							{
								$buy_sell_id = "1"; // buy
							}
							
							
							if(is_numeric($row[9]))
							{
								 $qty_normal = ($buy_sell_id =="2") ? ($row[9])*-1 : $row[9] ;
							}
							else
							{
								$qty_modyfy2 = $row[9];
								$qty_modyfy2 = str_replace("K","",$qty_modyfy2);
								$qty_k_custom = $qty_modyfy2 * 1000;
								$qty_k  =  ($buy_sell_id =="2") ? ($qty_k_custom)*-1 : $qty_k_custom;
							}		
							
							 // if($row[0]!="" ){
							
								$excel_array['abc'][]=array(
									'no'=>$row[0],  
									
									'date_excel'=>$dt[2],   // date_excel
									'month_excel'=>$dt[1], 
									'year_excel'=>$dt[0],  
									
									'ex_date'=>"", 
									
									
									'script_excel'=>$script_excel[0], 
									'qty_excel'=> is_numeric($row[9]) ? $qty_normal : $qty_k,
									'rate_excel'=>$row[10],
									
									'bid'=>"1",
									'broker_code'=>"1111",
									'brokrage_name'=>"HOME",
									
									'Exp_Mt_4'=>$script_excel[1],
									'buy_sell_id'=>$buy_sell_id,
									're_no'=>'0',
									
									'p_code_excel'=>$row[3], 
									
									
									'party_code'=>$row[3], 
									'pid'=>isset($party_query_id[$row[3]])? $party_query_id[$row[3]] : "null",
									'Tr_Party'=>isset($party_query_code[$row[3]]) ? $party_query_code[$row[3]] : "null" , 
									'Tr_Party_Name'=>isset($party_query_name[$row[3]]) ? $party_query_name[$row[3]] : "null" ,
									
									'symbol_id'=>isset($symbol_qry_id[$script_excel[0]]) ? $symbol_qry_id[$script_excel[0]] : "", 
									'Tr_Symbol'=>isset($symbol_qry_id[$script_excel[0]]) ? $script_excel[0] : "null", 
									
									'tr_date' =>$dt[2]."-".$dt[1]."-".$dt[0],
									
								);
							
							// }
						}
						
						/*
						echo "<pre>";
						print_r($excel_array['abc']);
						echo "</pre>";
						exit;
						*/						
						$excel_array['exp_dt'] = $this->Global_function_model->Select_Expirey();
						$this->load->view('process/import_excel_mt/Ajax_Display_Table' , $excel_array);
					
					}
			}
		  
	}
	
	public function SubmitData()
	{
		 $exchange = $_REQUEST['exchange'];
		 $setlment = $_REQUEST['setlment'];
		 $type = $_REQUEST['type'];
		 
		 $Tr_date = explode('-', $_REQUEST['Tr_date']);
		 $Tr_date = "$Tr_date[2]-$Tr_date[1]-$Tr_date[0]";
		 
		$buy_sell_id = $_REQUEST['buy_sell_id'];
		
		 $Symbol_id = (isset($_REQUEST['Symbol_id']) && !empty($_REQUEST['Symbol_id'])) ? $_REQUEST['Symbol_id'] : "ERROR";
		 $instument =  '';
		 $ex_date = (isset($_REQUEST['ex_date']) && !empty($_REQUEST['ex_date'])) ? $_REQUEST['ex_date'] : "ERROR";
		 
		 $qty1 = $_REQUEST['qty1'];
		 $rate1 = $_REQUEST['rate1'];
		 
		 $rate_new = str_replace(" ","",$rate1);
		 
		 $pid = (isset($_REQUEST['pid']) && !empty($_REQUEST['pid'])) ? $_REQUEST['pid'] : "ERROR";
		 $party_code = (isset($_REQUEST['party_code']) && !empty($_REQUEST['party_code'])) ? $_REQUEST['party_code'] : "ERROR";
		 
		 $party_name_new = (isset($_REQUEST['party_name_new']) && !empty($_REQUEST['party_name_new'])) ? $_REQUEST['party_name_new'] : "ERROR";
		 
		 $bid ="1";
		 $brokrage_code ="1111";
		 $brokrage_name ="Home";
		 $re_no ="0";
		 
		 
		  $Insert = array (
			 'exchange_id' => $exchange,
			 'setlement_id' => $setlment,
			 'type' => $type,
			'date1' => $Tr_date,
			'buy_sell_id' =>$buy_sell_id, 
			
			 'symbol_id' => $Symbol_id,
			 'instument' => $instument, 
			 'ex_date' => $ex_date, 
			 
			  'qty1' => $qty1, 
			  'rate1' => $rate_new, 
			  'total_amt1'=>$qty1*$rate_new,
			  
			 'pid' => $pid, 
			 'party_code' => $party_code, 
			 'party_name' => $party_name_new, 
			 
			 
			 'bid' => $bid, 
			 'brokrage_code' => $brokrage_code, 
			 'brokrage_name' => $brokrage_name, 
			 're_no' => $re_no, 
		 );
		  
		//  print_r($Insert['symbol_id']);
		  
		 
		
		  if($Insert['symbol_id']!=="ERROR" && $Insert['pid']!=="ERROR" && $Insert['party_code']!=="ERROR" && $Insert['party_name']!=="ERROR" && $Insert['ex_date']!=="ERROR" ) 		
		  {
				$data = $this->Import_Excel_Mt_model->insert_excel($Insert);
			    echo "1";
				return;
		  }
		  else
		  {
			 
			   echo "2";
			  return; 
		  }	  
		  
		  
		  
	}
	
	
	
}