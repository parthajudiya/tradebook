<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";
// error_reporting(0);
ini_set('max_input_vars','6000');
class Import_form_excel extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('process/Import_form_excel_model');
		
		$this->load->model('global/Global_function_model'); 
	}
	
	public function SubmitData()
	{ 
		
		 $exchange = $_REQUEST['exchange'];
		 $setlment = $_REQUEST['setlment'];
		 $type = $_REQUEST['type'];
		 
		// $buy_sell_id = $_REQUEST['buy_sell_id'];
		 
		 
		 $Tr_date = explode('-', $_REQUEST['Tr_date']);
		 $Tr_date = "$Tr_date[2]-$Tr_date[1]-$Tr_date[0]";
		 
		 
		 $Symbol_id = (isset($_REQUEST['Symbol_id']) && !empty($_REQUEST['Symbol_id'])) ? $_REQUEST['Symbol_id'] : "ERROR";
		 $instument = $_REQUEST['instument'];
		 $ex_date = (isset($_REQUEST['ex_date']) && !empty($_REQUEST['ex_date'])) ? $_REQUEST['ex_date'] : "ERROR";
		 $qty1 = $_REQUEST['qty1'];
		 $rate1 = $_REQUEST['rate1'];
		 $pid = (isset($_REQUEST['pid']) && !empty($_REQUEST['pid'])) ? $_REQUEST['pid'] : "ERROR";
		 $party_code = (isset($_REQUEST['party_code']) && !empty($_REQUEST['party_code'])) ? $_REQUEST['party_code'] : "ERROR";
		 
		 $party_name_new = (isset($_REQUEST['party_name_new']) && !empty($_REQUEST['party_name_new'])) ? $_REQUEST['party_name_new'] : "ERROR";
		 
	//	 $party_name = $_REQUEST['party_name'];
		 
		 $bid ="1";
		 $brokrage_code ="1111";
		 $brokrage_name ="Home";
		 $re_no ="0";
		 
			if($qty1 < 0)
			{
				 $buy_sell_id = "2";  // sell
			}
			else
			{
				  
				  
				  $buy_sell_id = "1"; // Buy
			}		
			
		 
		 
		 $Insert = array (
			 'exchange_id' => $exchange,
			 'setlement_id' => $setlment,
			 'type' => $type,
			 'date1' => $Tr_date,
			 'buy_sell_id' =>$buy_sell_id, 
			
			 'symbol_id' => $Symbol_id,
			 'instument' => $instument, 
			 'ex_date' => $ex_date, 
			 
			  'qty1' => $qty1, 
			  'rate1' => $rate1, 
			  'total_amt1'=> $qty1*$rate1,
			  
			 'pid' => $pid, 
			 'party_code' => $party_code, 
			  'party_name' => $party_name_new, 
			 
			 
			 'bid' => $bid, 
			 'brokrage_code' => $brokrage_code, 
			 'brokrage_name' => $brokrage_name, 
			 're_no' => $re_no, 
		 );
		  
		//  print_r($Insert['symbol_id']);
		  
		 
		
		  if($Insert['symbol_id']!=="ERROR" && $Insert['pid']!=="ERROR" && $Insert['party_code']!=="ERROR" && $Insert['party_name']!=="ERROR" && $Insert['ex_date']!=="ERROR" ) 		
		  {
				$data = $this->Import_form_excel_model->insert_excel($Insert);
			    echo "1";
				return;
		  }
		  else
		  {
			 
			   echo "2";
			  return; 
		  }	  
		 
		 // $data = $this->Import_form_excel_model->insert_excel($Insert);
		  
		
		//  $this = 
		
		//print_r($Insert);
	
			// foreach ($Date as $x) {
			// 	echo $x;
			// }
			// print_r($Tr_Dt);
	
			// echo "<pre>"; print_r($_POST); 
		

	//	echo $Date = $this->input->post('date'); 
		// echo $Script = $this->input->post('Script[]'); 
		// print_r($Script);
		//die();
		// $qty1 = $this->input->post('qty1[]'); 
	}
	
	public function index()
	{
		$data["Exchange"] = $this->Import_form_excel_model->Select_Exchange();
        $this->display("index", $data);
	}
	
	
	/*
	
	*/
	public function UploadData()
	{
			# POST Data
			$exchange_id = $this->input->post('exchange_id');
			$setlement_id = $this->input->post('setlement_id');
			$type = $this->input->post('type');  # FW CF
			
			######################## Match Data ##############################
			# Qry Match Data # Symbol
			$symbol_qry_id =array();
			$symbol_qry = $this->Global_function_model->Sel_Symbol();
			foreach ($symbol_qry as $key=>$val)
			{
				$symbol_qry_id[$val["symbol"]]= trim($val["symbol_id"]);
			}	
			
			# Qry Match Data # Party
			$party_query_id =array();
			$party_query_code =array();
			$party_query_name =array();
			$party_query =  $this->Global_function_model->Sel_Users();
			foreach ($party_query as $key=>$val)
			{
				# First is Data base Var	
				$party_query_id[$val["code"]] = trim($val["aid"]);
				$party_query_code[$val["code"]]= trim($val["code"]); # User Name
				$party_query_name[$val["code"]]= trim($val["username"]); # User Name
			}
			
			# Qry Match Data # Expirey
			$expirey_qry_date =array();
			$expirey_qry_id =array();
			$expirey_qry = $this->Global_function_model->Select_Expirey();
			foreach ($expirey_qry as $key=>$val)
			{
				$expirey_qry_id[$val["ex_date"]]= trim($val["ex_date"]);
				
				$expirey_qry_date[$val["ex_date"]]= trim($val["expiries_id"]);
			}
			echo "<pre>";
			//print_r($expirey_qry_date);
			echo "<pre>";
			// die();
			########################################################################
			
			
			$config['upload_path']   = './assets/upload/importformexcel/'; 
			$config['allowed_types'] = 'csv|xlsx'; 
			$config['file_name'] = $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
			
			if($exchange_id == "63")
			{	
				if (!$uploadData ) {
					echo "Upload Error 5 ";
				}
				else{
					
					$uploadData2 = $this->upload->data();
					$FileData = $uploadData2['file_name'];
					$data = file_get_contents(base_url("assets/upload/importformexcel/$FileData"),"r");
					$csv = explode("\n", $data);
					$count = 0;
					$excel_array=array();
					foreach ($csv as $csvData)
					{
						$row = explode(",", $csvData);
						if($row[0]!="" ){	 
						
								# Date Explode		
								$DATE=explode('/',$row[17]);
								$dtdate = explode(" ", $DATE[2]);
								
								# Expirey Date Explode		
								$Exp_Date = explode('-',$row[4]);
								# 'Date'=>$DATE[0]."-".$DATE[1]."-".$dtdate[0],	
								
								$Tr_Expirey =($Exp_Date[0])."-".($Exp_Date[1])."-".($Exp_Date[2]+2000);
						
								$excel_array['abc'][]=array(
									
									'Date'=>trim($DATE[0]),
									'Month'=>trim($DATE[1]),
									'Year'=>trim($dtdate[0]),
									
									'Exp_Date'=>trim($Exp_Date[0]),
									'Exp_Month'=>trim($Exp_Date[1]),
									'Exp_Year'=>trim($Exp_Date[2]+2000),
									
									'symbol_id'=>isset($symbol_qry_id[$row[7]]) ? $symbol_qry_id[$row[7]] : "", 
									'symbol'=>trim($row[7]),
									
									'buy_sell_id'=>trim($row[12]), 
									'Qty'=>trim($row[13])*(trim($row[12]==1)?1:-1), # QTY  + OR - 
									
									'rate1'=>trim($row[14]),
									# $total = trim($row[13])*trim($row[14])
									'total_amt1'=>trim($row[13])*(trim($row[12]==1)?1:-1)*(trim($row[14])), 
									
									'p_code_excel'=>$row[16], 
									
									
									
									'b_id'=>'1',
									'brokrage_code'=>'1111',
									'brokrage_name'=>'HOME',
									
									'Tr_Dt'=>$DATE[0]."-".$DATE[1]."-".$dtdate[0],
								
									'Tr_Symbol'=>isset($symbol_qry_id[$row[7]]) ? $row[7] : "null", 
									'Tr_Instrument'=>trim($row[3]), 
									
									
									// 'Tr_Expirey'=>$Tr_Expirey, 
									'ex_date'=>isset($expirey_qry_id[$Tr_Expirey]) ? $expirey_qry_id[$Tr_Expirey] : "null",
									'expiries_id'=>$expirey_qry_date,
									
									'pid'=>isset($party_query_id[$row[16]])? $party_query_id[$row[16]] : "null",
									
									'party_code'=>trim($row[16]),
									
									'Tr_Party'=>isset($party_query_code[$row[16]]) ? $party_query_code[$row[16]] : "null" ,
									'Tr_Party_Name'=>isset($party_query_name[$row[16]]) ? $party_query_name[$row[16]] : "null" ,
									
									
									
									
									'Tr_Broker'=>"1111", 
									're_no'=>"0", 
									'exchange_id '=>$exchange_id,
									'setlement_id '=>$setlement_id,
									'type'=>isset($type) ? $type : "1",
								);
							$count++;
						}
					}
					$excel_array['exp_dt'] = $this->Global_function_model->Select_Expirey();
					$this->load->view('process/import_form_excel/Ajax_Display_Table' , $excel_array);
					
					
				}
			}
		}
		
	
		// elseif($exchange_id == "70")
			// {
					// echo $exchange_id;
					
			// }
}

				// echo "<pre>";
					// print_r($excel_array);
					// echo "</pre>";
					//die();
			
				/*
				foreach($symbol as $d){
					$data =	$this->Symbol_model->AddSymbol($d);
				}
				echo $data;
				return $data;
				*/

