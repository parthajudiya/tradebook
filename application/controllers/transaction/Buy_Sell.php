<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";

class Buy_sell extends Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('transaction/Buy_sell_model');
        $this->load->model('global/Global_function_model');
    }

    # Display Index

    public function buy_sell_js_popup() {

        $data['Search_Settlement'] = $this->Global_function_model->Sel_Users();
        $data['Search_User'] = $this->Global_function_model->Sel_Users();
        $data["bro_code"] = $this->Buy_sell_model->select_brocode();
        $data["Count_Variable"] = $this->Buy_sell_model->countRow(); // Count row Qry sssssss
        $data["Exchange"] = $this->Buy_sell_model->Select_Exchange();
        $this->load->view('transaction/buy_sell/buy_sell_js_popup', $data);
    }

    # VIEW OF TABLE

    public function Ajax_View_Table() {
        $data['buy_sell'] = $this->Buy_sell_model->view();

        $this->load->view('transaction/buy_sell/Ajax_Display_Table', $data);
    }

    # Search 
    /*
      public function search_record($From_Date = null , $To_Date = null , $Type_Search = null , $Client_Code_Search = null)
      {

      $From_Date = explode('-', $_POST['From_Date']);
      $From_Date = "$From_Date[2]-$From_Date[1]-$From_Date[0]";

      $To_Date = explode('-', $_POST['To_Date']);
      $To_Date = "$To_Date[2]-$To_Date[1]-$To_Date[0]";

      $Type_Search  = $this->input->post('Type_Search');

      $Client_Code_Search  = $this->input->post('Client_Code_Search');

      $data['buy_sell'] = $this->Buy_sell_model->search_record($From_Date , $To_Date , $Type_Search , $Client_Code_Search);
      $this->load->view('transaction/buy_sell/Ajax_Display_Table' ,$data);

      }
     */

    public function Edit($id) {
        $BuySellEdit = $this->Buy_sell_model->Edit($id);
        echo json_encode($BuySellEdit);
    }

    /*
      public function Bs_Popup()
      {

      $id = $_REQUEST['id'];
      $data['datas'] = $this->Buy_sell_model->Edit($id);
      $data["Exchange"] = $this->Buy_sell_model->Select_Exchange();
      $data["Settlement"] = $this->Buy_sell_model->Sel_Settlement();
      $data["Symbol"] = $this->Buy_sell_model->Sel_Symbol();
      $data["Expiries"] = $this->Buy_sell_model->Sel_Exp();
      $this->load->view('transaction/buy_sell/buy_sell_new', $data)
      }
     */
    # JOIN 2 nd Droup Down ==> setlement

    public function setlement() {
        $id = $this->input->post('id');
        $data = $this->Buy_sell_model->Select_Setlement($id)->result();
        echo json_encode($data);
    }

    # Symbol Search base on Exchange id 

    public function Symbol_Search() {
        $id = $this->input->post('id');
        $data = $this->Buy_sell_model->Select_Symbol($id)->result();
        echo json_encode($data);
    }

    # Add     ,  party_code  , party_name  , brokrage_code  , brokrage_name

    public function Add() {
        $date1 = explode('-', $_POST['date1']);
        $date1 = "$date1[2]-$date1[1]-$date1[0]";

        $data = array(
            "exchange_id" => $this->input->post("exchange_id"),
            "setlement_id" => $this->input->post("setlement_id"),
            #"tr_no"=>$this->input->post("tr_no"),
            "type" => $this->input->post("type"),
            "date1" => $date1,
            "buy_sell_id" => $this->input->post("buy_sell_id"),
            "symbol_id" => $this->input->post("symbol_id"),
            // "instument"=>$this->input->post("instument"),
            // "ex_date"=>$this->input->post("ex_date"), // 15-dec
            "expiries_id" => $this->input->post("expiries_id"),
            "qty1" => $this->input->post("qty1"),
            "rate1" => $this->input->post("rate1"),
            "pid" => $this->input->post("pid"),
            //	"party_code"=>$this->input->post("party_code"), // 27-dec
            //	"party_name"=>$this->input->post("party_name"),  // 27-dec
            "bid" => $this->input->post("bid"),
            // 	"brokrage_code"=>$this->input->post("brokrage_code"),  // 27-dec
            //	"brokrage_name"=>$this->input->post("brokrage_name"),  // 27-dec
            "re_no" => $this->input->post("re_no"),
                #"remark"=>$this->input->post("remark"),
        );
        #print_r($data);
        $party_code = $this->input->post("party_code");
        $party_name = $this->input->post("party_name");
        $symbol_id = $this->input->post("symbol_id"); // not need
        $exchange_id = $this->input->post("exchange_id");
        $Chk_Qry_Brokrage = $this->Buy_sell_model->Chk_Qry_Brokrage($party_code, $party_name, $symbol_id, $exchange_id);

        # Added Brokrage In Users
        if ($Chk_Qry_Brokrage) {
            # echo "yes ";
            # die();
            foreach ($Chk_Qry_Brokrage as $rows) {
                # print_r($Chk_Qry_Brokrage);

                $Intraday = $rows['intraday'];
                $base_on_id = $rows['base_on_id'];
                $side_id = $rows['side_id'];
                $nxt_day = $rows['nxt_day'];

                $brokrage_id = $rows['bid'] ? $rows['bid'] : "0";  // brokrage table id
            }

            $qty1 = $this->input->post("qty1");
            $rate1 = $this->input->post("rate1");
            $Intraday;
            $nxt_day;
            # die();
            $finalbrokrage = $qty1 * $rate1 * $Intraday / 100;
            # echo $base_on_id;
            # echo $Intraday;
            # echo $side_id;
            # echo $nxt_day;

            $data_3 = array(
                "nxt_day" => $nxt_day, # brokrage == $Intraday
                "brokrage" => abs($finalbrokrage), # brokrage == $Intraday
                "base_on_id" => $base_on_id,
                "side_id" => $side_id,
                "brokrage_id" => empty($brokrage_id) ? 0 : $brokrage_id
            );

            # print_r($data_3);
            # die();

            $data = $this->Buy_sell_model->add($data, $data_3);
            echo $data;
            return $data;
        }

        # Do Not add Brokrage Because it's Friend
        else {
            # echo "no";
            # die();

            $data_3 = array(
                "brokrage" => "0", // brokrage == $Intraday
                "base_on_id" => "0",
                "side_id" => "0",
                "nxt_day" => "0",
                "brokrage_id" => empty($brokrage_id) ? 0 : $brokrage_id
            );

            $data = $this->Buy_sell_model->add_without_brokrage($data, $data_3);
            echo $data;
            return $data;
        }
    }

    public function Update() {
        $date1 = explode('-', $_POST['date1']);
        $date1 = "$date1[2]-$date1[1]-$date1[0]";

        $data = array(
            "exchange_id" => $this->input->post("exchange_id"),
            "setlement_id" => $this->input->post("setlement_id"),
            //	"tr_no"=>$this->input->post("tr_no"),
            "type" => $this->input->post("type"),
            "date1" => $date1,
            "buy_sell_id" => $this->input->post("buy_sell_id"),
            "symbol_id" => $this->input->post("symbol_id"),
            //	"instument"=>$this->input->post("instument"),
            // "ex_date"=>$this->input->post("ex_date"), // 15-dec
            "expiries_id" => $this->input->post("expiries_id"),
            "qty1" => $this->input->post("qty1"),
            "rate1" => $this->input->post("rate1"),
            "pid" => $this->input->post("pid"),
            //	"party_code"=>$this->input->post("party_code"),
            //	"party_name"=>$this->input->post("party_name"),
            "bid" => $this->input->post("bid"),
            //	"brokrage_code"=>$this->input->post("brokrage_code"),
            //	"brokrage_name"=>$this->input->post("brokrage_name"),
            "re_no" => $this->input->post("re_no"),
                //	"remark"=>$this->input->post("remark"),
        );
        $buysell_id = $this->input->post("buysell_id"); // Update Id 
        $party_code = $this->input->post("party_code");
        $party_name = $this->input->post("party_name");
        $symbol_id = $this->input->post("symbol_id"); // not need
        $exchange_id = $this->input->post("exchange_id");
        $Chk_Qry_Brokrage = $this->Buy_sell_model->Chk_Qry_Brokrage($party_code, $party_name, $symbol_id, $exchange_id);

        # Added Brokrage In Users
        if ($Chk_Qry_Brokrage) {
            # echo "yes ";
            # die();
            foreach ($Chk_Qry_Brokrage as $rows) {
                # print_r($Chk_Qry_Brokrage);

                $Intraday = $rows['intraday'];
                $base_on_id = $rows['base_on_id'];
                $side_id = $rows['side_id'];
                $nxt_day = $rows['nxt_day'];

                // $brokrage_id =  $rows['bid'];  // brokrage table id
                $brokrage_id = $rows['bid'] ? $rows['bid'] : "0";
            }

            $qty1 = $this->input->post("qty1");
            $rate1 = $this->input->post("rate1");
            $Intraday;
            $nxt_day;
            # die();
            $finalbrokrage = $qty1 * $rate1 * $Intraday / 100;
            # echo $base_on_id;
            # echo $Intraday;
            # echo $side_id;
            # echo $nxt_day;

            $data_3 = array(
                "nxt_day" => $nxt_day, // brokrage == $Intraday
                "brokrage" => abs($finalbrokrage), // brokrage == $Intraday
                "base_on_id" => $base_on_id,
                "side_id" => $side_id,
                // "brokrage_id"=>$brokrage_id  // brokrage table id
                "brokrage_id" => empty($brokrage_id) ? 0 : $brokrage_id
            );

            # print_r($data_3);
            # die();

            $data = $this->Buy_sell_model->update($data, $data_3, $buysell_id);
            echo $data;
            return $data;
        }

        # Do Not add Brokrage Because it's Friend
        else {
            # echo "no";
            # die();

            $data_3 = array(
                "brokrage" => "0", // brokrage == $Intraday
                "base_on_id" => "0",
                "side_id" => "0",
                "nxt_day" => "0",
                // "brokrage_id"=>$brokrage_id  // brokrage table id
                "brokrage_id" => empty($brokrage_id) ? 0 : $brokrage_id
            );

            $data = $this->Buy_sell_model->update_without_brokrage($data, $data_3, $buysell_id);
            echo $data;
            return $data;
        }
        /*
          echo "<pre>";
          print_r($data);
          echo "</pre>";
         */
    }

    // For Autocompleted Search Account Name // Party Name
    public function Search_Account_Name() {
        $postData = $this->input->post();
        $data = $this->Buy_sell_model->Search_Account_Name($postData);
        echo json_encode($data);
    }

    // For Autocompleted Search Account Code // Party Cdde
    public function Search_Account_Code() {

        $postData = $this->input->post();
        $data = $this->Buy_sell_model->Search_Account_Code($postData);
        echo json_encode($data);
    }

    /*     * *********************** *************** */

    public function Search_Broker_Name() {
        $postData = $this->input->post();
        $data = $this->Buy_sell_model->Search_Broker_Name($postData);
        echo json_encode($data);
    }

    public function Search_Brokrage_Code() {

        $postData = $this->input->post();
        $data = $this->Buy_sell_model->Search_Brokrage_Code($postData);
        echo json_encode($data);
    }

    # Symbol_OnChange and Expires Result

    public function Symbol_OnChange() {
        $id = $this->input->post('id');
        $exchange_id = $this->input->post('exchange_id');
        $data = $this->Buy_sell_model->Symbol_OnChange($id, $exchange_id)->result();
        if (empty($data)) {
            $data = $this->Buy_sell_model->Symbol_OnChange(0, $exchange_id)->result();
        }
        echo json_encode($data);
    }

    #DELETE

    public function Delete($id) {
        $id = $this->input->post("id");
        $data = $this->Buy_sell_model->delete($id);
        if ($result = true) {
            redirect(base_url() . "transaction/Buy_sell", "refresh");
        }
    }

    # DELETE BULK

    public function Delete_Bulk() {
        $id = $this->input->post('id'); # This 2 id comes in jq
        $this->Buy_sell_model->delete_bulk($id);  # This id comes in jq
        if ($result = true) {
            redirect(base_url() . "transaction/Buy_sell", "refresh");
        }
    }

    public function singlerecord() {
        $data = $this->Buy_sell_model->singlerecord();
        echo json_encode($data);
        #$this->load->view('transaction/buy_sell/singlerecord' ,$data);
    }

    public function party_id_search() {
        $pid = $this->input->post('pid');
        $data = $this->Buy_sell_model->party_id_search($pid);
        echo json_encode($data);
    }

    public function broker_id_search() {
        $bid = $this->input->post('bid');
        $data = $this->Buy_sell_model->broker_id_search($bid);
        echo json_encode($data);
    }

    public function party_code_search() {
        $code = $_REQUEST["code"];
        $data = $this->Buy_sell_model->party_code_search($code);
        echo json_encode($data);
    }

    public function brokrage_code_search() {
        $code = $_REQUEST["code"];
        $data = $this->Buy_sell_model->brokrage_code_search($code);
        echo json_encode($data);
    }

    public function chk_one() {
        $id = $_REQUEST["id"];
        $chk_one = $_REQUEST["chk_one"];
        $data = $this->Buy_sell_model->chk_one($id, $chk_one);
    }

    public function chk_two() {
        $id = $_REQUEST["id"];
        $chk_two = $_REQUEST["chk_two"];
        $data = $this->Buy_sell_model->chk_two($id, $chk_two);
    }

}
