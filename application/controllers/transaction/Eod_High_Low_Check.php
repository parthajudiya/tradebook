<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";
error_reporting(0);
// ini_set('max_execution_time', 5000);
class Eod_high_low_check extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('transaction/Eod_high_low_check_model');
		 $this->load->helper('url', 'form');
	}
	
	public function index()
	{
		 $data["Exchange"] = $this->Eod_high_low_check_model->Select_Exchange(); 
		 $this->display("index" , $data );
	}
	
	 # JOIN 2 nd Droup Down ==> setlement
	public function setlement()
	{
		$id = $this->input->post('id');
		$data = $this->Eod_high_low_check_model->Select_Setlement($id)->result();
		echo json_encode($data);
	}
	
	
	// NMDC demo working Data =  fo- 14-01-2019 To 18-01-2019
	// INSTRUMENT panding 
	public function upload()
	{
	     $config['upload_path']   = './temp/'; 
         $config['allowed_types'] = 'csv'; 
		 $config['file_name'] = $_FILES['picture']['name'];
		 $this->load->library('upload', $config);
		 $uploadData = $this->upload->do_upload('picture');
		 if ( !  $uploadData )
		 {
			 echo "err";
         }
		 else
		 { 
				$uploadData2 = $this->upload->data();
                $picture = $uploadData2['file_name'];
				$dataA = file_get_contents(base_url("temp/$picture"),"r");
				#echo base_url("temp/$picture");
				#$dataA = file_get_contents("http://localhost/tradebook/fo14JAN2019bhav.csv","r");
				#print_r($dataA);
         } 
	}
	
	
	
	public function readCsv()
	{
		# POST From This Form
	 	$sid_HL_Form = $this->input->post("setlement_id"); 
		$AsOnDate1 = explode('-', $this->input->post("AsOnDate"));
	    $AsOnDate1 = "$AsOnDate1[2]-$AsOnDate1[1]-$AsOnDate1[0]";
		



		$config['upload_path']   = './temp/'; 
         $config['allowed_types'] = 'csv'; 
		 $config['file_name'] = $_FILES['picture']['name'];
		 $this->load->library('upload', $config);
		 $uploadData = $this->upload->do_upload('picture');
		 if ( !  $uploadData )
		 {
			 echo "err";
         }
		 else
		 { 
				$uploadData2 = $this->upload->data();
                $picture = $uploadData2['file_name'];
				$dataA = file_get_contents(base_url("temp/$picture"),"r");
				#echo base_url("temp/$picture");
				#$dataA = file_get_contents("http://localhost/tradebook/fo14JAN2019bhav.csv","r");
				#$dataA = file_get_contents("http://localhost/tradebook/fo14JAN2019bhav.csv","r");
				# print_r($dataA);
         
		
			
		
		
		$csv = explode("\n", $dataA);
		$count = 0;
		$symbol = array();
		$symbol_Notmatch = array();
		foreach ($csv as $csvData)
		{
			if ($count == 0){}
			else 
			{
				$row = explode(",", $csvData);
				if (isset($row[1]) && $row[6]!="0" && $row[7]!="0" ) 
				{
					$symbol[] = array(
						// "INSTRUMENT"=>trim($row[0]),
						//"INSTRUMENT"=>"FUTSTK",
						"OPTION_TYP"=>trim($row[4]),
						"Symbol"=>trim($row[1]),
						"Open"=>trim($row[5]),
						"Close"=>trim($row[8]),
						"HIGH"=>trim($row[6]),
						"Low"=>trim($row[7]),
						"EXPIRY_DT"=>trim($row[2]),
						"TIMESTAMP"=>trim($row[14])
					);
				}
				else
				{
					#	echo "else";
				}
			}
			$count++;
		}
		# print_r($symbol);
		
		
		# Select Buy_sell # POST DATA Where Query
		$dataB = $this->Eod_high_low_check_model->Buy_Sell_Chk_Fo($sid_HL_Form , $AsOnDate1);
		 # print_r($dataB);
		 # die();
		foreach($dataB as $loop)
		{
			# print_r($loop);
			# This Data is 
			$tr_no  =  $loop->tr_no;
			$EXPIRY_DT  =  $loop->ex_date;
			$Symbol  =  $loop->symbol;
			//$INSTRUMENT = "FUTSTK"; panding for coding
			$OPTION_TYP = "XX";
			$Rate =$loop->rate1; # We inserted Rate In Buy_Sell
			$HIGH =$loop->rate1;
			$Low =$loop->rate1;
			
			$setlement_id = $loop->setlement_id; 
			
			$date1 = explode('-', $loop->date1);
			$date1 = "$date1[2]-$date1[1]-$date1[0]";
			
			foreach($symbol as $disp)
			{
				#	echo "if";
				if($disp['EXPIRY_DT'] == $EXPIRY_DT && $disp['Symbol'] == $Symbol && $disp['OPTION_TYP'] == $OPTION_TYP && !($disp['Low'] <= $Low && $disp['HIGH'] >= $HIGH) ) // && $setlement_id == $sid_HL_Form && $date1 == $AsOnDate
				{
				
						$data = array(
						"INSTRUMENT"=> "FUTSTK",
						"Symbol"=> $Symbol,
						"EXPIRY_DT"=> $EXPIRY_DT,
						"Low"=> $disp['Low'],
						"HIGH"=> $disp['HIGH'],
						"Rate"=> $Rate,
						"tr_no"=>$tr_no
					);
				}	
				else{}		
			}
			$data['mydata'] = $data;
			
			 if($data['mydata'])
			 {
					$this->load->view('transaction/eod_high_low_check/Ajax_Display_Table',$data);
			 }	 
			 else
			 {
				 $this->load->view('transaction/eod_high_low_check/Ajax_Display_Table');
				 # echo "NOTFOUND";
			 }
		}
		
	  }	
	}	
}		
		
		
		
		
		
		
		
		
		
		//   [expiries_id] => 70
		// ex_date
		
		// print_r($dataB['Buy_Sell']);
		
		//echo $EXPIRY_DT; 
		#die();
		
		// $INSTRUMENT = "FUTSTK";
		// $Symbol = "NMDC";
		// $EXPIRY_DT = "31-Jan-2019";
		// $Low = "1";
		// $HIGH = "1";
		
			/*
		foreach($symbol as $disp)
		{
				
			
			if ($disp['INSTRUMENT'] == $INSTRUMENT && $disp['Symbol'] == $Symbol && $disp['EXPIRY_DT'] == $EXPIRY_DT  && !($disp['Low'] <= $Low && $disp['HIGH'] >= $HIGH))
			{
					#echo "Not Match";
				
				
					$data = array(
						"INSTRUMENT"=> $INSTRUMENT,
						"Symbol"=> $Symbol,
						"EXPIRY_DT"=> $EXPIRY_DT,
						"Low"=> $Low,
						"HIGH"=> $HIGH,
					);
				# print_r($symbol_Notmatch);
			}
			else {}
		}

		 $data['mydata'] = $data;
		
		 if($data['mydata'])
		 {
				$this->load->view('transaction/eod_high_low_check/Ajax_Display_Table',$data);
		 }	 
		 else
		 {
			 echo "NOTFOUND";
		 }	
			*/


			/*
			if($disp['INSTRUMENT'] == $INSTRUMENT && $disp['Symbol'] == $Symbol && $disp['EXPIRY_DT'] == $EXPIRY_DT  && $disp['Low'] <= $Low && $disp['HIGH'] >= $HIGH)  
			{
				print_r($disp);
			}
			*/
		 
		
		
	

	//	return $data;
		// print_r($data);
		// $array['Eod_HL_CHK'] = $this->Eod_high_low_check_model->Eod_HL_Chk_Fo($data);
//echo json_encode($data);
		# print_r($symbol_Notmatch);

// if(!($x >= $y)) echo "True";

/*
		foreach($symbol as $disp)
		{
			if($disp['INSTRUMENT'] == $INSTRUMENT && $disp['Symbol'] == $Symbol && $disp['EXPIRY_DT'] == $EXPIRY_DT  && $disp['Low'] <= $Low && $disp['HIGH'] >= $HIGH)  
			{
				print_r($disp);
			}	
			else if ($disp['INSTRUMENT'] == $INSTRUMENT && $disp['Symbol'] == $Symbol && $disp['EXPIRY_DT'] == $EXPIRY_DT  && !($disp['Low'] <= $Low && $disp['HIGH'] >= $HIGH))
			{
					#echo "not";
				
				
					$symbol_Notmatch = array(
						"INSTRUMENT"=> $INSTRUMENT,
						"Symbol"=> $Symbol,
						"EXPIRY_DT"=> $EXPIRY_DT,
						"Low"=> $Low,
						"HIGH"=> $HIGH,
				);
				print_r($symbol_Notmatch);
			}
			else
			{
				 echo "";
			}
		}
*/
/*
		$tmpName = $_FILES['files']['tmp_name'];
		$csvAsArray = array_map('str_getcsv', file($tmpName));
		$csv = explode("\n", $csvAsArray);
		$count = 0;
		*/