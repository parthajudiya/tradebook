<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";

// error_reporting(0);

class Stock_position extends Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('reports/Stock_position_model');
    }

    public function insert_ltp() {
        $symbol_id_glob = $this->input->post('symbol_id_glob');
        $ltp_global = $this->input->post('ltp_global');
        $insert = array(
            'exchange_id' => $this->input->post('exchange_id'),
            'setlement_id' => $this->input->post('setlement_id')
        );


        foreach ($symbol_id_glob as $key => $value) {
            $temp = array();
            $temp = $insert + ["symbol_id_glob" => $symbol_id_glob[$key], "ltp" => $ltp_global[$key]];
            #print_r($temp);#
            $data = $this->Stock_position_model->insert_ltp($temp);
        }
    }

    /* GET LTP IN MONEY Controller  IN PUT DATA Table 2 */

    public function GetData_Online() {

        $data_array = array();
        $array = array();

        $array = array(
            'Symbol' => $this->input->post("syname_glob"),
            //'Instrument' =>'FUTSTK',
            // 'Instrument' => $this->input->post("ins_glob"),
            'Expirey' => $this->input->post("exdt_glob")
        );

        # Count Arry post data and check how much types of loop
        $syname_glob = count($this->input->post("syname_glob"));
        for ($i = 0; $i < $syname_glob; $i++) {
            # echo $i;


            $Symbol_Code = $array['Symbol'][$i];
            // $SY_TYPE = $array['Instrument'][$i];
            $SY_EXP = $array['Expirey'][$i];





            $url = "https://www.moneycontrol.com/mc/widget/fnoquote/updateFnOTable?classic=true&sc_did=$Symbol_Code&instType=FUTSTK&optiontype=&expDate=$SY_EXP&strikeprice=Select&pricecurr=1";



            $cURLConnection = curl_init();
            curl_setopt($cURLConnection, CURLOPT_URL, $url);
            curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
            $phoneList = curl_exec($cURLConnection);
            $error = curl_error($cURLConnection);
            curl_close($cURLConnection);
            print_r($error);
            $response = json_decode($phoneList);


            $lastprice = $response->fno_list->item[0]->lastprice;
            $expiry_date = $response->fno_list->item[0]->expiry_date;
            $fno_symbol = $response->fno_list->item[0]->fno_symbol;
            $fno_instrument = $response->fno_list->item[0]->fno_instrument;

            $data_array[] = array(
                'Passing_Symbol_Code' => $Symbol_Code,
                'instrument' => $fno_instrument,
                'symbol' => $fno_symbol,
                'expiry_date' => $expiry_date,
                'lastprice' => $lastprice,
            );

            /* $insert = array(
              'symbol_id'=>$this->input->post('symbol_id_glob'),
              'ltp' =>$lastprice,
              'exchange_id' =>63,
              'setlement_id' =>$this->input->post('setlement_id')
              );
              $data = $this->Stock_position_model->insert_ltp($insert); */
        }


        echo json_encode($data_array);
    }

    public function Table1_Print() {

        // For Table 
        $arr1DtA = explode('-', $_POST['arr1Dt']);
        $data["arr1DtA"] = "$arr1DtA[0]-$arr1DtA[1]-$arr1DtA[2]";

        $arr1DtB = explode('-', $_POST['arr1Dt2']);
        $data["arr1DtB"] = "$arr1DtB[0]-$arr1DtB[1]-$arr1DtB[2]";

        $data["Ex"] = $this->input->post('exchange_id');
        // For Table 

        $exchange_id = $this->input->post('exchange_id');
        $setlement_id = $this->input->post('setlement_id');
        $party_code = $this->input->post('party_code');

        # SD 
        $arr1Dt = explode('-', $_POST['arr1Dt']);
        $arr1Dt = "$arr1Dt[2]-$arr1Dt[1]-$arr1Dt[0]";

        # ED
        $arr1Dt2 = explode('-', $_POST['arr1Dt2']);
        $arr1Dt2 = "$arr1Dt2[2]-$arr1Dt2[1]-$arr1Dt2[0]";

        if ("" == trim($party_code)) { # POST DATA IS NULL
            $data['rec'] = $this->Stock_position_model->load_first($exchange_id, $setlement_id, $arr1Dt, $arr1Dt2);
        } else {
            $data['rec'] = $this->Stock_position_model->load_first_code($exchange_id, $setlement_id, $party_code, $arr1Dt, $arr1Dt2); // $start_date, $arr1Dt2,
        }
        $data = $this->load->view('reports/Stock_position/Table1_Print', $data, TRUE);
        echo $data;
        // return  $data;
    }

    public function Table2_Print() {
        $id = $this->input->post('id');
        if (!empty($id)) {
            # On Change Table DBL CLICK TABLE
            $data['rec'] = $this->Stock_position_model->onchange_second($id);
            $data = $this->load->view('reports/Stock_position/Table2_Print', $data, TRUE);
        } else {
            $data['rec'] = $this->Stock_position_model->load_second();
            $data = $this->load->view('reports/Stock_position/Table2_Print', $data, TRUE);
        }
        echo $data;
    }

    # Table 4

    public function Current_Symbol() {
        /* For Table */
        $arr1DtA = explode('-', $_POST['arr1Dt']);
        $data["arr1DtA"] = "$arr1DtA[0]-$arr1DtA[1]-$arr1DtA[2]";
        $arr1DtB = explode('-', $_POST['arr1Dt2']);
        $data["arr1DtB"] = "$arr1DtB[0]-$arr1DtB[1]-$arr1DtB[2]";
        $data["Ex"] = $this->input->post('exchange_id');
        /* For Table */

        $id = $this->input->post('Symbol_id_Global');
        $data['rec'] = $this->Stock_position_model->onchange_four($id);
        $data = $this->load->view('reports/Stock_position/Current_Symbol', $data, TRUE);
        echo $data;
    }

    # Table 4

    public function Summary() {
        /* For Table */
        $arr1DtA = explode('-', $_POST['arr1Dt']);
        $data["arr1DtA"] = "$arr1DtA[0]-$arr1DtA[1]-$arr1DtA[2]";

        $arr1DtB = explode('-', $_POST['arr1Dt2']);
        $data["arr1DtB"] = "$arr1DtB[0]-$arr1DtB[1]-$arr1DtB[2]";

        $data["Ex"] = $this->input->post('exchange_id');
        /* For Table */
        // $id = $this->input->post('Symbol_id_Global');
        $data['rec'] = $this->Stock_position_model->Summary();
        $data = $this->load->view('reports/Stock_position/Summary', $data, TRUE);
        echo $data;
    }

    public function Print_Table_2_Excel() {

        $arr1DtA = explode('-', $_POST['arr1DtA']);
        $arr1DtA = "$arr1DtA[2]-$arr1DtA[1]-$arr1DtA[0]";

        $arr1DtB = explode('-', $_POST['arr1DtB']);
        $arr1DtB = "$arr1DtB[2]-$arr1DtB[1]-$arr1DtB[0]";

        $exchange_id = $this->input->post('exchange_id');
        $setlement_id = $this->input->post('setlement_id');


        $data['strSQL'] = $this->Stock_position_model->Print_Table_2_Excel($arr1DtA, $arr1DtB, $exchange_id, $setlement_id);
        # echo "<pre>";
        # print_r($data);
        # echo "<pre>";
        # die();



        $arr2 = array("Partty");
        $arr = array("Symbol", "Expiry", "Stock", "Ltp", "Average", "Profit/Loss", "M2M", "NET", "DiffrentWB");
        $final = array();
        $arrData = array();
        array_push($final, $arr, $arr2);

        foreach ($data['strSQL'] as $Result) {




            #LTP  ***
            if ($Result->stock > 0) {
                $LTP = 0; #
            } else if ($Result->stock == 0) {

                $LTP = 0;
            } else {
                $LTP = 0;  #
            }



            # Avarage
            if ($Result->stock > 0) {
                $Avarage = $Result->TotalBuyRs / $Result->stock;
            } else if ($Result->stock == 0) {
                $Avarage = "0";
            } else {
                $Avarage = $Result->TotalSellRs / abs($Result->stock);
            }

            # Profit/Loss ***
            if ($Result->stock > 0) {
                $profitLoss_With_Brokrage = "0"; #
            } else if ($Result->stock == 0) {
                $brokrage = $Result->brokrage;
                $profitLoss = $Result->TotalSellRs - $Result->TotalBuyRs;
                $profitLoss_With_Brokrage = $profitLoss - $brokrage;
            } else {
                $profitLoss_With_Brokrage = "0"; #
            }

            #M2M
            if ($Result->stock > 0) {
                $brokrage_Z = $Result->brokrage;
                $buy_sell_With_Brokrage = $Result->TotalBuyRs + $brokrage_Z;
                $M2M = $buy_sell_With_Brokrage * -1;
            } else if ($Result->stock == 0) {
                $M2M = 0;
            } else {
                $brokrage_Z = $Result->brokrage;
                $buy_sell_With_Brokrage = $Result->TotalSellRs - $brokrage_Z;
                $M2M = $buy_sell_With_Brokrage;
            }

            #NET
            if ($Result->stock > 0) {
                $brokrage_Z = $Result->brokrage;
                $buy_sell_With_Brokrage = $Result->TotalBuyRs + $brokrage_Z;
                $NET = $buy_sell_With_Brokrage * -1;
            } else if ($Result->stock == 0) {
                $brokrage = $Result->brokrage;
                $profitLoss = $Result->TotalSellRs - $Result->TotalBuyRs;
                $profitLoss_With_Brokrage = $profitLoss - $brokrage;
                $NET = $profitLoss_With_Brokrage;
            } else {
                $brokrage_Z = $Result->brokrage;
                $buy_sell_With_Brokrage = $Result->TotalSellRs - $brokrage_Z;
                $NET = $buy_sell_With_Brokrage;
            }

            #Diffrent-WB

            if ($Result->stock > 0) {
                $WithoutBrokrage = $Result->TotalBuyRs / $Result->stock;
                $DWB = $WithoutBrokrage * -1;
            } else if ($Result->stock == 0) {
                $DWB = $Result->TotalSellRs - $Result->TotalBuyRs;
            } else {
                $WithoutBrokrage = $Result->TotalSellRs / abs($Result->stock);
                $DWB = $WithoutBrokrage;
            }

            $arrData2 = array($Result->party_name);
            $arrData = array($Result->symbol, $Result->ex_date, $Result->stock, $LTP, $Avarage, $profitLoss_With_Brokrage, $M2M, $NET, $DWB);
            # echo "<pre>";
            # print_r($arrData);
            # echo "</pre>";
            array_push($final, $arrData, $arrData2);
            $k++;
        }
        $final_data = "";
        $tempNo = 0;
        $sep = "\t";

        # Loop Data In Excel
        foreach ($final as $v) {
            $schema_insert = "";
            foreach ($v as $v1) {
                $schema_insert .= $v1 . $sep;
            }

            $schema_insert = str_replace($sep . "$", "", $schema_insert);
            $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
            $schema_insert .= "\t";

            $final_data .= trim($schema_insert);
            $final_data .= "\n";

            if ($tempNo == 0) {
                $final_data .= "\n";
            }
            $tempNo++;
        }
        # $final_data;


        $file = "stock_position.xls";
        file_put_contents("$file", $final_data);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        readfile($file);
    }

    /* Print End */

    public function index() {
        $data["Exchange"] = $this->Stock_position_model->Select_Exchange();
        $this->display("index", $data);
    }

    // glob added
    public function Exchange_On_Change() {
        $id = $this->input->post('id');
        $data = $this->Stock_position_model->Select_Exchange_On_Change($id)->result();
        echo json_encode($data);
    }

    // DATE 2-9-2020 start
    /*
      public function party_code_search() {
      $code = $_REQUEST["code"];
      $data = $this->Stock_position_model->party_code_search($code);
      echo json_encode($data);
      }

      public function Search_Account_Code() {

      $postData = $this->input->post();
      $data = $this->Stock_position_model->Search_Account_Code($postData);
      echo json_encode($data);
      }
     */

    # Table 1
    public function ResultTable() {

        // $party_code = $this->input->post('party_code'); // 28-dec
        $aid = $this->input->post('aid'); // 28-dec

        $exchange_id = $this->input->post('exchange_id');
        $setlement_id = $this->input->post('setlement_id');


        # SD 
        $arr1Dt = explode('-', $_POST['arr1Dt']);
        $arr1Dt = "$arr1Dt[2]-$arr1Dt[1]-$arr1Dt[0]";
        # ED
        $arr1Dt2 = explode('-', $_POST['arr1Dt2']);
        $arr1Dt2 = "$arr1Dt2[2]-$arr1Dt2[1]-$arr1Dt2[0]";

        $data['rec'] = $this->Stock_position_model->load_first($exchange_id, $setlement_id, $arr1Dt, $arr1Dt2, $aid); // party_code
        $this->load->view('reports/Stock_position/Ajax_Display_Table1', $data);
    }

    public function load_second($id = null) {

        // echo $id;
        $id = $this->input->post('id'); // on click Client ID
        // $party_code = $this->input->post('party_code');
        //exit;
        /*
          $exchange_id = $this->input->post('exchange_id');
          $setlement_id = $this->input->post('setlement_id');
          # SD
          $arr1Dt = explode('-', $_POST['arr1Dt']);
          $arr1Dt = "$arr1Dt[2]-$arr1Dt[1]-$arr1Dt[0]";
          # ED
          $arr1Dt2 = explode('-', $_POST['arr1Dt2']);
          $arr1Dt2 = "$arr1Dt2[2]-$arr1Dt2[1]-$arr1Dt2[0]";
         */


        # On Change Table DBL CLICK TABLE
        $data['rec'] = $this->Stock_position_model->onchange_second($id);
        $this->load->view('reports/Stock_position/Ajax_Display_Table2', $data);
    }

    # Three Table

    public function load_three() {

        $party_code = $this->input->post('party_code');
        // exit;

        $exchange_id = $this->input->post('exchange_id');
        $setlement_id = $this->input->post('setlement_id');
        $arr1Dt = explode('-', $_POST['arr1Dt']);
        $arr1Dt = "$arr1Dt[2]-$arr1Dt[1]-$arr1Dt[0]";
        $arr1Dt2 = explode('-', $_POST['arr1Dt2']);
        $arr1Dt2 = "$arr1Dt2[2]-$arr1Dt2[1]-$arr1Dt2[0]";

        if ("" == trim($party_code)) { # POST DATA IS NULL
            // echo "if";
            $data['rec'] = $this->Stock_position_model->load_three($exchange_id, $setlement_id, $arr1Dt, $arr1Dt2);
            //print_r($data);
        } else {
            // echo "else";
            $data['rec'] = $this->Stock_position_model->load_three_code($exchange_id, $setlement_id, $arr1Dt, $arr1Dt2, $party_code);
        }
        $this->load->view('reports/Stock_position/Ajax_Display_Table3', $data);
    }

    /*
      # Four Table  | + | On Change Table DBL CLICK TABLE

      public function load_four() {
      $id = $this->input->post('id');
      if (!empty($id)) {

      $data['rec'] = $this->Stock_position_model->onchange_four($id);
      $this->load->view('reports/Stock_position/Ajax_Display_Table4', $data);
      } else {
      $data['rec'] = $this->Stock_position_model->load_four();
      $this->load->view('reports/Stock_position/Ajax_Display_Table4', $data);
      }
      }
     */

    public function PopupTbl2() {

        $id = $this->input->post('id');
        $data['recA'] = $this->Stock_position_model->PopupTbl2($id);
        $this->load->view('reports/Stock_position/PopupTbl2', $data);
    }

    public function UpdateBs() {
        $ltp = $this->input->post('ltp');
        $Profit_Loss = $this->input->post('total_Profit_Loss');
        $exchange_id = $this->input->post('exchange_id');
        $setlement_id = $this->input->post('setlement_id');
    }

}
