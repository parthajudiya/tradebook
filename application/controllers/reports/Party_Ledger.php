<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";


class Party_ledger extends Controller {


	function __construct()
	{
		parent::__construct();
        $this->load->model('reports/Party_ledger_model');
	}

	public function index()
	{
		$data["Exchange"] = $this->Party_ledger_model->Select_Exchange();
		$this->display("index" , $data );
	}
	
	public function ResultTable()
	{
		 $sd = $this->input->post('arr1Dt');
		
		
		$party_code = $this->input->post('party_code');
		$party_name = $this->input->post('party_name');
		$exchange_id = $this->input->post('exchange_id');
		
		# SD 
		$arr1Dt = explode('-', $_POST['arr1Dt']);
		$arr1Dt = "$arr1Dt[2]-$arr1Dt[1]-$arr1Dt[0]";
		# ED
		$arr1Dt2 =  explode('-', $_POST['arr1Dt2']);
		$arr1Dt2 = "$arr1Dt2[2]-$arr1Dt2[1]-$arr1Dt2[0]";
		
		
			$data['recopen'] = $this->Party_ledger_model->ResultTable($exchange_id ,  $arr1Dt , null , $party_code , $party_name );
			$data['rec'] = $this->Party_ledger_model->ResultTable($exchange_id ,  $arr1Dt , $arr1Dt2 , $party_code , $party_name );
				
		$this->load->view('reports/Party_ledger/Ajax_Display_Table1' , $data);
	}
	public function Table1_Print()
	{
		/* For Table */
		$arr1DtA = explode('-', $_POST['arr1Dt']);
		$data["arr1DtA"] = "$arr1DtA[0]-$arr1DtA[1]-$arr1DtA[2]";
		
		$arr1DtB = explode('-', $_POST['arr1Dt2']);
        $data["arr1DtB"] = "$arr1DtB[0]-$arr1DtB[1]-$arr1DtB[2]";
		
		$data["Ex"] = $this->input->post('exchange_id');
		
		$data["party_name"] = $this->input->post('party_name');
		$data["party_code"] = $this->input->post('party_code');
		
		/* For Table */	
		
		
	    $party_code = $this->input->post('party_code');
		$party_name = $this->input->post('party_name');
		$exchange_id = $this->input->post('exchange_id');
		
		# SD 
		$arr1Dt = explode('-', $_POST['arr1Dt']);
		$arr1Dt = "$arr1Dt[2]-$arr1Dt[1]-$arr1Dt[0]";
		# ED
		$arr1Dt2 =  explode('-', $_POST['arr1Dt2']);
		$arr1Dt2 = "$arr1Dt2[2]-$arr1Dt2[1]-$arr1Dt2[0]";
		
		
		$data['recopen'] = $this->Party_ledger_model->ResultTable($exchange_id ,  $arr1Dt , null , $party_code , $party_name );
		$data['rec'] = $this->Party_ledger_model->ResultTable($exchange_id ,  $arr1Dt , $arr1Dt2 , $party_code , $party_name );
			
		$data = $this->load->view('reports/Party_ledger/Table1_Print' , $data ,TRUE);
		echo  $data;
	}
	
	
}
