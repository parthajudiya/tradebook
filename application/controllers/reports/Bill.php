<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";
// error_reporting(0);

class Bill extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('reports/Bill_model');
	}
	
	public function index()
	{
		$data["Exchange"] = $this->Bill_model->Select_Exchange();
		$this->display("index", $data);
	}
	public function Exchange_On_Change()
	{
		$id = $this->input->post('id');
		$data = $this->Bill_model->Select_Exchange_On_Change($id)->result();
		echo json_encode($data);
	}
	
	public function party_code_search()
	{
		$code = $_REQUEST["code"];
		$data = $this->Bill_model->party_code_search($code);
		echo json_encode($data);
	}
	
	public function Search_Account_Code()
	{ 		
		
		$postData = $this->input->post();
		$data  = $this->Bill_model->Search_Account_Code($postData);
		echo json_encode($data);
	}
	
	public function ResultTable()  
	{
		$party_code = $this->input->post('party_code');
		$exchange_id = $this->input->post('exchange_id');
		$setlement_id = $this->input->post('setlement_id');
		
		# SD 
		$arr1Dt = explode('-', $_POST['arr1Dt']);
		$arr1Dt = "$arr1Dt[2]-$arr1Dt[1]-$arr1Dt[0]";
		# ED
		$arr1Dt2 =  explode('-', $_POST['arr1Dt2']);
		$arr1Dt2 = "$arr1Dt2[2]-$arr1Dt2[1]-$arr1Dt2[0]";
		// die();
			
		if("" == trim($party_code)) # POST DATA IS NULL
		{
			$data['rec'] = $this->Bill_model->load_first($exchange_id , $setlement_id, $arr1Dt , $arr1Dt2 );
		}
		else
		{
			$data['rec'] = $this->Bill_model->load_first($exchange_id , $setlement_id, $arr1Dt , $arr1Dt2 , $party_code );
		}		
		$this->load->view('reports/Bill/Ajax_Display_Table1' , $data); 
	}
	
	public function ResultTable2()  
	{
		$id = $this->input->post('id');
		$data2['rec2'] = $this->Bill_model->load_second($id);
		$this->load->view('reports/Bill/Ajax_Display_Table2' , $data2); 
	}	
	
	
}

