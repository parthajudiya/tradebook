<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";

class Account extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('master/Account_model');
	}
	
	public function index()
	{
		$this->display("index");
	}
	
	public function Ajax_View_Table()
	{
		$data['Account'] = $this->Account_model->view();
		$this->load->view('master/Account/Ajax_Display_Table' ,$data);
	}
	
	public function Add()
	{
		
		$uid = $this->input->post("uid");
		

		if(!empty($uid))
		{
				//ADD 
				$data = array(
				"uid"=>$this->input->post("uid"),
				"code"=>$this->input->post("code"),
				"mobile"=>$this->input->post("mobile"),
				"username"=>$this->input->post("username"),
				"email"=>$this->input->post("email"),
				"ac_type"=>$this->input->post("ac_type"),
				"ac_group"=>$this->input->post("ac_group"),
				"disp_order"=>$this->input->post("disp_order"),
				"closing_rate_type_id"=>$this->input->post("closing_rate_type_id"),
				);
				
				$data = $this->Account_model->add($data);
				echo $data;
				return $data;
		}
		else
		{
				// ADD null uid
				$data = array(
				"uid"=>null,
				"code"=>$this->input->post("code"),
				"mobile"=>$this->input->post("mobile"),
				"username"=>$this->input->post("username"),
				"email"=>$this->input->post("email"),
				"ac_type"=>$this->input->post("ac_type"),
				"ac_group"=>$this->input->post("ac_group"),
				"disp_order"=>$this->input->post("disp_order"),
				"closing_rate_type_id"=>$this->input->post("closing_rate_type_id"),
				);
				
				$data = $this->Account_model->add($data);
				echo $data;
				return $data;
		}
		
		
	}
	
	public function delete($id)
	{
		$id = $this->input->post("id");
		$data = $this->Account_model->delete($id);
		if($result = true)
		{
		  redirect(base_url() . "master/account" , "refresh");
		}
	}
	
	
	public function Delete_Bulk()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		$this->Account_model->delete_bulk($id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/account" , "refresh");
		}
	}
	
	public function Edit($id)
	{

	
		$AccountEdit = $this->Account_model->Edit($id);
		echo  json_encode($AccountEdit);
	
		
	}
	
	// For Droup Down
	public function Search_Account()
	{ 		
		
			$postData = $this->input->post();
			$data = $this->Account_model->Search_Account($postData);
			echo json_encode($data);
	}
	
	

	

	public function Update()
	{
		
			$uid = $this->input->post("uid");
			//if($uid == null && !empty($aid))
			if($uid == null)
			{
				
				$data = array(
				"uid"=>null,
				"code"=>$this->input->post("code"),
				"mobile"=>$this->input->post("mobile"),
				"username"=>$this->input->post("username"),
				"email"=>$this->input->post("email"),
				"ac_type"=>$this->input->post("ac_type"),
				"ac_group"=>$this->input->post("ac_group"),
				"disp_order"=>$this->input->post("disp_order"),
				"closing_rate_type_id"=>$this->input->post("closing_rate_type_id")
				);

				$aid =  $this->input->post("aid");
				$this->Account_model->Update($data ,$aid);
			}
			else
			{
				$data = array(
				"uid"=>$this->input->post("uid"),
				"code"=>$this->input->post("code"),
				"mobile"=>$this->input->post("mobile"),
				"username"=>$this->input->post("username"),
				"email"=>$this->input->post("email"),
				"ac_type"=>$this->input->post("ac_type"),
				"ac_group"=>$this->input->post("ac_group"),
				"disp_order"=>$this->input->post("disp_order"),
				"closing_rate_type_id"=>$this->input->post("closing_rate_type_id")
				);

				$aid =  $this->input->post("aid");
				$this->Account_model->Update($data ,$aid);
			}
	
	}
	

	
	
	
	
	
	
	
	
	
	

}
