<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";
ini_set('max_execution_time', 5000);
// error_reporting(0);

class Symbol extends Controller {

    public function __construct() 
	{
        parent::__construct();
		$this->load->model("master/Symbol_model");
    }
	
	public function Symbol_Moneycontroll()
	{
		$this->load->library('DOM');
		$html = file_get_html('https://www.moneycontrol.com/markets/fno-market-snapshot');  // get money controll shoutcut id for symbol
		$data=array();

		foreach($html->find("#nsecont1") as $divClass)
		{
			foreach($divClass->find("select#stock_code") as $aa)
			{
				foreach($aa->find("option") as $op)
				{
					if($op->value!=null)
					{	
						$data[$op->value]=$op->plaintext;
						
							
						  $url = "https://priceapi.moneycontrol.com/pricefeed/nse/equitycash/$op->value";    // in this shoucut id goes to this link and get real symbol and insert record db
						  $cURLConnection = curl_init();
							curl_setopt($cURLConnection, CURLOPT_URL, $url);
							curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
							$phoneList = curl_exec($cURLConnection);
							$error=curl_error($cURLConnection);
							curl_close($cURLConnection);
							print_r($error);
							$response = json_decode($phoneList);
							
							// $nsc_id = $response->data->NSEID;
							
				
						
						$data = array(
						array(
							'exchange_id' =>"63",
							// 'symbol' =>$op->plaintext,
							'symbol' =>$response->data->NSEID,
							'instument' =>"FUTSTK",
							'lot_size' => "1",
							'sy_code' => $op->value,
							'nsc_id' => $response->data->NSEID,
							)
						);	
						$data =	$this->Symbol_model->AddSymbol($data);
						
					}
				}
			}	
		}
	}
	
	/*
	public function Symbol_Moneycontroll()
	{
		$postArry  = $this->input->post();
		$SY = $postArry['search'];
		;
		//$SY = "ICI02";
		$SY_TYPE = "FUTSTK";
		$SY_EXP = "2020-11-26";
		
		//echo $url = "https://www.moneycontrol.com/mc/widget/fnoquote/updateFnOTable?classic=true&sc_did=$SY&instType=$SY_TYPE&optiontype=&expDate=$SY_EXP&strikeprice=Select&pricecurr=1";
		
		
		//https://www.moneycontrol.com/mc/widget/fnoquote/updateFnOTable?classic=true&sc_did=acc&instType=FUTSTK&optiontype=&expDate=2020-11-26&strikeprice=Select&pricecurr=1
		
		
	 $url =	"https://www.moneycontrol.com/mccode/common/autosuggestion_solr.php?classic=true&query=$SY&type=1&format=json&callback";
		
		//die();
		
		$cURLConnection = curl_init();

		curl_setopt($cURLConnection, CURLOPT_URL, $url);
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$phoneList = curl_exec($cURLConnection);
		
		
		$error=curl_error($cURLConnection);
		curl_close($cURLConnection);
		print_r($error);
		$response = json_decode($phoneList);
		
			// echo "<pre>";
	// print_r($response);
	// echo "<pre>";
		// die();
		
		echo $data = $response->stock_name;
		// 
		

		// echo json_encode($data);
	}
	*/
	
	/********************************** start ******************************************/
	
		// 63 == fo
		// 70 == mcx
		// 71 == ncdex
	
	
	public function UploadData()
	{
		$exchange_id = $this->input->post("exchange_id"); 
		
		if($exchange_id == "63")
		{
			$config['upload_path']   = './temp/'; 
			$config['allowed_types'] = 'csv'; 
			$config['file_name'] = $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
			if (!$uploadData ) // if ($count == 0) {}  && $count == 0
			{
				echo "Upload Error 5 ";
			}
			else
			{ 
				$uploadData2 = $this->upload->data();
				$FileData = $uploadData2['file_name'];
				$data = file_get_contents(base_url("temp/$FileData"),"r");
				$csv = explode("\n", $data);
				$count = 0;
				$symbol=array();
				foreach ($csv as $csvData)
				{
					if ($count == 0) {}
					else 
					{
						$row = explode(",", $csvData);
						if($row[4]!="PE" && $row[4]!="CE"  && $row[1]!="" )  // !is_null($row[1]) || $row[1]!="" && 
						{
							$symbol[$row[1]]=array(
							'exchange_id' =>$exchange_id,
							'symbol' =>trim($row[1]),
							'instument' =>trim($row[0]),
							'lot_size' => "1"
							);
						}
					}
					$count++;
				}
				foreach($symbol as $d){
					$data =	$this->Symbol_model->AddSymbol($d);
				}
				echo $data;
				return $data;
			}
		}	
		
		elseif($exchange_id == "70")
		{
			$config['upload_path']   = './temp/'; 
			$config['allowed_types'] = 'csv'; 
			$config['file_name'] = $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
			if (!$uploadData ) // if ($count == 0) {}  && $count == 0
			{
				echo "Upload Error 1 ";
			}
			else
			{ 
				$uploadData2 = $this->upload->data();
				$FileData = $uploadData2['file_name'];
				$data = file_get_contents(base_url("temp/$FileData"),"r");
				$csv = explode("\n", $data);
				$count = 0;
				$symbol=array();
				foreach ($csv as $csvData)
				{
					if ($count == 0) {}
					else 
					{
						$row = explode(",", $csvData);
						if($row[0]!="" && !is_null($row[0]))  // !is_null($row[1]) || $row[1]!="" && 
						{
							$symbol[$row[0]]=array(
							'exchange_id' =>$exchange_id,
							'symbol' =>trim($row[0]),
							'instument' =>"FUTCOM",
							'lot_size' => "1"
							);
						}
					}
					$count++;
				}
				foreach($symbol as $d){
					$data =	$this->Symbol_model->AddSymbol($d);
				}
				echo $data;
				return $data;
			}
		}
		elseif($exchange_id == "71")
		{
			$config['upload_path']   = './temp/'; 
			$config['allowed_types'] = 'csv'; 
			$config['file_name'] = $_FILES['FileData']['name'];
			$this->load->library('upload', $config);
			$uploadData = $this->upload->do_upload('FileData');
			if (!$uploadData ) // if ($count == 0) {}  && $count == 0
			{
				echo "Upload Error 3 ";
			}
			else
			{ 
				$uploadData2 = $this->upload->data();
				$FileData = $uploadData2['file_name'];
				$data = file_get_contents(base_url("temp/$FileData"),"r");
				$csv = explode("\n", $data);
				$count = 0;
				$symbol=array();
				foreach ($csv as $csvData)
				{
					if ($count == 0) {}
					else 
					{
						$row = explode(",", $csvData);
						if($row[0]!="" && !is_null($row[0])) 
						{
							$symbol[$row[0]]=array(
							'exchange_id' =>$exchange_id,
							'symbol' =>trim($row[0]),
							'instument' =>"FUTCOM",
							'lot_size' => "1"
							);
						}
					}
					$count++;
				}
				foreach($symbol as $d){
					$data =	$this->Symbol_model->AddSymbol($d);
				}
				echo $data;
				return $data;
			}
		}	
		else
		{
			echo "Upload Error 2";
		}
		
		  
	}
	
	
	/**********************************end ******************************************/
	
	
	
		
/*********************************   CSV Import Data Start **************************/		
/*
	public function mcxcsv()  
	{
		$data = file_get_contents("http://tradeday2day.com/mcx.csv");
		$csv = explode("\n", $data);
		$count = 0;
		$symbol=array();
		foreach ($csv as $csvData)
		{
			if ($count == 0) {}
			else 
			{
				$row = explode(",", $csvData);
				if(!is_null($row[0]) || $row[0]!="")
				{
					$symbol[$row[0]]=array(
					'exchange_id' =>"70",
					'symbol' =>$row[0],
					'instument' =>"instument", 
					'lot_size' => "1"
					);
				}
			}
			$count++;
		}
		foreach($symbol as $d){
			$result =	$this->Symbol_model->AddSymbol($d);
		}
		echo $result;
        return $result;
			
	}
		
	public function ncdexcsv() 
	{
		$data = file_get_contents("http://tradeday2day.com/ncdex.csv");
		$csv = explode("\n", $data);
		$count = 0;
		$symbol=array();
		foreach ($csv as $csvData)
		{
			if ($count == 0) {}
			else 
			{
				$row = explode(",", $csvData);
				if(!is_null($row[0]) || $row[0]!="")
				{
					$symbol[$row[0]]=array(
					'exchange_id' =>"71",
					'symbol' =>$row[0],
					'instument' =>"instument", 
					'lot_size' => "1"
					);
				}
			}
			$count++;
		}
		foreach($symbol as $d){
			$result = $this->Symbol_model->AddSymbol($d);
		}
		echo $result;
        return $result;
	}
			
			
		
	
	public function focsv() 
	{
		// $data = file_get_contents("http://localhost/tradebook/fo.csv");
		$data = file_get_contents("http://tradeday2day.com/fo.csv");
		$csv = explode("\n", $data);
		$count = 0;
		$symbol=array();
		foreach ($csv as $csvData)
		{
			if ($count == 0) {}
			else 
			{
				$row = explode(",", $csvData);
					//print_r($row[1]);
					if(!is_null($row[1]) || $row[1]!="")
					{
						$symbol[$row[1]]=array(
						'exchange_id' =>"63",
						'symbol' =>$row[1],
						'instument' =>$row[0],
						'lot_size' => "1"
						);
					}
			}
			$count++;
		}
		foreach($symbol as $d){
			$result =	$this->Symbol_model->AddSymbol($d);
		}
		echo $result;
        return $result;
		
	}
	*/
/*********************************   CSV Import Data Start **************************/	
		
    public function index() {
		$data["Exchange"] = $this->Symbol_model->Select_Exchange();
        $this->display("index", $data);
    }
	

    # VIEW
		public function Ajax_View_Table() {
        $data['symbol'] = $this->Symbol_model->view();
        $this->load->view('master/symbol/Ajax_Display_Table', $data);
    }

    # ADD
	public function Add() {
        $d = array(
            "exchange_id" => $this->input->post("exchange_id"),
            "symbol" => $this->input->post("symbol"),
            "instument" => $this->input->post("instument"),
            "lot_size" => $this->input->post("lot_size"),
        );
		$datas = $this->Symbol_model->AddSymbol($d);
		echo $datas;
		return $datas;
		
        //return $result;
    }


    # Edit
	public function Edit($id) {
        $SymbolEdit = $this->Symbol_model->edit($id);
        echo json_encode($SymbolEdit);
	}

	
    public function Update() {

        $data = array(
            "exchange_id" => $this->input->post("exchange_id"),
            "symbol" => $this->input->post("symbol"),
            "instument" => $this->input->post("instument"),
            "lot_size" => $this->input->post("lot_size")
        );

        $symbol_id = $this->input->post("symbol_id");
        $this->Symbol_model->update($data, $symbol_id);
    }

    #DELETE
	public function Delete($id) {
        $id = $this->input->post("id");
        $data = $this->Symbol_model->delete($id);
        if ($result = true) {
            redirect(base_url() . "master/symbol", "refresh");
        }
    }


    # DELETE BULK
	public function Delete_Bulk() {
        $id = $this->input->post('id'); # This 2 id comes in jq
        $this->Symbol_model->delete_bulk($id);  # This id comes in jq
        if ($result = true) {
            redirect(base_url() . "master/symbol", "refresh");
        }
    }



}
