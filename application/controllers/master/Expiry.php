<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";
ini_set('max_execution_time', 5000);
error_reporting(0);

class Expiry extends Controller {
	
	 public function __construct() {
        parent::__construct();
		$this->load->model("master/Expiry_model");
        
    }
	
	# Exchange For Droup Down
	public function index()
	{
		$data["Exchange"] = $this->Expiry_model->Select_Exchange(); 
		$this->display("index" , $data );
	}

	# JOIN 2 nd Droup Down
	public function Symbol()
	{
		
		$id = $this->input->post('id');
		 $data = $this->Expiry_model->Select_Symbol($id)->result();
		echo json_encode($data);
	}


	
	# VIEW
	public function Ajax_View_Table()
	{
		
		$data['expiries'] = $this->Expiry_model->view();
		$this->load->view('master/expiry/Ajax_Display_Table' ,$data);
	}
	
	# ADD
	public function Add()
	{
		// $ex_dt1 = explode('-', $_POST['ex_date']);
		// $ex_dt1 = "$ex_dt1[2]-$ex_dt1[1]-$ex_dt1[0]";
		
		$month=["JAN"=>1, "FEB"=>2, "MAR"=>3,"APR"=>4, "MAY"=>5, "JUN"=>6,"JUL"=>7, "AUG"=>8, "SEP"=>9,"OCT"=>10, "NOV"=>11,"DEC"=>12];
		$ex_date=strtoupper($this->input->post("ex_date"));
		$date=explode("-",$ex_date);
		
		
		$data = array(
			"exchange_id"=>$this->input->post("exchange_id"),
			"ex_date"=>$ex_date,
			"ex_date_value"=>$date[2]."-".$month[$date[1]]."-".$date[0],
			"status"=>$this->input->post("status"),
			"symbol_id"=>$this->input->post("symbol_id"),
		);
		
		$data = $this->Expiry_model->add($data);
		echo $data;
		return $data;
	}
	
	# Edit
	public function Edit($id)
	{
			$SymbolEdit = $this->Expiry_model->edit($id);
			echo  json_encode($SymbolEdit);
	}


	public function Update()
	{

		// $ex_dt1 = explode('-', $_POST['ex_date']);
		// $ex_dt1 = "$ex_dt1[2]-$ex_dt1[1]-$ex_dt1[0]";
		
		$data = array(

		"exchange_id"=>$this->input->post("exchange_id"),
		"symbol_id"=>$this->input->post("symbol_id"),
		"ex_date"=>$this->input->post("ex_date"),
		// "ex_date"=>$ex_dt1,
		"status"=>$this->input->post("status"),
		);

		$expiries_id =  $this->input->post("expiries_id");
		$this->Expiry_model->update($data ,$expiries_id);
			
	}

	
	
	#DELETE
	public function Delete($id)
	{
		$id = $this->input->post("id");
		$data = $this->Expiry_model->delete($id);
		if($result = true)
		{
		  redirect(base_url() . "master/expiry" , "refresh");
		}
	}
	
	# DELETE BULK
	public function Delete_Bulk()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		$this->Expiry_model->delete_bulk($id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/expiry" , "refresh");
		}
	}
	
	
	public function is_Default($id)
	{
		
		// $id = $this->input->post("id");
		// $data = $this->Expiry_model->Chk_Expiry_is_Default($id); 
		// echo json_encode($data);
		
		
		$data = array(
			"is_Default"=>"0"
		);
		
		$data = $this->Expiry_model->is_Default($data , null);
		$id = $this->input->post("id");
		
		$data = array(
			"is_Default"=>"1"
		);
		
		$data = $this->Expiry_model->is_Default($data , $id);
		if($result = true)
		{
		  redirect(base_url() . "master/expiry" , "refresh");
		}
		
	}
	
	public function UploadDate()
	{
		$this->load->library('DOM');
		$html = file_get_html('https://www.moneycontrol.com/stocks/fno/query_tool/get_expdate.php?symbol=ACC&inst_type=FUTSTK'); 
		
			foreach($html->find("option") as $op)
			{
				# print_r($op);
				$ex_date_value =  $op->value;
				
				$month=["01"=>"JAN", "02"=>"FEB", "03"=>"MAR","04"=>"APR", "05"=>"MAY", "06"=>"JUN", "07"=>"JUL", "8"=>"AUG", "9"=>"SEP", "10"=>"OCT", "11"=>"NOV","12"=>"DEC"];
				$ex_date=explode("-",$ex_date_value);

					$array[] =array( 
					'exchange_id' =>63, 
					'symbol_id' =>0,
					"ex_date"=>$ex_date[2]."-".$month[$ex_date[1]]."-".$ex_date[0],
					'ex_date_value'=>$ex_date_value,
					'status'=>0,
					'is_Default'=>0
					);
			}
			
		
			$datas = $this->Expiry_model->adddata($array); 
			echo $datas;
			return $datas;
		}
	
	
	/*
	public function UploadDate()
	{
		# all Common Date Expiry For FO
			$this->load->library('DOM');
		
			$exchange_id = $this->input->post("exchange_id");
	
			if($exchange_id == "63")
			{
			
				$html = file_get_html('https://www.moneycontrol.com/india/stockpricequote/cement-major/acc/ACC06'); 
				$data=array();
				foreach($html->find("#_sel_exp_date") as $divClass)
				{
					foreach($divClass->find("option") as $op)
					{
						if($op->value!=null)
						{	
							$data[$op->value]=$op->plaintext;
							
							$data = array(
							array(
								'symbol_id' =>"0",
								'exchange_id' =>"63",
								'ex_date' =>$op->plaintext,
								'ex_date_value' => $op->value,
								)
							);	
							$datas = $this->Expiry_model->adddata($data); 
						}
					}
				}
					echo $datas;
					return $datas;
			}
		
		
		
	}
	*/
	
	/*
	public function UploadDate()
	{
		// 63 == fo
		// 70 == mcx
		// 71 == ncdex
		
			
			
			
				
				
			$Symbol_Name=array();
			$symbol_qry_id =array();
			$symbol_qry= $this->Expiry_model->Sel_Symbol();
			foreach ($symbol_qry as $key=>$val)
			{
				$symbol_qry_id[$val["symbol"]]= trim($val["symbol_id"]);
			}	
			
			
		$exchange_id = $this->input->post("exchange_id"); 
		// die();
		if($exchange_id == "63")
		{
			// echo "me";
			// die();
			$data = file_get_contents("http://tradeday2day.com/fo.csv");
			$csv = explode("\n", $data);
			$count = 0;
			
			
			foreach ($csv as $csvData)
			{
				if ($count == 0) {}
				else 
				{
					$row = explode(",", $csvData);
			
					if($row[4]!="PE" && $row[4]!="CE"  && $row[1]!="" ) 	// $row[2]
					{
						// $Symbol_Name[$row[2]][$row[1]]=array( 
						$Symbol_Name[]=array( 
							'exchange_id' =>$exchange_id, 
							'symbol_id' =>0, // match and insert
							// 'symbol_id' =>$symbol_qry_id[$row[1]],// match and insert
							'ex_date'=>$row[2],
							'status'=>1,
							'is_Default'=>0
						);
					}
				}
				$count++;
			}
			
			
			# $data = $this->Expiry_model->adddata($Symbol_Name);
			# echo $data;
			# return $data;
			
			foreach($Symbol_Name as $d)
			{
				$datas = $this->Expiry_model->adddata($d); 
			}
			echo $datas;
			return $datas;
			
		}
		elseif($exchange_id == "70") // mcx
		{
			$datas = array();
			$data = file_get_contents("http://tradeday2day.com/mcx.csv");
			$csv = explode("\n", $data);
			$count = 0;
			
			
			foreach ($csv as $csvData)
			{
				$row = explode(",", $csvData);
				if($row[0]!="" ) 	
				{
						
					$Symbol_Name[]=array( 
						'exchange_id' =>$exchange_id, 
						'symbol_id' =>$symbol_qry_id[$row[0]], // match and insert
						'ex_date'=>$row[1],
						'status'=>1,
						'is_Default'=>0
					);
				}
				
				$count++;
			}
			
			
			#$data = $this->Expiry_model->adddata($Symbol_Name);
			#echo $data;
			#return $data;
			
			foreach($Symbol_Name as $d)
			{
				$datas = $this->Expiry_model->adddata($d); 
			}
			echo $datas;
			return $datas;
			
		}
		elseif($exchange_id == "71")
		{
			$datas = array();
			$data = file_get_contents("http://tradeday2day.com/ncdex.csv");
			$csv = explode("\n", $data);
			$count = 0;
			
			
			foreach ($csv as $csvData)
			{
				
					$row = explode(",", $csvData);
			
					if($row[0]!="") 	// $row[2]
					{
						
						$Symbol_Name[]=array( 
							'exchange_id' =>$exchange_id, 
							'symbol_id' =>trim($symbol_qry_id[$row[0]]), // match and insert
							//'symbol_id' =>$row[0], 
							'ex_date'=>$row[1],
							'status'=>1,
							'is_Default'=>0
						);
					}
				
				$count++;
			}
			
			foreach($Symbol_Name as $d)
			{
				$datas = $this->Expiry_model->adddata($d); 
			}
			echo $datas;
			return $datas;
			
		}
		else
		{
			$datas = "ERR"; 
			echo $datas;
			return $datas;
		}	
	
	}
	*/
	
	
	
	
	
	
	
	
	
	
}