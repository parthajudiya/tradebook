<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";


class Brokrage extends Controller {
	
	 public function __construct() 
	 {
        parent::__construct();
		$this->load->model("master/Brokrage_model");
    }
	
	public function index()
	{
		$data["Exchange"] = $this->Brokrage_model->Select_Exchange(); 
		$this->display("index" , $data);
	}
	
	/*
	# VIEW
	public function Ajax_View_Table()
	{
		// $postData = $this->input->post(786); // Added
		//print_r($postData );
		//echo "vv";
		//die();
		$data['Brokrage'] = $this->Brokrage_model->view(); // $postData  Added
		$this->load->view('master/brokrage/Ajax_Display_Table' ,$data);
	}
	*/
	
	# ADD AND Update
	public function Add()
	{
			$aid = $this->input->post("aid");
			$bid = $this->input->post("bid");
			
			
			$symbol_id = $this->input->post("symbol_id");
			
			// echo "AB";
			// die();
				
			# Add
			// if(!empty($uid))
			// if($bid == null && $aid == null)	
			if($bid == "" && $aid == ""  )	// && (!empty($symbol_id) or $symbol_id == "0")
			{
				// echo "Add";
				// die();
				# Add
				$data = array(
				"code"=>$this->input->post("code"),
				"name"=>$this->input->post("name"),
				"exchange_id"=>$this->input->post("exchange_id"),
				"symbol_id"=>$this->input->post("symbol_id"),
				"brokrage_type_id"=>$this->input->post("brokrage_type_id"),
				"base_on_id"=>$this->input->post("base_on_id"),
				"intraday"=>$this->input->post("intraday"),
				"side_id"=>$this->input->post("side_id"),
				"nxt_day"=>$this->input->post("nxt_day"),
				"minimum_id"=>$this->input->post("minimum_id"),
				);
				$data = $this->Brokrage_model->add($data);
				echo $data;
				return $data;
				
			}
			else
			{
				# Update
				// echo "Update";
				// die();
				// 
				$data = array(
				"bid"=>$bid,
				"aid"=>$aid,
				"code"=>$this->input->post("code"),
				"name"=>$this->input->post("name"),
				"exchange_id"=>$this->input->post("exchange_id"),
				"symbol_id"=>$this->input->post("symbol_id"),
				"brokrage_type_id"=>$this->input->post("brokrage_type_id"),
				"base_on_id"=>$this->input->post("base_on_id"),
				"intraday"=>$this->input->post("intraday"),
				"side_id"=>$this->input->post("side_id"),
				"nxt_day"=>$this->input->post("nxt_day"),
				"minimum_id"=>$this->input->post("minimum_id"),
				);
				// $aid =  $this->input->post("aid");
				
				$bid =  $this->input->post("bid");
				
				$data = $this->Brokrage_model->Upadte($data ,$bid);
				echo $data;
				return $data;
				
				
				
			}
			
		
	}
	
	// For Autocompleted Search Account Name
	public function Search_Account_Name()
	{ 		
		$postData = $this->input->post();
		$data = $this->Brokrage_model->Search_Account_Name($postData);
		echo json_encode($data);
	}
	
	public function Search_Account_Code()
	{ 		
		$postData = $this->input->post();
		$data  = $this->Brokrage_model->Search_Account_Code($postData);
		echo json_encode($data);
	}
	
	public function Search_Account_Code_keyup()
	{ 		
		$postData = $this->input->post();
		$data  = $this->Brokrage_model->Search_Account_Code_keyup($postData);
		echo json_encode($data);
	}
	
	
	/*
	public function searchTable_Code()
	{ 	
		
		if(isset($_REQUEST["term"]))
		{
			// $data['Brokrage'] = $this->Brokrage_model->view(); // $postData  Added
			// Brokrage 
			// echo "if";
			$CODE = $_REQUEST["term"];
			$data['Brokrage']  = $this->Brokrage_model->searchTable_Code($CODE);
			$this->load->view('master/brokrage/Ajax_Display_Table' ,$data);
		}
		else
		{	
			echo "abc";
		}
	
			// $postData =  $this->input->post("term");
			
			// print_r($postData);
			
	}	
	
	
	public function searchTable_NAME()
	{ 	
		
		if(isset($_REQUEST["term"]))
		{
			 $NAME = $_REQUEST["term"];
			$data['Brokrage']  = $this->Brokrage_model->searchTable_NAME($NAME);
			$this->load->view('master/brokrage/Ajax_Display_Table' ,$data);
		}
		else
		{	
			echo "abc";
		}
	}
	
	public function Search_Accounts_C()
	{ 		
		$code = $this->input->post();
		$data  = $this->Brokrage_model->Search_Accounts_C($code);
		echo json_encode($data);
	}
	
	   */
	   
	   public function searchTable()
	  { 	
	 
		  if(isset($_REQUEST["term1"]) && isset($_REQUEST["term2"]) && isset($_REQUEST["term3"]))
		  {
			$exchange = $_REQUEST["term1"];
			$name = $_REQUEST["term2"];
			$code = $_REQUEST["term3"];

			$data['Brokrage'] = $this->Brokrage_model->searchTable($exchange, $name ,$code);
			$this->load->view('master/brokrage/Ajax_Display_Table' ,$data);
			// echo json_encode($data);
		  }  
	  
	  /*
		if(isset($_REQUEST["term"]))
		{
			 $NAME = $_REQUEST["term"];
			$data['Brokrage']  = $this->Brokrage_model->searchTable($NAME);
			$this->load->view('master/brokrage/Ajax_Display_Table' ,$data);
		}
		*/
		else
		{	
			echo "NOT FOUND";
		}
		
	}
	
	
	public function Edit_Record()
	{
		
		if(isset($_REQUEST["code"]) && isset($_REQUEST["name"]) && isset($_REQUEST["exchange_id"]) && isset($_REQUEST["symbol_id"]))
		{
			 $code = $_REQUEST["code"];
			 $name = $_REQUEST["name"];
			 $exchange_id = $_REQUEST["exchange_id"];
			 $symbol_id = $_REQUEST["symbol_id"];
			
			 $data = $this->Brokrage_model->Edit_Record($code , $name , $exchange_id , $symbol_id);
			 echo json_encode($data);
		} 
		else
		{	
			echo "NOT FOUND";
		}
		// $ExchangeEdit = $this->Brokrage_model->edit($id);
		// echo  json_encode($ExchangeEdit);
	
	}
	
	
	    
    
	
	/*
	# EDIT
	public function Edit($id)
	{
		$ExchangeEdit = $this->Brokrage_model->edit($id);
		echo  json_encode($ExchangeEdit);
	
	}
	
	# UPDATE
	public function Update()
	{
		$data = array(
			"name"=>$this->input->post("name"),
			"decimal_points"=>$this->input->post("decimal_points")
		);
		
		$exchange_id =  $this->input->post("exchange_id");
		$this->Brokrage_model->update($data ,$exchange_id);
	}
	*/
	
	#DELETE
	public function Delete($id)
	{
		$id = $this->input->post("id");
		$data = $this->Brokrage_model->delete($id);
		if($result = true)
		{
		  redirect(base_url() . "master/brokrage" , "refresh");
		}
	}
	
	# DELETE BULK
	public function Delete_Bulk()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		$this->Brokrage_model->delete_bulk($id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/brokrage" , "refresh");
		}
	}
	
	
	
}