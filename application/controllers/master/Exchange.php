<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";

class Exchange extends Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("master/Exchange_model");
	}

	public function index()
	{
		$this->display("index");
	}
	
	# VIEW
	public function Ajax_View_Table()
	{
		$data['Exchange'] = $this->Exchange_model->view();
		$this->load->view('master/Exchange/Ajax_Display_Table' ,$data);
	}
	
	# ADD
	public function Add()
	{
		$data = array(
			"name"=> strtolower($this->input->post("name")),
			"decimal_points"=>$this->input->post("decimal_points"),
		);
		
		$data = $this->Exchange_model->add($data);
		echo $data;
		return $data;
	}
	
	# EDIT
	public function Edit($id)
	{
		$ExchangeEdit = $this->Exchange_model->edit($id);
		echo  json_encode($ExchangeEdit);
	
	}
	
	# UPDATE
	public function Update()
	{
		$data = array(
			"name"=>strtolower($this->input->post("name")),
			"decimal_points"=>$this->input->post("decimal_points")
		);
		
		$exchange_id =  $this->input->post("exchange_id");
		$this->Exchange_model->update($data ,$exchange_id);
	}
	
	#DELETE
	public function Delete($id)
	{
		$id = $this->input->post("id");
		$data = $this->Exchange_model->delete($id);
		if($result = true)
		{
		  redirect(base_url() . "master/exchange" , "refresh");
		}
	}
	
	# DELETE BULK
	public function Delete_Bulk()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		$this->Exchange_model->delete_bulk($id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/exchange" , "refresh");
		}
	}
	
	
	
	
	
	
}
