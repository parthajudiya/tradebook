<?php
// extends
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";

class Settlment extends Controller {
	
	 public function __construct() 
	 {
        parent::__construct();
		$this->load->model("master/Settlment_model");
     }
	
	# For Load Droup Down
	public function index()
	{
		$data["Count_Setlement"] = $this->Settlment_model->Count_Settlment(); 	
		
		
		$data["Exchange"] = $this->Settlment_model->Select_Exchange(); 
		// print_r($data["Count_Setlement"]);
		$this->display("index" , $data );
	}
	
	# VIEW
	public function Ajax_View_Table()
	{
		$data['setlement'] = $this->Settlment_model->view();
		$this->load->view('master/settlment/Ajax_Display_Table' ,$data);
	}
	
	# ADD
	public function Add()
	{
		$sd1 = explode('-', $_POST['start_date']);
		$sd1 = "$sd1[2]-$sd1[1]-$sd1[0]";
		
		$ed1 =  explode('-', $_POST['end_date']);
		$ed1 = "$ed1[2]-$ed1[1]-$ed1[0]";
		
		# print_r($sd1);
		# die();		
			
		$data = array(
			"exchange_id"=>$this->input->post("exchange_id"),
			"setlement_no"=>$this->input->post("setlement_no"),
			"start_date"=>$sd1,
			"end_date"=>$ed1,
			"description"=>$this->input->post("description"),
			//"ledger"=>$this->input->post("ledger"),
		);
		
		$data = $this->Settlment_model->add($data);
		echo $data;
		return $data;
	}
	
	# Edit
	public function Edit($id)
	{
			
		$SettlmentEdit = $this->Settlment_model->edit($id);
		echo  json_encode($SettlmentEdit);
	}
	
	public function Update()
	{
		
		$sd1 = explode('-', $_POST['start_date']);
		$sd1 = "$sd1[2]-$sd1[1]-$sd1[0]";
		
		$ed1 =  explode('-', $_POST['end_date']);
		$ed1 = "$ed1[2]-$ed1[1]-$ed1[0]";
		
		
		$data = array(
		"exchange_id"=>$this->input->post("exchange_id"),
		"setlement_no"=>$this->input->post("setlement_no"),
		"start_date"=>$sd1,
		"end_date"=>$ed1,
		"description"=>$this->input->post("description"),
		"ledger"=>$this->input->post("ledger")
		);
		$setlement_id =  $this->input->post("setlement_id");
		$this->Settlment_model->update($data ,$setlement_id);
	}
	
	#DELETE
	public function Delete($id)
	{
		$id = $this->input->post("id");
		$data = $this->Settlment_model->delete($id);
		if($result = true)
		{
		  redirect(base_url() . "master/settlment" , "refresh");
		}
	}
	
	# DELETE BULK
	public function Delete_Bulk()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		$this->Settlment_model->delete_bulk($id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/settlment" , "refresh");
		}
	}
	
	public function Bulk_Ledger_Yes()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		
		$data = array(
		"ledger"=>"1"
		);
		
		$this->Settlment_model->Bulk_Ledger_Yes($data , $id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/settlment" , "refresh");
		}
	}
	
	
	public function Bulk_Ledger_No()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		
		$data = array(
		"ledger"=>"0"
		);
		
		$this->Settlment_model->Bulk_Ledger_No($data , $id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/settlment" , "refresh");
		}
	}
	
	
	
	
}
