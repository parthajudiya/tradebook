<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";


class Remiser_brokrage extends Controller {
	
	 public function __construct() {
        parent::__construct();
		$this->load->model("master/Remiser_brokrage_model");
        
    }
	
	public function index()
	{
		$this->display("index");
	}
	
	# VIEW
	public function Ajax_View_Table()
	{
		$data['remiser_brokrage'] = $this->Remiser_brokrage_model->view();
		$this->load->view('master/remiser_brokrage/Ajax_Display_Table' ,$data);
	}
	
	#DELETE
	public function Delete($id)
	{
		$id = $this->input->post("id");
		$data = $this->Remiser_brokrage_model->delete($id);
		if($result = true)
		{
		  redirect(base_url() . "master/remiser_brokrage" , "refresh");
		}
	}
	
	# DELETE BULK
	public function Delete_Bulk()
	{
		$id = $this->input->post('id'); # This 2 id comes in jq
		$this->Remiser_brokrage_model->delete_bulk($id);  # This id comes in jq
		if($result = true)
		{
		  redirect(base_url() . "master/remiser_brokrage" , "refresh");
		}
	}
}
