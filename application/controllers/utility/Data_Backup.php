<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";

/**
 * Class Data_backup
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Data_backup extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('utility/Data_backup_model');
	}
	
	public function index()
	{
		$this->display("index");
	}
}
