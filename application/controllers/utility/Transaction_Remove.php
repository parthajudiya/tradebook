<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";


class Transaction_remove extends Controller {
	
	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('utility/Transaction_remove_model');
	}
	
	public function index()
	{
		$id = $this->input->post('id');
		$data['Exchange'] = $this->Transaction_remove_model->Exchange();
		$this->display("index" ,$data);
	}
	
	public function Remove_buy_sell_data()
	{
		$data = array();	
		$start_date = explode('-', $_POST['start_date']);
		$start_date = "$start_date[2]-$start_date[1]-$start_date[0]";
		
		$end_date =  explode('-', $_POST['end_date']);
		$end_date = "$end_date[2]-$end_date[1]-$end_date[0]";
					
		$party_code = $this->input->post('party_code');
		$party_name = $this->input->post('party_name');
		
		$exchange_id =  $this->input->post('exchange_id');
		$setlement_id =  $this->input->post('setlement_id');
		$type =  $this->input->post('type');
					
		
		 $search = $this->Transaction_remove_model->Search_Data($start_date , $end_date , $party_code , $party_name , $exchange_id ,  $setlement_id , $type);
		 
		 foreach ($search as $row)
		 {
			 $id = $row['buysell_id']. "<br>";
			 $data =  $this->Transaction_remove_model->Remove_buy_sell_data($id);
		 }
		 return $data;
	}
	
}
