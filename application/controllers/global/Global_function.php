
<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "core/Controller.php";

class Global_function extends Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('global/Global_function_model');
    }

    public function Exchange_On_Change() {
        $id = $this->input->post('id');
        $data = $this->Global_function_model->Select_Exchange_On_Change($id)->result();
        echo json_encode($data);
    }

    public function Search_Account_Code() {

        $postData = $this->input->post();
        $data = $this->Global_function_model->Search_Account_Code($postData);
        echo json_encode($data);
    }

    public function party_code_search() {
        $code = $_REQUEST["code"];
        $data = $this->Global_function_model->party_code_search($code);
        echo json_encode($data);
    }

    # For Buy Sell
    # All Party In Load Droup Down and Pass id
    public function all_party($aid = null) {
        $aid = $this->input->post('aid');
        $data = $this->Global_function_model->Sel_Users($aid);
        echo json_encode($data);
    }

}
