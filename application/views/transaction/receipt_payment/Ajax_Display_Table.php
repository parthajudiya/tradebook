	
	<table id="example2" class="table table-bordered table-hover MyTable">

		<thead>
			<tr>
			<th><input type="checkbox" id="select_all" value=""/></th> 
			<th><?=$this->lang->line("Expiry_Exchange"); ?></th>
			<th>Symbol</th>
			<th><?=$this->lang->line("Expiry_ex_date"); ?></th>
			<th>Status</th>
		
			<th><?=$this->lang->line("Action") ?></th>
			</tr>
		</thead>

		<tbody>
			<?php foreach($expiries as $row) { ?>
			
			<?php 
				$ex_dt1 = explode('-', $row->ex_date);
				$ex_dt1 = "$ex_dt1[2]-$ex_dt1[1]-$ex_dt1[0]";
			?>

			<tr> 
				<td><input type="checkbox"  name="checked_id[]" class="checkbox" value="<?php echo  $row->expiries_id; ?>"/></td>
				
				<td onclick="Edit(<?php echo $row->expiries_id; ?>)"><?php  echo  $row->name; ?></td>
				<td onclick="Edit(<?php echo $row->expiries_id; ?>)"><?php  echo  $row->symbol; ?></td>
				<td onclick="Edit(<?php echo $row->expiries_id; ?>)"><?php  echo  $ex_dt1; ?></td>
					
					<?php 
					if($row->status=="0")
					{
					?>	
				
						<td onclick="Edit(<?php echo $row->expiries_id; ?>)"><span class="badge bg-success">Active</span></td>
					<?php 	
					}
					else // 1
					{
					?>
						<td onclick="Edit(<?php echo $row->expiries_id; ?>)"><span class="badge bg-danger">Deactive</span></td>
					<?php 	
					}	
				?>

					<td>
					<button  type="button"  class="btn btn-sm btn-default" data-toggle="tooltip"  data-placement="left" title="Delete" data-original-title="Delete"  onclick="Delete(<?php echo $row->expiries_id; ?>)">
					<i class="fa fa-trash"></i>
					</button>
					</td>
			</tr>
			<?php  } ?>
		</tbody>

	</table>
					
					
<script src="<?php echo base_url(); ?>assets/customjs/multi_chkbox.js"></script>


