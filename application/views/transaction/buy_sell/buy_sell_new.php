<?php  $this->load->view("include/header_popup"); ?>

<?php

$TxtBoxSmll = "form-control form-control-sm";

$Qty1 = [
    'type' => 'number',
    "name" => "qty1",
    'id' => "qty1",
    'placeholder' => "Quantity",
	 'class' => $TxtBoxSmll,
	 'value'=>$datas['qty1'], 
]; 

$Rate1 = [
    'type' => 'text',
    "name" => "rate1",
    'id' => "rate1",
    'placeholder' => "Rate",
	 'class' => $TxtBoxSmll,
	 'value'=>$datas['rate1']
]; 

$pid = [
    'type' => 'hidden', // ***
    "name" => "pid",
    'id' => "pid",
    'placeholder' => "pid",
	'value'=>$datas['pid'] 
	
]; 

$Party_Code = [
    'type' => 'text',
    "name" => "party_code",
    'id' => "party_code",
    'placeholder' => "Party Code",
	 'class' => $TxtBoxSmll,
	 'value'=>$datas['party_code'] 
	 
];

$Party_Name  = [
    'type' => 'text',
    "name" => "party_name",
    'id' => "party_name",
    'placeholder' => "Party Name",
	 'class' => $TxtBoxSmll,
	 'value'=>$datas['party_name'] 
]; 

$bid = [
    'type' => 'hidden', // ***
    "name" => "bid",
    'id' => "bid",
    'placeholder' => "bid",
	  // 'value'=>$bro_code['aid'],
	  'value'=>$datas['bid'] 
];

$Brokrage_Code = [
    'type' => 'text',
    "name" => "brokrage_code",
    'id' => "brokrage_code",
    'placeholder' => "Brokrage Code",
	 'class' => $TxtBoxSmll,
	// 'value'=>$bro_code['code'],
	'value'=>$datas['brokrage_code'] 
	
	
];

$Brokrage_Name  = [
    'type' => 'text',
    "name" => "brokrage_name",
    'id' => "brokrage_name",
    'placeholder' => "Brokrage Name",
	 'class' => $TxtBoxSmll,
	 //'value'=>$bro_code['username'],
	 'value'=>$datas['brokrage_name'] 
	 
];

$Re_No = [
    'type' => 'text',
    "name" => "re_no",
    'id' => "re_no",
    'placeholder' => "Re no",
	 'class' => $TxtBoxSmll,
	 'value'=>$datas['re_no'] 
];


$Remark = [
    'type' => 'text',
    "name" => "remark",
    'id' => "remark",
    'placeholder' => "Remark",
	 'class' =>$TxtBoxSmll,
	 	'value'=>$datas['remark'] 
];


$Instument = [
    'type' => 'text',
    "name" => "instument",
    'id' => "instument",
    'placeholder' => "instument",
	 'class' => $TxtBoxSmll,
	 'readonly'=>'readonly',
	 	'value'=>$datas['instument'] 
];


	
			
$dt = explode('-', $datas['date1']);
$dt = "$dt[2]-$dt[1]-$dt[0]";
$Date1 = [
    'type' => 'text',
    "name" => "date1",
    'id' => "date1",
    'placeholder' => "Date",
	'class' => $TxtBoxSmll,
	'value'=>$dt
];




$BuySell_id = [
    'type' => 'hidden',   //**
    "name" => "buysell_id",
    'id' => "buysell_id",
	'value'=>$datas['buysell_id'] 
];
 
 


?>


	<div class="content-wrapper">

		
			<div class="col-md-12" style="padding-top: 6px;">
			<div class="card card-primary card-outline"> 
				
				<!--
				<div class="card-header" style="padding-top: 0px;padding-bottom: 0px;">
				<h3 class="card-title"><?php // echo $this->lang->line("BuySell_Title"); ?> </h3>
				
				<div class="card-tools">
				<button style="" class="btn btn-primary btn-sm viewbtn" id="viewbtn"  type="button"> View </button>
				</div>
				
				</div>
				-->

					<div class="card-body" style="padding-left: 20px;padding-top: 10px;">
					<form id="FormUpdate" autocomplete="off">  
					 <?php  echo form_input($BuySell_id); ?>
				
						
						<div class="row">
						
							<div class="col-sm-1">
							<div class="form-group">
								<label><?php echo $this->lang->line("label_Exchange"); ?></label>
								
								<select class="form-control select2" tabindex="0" style="width: 100%;" name="exchange_id" id="exchange_id">
								
								<option value="">Exchange</option>
								
								<?php 
								foreach ($Exchange as $row_B)
									{
										if($datas['exchange_id'] == $row_B->exchange_id)
										{
									?>	
										<option value="<?php echo $row_B->exchange_id; ?>" selected><?php echo strtoupper($row_B->name);?></option>
									<?php
										}
										else
										{
											?>
											<option value="<?php echo $row_B->exchange_id; ?>"><?php echo strtoupper($row_B->name);?></option>
											<?php
										}	
									}
								?>
								</select>
							</div>
						</div>
						
						
						
						<div class="col-sm-3">
							<div class="form-group">
								<label><?php echo $this->lang->line("label_settlment"); ?></label>
									<select class="form-control" id="setlement_id" name="setlement_id" tabindex="1" >
										<option value="">Select Settlment</option>
									<?php 	
									foreach ($Settlement as $row_C)
									{
										if($datas['setlement_id'] == $row_C->setlement_id)
										{	
									?>
										<option value="<?php echo $row_C->setlement_id; ?>" selected><?php echo $row_C->description?></option>
									<?php	
										}
										else
										{
										?>
										<option value="<?php echo $row_C->setlement_id; ?>"><?php echo $row_C->description?></option>
										<?php	
											
										}	
									}
									
									?>
										
									</select>
							</div>
						</div>
						
						
							<div class="col-sm-1">
							<div class="form-group">
							<label><?php echo $this->lang->line("label_Tr_No"); ?></label>
							<input type="text" name="tr_no" class="form-control" id="tr_no" value="<?php echo  $datas['tr_no'];?>" disabled>
							 
							</div>
							</div>
							
							<div class="col-sm-1">
							<div class="form-group">
							<label><?php echo $this->lang->line("label_Type"); ?></label>
							<select class="form-control " style="width: 100%;" tabindex="2" name="type" id="type">
							
								 
								<option value="" >Type</option>
								
								<?php 
								if($datas['type'] == "1")
								{
									?>
										<option value="1" selected>RL</option>
										<option value="2">FW</option>
										<option value="3" >CF</option>
										<option value="4" >BF</option>
									<?php		
								}	
								elseif($datas['type'] == "2")
								{
									?>
										<option value="1">RL</option>
										<option value="2" selected>FW</option>
										<option value="3" >CF</option>
										<option value="4" >BF</option>
									<?php		
								}	
								elseif($datas['type'] == "3")
								{
									?>
										<option value="1">RL</option>
										<option value="2">FW</option>
										<option value="3" selected>CF</option>
										<option value="4" >BF</option>
									<?php		
								}
								else // 4
								{
									?>
											<option value="1">RL</option>
											<option value="2">FW</option>
											<option value="3" >CF</option>
											<option value="4" selected>BF</option>
									<?php		
								}
								?>
								
							</select>
							</div>
							</div>
							
							<div class="col-sm-1">
							<div class="form-group">
							<label><?php echo $this->lang->line("label_Date"); ?></label>
							<?php  echo form_input($Date1); ?>
							</div>
							</div>
							
							
							
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label><?php echo $this->lang->line("label_BuySell"); ?></label>
							<select class="form-control" style="width: 100%;" name="buy_sell_id" tabindex="3" id="buy_sell_id">
								<option value="" >Select</option>
								
								<?php 
								if($datas['buy_sell_id'] == "1")
								{
									?>
										<option value="1" selected>Buy</option>
										<option value="2">Sell</option>
										
									<?php		
								}	
								else // 2
								{
									?>
										<option value="1">Buy</option>
										<option value="2" selected>Sell</option>
									<?php		
								}
								
								?>
								
							</select>
							</div>
							</div>
							
							<div class="col-sm-2">
							<div class="form-group">
								<label><?php echo $this->lang->line("label_Symbol"); ?></label>
									<select class="form-control select2" tabindex="4" style="width: 100%;" id="symbol_id" name="symbol_id" >
										<option value="">Select Symbol</option>
										<?php 	
										foreach ($Symbol as $row_D)
										{
											if($datas['symbol_id'] == $row_D->symbol_id)
											{	
											?>
											<option value="<?php echo $row_D->symbol_id; ?>" selected><?php echo $row_D->symbol;?></option>
											<?php 
											}
											else
											{
											?>
												<option value="<?php echo $row_D->symbol_id; ?>"><?php echo $row_D->symbol;?></option>
											<?php	
											}	
										}	
										?>
										
									</select>
							</div>
							</div>
						
							
							<div class="col-sm-1">
							<div class="form-group">
							<label><?php echo $this->lang->line("label_Instument"); ?></label>
							<?php  echo form_input($Instument); ?>
							</div>
							</div>
							
							<div class="col-sm-1">
							<div class="form-group">
								<label><?php echo $this->lang->line("label_Expiry"); ?></label>
								
								
								<select class="form-control" tabindex="5" name="ex_date" id="ex_date" style="width: 100%; padding: 0.1rem 0.4rem;font-size: .700rem;">
								
								
										<?php 	
										foreach ($Expiries as $row_E)  
										{
											if($datas['ex_date'] == $row_E->ex_date)
											{	
											?>
											<option value="<?php echo $row_E->expiries_id; ?>" selected><?php echo $row_E->ex_date;?></option>
											<?php 
											}
											else
											{
											?>
												<option value="<?php echo $row_E->expiries_id; ?>"><?php echo $row_E->ex_date;?></option>
											<?php	
											}	
										}	
										?>
								
							
								</select>
								
								</div>
							</div>
							
						
							<div class="col-sm-1">
							<div class="form-group">
							<label>Qty</label>
							<?php  echo form_input($Qty1); ?>
							</div>
							</div>
							
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>Rate</label>
							<?php  echo form_input($Rate1); ?>
							</div>
							</div>
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>Party Code</label>
							<?php  echo form_input($pid); ?>
							<?php  echo form_input($Party_Code); ?>
							</div>
							</div>
							
							
							<div class="col-sm-2">
							<div class="form-group">
							<label>Party Name</label>
							<?php  echo form_input($Party_Name); ?>
							</div>
							</div>
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>Broker Code</label> 
							<?php  echo form_input($bid); ?>
							<?php  echo form_input($Brokrage_Code); ?>
							</div>
							</div>
							
							
							<div class="col-sm-2">
							<div class="form-group">
							<label>Broker Name</label>
							<?php  echo form_input($Brokrage_Name); ?>
							</div>
							</div>
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>Re.No</label>
							<?php  echo form_input($Re_No); ?>
							</div>
							</div>
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>Remark</label>
							<?php  echo form_input($Remark); ?>
							</div>
							</div>
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>&nbsp;</label><br/>
							<button id="Submit" type="submit" tabindex="6" class="btn btn-sm btn-primary" style="padding: .18rem .5rem !important;">Submit</button>
							</div>
							</div>
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>&nbsp;</label><br/>
							<button style="" class="btn btn-primary viewbtn" id="viewbtn"  type="button"> View </button>
							</div>
							</div>
						
						</div>
							
						
							
					</form>  
					</div>
					
			</div>
			</div>


	

	
		
		<div id="ViewTable">
		<section class="content">
		<div class="container-fluid">
		<div class="row">
		<div class="col-12">

				<div class="card card-primary card-outline">
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("BuySell_Title"); ?></h3>
				
				
				
				
				<div class="card-tools">
				<button style="" class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><?php  echo $this->lang->line("Action"); ?>
				<span class="caret"></span></button>
				
				
				

					<ul class="dropdown-menu" style="cursor: pointer;">
					<li align="center" ><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"> <?php  echo $this->lang->line("Delete_Bulk"); ?></a></li>
					</ul>
				</div>
				
				</div>
					<div class="card-body" id="Response"></div>  
					</div>

		</div>
		</div>
		</div>
		</section>
		</div>
			
		
		
	
	</div>  <!-- content-wrapper End -->
	
	
	
	
	

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/buy_sell.js"></script>



<script>
	$(document).ready(function(){
		$(document).on('change focusout',  '#party_code', function(event) {
			
			var codeVal = $(this).val();
				if(codeVal.length){
						$.ajax({
							url: base_url+"transaction/Buy_sell/party_code_search/",
							type: 'post',
							data: {code: codeVal},
							dataType: "json",
							success:function(data)
							{
								
								if (!$.trim(data))
								{   
									$('#party_name').val("");
									$('#pid').val("");
									$('#party_code').val("");
								}
								else
								{
									$("#party_name").val(data.username);
									$("#pid").val(data.aid);
								}
							}
						});
					}
				});
			});
</script>


<script>
	$(document).ready(function(){
		$(document).on('change focusout',  '#brokrage_code', function(event) {
			
			var codeVal = $(this).val();
				if(codeVal.length){
						$.ajax({
							url: base_url+"transaction/Buy_sell/brokrage_code_search/",
							type: 'post',
							data: {code: codeVal},
							dataType: "json",
							success:function(data)
							{
								
								if (!$.trim(data))
								{   
									$('#bid').val("");
									$('#brokrage_name').val("");
									$('#brokrage_code').val("");
								}
								else
								{
									$("#brokrage_name").val(data.username);
									$("#bid").val(data.aid);
								}
							}
						});
					}
				});
			});
</script>


<script>


var setlement_id_global;
var instument=[];

		$(document).ready(function(){
 
            $('#exchange_id').change(function(){  // cate
                var id=$(this).val();
                $.ajax({
					url: base_url+"transaction/Buy_sell/setlement",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
						var i;
						for(i=0; i<data.length; i++){
				
						html += '<option value='+data[i].setlement_id+' '+((setlement_id_global==data[i].setlement_id)?"selected":"")+'>'+data[i].description+'</option>';
						}
                        $('#setlement_id').html(html);
					}
                });
                return false;
            }); 
         });





		var symbol_id_global;
		
		$(document).ready(function(){
 
            $('#exchange_id').change(function(){  // cate
                var id=$(this).val();
                $.ajax({
					url: base_url+"transaction/Buy_sell/Symbol_Search",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
						var i;
						html += '<option value=""  >Select Symbol</option>'; // selected disabled
						for(i=0; i<data.length; i++){
						instument[data[i].symbol_id]=data[i].instument 
						html += '<option value='+data[i].symbol_id+' '+((symbol_id_global==data[i].symbol_id)?"selected":"")+'>'+data[i].symbol+'</option>';
						}
							$('#symbol_id').html(html)			
						
						 }
                });
                return false;
            }); 
      });

var ex_date_global = [];

		$(document).ready(function(){
			$(document).on('change',  '#symbol_id', function() { 
		
				var id=$(this).val();
                var exchange_id=$("#exchange_id").val();
                $.ajax({
					url: base_url+"transaction/Buy_sell/Symbol_OnChange",
                    method : "POST",
                    data : {id: id,exchange_id:exchange_id},
                    async : true,
                    dataType : 'json',
                    success: function(data)
					{
					
                        var html = '';
                        var i;
						for(i=0; i<data.length; i++){
						html += '<option value='+data[i].ex_date+' '+((ex_date_global==data[i].ex_date)?"selected":"")+'>'+data[i].ex_date+'</option>';
						}
						$('#ex_date').html(html);
					}
                });
                return false;
            }); 
      });
	  
			

</script>




<script>

	  $(document).ready(function()
	  {
			$("#party_name").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"transaction/Buy_sell/Search_Account_Name/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
						
							
							$('#party_name').val("");
							$('#pid').val("");
							$('#party_code').val("");
							toastr.error('Please Enter Correct Acccount Name')
					
						}
						else
						{
							
							response(data);
						}
					}
				});
			},
			select: function (event, ui) 
			{
				$('#pid').val(ui.item.value);
				$('#party_name').val(ui.item.label);
				$('#party_code').val(ui.item.party_code);
				return false;
			}
		});
	});
 
	  $(document).ready(function()
	  {
			$("#party_code").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"transaction/Buy_sell/Search_Account_Code/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							// If Wrong Data Type In Text Box Will null
							$('#pid').val("");  
							$('#party_code').val("");
							$('#party_name').val("");
							toastr.error('Please Enter Correct Acccount Code')
					
						}
						else
						{
							
							response(data); 
						}
					}
				});
			},
			select: function (event, ui) 
			{
				$('#pid').val(ui.item.value);
				$('#party_code').val(ui.item.label);
				$('#party_name').val(ui.item.party_name);
				return false;
			}
		});
	});

  
</script>


<script>


	  $(document).ready(function()
	  {
			$("#brokrage_name").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"transaction/Buy_sell/Search_Broker_Name/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							// If Wrong Data Type In Text Box Will null 
							
							$('#brokrage_name').val("");
							$('#bid').val("");
						
							$('#brokrage_code').val("");
							toastr.error('Please Enter Correct Broker Name')
					
						}
						else
						{
							
							response(data); 
						}
					}
				});
			},
			select: function (event, ui) 
			{
				$('#bid').val(ui.item.value);
				$('#brokrage_name').val(ui.item.label);
				// $('#uid').val(ui.item.value);
				
				$('#brokrage_code').val(ui.item.brokrage_code); // party_code
				return false;
			}
		});
	});

	  $(document).ready(function()
	  {
			$("#brokrage_code").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"transaction/Buy_sell/Search_Brokrage_Code/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							

							$('#brokrage_code').val("");
							$('#brokrage_name').val("");
							toastr.error('Please Enter Correct Brokrage  Code')
					
						}
						else
						{
							response(data); 
						}
					}
				});
			},
			select: function (event, ui) 
			{
				$('#bid').val(ui.item.value);
				$('#brokrage_code').val(ui.item.label);
			
				$('#brokrage_name').val(ui.item.brokrage_name);
				return false;
			}
		});
	});
	

</script>



<script>
		$(document).on('change',  '#symbol_id', function() { 
			var id=$(this).val();
			$("#instument").val(instument[id]);
      });
	
</script>





<style>
div.dataTables_wrapper div.dataTables_length select
{
	padding: 0px 25px 0px 8px !important;
}

.form-control-sm
{
	border: 1px solid #2196F3;
}

.form-control
{
	height: calc(1.8125rem + 2px);
    padding: .25rem .5rem;
    font-size: .875rem;
    line-height: 1.5;
    border-radius: .2rem;
	border: 1px solid #2196F3;
}

.select2-container--bootstrap4 .select2-selection
{
border: 0px solid #ced4da;
}

.select2-container--bootstrap4 .select2-selection--single .select2-selection__rendered
{
	 height: calc(1.8125rem + 2px);
    padding: .25rem .5rem;
    font-size: .875rem;
    line-height: 1.5;
    border-radius: .2rem;
	border: 1px solid #2196F3;
	
}	
.select2-container--bootstrap4.select2-container--focus .select2-selection
{
	box-shadow: none !important;
}

.select2-container--bootstrap4 .select2-selection--single .select2-selection__arrow b
{
margin-top: -5px;	
}

.form-group
{
	margin-bottom: 0px;
}
label
{
	margin-bottom: 0px !important;
}
label:not(.form-check-label):not(.custom-file-label) {
    font-weight: 700;
    font-size: 12px !important;
}
.card-title
{
	    font-size: 1.2em;
}
.card-footer
{
	padding-bottom: 0px !important;
}
#viewbtn
{
	padding: 0.0rem .4rem !important;
	margin: 2px;
}
.col-sm-1
{
	padding-right: 0px !important;
}
.card-primary.card-outline
{
	border-top: 4px solid #007bff;
}
</style>


	





<?php  $this->load->view("include/footer_popup"); ?>