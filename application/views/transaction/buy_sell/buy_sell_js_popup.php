<?php $this->load->view("include/header_popup"); ?>
<style>
    .lbl_black
    {
        color:black;
    }
</style>
<?php
$TxtBoxSmll = "form-control form-control-sm";

$Qty1 = [
    'type' => 'number',
    "name" => "qty1",
    'id' => "qty1",
    'placeholder' => "Quantity",
    'class' => $TxtBoxSmll,
];

$Rate1 = [
    'type' => 'text',
    "name" => "rate1",
    'id' => "rate1",
    'placeholder' => "Rate",
    'class' => $TxtBoxSmll,
];

$pid = [
    'type' => 'text',
    "name" => "pid",
    'id' => "pid",
    'placeholder' => "pid"
];

/* * * ## ** */
/*
$Party_Code = [
    'type' => 'text',
    "name" => "party_code",
    'id' => "party_code",
    'placeholder' => "Party Code",
    //'class' => $TxtBoxSmll
    'class' => "form-control",
];


$Party_Name = [
    'type' => 'text',
    "name" => "party_name",
    'id' => "party_name",
    'placeholder' => "Party Name",
    //'class' => $TxtBoxSmll,
    'class' => "form-control",
    "data-plus-as-tab" => "false"
];
*/

$bid = [
    'type' => 'hidden',
    "name" => "bid",
    'id' => "bid",
    'placeholder' => "bid",
    'value' => $bro_code['aid'],
];

$Brokrage_Code = [
    'type' => 'text',
    "name" => "brokrage_code",
    'id' => "brokrage_code",
    'placeholder' => "Brokrage Code",
    'class' => $TxtBoxSmll,
    'value' => $bro_code['code']
        //echo  $Count_Variable['count_rows'];
];

$Brokrage_Name = [
    'type' => 'text',
    "name" => "brokrage_name",
    'id' => "brokrage_name",
    'placeholder' => "Brokrage Name",
    'class' => $TxtBoxSmll,
    'value' => $bro_code['username'],
];

if ($Count_Variable['count_rows'] > 0) {
    $no = $Count_Variable['count_rows'];
} else {
    $no = "1";
}

$Re_No = [
    'type' => 'text',
    "name" => "re_no",
    'id' => "re_no",
    'placeholder' => "Re no",
    'class' => $TxtBoxSmll,
    'value' => $no,
];

$Instument = [
    'type' => 'text',
    "name" => "instument",
    'id' => "instument",
    'placeholder' => "",
    'class' => $TxtBoxSmll,
    'readonly' => 'readonly'
];

$Symbol = [
    'type' => 'text',
    "name" => "symbol",
    'id' => "symbol",
    'placeholder' => "Symbol",
    'class' => "form-control",
];

$Date1 = [
    'type' => 'text',
    "name" => "date1",
    'id' => "date1",
    'placeholder' => "Date",
    'class' => $TxtBoxSmll,
    'value' => "" . date("d-m-Y") . "",
];

$BuySell_id = [
    'type' => 'hidden',
    "name" => "buysell_id",
    'id' => "buysell_id"
];
?>
<div class="content-wrapper">  <!-- content-wrapper Start -->
    <div class="col-md-12" style="padding-top: 6px;">
        <div class="card card-primary card-outline" style="border-top: 0px !important;"> 
            <div class="card-body" id="topform" style="padding-left: 20px;padding-top: 10px;">
                <form id="FormSubmit" autocomplete="off" data-plus-as-tab="true">  
                    <?php echo form_input($BuySell_id); ?>
                    <div class="row">

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_Exchange"); ?></label>

                                <select class="form-control " tabindex="0" style="width: 100%;" name="exchange_id" id="exchange_id">

                                    <!-- select2
                                    <option value="" >Exchange</option>
                                    -->

                                    <?php
                                    foreach ($Exchange as $row_B) {
                                        ?>	
                                        <option value="<?php echo $row_B->exchange_id; ?>" ><?php echo strtoupper($row_B->name); ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_settlment"); ?></label>
                                <select class="form-control" id="setlement_id" name="setlement_id" tabindex="1" >
                                    <!-- <option value="">Select Settlment</option> -->
                                </select>
                            </div>
                        </div>

                        <!--
                        <div class="col-sm-1">
                        <div class="form-group">
                        <label><?php // echo $this->lang->line("label_Tr_No");        ?></label>
                        <input type="text" name="tr_no" class="form-control" id="tr_no" value="<?php // echo  $Count_Variable['count_rows'];       ?>" disabled>
                         
                        </div>
                        </div>
                        -->
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_Type"); ?></label>
                                <select class="form-control " style="width: 100%;" tabindex="2" name="type" id="type">

                                    <!--
                                            <option value="" >Type</option>
                                    -->
                                    <option value="1">RL</option>
                                    <option value="2">FW</option>
                                    <option value="3">CF</option>
                                    <option value="4">BF</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_Date"); ?></label>
                                <?php echo form_input($Date1); ?>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_BuySell"); ?></label>
                                <select class="form-control" style="width: 100%;" name="buy_sell_id" tabindex="3" id="buy_sell_id">
                                    <!--
                                            <option value="" >Select</option>
                                    -->
                                    <option value="1">Buy</option>
                                    <option value="2">Sell</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_Symbol"); ?></label>
                                <select class="form-control select2" tabindex="4" style="width: 100%;" id="symbol_id" name="symbol_id" >
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_Instument"); ?></label>
                                <?php echo form_input($Instument); ?>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("label_Expiry"); ?></label>

<!-- <select class="form-control" tabindex="5" name="ex_date" id="ex_date" style="width: 100%; "> 15 -DEC -->

                                <select class="form-control" tabindex="5" name="expiries_id" id="expiries_id" style="width: 100%;">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>Qty</label>
                                <?php echo form_input($Qty1); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>Rate</label>
                                <?php echo form_input($Rate1); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>Party Code</label>
                                <?php echo form_input($pid); ?>
                                <?php //echo form_input($Party_Code); ?>
                                <select class="form-control select2"  style="width: 100%;" id="party_code" name="party_code" >
                                </select>

                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Party Name</label>
                                <?php //echo form_input($Party_Name); ?>
                                <select class="form-control select2"  style="width: 100%;" id="party_name" name="party_name" >
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>Broker Code</label> 
                                <?php echo form_input($bid); ?>
                                <?php echo form_input($Brokrage_Code); ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Broker Name</label>
                                <?php echo form_input($Brokrage_Name); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>Re.No</label>
                                <?php echo form_input($Re_No); ?>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>&nbsp;</label><br/>

                                <button id="Submit" type="submit" tabindex="6" class="btn btn-sm btn-primary submit" style="padding: .18rem .5rem !important;" data-plus-as-tab="false">Submit</button> &nbsp;

                                <button style="padding: .18rem .5rem !important;" class="btn  btn-sm btn-primary  viewbtn" id="viewbtn"  type="button"> List </button>
                            </div>
                        </div>

                    </div>
                </form> 
            </div>

        </div>
    </div>
    <div id="ViewTable">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title"><?php echo $this->lang->line("BuySell_Title"); ?></h3>

                                <div class="card-tools">				

                                    <button style="" class="btn btn-primary btn-sm" id="Close" type="button"> Close</button>		
                                    <ul class="dropdown-menu" style="cursor: pointer;">
                                        <li align="center" ><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"> <?php echo $this->lang->line("Delete_Bulk"); ?></a></li>
                                    </ul>
                                </div>

                            </div>
                            <div class="card-body" id="Response"></div>  
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>  <!-- content-wrapper End -->

<script>
    var url = url;
    var base_url = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/buy_sell.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/plusastab.joelpurra.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/emulatetab.joelpurra.js"></script>
<script>

    JoelPurra.PlusAsTab.setOptions({

        key: 13
    });

    /*
     $("#FormSubmit").submit(simulateSubmitting);
     function simulateSubmitting(event) // event
     {
     event.preventDefault();
     FormAdd();
     return false;
     }
     */
</script>
<script>
// Add Record Start
    $(document).ready(function ()
    {

        $(document).on('submit', '#FormSubmit', function () {
            var exchange_id = $("#exchange_id").val();
            var setlement_id = $("#setlement_id").val();
            //	var tr_no = $("#tr_no").val();
            var type = $("#type").val();
            var date1 = $("#date1").val();
            var buy_sell_id = $("#buy_sell_id").val();
            var symbol_id = $("#symbol_id").val();
            var instument = $("#instument").val();
            var expiries_id = $("#expiries_id").val();
            // var ex_date = $("#ex_date").val();

            var qty1 = $("#qty1").val();
            var rate1 = $("#rate1").val();
            var pid = $("#pid").val();
            var party_code = $("#party_code").val();
            var party_name = $("#party_name").val();
            var bid = $("#bid").val();
            var brokrage_code = $("#brokrage_code").val();
            var brokrage_name = $("#brokrage_name").val();
            var re_no = $("#re_no").val();
            //var remark = $("#remark").val();

            //  alert(remark);

            // '&tr_no='+ tr_no + + '&remark='+ remark
            var dataString = 'exchange_id=' + exchange_id + '&setlement_id=' + setlement_id + '&type=' + type + '&date1=' + date1 + '&buy_sell_id=' + buy_sell_id + '&symbol_id=' + symbol_id + '&instument=' + instument + '&expiries_id=' + expiries_id + '&qty1=' + qty1 + '&rate1=' + rate1 + '&pid=' + pid + '&party_code=' + party_code + '&party_name=' + party_name + '&brokrage_code=' + brokrage_code + '&brokrage_name=' + brokrage_name + '&re_no=' + re_no + '&bid=' + bid;

            $.ajax({
                type: "POST",
                url: base_url + "transaction/Buy_sell/Add",
                data: dataString,
                cache: false,

                success: function (data)
                {
                    // alert(data);
                    /* */
                    if (data == 1)
                    {
                        $("#re_no").attr("value", parseInt(re_no) + 1);
                        $("#qty1").val("");
                        $("#rate1").val("");
                        $("#party_name").val("");
                        $("#party_code").val("");
                        $("#pid").val("");

                        $("#buy_sell_id").val("1").trigger('change');

                        // $("#FormSubmit")[0].reset();  // Form Clear after insert 
                        //load_data(); // Load Ajax
                        //$("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down
                        // $("#setlement_id").val("").trigger('change'); // After Change value Null In Droup Down
                        // alert(data);
                        singlerecord();
                        $(function () {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            toastr.success('Record Added Successfully.')
                        });
                        load_data(); // Load AJAX
                    }
                }
            });
            return false;
        });
<?php
$edit = $this->input->get("id", null);
if (!is_null($edit)) {
    echo "Edit($edit)";
}
?>
    });
// }		 
    // Add Record End
</script>
<script>
    /*
     $(document).ready(function(){
     $(document).on('click', '#Search', function() {	
     
     var From_Date = $("#From_Date").val();
     var To_Date = $("#To_Date").val();
     var Type_Search = $("#Type_Search").val();
     var Client_Code_Search = $("#Client_Code_Search").val();
     
     //console.log(Type_Search);
     
     $.ajax({  
     type: "POST",  
     url: base_url+"transaction/Buy_sell/search_record",	
     data: {From_Date:From_Date,To_Date:To_Date,Type_Search:Type_Search,Client_Code_Search:Client_Code_Search},
     success: function(data) {   
     
     //load_data();
     
     
     $('#Response').html(data);
     $('.MyTable').DataTable({
     "paging": true,
     "lengthChange": true,
     "searching": true,
     "ordering": true,
     "info": true,
     "autoWidth": false,
     "responsive": true,
     "aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
     "iDisplayLength": 5
     });
     
     
     
     
     }
     });
     });
     });
     */
</script>
<?php $this->load->view("include/footer_popup"); ?>