<?php
$Start_Date  = [
    'type' => 'text',
    "name" => "start_date",
    'id' => "start_date",
	'class'=>"form-control",
	'placeholder'=>"From Date",
  'value'  => "".date("d-m-Y")."",
];


$End_Date  = [
    'type' => 'text',
    "name" => "end_date",
    'id' => "end_date",
	'class'=>"form-control",
	'placeholder'=>"To Date",
  'value'  => "".date("d-m-Y")."",
];



$Code  = [
    'type' => 'text',
    "name" => "party_code",
    'id' => "party_code",
	'class'=>"form-control",
	'placeholder'=>"Account	Code",
];

$Name  = [
    'type' => 'text',
    "name" => "party_name",
    'id' => "party_name",
	'class'=>"form-control",
	'placeholder'=>"Account Name",
];
?>


<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1>Party Ledger</h1>
			</div>
		</div>
		</div>
		</section>


			<div class="col-md-12">
			<div class="card card-primary card-outline">
			
					<div class="card-body">
					<form id="FormSubmit" name="FormSubmit" autocomplete="off">

					<div class="row">
						<div class="col-sm-2">
							<div class="form-group">
							<label>Exchange</label>
									<select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
									<?php
									foreach ($Exchange as $row_4)
									{
									?>
										<option value="<?php echo $row_4->exchange_id; ?>" ><?php echo strtoupper($row_4->name);?></option>
									<?php
									}
								?>

								</select>
							</div>
						</div>

					<div class="col-sm-2">
					<div class="form-group">
						<label>From Date</label>
						<?php  echo form_input($Start_Date); ?>
					</div>
					</div>

				<div class="col-sm-2">
					<div class="form-group">
						<label>To Date</label>
							<?php  echo form_input($End_Date); ?>
					</div>
				</div>

	         </div>

			<div class="row">
			
			<div class="col-sm-2">
				<div class="form-group">
				<label>A/c Code</label>
				<?php  echo form_input($Code); ?>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-group">
				<label>A/c Name</label>
				<?php  echo form_input($Name); ?>
				</div>
			</div>

			<div class="col-sm-2">
				<label>&nbsp;</label><br>
				<button id="Submit" type="submit" class="btn btn-primary">Show</button>
			</div>
			
		</div>
		
		</form>
		</div>

		</div>
		</div>

			
			<section class="content">
				<div class="container-fluid">
				<div class="row">
				<div class="col-12">
				
				<div class="card card-primary card-outline">
				<div class="card-header">
				<h3 class="card-title">Party Ledger</h3>
				<div class="card-tools">
				<button id="PrintBtn" type="button" class="btn btn-sm btn-info">Print</button>
				</div>
				</div>

			
				<div class="card-body" id="Response1">  </div>

				</div>

				</div>
				</div>
				</div>
			</section>
		
		
		
		
		
		
		
		
		
		
		
		
		

	</div>  <!-- content-wrapper End -->

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>

<script src="<?php echo base_url(); ?>assets/customjs/global_acc_code_to_acc_name.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/global_datepicker.js"></script>

<script src="<?php echo base_url(); ?>assets/customjs/party_ledger.js"></script>


<script>
	
		$(document).on('click', '#PrintBtn', function ()  
        {
				var exchange_id = $("#exchange_id").val();
				var arr1Dt = $("#start_date").val();
				var arr1Dt2 = $("#end_date").val();
				var party_code = $("#party_code").val();
				var party_name = $("#party_name").val();
				
			
				
				  $.ajax({
                    url: base_url + "reports/Party_ledger/Table1_Print/",
                    type: 'post',
                    data: {exchange_id:exchange_id, arr1Dt:arr1Dt, arr1Dt2: arr1Dt2 , party_code:party_code , party_name:party_name },
                    success: function (data)
                    {
						var openWin =  window.open(data);
						openWin.document.writeln(data);
					}
                });
				return false;
		});
</script>
