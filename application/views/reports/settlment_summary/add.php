<?php
$name = [
    "name" => "name",
    "autofocus" => 1,
    "class" => "form-control",
    "data-validation"=>"length,unique",
    "data-validation-length"=>"1-55",
    "value" => isset($name) ? $name : "",
];
?>
<div class="col-lg-6 col-lg-offset-3">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?= $this->lang->line("zone"); ?></h3>
        </div>
        <?= form_open("", ["data-plus-as-tab" => "true"]); ?>
        <div class="box-body">
            <div class="form-group">
                <?= form_label($this->lang->line("input_zone"), 'zone'); ?>
                <?= form_input($name); ?>
            </div>
        </div>
        <div class="box-footer">
            <?= form_submit('', 'SUBMIT', ["class" => "btn-primary", "data-plus-as-tab"=>"false"]); ?>
            <a class="btn-default"  id="backbtn"  data-plus-as-tab="false" href="<?= base_url("master/zone") ?>">EXIT</a>
        </div>
        <?= form_close(); ?>
    </div>

</div>
<script>
    var uniquename="";
    <?php if ($this->router->method=="edit") { ?>
        uniquename=$("input[name='name']").val(); 
        <?php
    } ?>

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2)
            return parts.pop().split(";").shift();
    }

    $(window).on("focus", function(){
        $('input[name="<?= $this->security->get_csrf_token_name(); ?>"]').val(getCookie('csrf_cookie_name'));
    });
	
    $.extend({
        xResponse: function (data) {
            var d = null;
            $.ajax({
                url: "<?= base_url("master/zone/json/") ?>" + encodeURIComponent(btoa(data)),
                type: 'GET',
                data:{name:encodeURIComponent(btoa(uniquename))},
                dataType: "json",
                async: false,
                success: function (respText) {
                    if (respText) {
                        d = false;
                    } else {
                        d = true;
                    }
                    return d;
                }
            });
            return d;
        }
    });
    $.formUtils.addValidator({
        name: 'unique',
        validatorFunction: function (value, $el, config, language, $form) {
            return $.xResponse(value);
        },
        errorMessage: 'this value already inserted',
        errorMessageKey: 'badEvenNumber'
    });
</script>