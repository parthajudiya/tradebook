<?php
$Start_Date  = [
    'type' => 'text',
    "name" => "start_date",
    'id' => "start_date",
	'class'=>"form-control",
	'placeholder'=>"From Date",
];


$End_Date  = [
    'type' => 'text',
    "name" => "end_date",
    'id' => "end_date",
	'class'=>"form-control",
	'placeholder'=>"To Date",
];



$Code  = [
    'type' => 'text',
    "name" => "party_code",
    'id' => "party_code",
	'class'=>"form-control",
	'placeholder'=>"Code",
];

$Name  = [
    'type' => 'text',
    "name" => "party_name",
    'id' => "party_name",
	'class'=>"form-control",
	'placeholder'=>"Name",
];
?>


<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1>Bill</h1>
			</div>
		</div>
		</div>
		</section>
		
		
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
					
				
				
					<div class="card-body">
					<form id="FormSubmit" name="FormSubmit" autocomplete="off">
					
					
						
						<div class="row">
						
						<div class="col-sm-1">
							<div class="form-group">
								<label><?php echo $this->lang->line("Brokrage_Exchange"); ?></label>
								
								<select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
								
								<!--
								<option value="" >Ex</option>
								-->
								<?php 
							
									foreach ($Exchange as $row_B)
									{
									?>	
										<option value="<?php echo $row_B->exchange_id; ?>" ><?php echo strtoupper($row_B->name);?></option>
									<?php
									}
								?>
								</select>
							</div>
						</div>
						
						
						<div class="col-sm-3">
							<div class="form-group">
								<label>Setlment</label>
								<select class="form-control " style="width: 100%;" name="setlement_id" id="setlement_id">
								<option value="">Setlment</option>
								</select>
							</div>
						</div>
						
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>From Date</label>
								<?php  echo form_input($Start_Date); ?>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>To Date</label>
									<?php  echo form_input($End_Date); ?>
							</div>
						</div>
						
						
						<div class="col-sm-1">
							<div class="form-group">
								<label>A/c Code</label>
									<?php  echo form_input($Code); ?>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>A/c Name</label>
									<?php  echo form_input($Name); ?>
							</div>
						</div>
						
							<div class="col-sm-1">
							<div class="form-group">
								<label>&nbsp;</label><br>
								<button id="Submit" type="submit" class="btn btn-primary">Show</button>
							</div>
						</div>
						
						
						
					</div>	
				</form> 
		</div>
					
		</div>
		</div>

		
		<section class="content">
		<div class="container-fluid">
		
	
		<div class="row">
		
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
				<div class="card-body" id="Response1"> 
				</div> 
				</div>
			</div>
			
			</div>
						
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-body" id="Response2">  
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
	
	</div>  <!-- content-wrapper End -->
	
	
	
<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/bill.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/global_exchange_to_settlement_to_date.js"></script>	

	<script>
		$(document).ready(function()
		{	
			$(document).on('submit', '#FormSubmit', function()  // // $(document).on('click', '#Submit', function() 
			{ 
				var exchange_id=$("#exchange_id").val();
				var setlement_id=$("#setlement_id").val();
				
				var arr1Dt =$("#start_date").val();
				var arr1Dt2 =$("#end_date").val();
				var party_code =$("#party_code").val();
				
				$.ajax({ 
					url: base_url+"reports/Bill/ResultTable", 
                    method : "POST",
                    data : {exchange_id:exchange_id,  setlement_id: setlement_id , arr1Dt:arr1Dt , arr1Dt2:arr1Dt2 , party_code:party_code},
                  
                    success: function(data , data2)
					{
					
						$('#Response1').html(data);
						$('.MyTable1').DataTable({
							"paging": false, 
							"searching": true,
							"info": true,
							"responsive": true,
							"scrollY":'20vh',
							//"scrollX": true,
						});
						
						// load_second();
					}
                });  
				return false; 
            }); 
        });
	</script>
	
	
	<script>
	function ViewTBL2_Onclick(id)
	{
		$(document).ready(function()
		{
			$.ajax({
				data:  "id="+id,
				url: base_url+"reports/Bill/ResultTable2/"+id,
				type: "POST",
				
				success: function(data)
				{
					$('#Response2').html(data);
					
							$('.MyTable2').DataTable({
						
						"footerCallback": function ( row, data, start, end, display ) {
							var api = this.api(), data;
				 
							// converting to interger to find total
							var intVal = function ( i ) {
								return typeof i === 'string' ?
									i.replace(/[\$,]/g, '')*1 :
									typeof i === 'number' ?
										i : 0;
							};
							
							
							  

				 
							// computing column Total of the complete result 
							var twoTotal = api
								.column( 2 )
								.data()
								.reduce( function (a, b) {
									return intVal(a) + intVal(b);
								}, 0 );
								
							var threeTotal = api
							.column( 3 )
							.data()
							.reduce( function (a, b) {
							return intVal(a) + intVal(b);
							}, 0 );	
							
							var fourTotal = api
							.column( 4 )
							.data()
							.reduce( function (a, b) {
							return intVal(a) + intVal(b);
							}, 0 );	
							
							var tenTotal = api
							.column( 10 )
							.data()
							.reduce( function (a, b) {
							return intVal(a) + intVal(b);
							}, 0 );	
							
							var elevenTotal = api
							.column( 11 )
							.data()
							.reduce( function (a, b) {
							return intVal(a) + intVal(b);
							}, 0 );	
							
								var twelTotal = api
							.column( 12 )
							.data()
							.reduce( function (a, b) {
							return intVal(a) + intVal(b);
							}, 0 );	
								
								var thirteenTotal = api
							.column( 13 )
							.data()
							.reduce( function (a, b) {
							return intVal(a) + intVal(b);
							}, 0 );	
								
								
							// Update footer by showing the total with the reference of the column index 
							$( api.column( 0 ).footer() ).html('Total');
							$( api.column( 2 ).footer() ).html(twoTotal);
							$( api.column( 3 ).footer() ).html(threeTotal);
							$( api.column( 4 ).footer() ).html(fourTotal);
							$( api.column( 10 ).footer() ).html(tenTotal);
							$( api.column( 11 ).footer() ).html(elevenTotal);
							$( api.column( 12 ).footer() ).html(twelTotal);
							$( api.column( 13 ).footer() ).html(thirteenTotal);
						},
						"paging": false,
					"lengthChange": true,
					"searching": true,
					"ordering": true,
					"info": true,
					"autoWidth": false,
					"responsive": true,
					
					// "paging": false, 
						// "searching": true,
						// "info": false,
						// "responsive": true,
						//"scrollY":'30vh',
						
					});
					
					
					
					
				}	
			});
			 return false; 
		});
		
	}
	</script>
	
	
	