<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: left;
            }
            #t01 tr:nth-child(even) {
                background-color: #fff;

            }
            #t01 tr:nth-child(odd) {
                background-color: #edf0ff;
            }
            #t01 th {
                background-color: #7385cd;
                color: #000;
            }
        </style>
    </head>
    <body>
	
 <table id="t01">
		<thead>
			<tr>
				
				<th><?php echo $this->lang->line("LBL_Symbol") ?></th>
				<th><?php echo $this->lang->line("LBL_Expiry") ?></th>
				<th>LTP</th>
			</tr>
		</thead>
	
		<tbody>

				<?php
				
				foreach ($rec as $r)
				{
		
				
				?>
		
		<tr> 
			
			<td><?php echo $r->symbol; ?></td>
			<td><?php echo $r->ex_date; ?></td>
			<td>0.00</td>
		</tr>
		<?php 
			}
	?>
	</tbody>
	</table>
	</body>
</html>
<script type="text/javascript">
window.print();
</script>		

