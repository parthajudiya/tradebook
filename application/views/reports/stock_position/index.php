<style>
    #PrintBtn
    {
        float: left;
    }
    .dt-button 
    {
        margin: 0px 6px 0px 5px;
    }

    #PrintBtn2
    {
        float: left;
    }


</style>
<?php
/*
  .modal-dialog {
  max-width: 1250px;
  margin: 1.75rem auto;
  }

 */
$Start_Date = [
    'type' => 'text',
    "name" => "start_date",
    'id' => "start_date",
    'class' => "form-control",
    'placeholder' => "From Date",
];
$End_Date = [
    'type' => 'text',
    "name" => "end_date",
    'id' => "end_date",
    'class' => "form-control",
    'placeholder' => "To Date",
];

$Group = [
    'type' => 'text',
    "name" => "group",
    'id' => "group",
    'class' => "form-control",
    'placeholder' => "Group",
];

$Code = [
    'type' => 'text',
    "name" => "party_code",
    'id' => "party_code",
    'class' => "form-control",
    'placeholder' => "Account Code",
];

$Name = [
    'type' => 'text',
    "name" => "party_name",
    'id' => "party_name",
    'class' => "form-control",
    'placeholder' => "Account Name",
];
?>
<div class="content-wrapper">  <!-- content-wrapper Start -->

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Stock Position</h1>
                </div>
            </div>
        </div>
    </section>
    <div class="col-md-12">
        <div class="card card-primary card-outline"> 

            <div class="card-body">
                <form id="FormSubmit" name="FormSubmit" autocomplete="off">

                    <div class="row">

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label><?php echo $this->lang->line("Brokrage_Exchange"); ?></label>

                                <select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
                                    <?php
                                    foreach ($Exchange as $row_4) {
                                        ?>	
                                        <option value="<?php echo $row_4->exchange_id; ?>" ><?php echo strtoupper($row_4->name); ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Settlment</label>
                                <select class="form-control " style="width: 100%;" name="setlement_id" id="setlement_id">
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>From Dt</label>
                                <?php echo form_input($Start_Date); ?>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>To Dt</label>
                                <?php echo form_input($End_Date); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <input type="hidden" name="aid" name="aid" id="aid"> 
                            <div class="form-group">
                                <label>A/c Code</label>
                                <?php echo form_input($Code); ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>A/c Name</label>
                                <?php echo form_input($Name); ?>
                            </div>
                        </div>
                        <div class="col-2">
                            <label>&nbsp;</label><br>
                            <button id="Submit" type="submit" class="btn btn-primary">Show</button>

                        </div>
                    </div>
                </form> 
            </div>

        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-7">

                    <div class="card card-primary card-outline">
                        <div class="card-body" id="Response1"> <!-- TABLE  1-->
                        </div> 
                    </div>

                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                            <div class="card-tools">
                            </div>
                        </div>
                        <div class="card-body" id="Response2"></div>  <!-- TABLE  2-->
                    </div>

                </div>

                <div class="col-md-5">
                    <div class="card card-primary card-outline">
                        <div class="card-body" id="Response3">  </div>  <!-- TABLE  3-->

                        <div class="card-body" >
                            <button type="button" id="Apply" name="Apply" class="btn btn-sm btn-info">Apply All LTP</button>
                            <button type="button" id="OnlineID" name="OnlineID" class="btn btn-sm btn-info">Online LTP</button>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>  <!-- content-wrapper End -->

<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="max-width: 1308px !important;">
        <div class="modal-content">
            <div class="modal-body" id="xPopup1_Data">
                <p>Loading..</p>
            </div>

        </div>
        <!--/form -->
    </div>
</div>
<script>
    var url = url;
    var base_url = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/stock_position.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/global_acc_code_to_acc_name.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/global_datepicker.js"></script>
<script>
    var Symbol_id_Global;
    function EditBuySell(id)
    {
        window.open(base_url + "transaction/Buy_sell/buy_sell_js_popup/?id=" + id, "buysell", "top=80, left=10, width=1300, height=120 , channelmode=no, toolbar=no , titlebar=no , status=no, scrollbars=yes , resizable= no , menubar = no");
    }

</script>

<script>
    $(document).ready(function ()
    {
        $(document).on('submit', '#FormSubmit', function ()  // // $(document).on('click', '#Submit', function() 
        {

            var exchange_id = $("#exchange_id").val();
            var setlement_id = $("#setlement_id").val();

            var arr1Dt = $("#start_date").val();
            var arr1Dt2 = $("#end_date").val();

            // var party_code = $("#party_code").val(); // 28-dec
            var aid = $("#aid").val(); // 28-dec

            $.ajax({
                url: base_url + "reports/Stock_position/ResultTable", // ResultTable
                method: "POST",
                data: {exchange_id: exchange_id, setlement_id: setlement_id, arr1Dt: arr1Dt, arr1Dt2: arr1Dt2, aid: aid}, // party_code
                success: function (data)
                {

                    $('#Response1').html(data);
                    $('.MyTable1').DataTable({

                        "paging": false,
                        "searching": true,
                        "info": true,
                        "responsive": false,
                        "scrollY": '20vh',
                        "scrollX": true,
                    });

                    load_second();
                    load_three();
                    // load_four();
                }
            });
            return false;
        });
    });
</script>

<script>

    // Load  Three Table
    function load_three()
    {
        var exchange_id = $("#exchange_id").val();
        var setlement_id = $("#setlement_id").val();
        var arr1Dt = $("#start_date").val();
        var arr1Dt2 = $("#end_date").val();
        var party_code = $("#party_code").val();

        $.ajax({
            url: base_url + "reports/Stock_position/load_three",
            method: "POST",
            data: {exchange_id: exchange_id, setlement_id: setlement_id, arr1Dt: arr1Dt, arr1Dt2: arr1Dt2, party_code: party_code},
            success: function (data) {

                // $('#example3').css("background", "yellow");
                $('#Response3').html(data);
                // $('.MyTable3').DataTable({
                $('#example3').DataTable({
                    "paging": false,
                    "searching": true,
                    "info": true,
                    "responsive": false,
                    // "scrollY": '20vh',
                });
            }
        });
        return false;
    }
</script>
<script>
    // TABLE 2
    function ViewTBL2(id)
    {
        $(document).ready(function ()
        {

            // alert(id);
            var party_code = $("#party_code").val();
            var exchange_id = $("#exchange_id").val();
            var setlement_id = $("#setlement_id").val();
            var arr1Dt = $("#start_date").val();
            var arr1Dt2 = $("#end_date").val();

            $.ajax({
                // data: "id=" + id,
                data: {party_code: party_code, exchange_id: exchange_id, setlement_id: setlement_id, arr1Dt: arr1Dt, arr1Dt2: arr1Dt2, id: id},
                url: base_url + "reports/Stock_position/load_second/",
                // url: base_url + "reports/Stock_position/load_second/" + id,
                type: "POST",
                success: function (data)
                {
                    $('#Response2').html(data);
                    $('.MyTable2').DataTable({
                        "footerCallback": function (row, data, start, end, display) {
                            var api = this.api(), data;

                            // converting to interger to find total
                            var intVal = function (i) {
                                return typeof i === 'string' ?
                                        i.replace(/[\$,]/g, '') * 1 :
                                        typeof i === 'number' ?
                                        i : 0;
                            };

                            // computing column Total of the complete result 
                            var FiveTotal = api
                                    .column(5)
                                    .data()
                                    .reduce(function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0);

                            var SixTotal = api
                                    .column(6)
                                    .data()
                                    .reduce(function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0);

                            var SevenTotal = api
                                    .column(7)
                                    .data()
                                    .reduce(function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0);

                            var EightTotal = api
                                    .column(8)
                                    .data()
                                    .reduce(function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0);
                            // Update footer by showing the total with the reference of the column index 
                            $(api.column(0).footer()).html('Total');
                            $(api.column(5).footer()).html(FiveTotal);
                            $(api.column(6).footer()).html(SixTotal);
                            $(api.column(7).footer()).html(SevenTotal);  // Math.fround(SevenTotal)
                            $(api.column(8).footer()).html(EightTotal);
                        },
                        "paging": false,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        // dom: 'Blfrtip',
                        // buttons: [{extend: 'excel',}],
                    });
                }
            });
        });
    }
</script>

<script>
    function PopupTbl2(id)
    {

        $('#myModal1').modal();
        $.ajax({
            data: "id=" + id,
            url: base_url + "reports/Stock_position/PopupTbl2/" + id,
            type: "POST",
            success: function (data)
            {
                $('#xPopup1_Data').html(data);
            }
        });
    }
</script>

<!-- today js end-->

<script>

    /* Global */
    //var ins_glob = [];
    var exdt_glob = [];
    var syname_glob = [];
    var syname = [];
    var exdt = [];
    var symbol_id = [];
    var symbol_id_glob = [];
    // var insname = [];
    /* Global*/

    $(document).ready(function () {
        $(document).on('click', '#OnlineID', function () {

            var setlement_id = $("#setlement_id").val();

            syname_glob.length = 0; // Clear  Data Of APPEND
            //ins_glob.length = 0; // Clear Data Of APPEND
            exdt_glob.length = 0; // Clear Data Of APPEND
            $("#example3 tbody").find("tr").each(function ()
            {

                // syname = $(this).find(".syname").val();
                syname = $(this).find(".sycode").val();
                // insname = $(this).find(".insname").text();
                exdt = $(this).find(".exdt").text();

                syname_glob.push(syname);
                // ins_glob.push(insname);
                exdt_glob.push(exdt);

            });

            $.ajax({
                url: base_url + "reports/stock_position/GetData_Online",
                method: "POST",
                data: {syname_glob: syname_glob, exdt_glob: exdt_glob}, // ins_glob:ins_glob
                // async : false,
                dataType: 'JSON',
                success: function (data) {

                    //console.log(data);

                    for (i = 0; i < data.length; i++)
                    {

                        $("#example3").find("tr").each(function ()
                        {
                            var exdt = $(this).closest("tr").find(".exdt").text();
                            var syname = $(this).closest("tr").find(".sycode").val();
                            //	var insname = $(this).closest("tr").find(".insname").text();  
                            if (data[i].expiry_date == exdt && data[i].Passing_Symbol_Code == syname) { // && data[i].instrument == insname
                                $(this).find(".rate").val(data[i].lastprice);

                            }
                        });
                    }
                }
            });
            return false;
        });
    });
</script>

<script>
    // One Table Value To Another Value Go
    $(document).ready(function ()
    {
        $(document).on('click', '#Apply', function ()
        {
            var exchange_id = $("#exchange_id").val();
            var setlement_id = $("#setlement_id").val();

            symbol_id_glob.length = 0; // Clear Data Of APPEND  // me

            // When Click Apply Example 3 No Data Example 2 ma
            $("#example3 tbody").find("tr").each(function ()
            {
                var id = $(this).find(".rate").attr("id");
                var data = $(this).find(".rate").val();
                var LtpData = $("." + id).text(data);
                // console.log(data);

            });

            var ltp_global = [];
            $("#example2 tbody").find("tr").each(function () {

                symbol_id = $(this).find(".symbol_id").val(); // for add in buy_sell table
                symbol_id_glob.push(symbol_id); // me

                var avarage = parseFloat($(this).find(".Avarage").text());
                var ltp = parseFloat($(this).find("#LtpData_TBL2").text());
                // var ltp =  parseFloat($(this).find("input[name='LtpData[]']").val());
                var Stock = parseFloat($(this).find(".stock").text());

                var Profit_Loss_Per_Qty = ltp - avarage;
                var total_Profit_Loss = Profit_Loss_Per_Qty * Stock;

                // var Profit_Loss = $(this).find(".Profit_Loss").text(total_Profit_Loss);

                if (total_Profit_Loss > 0)
                {
                    $(".M2M").css("color", "");
                    $(".M2M").css("color", "blue");

                    $(".NET").css("color", "");
                    $(".NET").css("color", "blue");
                } else
                {
                    $(".M2M").css("color", "");
                    $(".M2M").css("color", "red");

                    $(".NET").css("color", "");
                    $(".NET").css("color", "red");
                }
                var M2M = $(this).find(".M2M").text(total_Profit_Loss);
                var NET = $(this).find(".NET").text(total_Profit_Loss);

                ltp_global.push(ltp);
            });

            // ajax start
            $.ajax({
                url: base_url + "reports/stock_position/insert_ltp",
                method: "POST",
                data: {symbol_id_glob: symbol_id_glob, exchange_id: exchange_id, setlement_id: setlement_id, ltp_global: ltp_global},
                // async : false,
                // dataType : 'JSON',
                success: function (data)
                {
                    console.log("insert");
                }
            });
            return false;
            // ajax end

        });
    });

</script>