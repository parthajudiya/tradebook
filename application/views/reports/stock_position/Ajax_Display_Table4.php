<!--
<input type="button" value="All Symbol" id="All_Symbol" />
<input type="button" value="Current Symbol" id="Current_Symbol" />
<input type="button" value="Summary" id="Summary" />
-->
<table id="example4" class="table table-bordered table-hover MyTable4">
    <thead>
        <tr>
            <th>Name</th>
            <th>Stock</th>
            <th>AVG</th>
            <th>P/L</th>
            <th>M2M</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Name</th>
            <th>Stock</th>
            <th>AVG</th>
            <th>P/L</th>
            <th>M2M</th>
        </tr>
    </tfoot>
    <tbody>

        <?php
        foreach ($rec as $rA) {
            ?>
            <tr> 

                <td><?php echo $rA->party_name; ?></td>
                <td><?php echo $rA->stock; ?></td>
                <td class="Avarage">
    <?php
    if ($rA->stock > 0) {

        echo $Avarage = $rA->TotalBuyRs / $rA->stock;
    } else if ($rA->stock == 0) {
        echo "0";
    } else {

        echo $Avarage = $rA->TotalSellRs / abs($rA->stock);
    }
    ?>
                </td>

                <td>
                    <?php if ($rA->stock > 0) { ?>
                        <span class="Profit_Loss"></span>
                    <?php } else if ($rA->stock == 0) { ?>
                        <span class="Profit_Loss">
        <?php
        $brokrage = $rA->brokrage;
        $profitLoss = $rA->TotalSellRs - $rA->TotalBuyRs;
        $profitLoss_With_Brokrage = $profitLoss - $brokrage;
        echo $profitLoss_With_Brokrage;
        ?>
                        </span>
                        <?php } else {
                            ?>
                        <span class="Profit_Loss"></span>
                            <?php }
                        ?>
                </td>
                <td>.00</td>

            </tr>
                    <?php
                }
                ?>

    </tbody>
</table>




