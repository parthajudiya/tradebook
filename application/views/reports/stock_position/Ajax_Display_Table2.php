<!--
<input type="button" value="Print" id="PrintBtn2" />
-->
<table id="example2" class="table table-bordered table-hover MyTable2">
    <thead>
        <tr>
            <th>Symbol</th>
            <th>Expirey</th>
            <th>Stock</th>
            <th>Ltp</th>
            <th>AVG</th>
            <th>P/L</th>
            <th>M2M</th>
            <th>Net</th>
            <th>Diffrence-WB</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Symbol</th>
            <th>Expirey</th>
            <th>Stock</th>
            <th>Ltp</th>
            <th>AVG</th>
            <th>P/L</th>
            <th>M2M</th>
            <th>Net</th>
            <th>Diffrence-WB</th>
        </tr>
    </tfoot>
    <tbody>
        <?php
        $no = 0;
        foreach ($rec as $r) {
            $no++;
            $TotalBuyAVG = $r->TotalBuyRs;
            $TotalSellAVG = $r->TotalSellRs;
            $Brokrage = $r->brokrage;
            $Stock = $r->stock
            ?>
            <tr ondblclick="PopupTbl2('<?php echo $r->symbol_id; ?>')" > 
                <td data-symbol="<?php echo $r->sy_code; ?>"><input type="hidden" class="symbol_id" value="<?php echo $r->symbol_id; ?>"><?php echo $r->symbol; ?></td>
                <td data-ex_date="<?php echo $r->expiries_id; ?>"><?php echo $r->ex_date_value; ?></td>
                <td class="stock"><?php
                    if ($r->stock == 0) {
                        echo $r->stock;
                    } else if ($r->stock > 0) {
                        echo "<span style='color: blue;'>" . $r->stock . "</span>";
                    } else {
                        echo "<span style='color: red;'>" . $r->stock . "</span>";
                    }
                    ?></td>
                <!--
                <td><input type="number" name="LtpData[]" class="LtpData_<?php // echo $r->symbol_id;   ?>_<?php //echo $r->ex_date;   ?>" class="form-control" ></td>
                
                -->
                <?php # LTP ?>
                <td>
                    <?php
                    if ($r->stock > 0) {
                        ?>
                        <span><?php echo $r->ltp; ?></span>  <?php # 22-DEC ***    ?>
                        <span id="LtpData_TBL2" class="LtpData_<?php echo $r->sy_code; ?>_<?php echo $r->ex_date_value; ?>"></span>
                        <?php
                    } else if ($r->stock == 0) {
                        echo "0";
                    } else {
                        ?>
                        <span><?php echo $r->ltp; ?></span>  <?php # 22-DEC ***   ?>
                        <span id="LtpData_TBL2" class="LtpData_<?php echo $r->sy_code; ?>_<?php echo $r->ex_date_value; ?>"></span>
                        <?php
                    }
                    ?>
                <td class="Avarage">
                    <?php
                    if ($r->stock > 0) {
                        if ($r->party_code == 1111) {
                            $Avarage = $r->TotalSellRs / $r->stock;
                            echo $Avarage;
                        } else {
                            $SellAvg = $r->TotalSellRs / $r->stock;
                            $Avarage = $r->TotalBuyRs / $r->stock;
                            $Avarage = $Avarage + $r->brokrage;
                            echo $Avarage = $Avarage - $SellAvg;
                            // $brokrage = $r->brokrage;
                            // $Avarage = $r->TotalBuyRs/$r->stock;  
                            // echo $Avarage = $Avarage + $brokrage;
                        }
                    } elseif ($r->stock == 0) {
                        echo "0";
                    } else {
                        if ($r->party_code == 1111) { // brokrage_code
                            $stock = $r->stock * -1;
                            $TotalBuyRs = $r->TotalBuyRs;
                            $Avarage = $TotalBuyRs / $stock;
                            echo abs($Avarage);
                        } else {
                            $BuyAvg = $TotalBuyAVG / $Stock;
                            $Avarage = $TotalSellAVG / $Stock;
                            $Avarage = $Avarage - $Brokrage;
                            $Avarage = $Avarage - $BuyAvg;
                            echo abs($Avarage);
                        }
                    }
                    ?>
                </td>
                <?php # PL  ?>
                <td>
                    <?php
                    if ($r->stock > 0) {
                        ?>
                        <span class="Profit_Loss"></span>
                        <?php
                    } elseif ($r->stock == 0) {
                        if ($r->party_code == 1111) {
                            $profitLoss = $r->TotalSellRs - $r->TotalBuyRs;
                            if ($profitLoss > 0) {
                                echo "<span style='color:blue'>" . $profitLoss . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $profitLoss . "</span>";
                            }
                        } else {
                            $brokrage = $r->brokrage;
                            $profitLoss = $r->TotalSellRs - $r->TotalBuyRs;
                            $profitLoss = $profitLoss - $brokrage;
                            if ($profitLoss > 0) {
                                echo "<span style='color:blue'>" . $profitLoss . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $profitLoss . "</span>";
                            }
                        }
                    } else {
                        ?>
                        <span class="Profit_Loss"></span>
                        <?php
                    }
                    ?>
                </td>
                <?php # M2M  ?>
                <td> 
                    <?php
                    if ($r->stock > 0) {
                        if ($r->party_code == 1111) {
                            $stock = $r->stock;
                            $M2M = $r->TotalSellRs; // /$stock; 
                            // $M2M = $r->TotalSellRs; // /$stock; 
                            echo "<span class='M2M' style='color: red;'>" . $M2M . "</span>";
                        } else {
                            $TotalSellAVG = $r->TotalSellRs;
                            $TotalBuyAVG = $r->TotalBuyRs;
                            $brokrage_Z = $r->brokrage;
                            $M2M = $TotalBuyAVG + $brokrage_Z;
                            $M2M = $M2M - $TotalSellAVG;
                            echo "<span class='M2M' style='color: red;'>" . $M2M * -1 . "</span>";
                        }
                    } else if ($r->stock == 0) {
                        ?>
                        <span>0</span>
                        <?php
                    } else {
                        ?>
                        <span class="M2M">
                            <?php
                            if ($r->party_code == 1111) { // brokrage_code
                                // $stock = $r->stock*-1;
                                $M2M = $TotalBuyRs = $r->TotalBuyRs * -1;
                                $M2M = $TotalBuyRs; // /$stock;
                                // $M2M = $TotalBuyRs*$stock;
                                if ($M2M > 0) {
                                    echo "<span class='M2M' style='color: blue;'>" . $M2M . "</span>";
                                } else {
                                    //echo "<span class='M2M' style='color: red;'>" .$M2M ."</span>";
                                }
                            } else {
                                $SellAvg = $TotalSellAVG;
                                $BuyAvg = $TotalBuyAVG;
                                $M2M = $SellAvg - $Brokrage;
                                $M2M = $M2M - $BuyAvg;
                                echo "<span class='M2M' style='color: blue;'>" . $M2M . "</span>";
                            }
                            ?>
                        </span>
                        <?php
                    }
                    ?>	
                </td>
                <?php # NET  ?>
                <td>
                    <?php
                    if ($r->stock > 0) {
                        if ($r->party_code == 1111) {
                            $stock = $r->stock;
                            $NET = $r->TotalSellRs; // /$stock; 
                            echo "<span class='NET' style='color: red;'>" . $NET . "</span>";
                        } else {
                            $TotalSellAVG = $r->TotalSellRs;
                            $TotalBuyAVG = $r->TotalBuyRs;
                            $brokrage_Z = $r->brokrage;
                            $NET = $TotalBuyAVG + $brokrage_Z;
                            $NET = $NET - $TotalSellAVG;
                            echo "<span class='NET' style='color: red;'>" . $NET * -1 . "</span>";
                        }
                    } else if ($r->stock == 0) {
                        if ($r->party_code == 1111) {
                            $NET = $r->TotalSellRs - $r->TotalBuyRs;
                            if ($NET > 0) {
                                echo "<span style='color:blue'>" . $NET . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $NET . "</span>";
                            }
                        } else {
                            $brokrage = $r->brokrage;
                            $profitLoss = $r->TotalSellRs - $r->TotalBuyRs;
                            $profitLoss_With_Brokrage = $profitLoss - $brokrage;
                            if ($profitLoss_With_Brokrage > 0) {
                                echo "<span style='color:blue'>" . $profitLoss_With_Brokrage . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $profitLoss_With_Brokrage . "</span>";
                            }
                        }
                    } else {
                        if ($r->party_code == 1111) { // brokrage_code
                            $NET = $TotalBuyRs = $r->TotalBuyRs * -1;
                            $NET = $TotalBuyRs;
                            if ($NET > 0) {
                                echo "<span class='NET' style='color: blue;'>" . $NET . "</span>";
                            } else {
                                //echo "<span class='NET' style='color: red;'>" .$NET ."</span>";
                            }
                        } else {
                            $NET = $TotalSellAVG - $Brokrage;
                            $NET = $NET - $TotalBuyAVG;
                            echo "<span class='NET' style='color: blue;'>" . $NET . "</span>";
                        }
                    }
                    ?>
                </td>
                <?php # Without-Brokrage ?>
                <td>
                    <?php
                    if ($r->stock > 0) {
                        if ($r->party_code == 1111) {
                            $WithoutBrokrage = $r->TotalSellRs;
                            echo "<span style='color: red;'>" . $WithoutBrokrage . "</span>";
                        } else {
                            $TotalSellAVG = $r->TotalSellRs;
                            $TotalBuyAVG = $r->TotalBuyRs;
                            $brokrage_Z = $r->brokrage;
                            $WithoutBrokrage = $TotalBuyAVG + $brokrage_Z;
                            $WithoutBrokrage = $WithoutBrokrage - $TotalSellAVG;
                            echo "<span style='color: red;'>" . $WithoutBrokrage * -1 . "</span>";
                        }
                    } else if ($r->stock == 0) {
                        if ($r->party_code == 1111) {
                            $WithoutBrokrage = $r->TotalSellRs - $r->TotalBuyRs;
                            if ($WithoutBrokrage > 0) {
                                echo "<span style='color:blue'>" . $WithoutBrokrage . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $WithoutBrokrage . "</span>";
                            }
                        } else {
                            $WithoutBrokrage = $r->TotalSellRs - $r->TotalBuyRs;
                            if ($WithoutBrokrage > 0) {
                                echo "<span style='color:blue'>" . $WithoutBrokrage . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $WithoutBrokrage . "</span>";
                            }
                        }
                    } else {
                        if ($r->party_code == 1111) { //brokrage_code
                            $WithoutBrokrage = $TotalBuyRs = $r->TotalBuyRs * -1;
                            $WithoutBrokrage = $TotalBuyRs;
                            if ($WithoutBrokrage > 0) {
                                echo "<span class='WithoutBrokrage' style='color: blue;'>" . $WithoutBrokrage . "</span>";
                            } else {
                                //echo "<span class='WithoutBrokrage' style='color: red;'>" .$WithoutBrokrage ."</span>";
                            }
                        } else {
                            $WithoutBrokrage = $TotalSellAVG - $TotalBuyAVG;
                            echo "<span class='' style='color: blue;'>" . $WithoutBrokrage . "</span>";
                        }
                    }
                    ?>
                </td>
            </tr>	
            <?php
        }
        ?>
    </tbody>
</table>
