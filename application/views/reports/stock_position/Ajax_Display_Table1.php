<!--
<input type="button" value="Print" id="PrintBtn" />
party_code to code
code to uid
party_name to username
-->
<table id="example1" class="table table-bordered  MyTable1">
    <thead>
        <tr>
            <th>QTY</th>
            <th>Code</th>
            <th>Name</th>
            <th>P/l</th>
            <th>M2M</th>
            <th>Net</th>
            <th>Diffrence-WB</th>
            <th>Group</th>
            <th>Margin / AMT</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($rec as $r1) {
            // ***
            $TotalBuyAVG = $r1->TotalBuyRs;
            $TotalSellAVG = $r1->TotalSellRs;
            $Brokrage = $r1->brokrage;
            $Stock = $r1->stock
            ?>
            <tr> 
                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')">
                    <?php
                    #qty
                    if ($r1->stock > 0) {
                        echo $r1->stock;
                    } else if ($r1->stock == 0) {
                        echo "0";
                    } else {
                        echo $r1->stock;
                    }
                    ?>
                </td>

                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')"><?php echo $r1->code; ?></td>
                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')"><?php echo $r1->username; ?></td>
                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')">
                    <?php
                    #PL
                    if ($r1->stock > 0) { # + 
                        echo "<span style='color:blue'>0</span>";
                    } else if ($r1->stock == 0) { # 0
                        if ($r1->code == 1111) {
                            $profitLoss = $r1->TotalSellRs - $r1->TotalBuyRs;
                            if ($profitLoss > 0) {
                                echo "<span style='color:red'>" . $profitLoss * -1 . "</span>";
                            } else {
                                echo "<span style='color:blue'>" . $profitLoss * -1 . "</span>"; // TEmp
                            }
                        } else {
                            $brokrage = $r1->brokrage;
                            $profitLoss = $r1->TotalSellRs - $r1->TotalBuyRs;
                            $profitLoss = $profitLoss - $brokrage;

                            if ($profitLoss > 0) {
                                echo "<span style='color:blue'>" . $profitLoss . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $profitLoss . "</span>";
                            }
                        }
                    } else {
                        echo "<span style='color:blue'>0</span>";
                    }
                    ?>

                </td> 
                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')">
                    <?php
                    #M2M
                    if ($r1->stock > 0) {
                        if ($r1->code == 1111) {
                            $M2M = $r1->TotalSellRs * -1;
                            echo "<span style='color:red'>" . $M2M . "</span>";
                        } else {
                            $M2M = $TotalBuyAVG + $Brokrage;
                            $M2M = $M2M - $TotalSellAVG;
                            echo "<span style='color:red'>" . $M2M * -1 . "</span>";
                        }
                    } else if ($r1->stock == 0) {
                        if ($r1->code == 1111) {
                            $M2M = $TotalBuyAVG - $Brokrage;
                            $M2M = $M2M - $TotalSellAVG;
                            echo "<span style='color:blue'>" . $M2M * -1 . "</span>";
                        } else {
                            $M2M = $TotalBuyAVG + $Brokrage;
                            $M2M = $M2M - $TotalSellAVG;
                            echo "<span style='color:blue'>" . $M2M * -1 . "</span>";
                        }
                    } else {
                        if ($r1->code == 1111) {
                            echo "<span style='color:blue'>" . $r1->TotalBuyRs . "</span>";
                        } else {
                            $brokrage_Z = $r1->brokrage;
                            $M2M = $r1->TotalSellRs - $brokrage_Z;
                            echo "<span style='color:blue'>" . $M2M . "</span>";
                        }
                    }
                    ?>
                </td>
                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')">
                    <?php
                    #NET
                    if ($r1->stock > 0) {

                        if ($r1->code == 1111) {
                            $NET = $r1->TotalSellRs * -1;
                            echo "<span style='color:red'>" . $NET . "</span>";
                        } else {
                            $NET = $TotalBuyAVG + $Brokrage;
                            $NET = $TotalBuyAVG - $TotalSellAVG;
                            echo "<span style='color:red'>" . $NET * -1 . "</span>";
                        }
                    } else if ($r1->stock == 0) {
                        if ($r1->code == 1111) {
                            $NET = $r1->TotalSellRs - $r1->TotalBuyRs;
                            if ($NET > 0) {
                                echo "<span style='color:red'>" . $NET * -1 . "</span>"; // temp
                            } else {
                                echo "<span style='color:blue'>" . $NET * -1 . "</span>"; // temp
                            }
                            // echo "<span style='color:red'>" .$NET*-1 ."</span>";
                        } else {
                            $brokrage = $r1->brokrage;
                            $NET = $r1->TotalSellRs - $r1->TotalBuyRs;
                            $NET = $NET - $brokrage;
                            if ($NET > 0) {
                                echo "<span style='color:blue'>" . $NET . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $NET . "</span>";
                            }
                        }
                    } else {
                        if ($r1->code == 1111) {
                            echo "<span style='color:blue'>" . $r1->TotalBuyRs . "</span>";
                        } else {
                            $brokrage_Z = $r1->brokrage;
                            $NET = $r1->TotalSellRs - $brokrage_Z;
                            echo "<span style='color:blue'>" . $NET . "</span>";
                        }
                    }
                    ?>
                </td>

                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')">

                    <?php
                    # WithoutBrokrage
                    if ($r1->stock > 0) {
                        if ($r1->code == 1111) {
                            $WithoutBrokrage = $r1->TotalSellRs * -1;
                            echo "<span style='color:red'>" . $WithoutBrokrage . "</span>";
                        } else {

                            $WithoutBrokrage = $TotalBuyAVG - $TotalSellAVG;
                            echo "<span style='color:red'>" . $WithoutBrokrage * -1 . "</span>";
                        }
                    } else if ($r1->stock == 0) {
                        if ($r1->code == 1111) {
                            $WithoutBrokrage = $r1->TotalSellRs - $r1->TotalBuyRs;
                            if ($WithoutBrokrage > 0) {
                                echo "<span style='color:red'>" . $WithoutBrokrage * -1 . "</span>";
                            } else {
                                echo "<span style='color:blue'>" . $WithoutBrokrage * -1 . "</span>";
                            }
                        } else {
                            $WithoutBrokrage = $r1->TotalSellRs - $r1->TotalBuyRs;
                            if ($WithoutBrokrage > 0) {
                                echo "<span style='color:blue'>" . $WithoutBrokrage . "</span>";
                            } else {
                                echo "<span style='color:red'>" . $WithoutBrokrage . "</span>";
                            }
                        }
                    } else {
                        if ($r1->code == 1111) {
                            echo "<span style='color:blue'>" . $r1->TotalBuyRs . "</span>";
                        } else {
                            $WithoutBrokrage = $r1->TotalSellRs;
                            echo "<span style='color:blue'>" . $WithoutBrokrage . "</span>";
                        }
                    }
                    ?>


                </td> 


                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')"></td> 
                <td ondblclick="ViewTBL2('<?php echo $r1->aid; ?>')"></td>
            </tr>	
            <?php
        }
        ?>
    </tbody>
</table>

<script>
    $(document).ready(function () {
        $("#example1 tr td:first").trigger('dblclick');
        //$('#example3 tr:last').css("background-color", "yellow");
    });
</script>
<script>
    $("#example1 tr").dblclick(function () {
        $(this).addClass('selected').siblings().removeClass('selected');
    });
</script>
<style>
    .selected {
        background-color: yellow;
    }
</style>
