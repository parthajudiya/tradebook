
<div class="modal-header">
    <h4 class="modal-title"> Transaction List </h4>
</div>

<div class="modal-body">

    <table id="" class="table table-bordered table-hover ">
        <thead>
            <tr>
                <th>Ref No #</th>
                <th>Type</th>
                <th>Date</th>
                <th>Ex</th>
                <th>Symbol</th>
                <th>Exp.</th>
                <th>B/s</th>
                <th>Qty</th>
                <th>Rate</th>
                <th>P.Code</th>
                <th>P.Name</th>
                <th>B.Code</th>
                <th>B.Name</th>
                <th>Time</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th>Ref No #</th>
                <th>Type</th>
                <th>Date</th>
                <th>Ex</th>
                <th>Symbol</th>
                <th>Exp.</th>
                <th>B/s</th>
                <th>Qty</th>
                <th>Rate</th>
                <th>P.Code</th>
                <th>P.Name</th>
                <th>B.Code</th>
                <th>B.Name</th>
                <th>Time</th>
            </tr>
        </tfoot>
        <tbody>
            <?php
            $buy = 0;
            $buyAmout = 0;
            $sale = 0;
            $saleAmout = 0;
            foreach ($recA as $rA) {
                ?>
                <tr ondblclick="EditBuySell(<?php echo $rA->buysell_id; ?>)">
                    <td> <?php echo $rA->re_no; ?></td>
                    <?php
                    if ($rA->type == "1") {
                        ?>	
                        <td> RL</td>
                        <?php
                    } else if ($rA->type == "2") {
                        ?>
                        <td> FW</td>
                        <?php
                    } else if ($rA->type == "3") {
                        ?>
                        <td> CF</td>
                        <?php
                    } else if ($rA->type == "4") {
                        ?>
                        <td> BF</td>
                        <?php
                    }
                    ?>

                    <?php
                    $date1 = explode('-', $rA->date1);
                    $date1 = "$date1[2]-$date1[1]-$date1[0]";
                    ?>

                    <td><?php echo $date1; ?></td>

                    <?php
                    if ($rA->exchange_id == "63") {
                        ?>	
                        <td> FO</td>
                        <?php
                    } else if ($rA->exchange_id == "70") {
                        ?>
                        <td> MCX</td>
                        <?php
                    } else { // 71
                        ?>
                        <td>NCDEX</td>
                        <?php
                    }
                    ?>

                    <td> <?php echo $rA->symbol; ?></td>
                    <td> <?php echo $rA->ex_date; ?></td>
                    <?php
                    if ($rA->buy_sell_id == "1") {
                        $buy += $rA->qty1;
                        $buyAmout += ($rA->qty1 * $rA->rate1);
                        ?>	
                        <td><span class="badge bg-success">Buy</span></td>
                        <?php
                    } else {
                        $sale += $rA->qty1;
                        $saleAmout += ($rA->qty1 * $rA->rate1);
                        ?>

                        <td><span class="badge bg-danger">Sell</span></td>
                        <?php
                    }
                    ?>
                    <td> <?php echo $rA->qty1; ?></td>
                    <td> <?php echo $rA->rate1; ?></td>
                    <td> <?php echo $rA->party_code; ?></td>
                    <td> <?php echo $rA->party_name; ?></td>
                    <td> <?php echo $rA->brokrage_code; ?></td>

                    <td> <?php echo $rA->brokrage_name; ?></td>

                    <?php $time = date('H:i:s', strtotime($rA->curr_time)); ?>
                    <td><?php echo $time; ?></td>

                </tr>
                <?php
            }
            ?>

        </tbody>
    </table>
</div>

<p align="center">
    <b style="color:green">Total Buy Qty :</b>   <?php echo $buy; ?>
    <b style="color:green">Total Buy Amount :</b> <?php echo $buyAmout; ?>		  
    <b style="color:red">Total Sell Qty :</b> <?php echo abs($sale); ?> 
    <b style="color:red">Total Sell Amount :</b> <?php echo abs($saleAmout); ?>				
</p>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>

