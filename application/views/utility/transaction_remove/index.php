<?php
$Start_Date  = [
    'type' => 'text',
    "name" => "start_date",
    'id' => "start_date",
	'class'=>"form-control",
	'placeholder'=>"From Date",
];


$End_Date  = [
    'type' => 'text',
    "name" => "end_date",
    'id' => "end_date",
	'class'=>"form-control",
	'placeholder'=>"To Date",
];



$Code  = [
    'type' => 'text',
    "name" => "party_code",
    'id' => "party_code",
	'class'=>"form-control",
	'placeholder'=>"Account	Code",
];

$Name  = [
    'type' => 'text',
    "name" => "party_name",
    'id' => "party_name",
	'class'=>"form-control",
	'placeholder'=>"Account Name",
];
?>


<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1>Transaction Remove</h1>
			</div>
		</div>
		</div>
		</section>
		
		
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
					
				
				
					<div class="card-body">
					<form id="FormSubmit" name="FormSubmit" autocomplete="off">
					
					
						
						<div class="row">
						
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>Exchange</label>
								
								<select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
							
									<?php 
							
									foreach ($Exchange as $row_4)
									{
									?>	
										<option value="<?php echo $row_4->exchange_id; ?>" ><?php echo strtoupper($row_4->name);?></option>
									<?php
									}
								?>
								
								</select>
							</div>
						</div>
						
						
						<div class="col-sm-4">
							<div class="form-group">
								<label>Setlment</label>
								<select class="form-control " style="width: 100%;" name="setlement_id" id="setlement_id">
							
								</select>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>Type</label>
								
								<select class="form-control " style="width: 100%;" name="type" id="type">
							
									<option value="1">RL</option>
									<option value="2">FW</option>
									<option value="3">CF</option>
									<option value="4">BF</option>
								
								</select>
							</div>
						</div>
						
					</div>


				
					<div class="row">	
							<div class="col-sm-2">
							<div class="form-group">
							<label>A/c Code</label>
							<?php  echo form_input($Code); ?>
							</div>
							</div>


							<div class="col-sm-6">
							<div class="form-group">
							<label>A/c Name</label>
							<?php  echo form_input($Name); ?>
							</div>
							</div>					
						</div>					

					<div class="row">	
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>From Date</label>
								<?php  echo form_input($Start_Date); ?>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>To Date</label>
									<?php  echo form_input($End_Date); ?>
							</div>
						</div>
						
					<div class="col-2">
							<label>&nbsp;</label><br>
							<button id="Submit" type="submit" class="btn btn-primary">Remove</button>
					</div>
				</div>
			</form> 
		</div>
					
		</div>
		</div>

		
	</div>  <!-- content-wrapper End -->
	
<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/global_exchange_to_settlement_to_date.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/global_acc_code_to_acc_name.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/global_datepicker.js"></script>

<script src="<?php echo base_url(); ?>assets/customjs/transcation_remove.js"></script>


