<link rel="stylesheet" href="<?= base_url("Assert/css/jquery.dataTables.min.css")?>">
<script src="<?= base_url("Assert/js/datatables.min.js") ?>"></script>
<script src="<?= base_url("Assert/js/script/datatable_function.js") ?>"></script>
update
<div class="col-lg-6 col-lg-offset-3">
        <div class="box-header">
            <h3 class="box-title"><?= $this->lang->line("zone"); ?></h3>
        </div>
            <table id="zone_table" class="table-bordered"></table>
        <div class="box-footer">
            <a href="<?= base_url("master/zone/add"); ?>" id="ins_base" class="btn-primary">ADD</a>
            <button class="btn-success edit-row">MODIFY</button>
            <button class="btn-danger delete-row">DELETE</button>
            <a class="btn-default" id="backbtn" href="<?=base_url()?>">EXIT</a>
        </div>
</div>
<script>
    $(document).ready(function () {
        data_table("#zone_table", "<?= base_url('master/zone/delete') ?>", "<?= base_url('master/zone/edit') ?>", '<?= base_url('master/zone/json?select=name,id') ?>');
        $('input[type="search"]').addClass("mousetrap");
    });
</script>