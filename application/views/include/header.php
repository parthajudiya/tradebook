
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>TradeBook</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="<?php echo base_url("assets/") ?>plugins/fontawesome-free/css/all.min.css">

		<!-- SweetAlert2  Toastr -->
		<link rel="stylesheet" href="<?php  echo base_url("assets/") ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
		<link rel="stylesheet" href="<?php echo base_url("assets/") ?>plugins/toastr/toastr.min.css">

		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/adminlte.min.css">

		<!-- Google Font: Source Sans Pro -->
		<!--   <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> 	 -->

		<!-- DataTables -->
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">


		<!-- SweetAlert
		<link rel="stylesheet" href="<?php //echo  base_url("assets/") ?>plugins/SweetAlert/sweetalert.css">
		<script src="<?php // echo  base_url("assets/") ?>plugins/SweetAlert/sweetalert.js"></script>
		 -->

		<!-- daterangepicker -->
		 <link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/daterangepicker/daterangepicker.css">

		<!-- Select2 -->
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/select2/css/select2.min.css">
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

		<!-- Autocomplte  - Jquery Ui css  -->
		 <link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/Autocomplte/jquery-ui.css">

		  <!-- icheck -->
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">


		 <!-- jquery -->
		<script src="<?php echo  base_url("assets/") ?>plugins/jquery/jquery.min.js"></script>

		<!-- Jquery validate -->
		<script src="<?php echo base_url("assets/") ?>plugins/Validation/jquery.validate.js"></script>
		<script src="<?php echo base_url("assets/") ?>plugins/Validation/jquery.validate.min.js"></script>

		<script type="text/javascript">
		/*
            $(window).on("load", function () {
                $("#loader-overlay").hide();
            });
            $(document).ajaxStart(function () {
                $("#loader-overlay").show();
            });

            $(document).ajaxComplete(function () {
                $("#loader-overlay").hide();
            });
			*/
        </script>
		 <!--
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
		-->




<style>

.importbtn
{
	border-color: #000000;
	background-color: #000000;
	color: #fff;
}
.importbtn:hover
{
	border-color: #000000;
	background-color: #000000;
	color: #fff;
}

.checkbtn
{
	border-color: #000000;
	background-color: #000000;
	color: #fff;
}
.checkbtn:hover
{
	border-color: #000000;
	background-color: #000000;
	color: #fff;
}

.filebtn
{
	border-color: #000000;
	background-color: #000000;
	color: #fff;
}

.filebtn:hover
{
	border-color: #000000;
	background-color: #000000;
	color: #fff;
}


</style>

<style>
.ui-state-default, .ui-widget-content .ui-state-default
{
	    background: #d3d3d352 !important;
}
</style>





    </head>
	<body class="hold-transition layout-top-nav layout-navbar-fixed">

     <div class="wrapper">
		<!-- <div id="loader-overlay"></div>
		 Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light " >
                <div class="container-fluid">


                    <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                        <!-- Left navbar links -->
                        <ul class="navbar-nav">

						 <!-- Master Start  -->
                            <li class="nav-item dropdown">
                                <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Master</a>
                                <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">

                                    <li><a href="<?php echo base_url("master/account/")?>" class="dropdown-item">Account </a></li>
									<li><a href="<?php echo base_url("master/brokrage/")?>" class="dropdown-item">Brokrage </a></li>
                                    <li><a href="<?php echo base_url("master/settlment/")?>" class="dropdown-item">Settlment </a></li>
                                    <li><a href="<?php echo base_url("master/exchange/")?>" class="dropdown-item">Exchange </a></li>
                                    <li><a href="<?php echo base_url("master/symbol/")?>" class="dropdown-item">Symbol </a></li>
                                    <li><a href="<?php echo base_url("master/expiry/")?>" class="dropdown-item">Expiry </a></li>
									<!--
                                    <li><a href="<?php echo base_url("master/remiser_brokrage/")?>" class="dropdown-item">Remiser Brokrage </a></li>
									-->




                                </ul>
                            </li>
							 <!-- Master End  -->

							 <!-- Transaction Start  -->
							<li class="nav-item dropdown">
                                <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Transaction</a>
                                <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                     <li><a href="javascript:void(0)" onClick="openWin()" class="dropdown-item">Buy/Sell </a></li>




									 <li><a href="<?php echo base_url("transaction/eod_high_low_check/")?>" class="dropdown-item">EOD High/Low Check </a></li>


									 <!--
                                     <li><a href="<?php echo base_url("transaction/receipt_payment/")?>" class="dropdown-item">Receipt/Payment </a></li>
									 -->

                                </ul>
                            </li>
							 <!-- Transaction End  -->

							  <!-- Process Start  -->
							<li class="nav-item dropdown">
                                <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Process</a>
                                <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                    <li><a href="<?php echo base_url("process/closing_forward/")?>" class="dropdown-item">Closing Forward </a></li>


                                    <li><a href="<?php echo base_url("process/brokrage_update/")?>" class="dropdown-item">Brokrage Update </a></li>



                                    <li><a href="<?php echo base_url("process/import_form_excel/")?>" class="dropdown-item">Import Form Excel - CP</a></li>
                                    <li><a href="<?php echo base_url("process/Import_excel_mt/")?>" class="dropdown-item">Import Form Excel -  MT</a></li>


								</ul>
                            </li>
							 <!-- Process End  -->

							  <!-- Reports Start  -->
							<li class="nav-item dropdown">
                                <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Reports</a>
                                <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                    <li><a href="<?php echo base_url("reports/stock_position/")?>" class="dropdown-item">Stock Position  </a></li>
                                    <li><a href="<?php echo base_url("reports/bill/")?>" class="dropdown-item">Bill  </a></li>


                                    <li><a href="<?php echo base_url("reports/party_ledger/")?>" class="dropdown-item">Party Ledger  </a></li>
                                    	<!--
                                    <li><a href="<?php echo base_url("reports/ledger_summary/")?>" class="dropdown-item">Ledger Summary  </a></li>
                                    <li><a href="<?php echo base_url("reports/settlment_summary/")?>" class="dropdown-item">Settlment Summary  </a></li>
                                    <li><a href="<?php echo base_url("reports/remiser_brokrage/")?>" class="dropdown-item">Remiser Brokrage  </a></li>
									-->


                               </ul>
                            </li>
							 <!-- Reports End  -->

							 <!-- Utility Start   Utility End  -->
							<li class="nav-item dropdown">
                                <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Utility</a>
                                <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
								<!--
									 <li><a href="<?php echo base_url("utility/setup/")?>" class="dropdown-item">Setup  </a></li>
									 <li><a href="<?php echo base_url("utility/activation/")?>" class="dropdown-item">Activation  </a></li>
									 <li><a href="<?php echo base_url("utility/data_backup/")?>" class="dropdown-item">Data Backup  </a></li>
									 <li><a href="<?php echo base_url("utility/send_sms/")?>" class="dropdown-item">Send Sms  </a></li>
									 <li><a href="<?php echo base_url("utility/update/")?>" class="dropdown-item">Update  </a></li>
									 <li><a href="<?php echo base_url("utility/ltp_activation/")?>" class="dropdown-item">Ltp Activation
									-->
									 <li><a href="<?php echo base_url("utility/transaction_remove/")?>" class="dropdown-item">Transaction Remove  </a></li>
									  </a></li>
                               </ul>
                            </li>

					  </ul>

					 </div>


                    <!-- Right navbar links -->
                    <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link" data-toggle="dropdown" href="#">
                                <i class="fas fa-user"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                <span class="dropdown-header"><?php echo  $this->session->userdata("email") ?></span>
                                <div class="dropdown-divider"></div>
                                <a href="<?php echo   base_url("auth/change-password")?>" class="dropdown-item">
                                    <i class="fas fa-key mr-2"></i> change password
                                </a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo   base_url("auth/logout")?>" class="nav-link"><i class="fas fa-power-off"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>



<script>
var base_url="<?php echo base_url(); ?>";


// var buy_sell;

	function openWin()
	{
			 window.open(base_url+"transaction/Buy_sell/buy_sell_js_popup", "buy_sell", "top=80, left=10, width=1300, height=120 , channelmode=no, toolbar=no , titlebar=no , status=no, scrollbars=yes , resizable= no , menubar = no");
	}


</script>

<style>
.navbar
{
	padding: .0rem .0rem;

}

.layout-navbar-fixed .wrapper .content-wrapper {
    margin-top: calc(2.5rem + 1px);
}
</style>
