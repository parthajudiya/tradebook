<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>TradeBook - Buy Sell</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="<?php echo base_url("assets/") ?>plugins/fontawesome-free/css/all.min.css">
		
		<!-- SweetAlert2  Toastr -->
		<link rel="stylesheet" href="<?php  echo base_url("assets/") ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
		<link rel="stylesheet" href="<?php echo base_url("assets/") ?>plugins/toastr/toastr.min.css">

		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/adminlte.min.css">

		<!-- Google Font: Source Sans Pro -->
		<!--   <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> 	 -->

		<!-- DataTables -->
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">


		<!-- SweetAlert
		<link rel="stylesheet" href="<?php //echo  base_url("assets/") ?>plugins/SweetAlert/sweetalert.css">
		<script src="<?php // echo  base_url("assets/") ?>plugins/SweetAlert/sweetalert.js"></script>
		 -->
		 
		<!-- daterangepicker -->	
		 <link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/daterangepicker/daterangepicker.css">
	
		<!-- Select2 -->
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/select2/css/select2.min.css">
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

		<!-- Autocomplte  - Jquery Ui css  -->
		 <link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/Autocomplte/jquery-ui.css">
		 
		  <!-- icheck -->
		<link rel="stylesheet" href="<?php echo  base_url("assets/") ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
		 
		 
		 <!-- jquery -->
		<script src="<?php echo  base_url("assets/") ?>plugins/jquery/jquery.min.js"></script>
		
		<!-- Jquery validate -->
		<script src="<?php echo base_url("assets/") ?>plugins/Validation/jquery.validate.js"></script>
		<script src="<?php echo base_url("assets/") ?>plugins/Validation/jquery.validate.min.js"></script>
		
<style>		
.viewbtn 
{
	border-color: #000000; 
	background-color: #000000; 
	color: #fff;
}
.viewbtn:hover
{
	border-color: #000000; 
	background-color: #000000; 
	color: #fff;
}
</style>		
		
<style>
.ui-state-default, .ui-widget-content .ui-state-default
{
	    background: #d3d3d352 !important;
}
body
{
	zoom:70%
}
</style>
		
    </head>
	<body class="hold-transition layout-top-nav">
	<div class="wrapper">

	

          
						
		