<?php
	/*
		$Name = [
		'type' => 'text',
		"name" => "user_id",
		'class' => 'form-control',
		'id' => "user_id",
		'placeholder' => "Enter 1",
		//'value' => isset($name) ? $name : "",
		];
	*/
	
	
	$Code = [
    'type' => 'text',
    "name" => "code",
    'class' => 'form-control',
    'id' => "code",
    'placeholder' => "Enter Account code",
	//'value' => isset($name) ? $name : "",
	];
	
	
	$Mobile = [
    'type' => 'text',
    "name" => "mobile",
    'class' => 'form-control',
	'id' => "mobile",
	'placeholder' => "Enter Mobile",
	];
	
	$Email  = [
    'type' => 'text',
    "name" => "email",
    'class' => 'form-control',
	'id' => "email",
	'placeholder' => "Enter Email",
	];
	
	
	
	$Ac_Group = [
    'type' => 'text',
    "name" => "ac_group",
    'class' => 'form-control',
	'id' => "ac_group",
	'placeholder' => "Enter Account Group",
	];
	
	$Disp_Order = [
    'type' => 'text',
    "name" => "disp_order",
    'class' => 'form-control',
	'id' => "disp_order",
	'placeholder' => "Enter Display Order",
	"data-plus-as-tab"=>"false"
	];
	
	/*
		
		$Closing_Rate_Type = [
		'type' => 'text',
		"name" => "closing_rate_type",
		'class' => 'form-control',
		'id' => "closing_rate_type",
		'placeholder' => "Enter Closing Rate Type",
		];
	*/
	
	$aid =  [
    'type' => 'hidden',
    "name" => "aid",
    'id' => "aid",
    'value' => "",
	'placeholder' => "aid",
	];
	
?>


<div class="content-wrapper">
	
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $this->lang->line("Account_Header"); ?></h1>
				</div>
			</div>
		</div>
	</section>
	
	
	<div class="col-md-12">
		<div class="card card-primary card-outline">
			
			<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Account_Title"); ?> </h3>
			</div>
			
			<div class="card-body">
				
				<form id="FormSubmit" autocomplete="off" data-plus-as-tab="true">   	  <?php //echo form_open(); ?>      
					<?php  echo form_input($aid); ?>
					<input id="id"  name="id"  type="hidden"  placeholder="id" value="">	
					<div class="row">
						
						<div class="col-sm-2">
							<div class="form-group">
								<label><?php echo $this->lang->line("Account_Code_F"); ?></label>
								<?php  echo form_input($Code); ?>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">  
								<label><?php echo $this->lang->line("Account_Name_F"); ?></label>
								
								<!-- <select class=" form-control select2bs4 " name="username"  id="username" ></select> 	-->
								
								
								<input id="uid"  name="uid" placeholder="uid"  type="hidden" value="">	
								
								
								<input id="username" name="username"  class="form-control" placeholder="Enter User Name or Search " type="text" value="" >
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group">
								<label><?php echo $this->lang->line("Ac_Type_F"); ?></label>
								<select name="ac_type" id="ac_type" class="form-control">
									<!--
										<option value="">Select Account Type</option>
									-->
									<option value="3">Client</option>
									<option value="2">Brokar</option>
									<option value="1">Admin</option>
									
								</select>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group">  
								<label><?php echo $this->lang->line("Account_Group"); ?></label>
								<?php  echo form_input($Ac_Group); ?>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group"> 
								<label><?php echo $this->lang->line("Closing_Rate_Type"); ?></label>
								
								
								<select class="form-control "  name="closing_rate_type_id" id="closing_rate_type_id">
									
									<option selected="selected" value="">Select Closing Rate Type</option>
									
									<option value="1">LTP</option>
									<option value="2">Bhav Copy</option>
								</select>
								
							</div>
						</div>
						
					</div>
					
					<div class="row">
						
						<div class="col-sm-2">
							<div class="form-group">
								<label><?php echo $this->lang->line("Mobile_F"); ?></label>
								<?php  echo form_input($Mobile); ?>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group">  
								<label><?php echo $this->lang->line("Email_F"); ?></label>
								<?php  echo form_input($Email); ?>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group"> 
								<label><?php echo $this->lang->line("Disp_Ord_F"); ?></label>
								<?php  echo form_input($Disp_Order); ?>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group"> 
								<label>&nbsp;</label><br/>
								<button id="AddRec" type="submit" class="btn btn-primary" data-plus-as-tab="false" >Submit</button>
							</div>
						</div>
						
						
						
					</div>
					
					
					
					<!--  disabled="disabled" -->
					
					
				</form>  
				
			</div>
		</div>
	</div>
	
	
	
	
	
    <!-- Main content  -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					
					<div class="card card-primary card-outline">
						<div class="card-header">
							<h3 class="card-title"><?php echo $this->lang->line("Account_Title") ?></h3>
							<div class="card-tools">
								<button style="" class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->lang->line("Action"); ?>
								<span class="caret"></span></button>
								<ul class="dropdown-menu" style="cursor: pointer;">
									<li align="center"><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"><?php echo $this->lang->line("Delete_Bulk"); ?></a></li>
								</ul>
							</div>
						</div>
						
						<div class="card-body" id="Response">   </div>
						
					</div>
					
				</div>
			</div>
		</div>
	</section>
	
</div> <!-- content-wrapper-->


<script>
	var url = url;
	var base_url="<?php echo base_url();?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/account.js"></script>

<style>
	.ui-widget.ui-widget-content
	{
	border: 1px solid #28a745;
	}
	
	
	.ui-widget.ui-widget-content
	{
	color:black;
	}
	
</style>
