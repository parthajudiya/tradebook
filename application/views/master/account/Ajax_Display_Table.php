<table id="example2" class="table table-bordered table-hover MyTable"> 
	<thead>
		<tr>
			<th><input type="checkbox" id="select_all" value=""/></th> 
			<th>ID</th>
			<th><?php echo $this->lang->line("Account_Code") ?></th>
			<th><?php echo $this->lang->line("Account_Name") ?></th>
			<th><?php echo $this->lang->line("Account_Type") ?></th>
			<th><?php echo $this->lang->line("Mobile_No") ?></th>
			<th><?php echo $this->lang->line("Email_id") ?></th>
			<th><?php echo $this->lang->line("Account_Group") ?></th>
			<th><?php echo $this->lang->line("Disp_Order") ?></th>
			<th><?php echo $this->lang->line("Closing_Rate_Type") ?></th>
			<th><?php echo $this->lang->line("Action") ?></th>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($Account as $row) { ?>
			
			<tr> 
				
				<td><input type="checkbox"  name="checked_id[]" class="checkbox" value="<?php echo  $row->aid; ?>"/></td>
				<td><?php echo  $row->aid; ?></td>
				
				
				<td ondblclick="Edit(<?php echo $row->aid; ?>)"><?php  echo  $row->code; ?></td>
				<?php /*	<td ondblclick="Edit(<?php echo $row->uid; ?>)"><?php  echo $row->user_id; ?></td>  */ ?>
				<td ondblclick="Edit(<?php echo $row->aid; ?>)"><?php  echo $row->username; ?></td>
				
				<?php 
					if($row->ac_type=="1")
					{
					?>	
					<td ondblclick="Edit(<?php echo $row->aid; ?>)">Admin</td>
					<?php 	
					}
					else if($row->ac_type=="2")
					{
					?>
					<td ondblclick="Edit(<?php echo $row->aid; ?>)">Brokar</td>
					<?php 	
					}
					else
					{
					?>
					<td ondblclick="Edit(<?php echo $row->aid; ?>)">Client</td>
					<?php 
					}		
				?>
				
				<td ondblclick="Edit(<?php echo $row->aid; ?>)"><?php  echo $row->mobile;  ?></td>
				<td ondblclick="Edit(<?php echo $row->aid; ?>)"><?php  echo $row->email; ?></td>
				<td ondblclick="Edit(<?php echo $row->aid; ?>)"><?php  echo $row->ac_group; ?></td>
				<td ondblclick="Edit(<?php echo $row->aid; ?>)"><?php  echo $row->disp_order; ?></td>
				
				
				<?php 
					if($row->closing_rate_type_id=="1")
					{
					?>	
					<td ondblclick="Edit(<?php echo $row->aid; ?>)">LTP</td>
					<?php 	
					}
					else
					{
					?>
					<td ondblclick="Edit(<?php echo $row->aid; ?>)">Bhav Copy</td>
					<?php 	
					}
					
				?>
				
				<td>
					<button  type="button"  class="btn btn-sm btn-default" data-toggle="tooltip"  data-placement="left" title="Delete" data-original-title="Delete"  onclick="Delete(<?php echo $row->aid; ?>)">
						<i class="fa fa-trash"></i>
					</button>
				</td>
			</tr>
		<?php  } ?>
	</tbody>
	
</table>

<script src="<?php echo base_url(); ?>assets/customjs/multi_chkbox.js"></script>		
