<style>	
#loader-overlay
{
	z-index: 99999;
	background: #04040480;
	position: fixed;
	top: 0;
	bottom: 0;
	width: 100%;
	height: 100%;
}
</style>	
<script type="text/javascript">
		/*
            $(window).on("load", function () {
                $("#loader-overlay").hide();
            });
            $(document).ajaxStart(function () {
                $("#loader-overlay").show();
            });

            $(document).ajaxComplete(function () {
                $("#loader-overlay").hide();
            });
			<div id="loader-overlay"></div>
*/			
</script>



<?php

$Lot_size = [
    'type' => 'text',
    "name" => "lot_size",
    'class' => 'form-control',
	'id' => "lot_size",
	'placeholder' => "Lot size",
	"data-plus-as-tab"=>"false"

];

$Instument = [
    'type' => 'text',
    "name" => "instument",
    'class' => 'form-control',
	'id' => "instument",
	'placeholder' => "Enter instument",
];


$Symbol = [
    'type' => 'text',
    "name" => "symbol",
    'class' => 'form-control',
	'id' => "symbol",
	'placeholder' => "Enter symbol",
];

$Symbol_id = [
    'type' => 'hidden',
    "name" => "symbol_id",
    'id' => "symbol_id",
    'value' => "",
	
];
 
?>

	<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1><?php echo $this->lang->line("Symbol_Header"); ?></h1>
			</div>
		</div>
		</div>
		</section>
	
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
			
			<form id="FormSubmit"  autocomplete="off" method="post" enctype="multipart/form-data" data-plus-as-tab="true">   
		
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Symbol_Title"); ?> </h3>
				
					<div class="card-tools">
					
					<!--input type="button" name="filecsv" id="filecsv" value="Import" class="btn  btn-sm importbtn" -->
					
						<button id="Submit" name="submit" type="submit" class="btn btn-sm btn-primary" data-plus-as-tab="false">Submit</button>
					
					</div>
					
				</div>

					<div class="card-body">
					
					 <?php  echo form_input($Symbol_id); ?>
					
						
						<div class="row">
						
						<div class="col-sm-2">
							<div class="form-group">
								<label><?php echo $this->lang->line("Brokrage_Exchange"); ?></label>
								
								<select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
								<!--
								<option value="" >Select Exchange</option>
								-->
								<?php 
								
									foreach ($Exchange as $row_B)
									{
									?>	
										<option value="<?php echo $row_B->exchange_id; ?>" ><?php echo strtoupper($row_B->name); ?></option>
									<?php
									}
								?>
								</select>
							</div>
						</div>
							
							<div class="col-sm-3">
							<div class="form-group">
							<label><?php echo $this->lang->line("Symbol"); ?></label>
							<?php  echo form_input($Symbol); ?>
							</div>
							</div>
						
							<div class="col-sm-2">
							<div class="form-group">
							<label><?php echo $this->lang->line("Symbol_Instument"); ?></label>  
							<?php  echo form_input($Instument); ?>
							</div>
							</div>
							 
							<div class="col-sm-2">
							<div class="form-group">
							<label><?php echo $this->lang->line("Symbol_Lot_size"); ?></label>
							<?php  echo form_input($Lot_size); ?>
							</div>
							</div>
							
							<div class="col-sm-2">
							<div class="form-group">
							<label>Upload</label>
							<input type="file" id="FileData" name="FileData">
							</div>
							</div>
							
						</div>
					</form>  
					</div>
					
			</div>
			</div>

		<section class="content">
		<div class="container-fluid">
		<div class="row">
		<div class="col-12">

				<div class="card card-primary card-outline">
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Symbol_Title"); ?></h3> &nbsp;
				
				<a href="http://tradeday2day.com/fo.csv"> Download Fo : </a> 
				<a href="http://tradeday2day.com/mcx.csv"> Download Mcx : </a>
				<a href="http://tradeday2day.com/ncdex.csv"> Download Ncdex : </a>
		
				
				
				<div class="card-tools">
				<button id="Import_Symbol" class="btn btn-primary btn-sm" type="button">Import Symbol</button>
					
				
				
				<button style="" class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->lang->line("Action"); ?>
				<span class="caret"></span></button>

					<ul class="dropdown-menu" style="cursor: pointer;">
					<li align="center" ><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"> <?php echo $this->lang->line("Delete_Bulk"); ?></a></li>
					</ul>
				</div>
				
				</div>
					<div class="card-body" id="Response"></div> <!-- Load AJAX Table -->
				</div>

		</div>
		</div>
		</div>
		</section>
	
	</div>  <!-- content-wrapper End -->

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/symbol.js"></script>


<script>
	
		$(document).ready(function()
		{
			// $(document).on('click', '#Submit', function(){
			$(document).on('submit', '#FormSubmit', function() { 
			//   alert('add');
				
				var exchange_id = $("#exchange_id").val();
				var symbol = $("#symbol").val();
				var instument = $("#instument").val();
				var lot_size = $("#lot_size").val();
				
				var dataString = 'exchange_id='+ exchange_id + '&symbol='+ symbol + '&instument='+ instument + '&lot_size='+ lot_size;

                $.ajax({
					type: "POST",
					url: base_url+"master/Symbol/Add",
					data: dataString,
					cache: false,

					success: function(result)
					{
						load_data(); 
						msg();
						$("#FormSubmit")[0].reset(); 
						
						
						/*
						  if (result == 1) 
						  {
							$("#FormSubmit")[0].reset();  // Form Clear after insert
							load_data(); // Load Ajax
							// $("#exchange_id").val("").trigger('change'); // After Change value Null In Droup Down
							// alert(data);
						
							$(function() {
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.success('Symbol Added Successfully.')
							});
						}
						else if (result == 2) 
						{
								$(function() {
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.error('Symbol Already Avalilable.')
							});
						}
						else
						{
								$(function() {
								const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 3000
								});
								toastr.error('error')
							});
						}
						*/
						
					}
				});
                return false;
             });
         });
		
		
			$(document).ready(function (){
				$(document).on('submit', '#FormSubmit', function(){ 
				// $(document).on('click', '#Check', function(){
				
					$.ajax({
							url: base_url+"master/Symbol/UploadData",
							type: "POST",
							// dataType: "json", // 
							data:  new FormData(this),
							contentType: false,
							cache: false,
							processData:false,
							success: function(datas , result)
							{
								load_data();
								
								/*
										  if (datas == 1) 
										  {
											$("#FormSubmit")[0].reset();  // Form Clear after insert
											load_data(); // Load Ajax
											
											$(function() {
												const Toast = Swal.mixin({
												toast: true,
												position: 'top-end',
												showConfirmButton: false,
												timer: 3000
												});
												toastr.success(' Added Successfully.')
											});
										}
										else  // if (datas == 2) 
										{
												$(function() {
												const Toast = Swal.mixin({
												toast: true,
												position: 'top-end',
												showConfirmButton: false,
												timer: 3000
												});
												toastr.error(' Already Avalilable.')
											});
										}
										*/
									
										/*
										else
										{
												$(function() {
												const Toast = Swal.mixin({
												toast: true,
												position: 'top-end',
												showConfirmButton: false,
												timer: 3000
												});
												toastr.error('error')
											});
										}
										*/
							}
					});
					return false;
				});
			});
			
			function msg()
			{
				toastr.success('Symbol Added Successfully.');
			}
			
</script>

<?php /* 03-09-2020 end */ ?>


<script type="text/javascript">
/*
	$(document).ready(function()
	{
		$('#filecsv').on('click', function(event)
		{
			var str1 = "fo";
			var res1 = str1;
			// var res1 = str1.toLowerCase();

			var str2 = "mcx";
			var res2 = str2;
			// var res2 = str2.toLowerCase();

			var str3 = "ncdex";
			var res3 = str3;
			// var res3 = str3.toLowerCase();
		 
		
		
			// if($('#exchange_id').val() == "63") // fo // 63
			// 
			// var value = $("#myselection option:selected"); 
			// alert(value.text()); 
			
		
			if($("#exchange_id option:selected").text() == res1)
			{
				alert(res1);
				
					$.ajax({
					url: base_url+"master/Symbol/focsv",
					type : "POST",
					success: function(result) 
					{
						if(result == "2")
						{
							//alert('already fo'); 
						}
						else
						{
							//alert('success fo'); 
						}		
						load_data();
					}
				});
			}
		
				
			
			else if($("#exchange_id option:selected").text() == res2)
			{
			alert(res2);
			
			
			$.ajax({
				url: base_url+"master/Symbol/mcxcsv",
				type : "POST",
				success: function(result) 
				{
					
					load_data();
					
						
				}
			});
		}
		
		else if($("#exchange_id option:selected").text() == res3)
		{
			
			alert(res3);
			
			
				$.ajax({
					url: base_url+"master/Symbol/ncdexcsv",
					type : "POST",
					success: function(result) 
					{
						
						load_data();
					}
				});
		}	
		else
		{
			Swal.fire("Info", "Select First Exchange ", "info");
		}
	});	
	
});

*/
</script>
<script>
	$(document).ready(function()
	{
		$('#Import_Symbol').on('click', function(event)
		{
			// var Symbol = "ONE";
			$.ajax({
				url: base_url+"master/Symbol/Symbol_Moneycontroll",
				type : "GET",
					success: function(result) 
					{
						// if(result == "2")
						// {
							// alert('already fo'); 
						// }
						// else
						// {
							// alert('success fo'); 
						// }		
						load_data();
					}
				});
		});	
	});
</script>











