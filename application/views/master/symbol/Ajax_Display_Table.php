	
	<table id="example2" class="table table-bordered table-hover MyTable">

		<thead>
			<tr>
			<th><input type="checkbox" id="select_all" value=""/></th> 
			<th><?=$this->lang->line("Symbol_Exchange"); ?></th>
			<th><?=$this->lang->line("Symbol"); ?></th>
			<th><?=$this->lang->line("Symbol_Instument"); ?></th>
			<th><?=$this->lang->line("Symbol_Lot_size"); ?></th>
			<th><?=$this->lang->line("Action") ?></th>
			</tr>
		</thead>

		<tbody>
			<?php foreach($symbol as $row) { ?>

			<tr> 
				<td><input type="checkbox"  name="checked_id[]" class="checkbox" value="<?php echo  $row->symbol_id; ?>"/></td>
				<td ondblclick="Edit(<?php echo $row->symbol_id; ?>)"><?php  echo  strtoupper($row->name); ?></td>
				<td ondblclick="Edit(<?php echo $row->symbol_id; ?>)"><?php  echo  $row->symbol; ?></td>
				<td ondblclick="Edit(<?php echo $row->symbol_id; ?>)"><?php  echo  $row->instument; ?></td>
				<td ondblclick="Edit(<?php echo $row->symbol_id; ?>)"><?php  echo  $row->lot_size; ?></td>
				<td>
					<button  type="button"  class="btn btn-sm btn-default" data-toggle="tooltip"  data-placement="left" title="Delete" data-original-title="Delete"  onclick="Delete(<?php echo $row->symbol_id; ?>)">
					<i class="fa fa-trash"></i>
					</button>
				</td>
			</tr>
			<?php  } ?>
		</tbody>

	</table>
					
<script src="<?php echo base_url(); ?>assets/customjs/multi_chkbox.js"></script>



