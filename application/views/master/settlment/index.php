<?php

$description = [
    'type' => 'text',
    "name" => "description",
    'class' => 'form-control',
	'id' => "description",
	'placeholder' => "Enter description",
	'disabled'=>"disabled",
];


$next = date('d-m-Y', strtotime('+4 day'));
$end_date = [
    'type' => 'text',
    "name" => "end_date",
    'class' => 'form-control',
	'id' => "end_date",
	'value'  => $next,
];


$start_date = [
    'type' => 'text',
    "name" => "start_date",
    'class' => 'form-control',
	'id' => "start_date",
	// 'placeholder' => "Enter start date",
	'value'  => "".date("d-m-Y")."",
];

$setlement_id = [
    'type' => 'hidden',
    "name" => "setlement_id",
    'id' => "setlement_id",
    'value' => "",
	
];


?>

	<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1><?php echo $this->lang->line("Settlment_Header"); ?></h1>
			</div>
		</div>
		</div>
		</section>
	
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
					
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Settlment_Title"); ?> </h3>
				</div>

					<div class="card-body">
					<form id="FormSubmit" autocomplete="off" data-plus-as-tab="true">  
					 <?php  echo form_input($setlement_id); ?>
					
						
						<div class="row">
						
							<div class="col-sm-2">
							<div class="form-group">
							<label><?php echo $this->lang->line("Settlment_Exchange"); ?></label>
								
							<select class="form-control" style="width: 100%;" name="exchange_id" id="exchange_id">
							
							<!--
							<option value="" disabled>Select Exchange</option>
							-->
							
							<?php 
								foreach ($Exchange as $row_B)
								{
								?>	
									<option value="<?php echo $row_B->exchange_id; ?>" class="active"><?php echo strtoupper($row_B->name); ?></option>
								<?php
								}
							?>
							</select>
							
							</div>
							</div>
							
							
							<div class="col-sm-1">
							<div class="form-group">
							<label>S.No</label>
							
							
							
								<?php 
												
								if($Count_Setlement['count_row'] > 0)
								{
									$no = $Count_Setlement['count_row']; 
								}	
								else
								{
									$no = "1";
								}
								
								?>
							
							
							
							<input type="text" name="setlement_no" id="setlement_no" class="form-control" value="<?php echo $no;  ?>" disabled>
							
							</div>
							</div>
							
							
							
							<div class="col-sm-2">
							<div class="form-group">
							<label><?php echo $this->lang->line("Settlment_Start_Date"); ?></label>
							<?php  echo form_input($start_date); ?>
							</div>
							</div>
							
							
							
							
							
						
							<div class="col-sm-2">
							<div class="form-group">
							<label><?php echo $this->lang->line("Settlment_End_Date"); ?></label>  
							<?php  echo form_input($end_date); ?>
							</div>
							</div>
							 
						
							
							<div class="col-sm-3">
							<div class="form-group">
							<label><?php echo $this->lang->line("Settlment_Description"); ?></label>
							<?php  echo form_input($description); ?>
							</div>
							</div>
							
							<div class="col-sm-2">
							<div class="form-group">
							<label>&nbsp;</label><br/>
							<button id="Submit" type="submit" class="btn btn-primary">Submit</button>
							</div>
							</div>
							
							
							
						</div>
							
						
							
					</form>  
					</div>
					
			</div>
			</div>

		<section class="content">
		<div class="container-fluid">
		<div class="row">
		<div class="col-12">

				<div class="card card-primary card-outline">
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Settlment_Title"); ?></h3>

				<div class="card-tools">
				<button style="" class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->lang->line("Action"); ?>
				<span class="caret"></span></button>

					<ul class="dropdown-menu" style="cursor: pointer;">
					<li align="center"><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"> <?php echo $this->lang->line("Delete_Bulk"); ?></a></li>
					<li align="center"><a name="Ledger_Yes" id="Ledger_Yes" class="Ledger_Yes"> Ledger Yes</a></li>
					<li align="center"><a name="Ledger_No" id="Ledger_No" class="Ledger_No"> Ledger No</a></li>
					</ul>
				</div>
				
				</div>
					<div class="card-body" id="Response"></div> <!-- Load AJAX Table -->
				</div>

		</div>
		</div>
		</div>
		</section>
	
	</div>  <!-- content-wrapper End -->

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/settlment.js"></script>


