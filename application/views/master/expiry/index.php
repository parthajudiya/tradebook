<?php


$ex_date = [
    'type' => 'text',
    "name" => "ex_date",
    'class' => 'form-control',
	'id' => "ex_date",
	'placeholder' => "Enter Expiry Date",

];

$expiries_id = [
    'type' => 'hidden',
    "name" => "expiries_id",
    'id' => "expiries_id",
    'value' => "",
	
];
?>




	<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1><?php echo $this->lang->line("Expiry_Header"); ?></h1>
			</div>
		</div>
		</div>
		</section>
	
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
					
				
				
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Expiry_Title"); ?></h3>

				<div class="card-tools">
				<input type="button" name="fileDate" id="fileDate" value="Import Date" class="btn  btn-sm importbtn">
				</div>
				
				</div>
				<div class="card-body">
					<form id="FormSubmit" autocomplete="off"> 
					 <?php  echo form_input($expiries_id); ?>
					
						
						<div class="row">
						
						<div class="col-sm-2">
							<div class="form-group">
								<label><?php echo $this->lang->line("Expiry_Exchange"); ?></label>
								
								<select class="form-control" style="width: 100%;" name="exchange_id" id="exchange_id">
								
								<!--
								<option value="" >Select Exchange</option>
								-->
								
								<?php 
									foreach ($Exchange as $row_B)
									{
									?>	
										<option value="<?php echo $row_B->exchange_id; ?>"><?php echo strtoupper($row_B->name);?></option>
									<?php
									}
								?>
								</select>
							</div>
						</div>

	
		
						<div class="col-sm-3">
							<div class="form-group">
								<label>Symbol</label>
									<select class="form-control select2" id="symbol_id" name="symbol_id" style="width: 100%;" required>
									<!--<option>Select First Exchange</option>-->	
									</select>
								</div>
						</div>



							
							
							<div class="col-sm-2">
							<div class="form-group">
							<label><?php echo $this->lang->line("Expiry_ex_date"); ?></label>
							<?php  echo form_input($ex_date); ?>
							</div>
							</div>
							
							<div class="col-sm-2">
							<label>Status</label>
							<select class="form-control" style="width: 100%;" name="status" id="status">
							<option value="0">Active</option>
							<option value="1">Deactive</option>
							</select>
							</div>	

							
							<div class="col-sm-2">
							<div class="form-group">
							<label>&nbsp;</label> <br/>
							<button id="Submit" type="submit" class="btn btn-primary">Submit</button>
							</div>
							</div>
						
							 
						</div>
							
						
							
					</form> 
					</div>
					
			</div>
			</div>

		<section class="content">
		<div class="container-fluid">
		<div class="row">
		<div class="col-12">

				<div class="card card-primary card-outline">
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Expiry_Title"); ?></h3>

				<div class="card-tools">
				<button style="" class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->lang->line("Action"); ?>
				<span class="caret"></span></button>

					<ul class="dropdown-menu" style="cursor: pointer;">
					<li align="center" ><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"> <?php echo $this->lang->line("Delete_Bulk"); ?></a></li>
					</ul>
				</div>
				
				</div>
					<div class="card-body" id="Response"></div> <!-- Load AJAX Table -->
				</div>

		</div>
		</div>
		</div>
		</section>
	
	</div>  <!-- content-wrapper End -->

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/expiry.js"></script>
  
<script type="text/javascript">
			$(document).ready(function (){
				$(document).on('click', '#fileDate', function(){	
				
				var exchange_id = $("#exchange_id").val();
				 // alert(exchange_id);
					$.ajax({
							url: base_url+"master/expiry/UploadDate",
							type: "POST",
							// dataType: "json", // 
							data:  {exchange_id:exchange_id},
							success: function(datas , result)
							{
								
										  if (datas == 1 || datas == 3) 
										  {
											$("#FormSubmit")[0].reset();  // Form Clear after insert
											load_data(); // Load Ajax
											
											$(function() {
												const Toast = Swal.mixin({
												toast: true,
												position: 'top-end',
												showConfirmButton: false,
												timer: 3000
												});
												toastr.success(' Added Successfully.')
											});
										}
										else if (datas == 2) 
										{
												$(function() {
												const Toast = Swal.mixin({
												toast: true,
												position: 'top-end',
												showConfirmButton: false,
												timer: 3000
												});
												toastr.error(' Already Avalilable.')
											});
										}
										else
										{
												$(function() {
												const Toast = Swal.mixin({
												toast: true,
												position: 'top-end',
												showConfirmButton: false,
												timer: 3000
												});
												toastr.error('error')
											});
										}
										
									
								
							}
					});
					return false;
				});
			});
</script>

	
	<script>
		// Edit Record Start
		function Edit(id)
        {
			$(document).ready(function(){
			$.ajax({
					data:  "id="+id,
					url: base_url+"master/expiry/Edit/"+id,
					type: "POST",
					dataType: 'json',
				
					success: function(data)
					{
					
						$("#expiries_id").val(data.expiries_id); 
						symbol_id_global=Object.seal(data.symbol_id);	
						
						
						$("#exchange_id").val(data.exchange_id).trigger('change');
						var arr1 = data.ex_date.split('-');
						
						
						/*
						console.log('year: ', arr1[0]);
						console.log('month: ', arr1[1]);
						console.log('date: ', arr1[2]);
						*/
						
								
						$("#ex_date").val(arr1[0] + "-" + arr1[1] + "-" + arr1[2]);		
						
						$("#status").val(data.status);
						$('#FormSubmit').attr('id','FormUpdate');  // Change ID
					}
				});
				return false;
			});
		}
		// Edit Record End
		
		
		
	 </script>
	
	