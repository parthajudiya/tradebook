	
	<table id="example2" class="table table-bordered table-hover MyTable">

		<thead>
			<tr>
			<th><input type="checkbox" id="select_all" value=""/></th> 
			<th><?=$this->lang->line("Expiry_Exchange"); ?></th>
			<th>Symbol</th>
			<th><?=$this->lang->line("Expiry_ex_date"); ?></th>
			<th>Status</th>
			<th>Default</th>
		
			<th><?=$this->lang->line("Action") ?></th>
			</tr>
		</thead>

		<tbody>
			
		
			<?php foreach($expiries as $row) { ?>
			
			<?php 
				// $ex_dt1 = explode('-', $row->ex_date);
				// $ex_dt1 = "$ex_dt1[2]-$ex_dt1[1]-$ex_dt1[0]";
			?>

			<?php 
			 

			$CheckBox="";
			$DeletebtnStyle="";
			$color="";
			if($row->status=="0" && $row->is_Default=="1")
			{
				$color="style='background: skyblue;'";
				$DeletebtnStyle="display:none;";
				$CheckBox="";
				
			}else{
				$CheckBox='<input style="<?php echo $CheckBox; ?>" type="checkbox"  name="checked_id[]" class="checkbox" value="'.$row->expiries_id.'">';
			}
				
			
			
			
			
			?>
				<tr <?=$color?>> 
				<td><?=$CheckBox?></td>
				<td ondblclick="Edit(<?php echo $row->expiries_id; ?>)"><?php  echo  strtoupper($row->name); ?></td>
				<td ondblclick="Edit(<?php echo $row->expiries_id; ?>)"><?php echo($row->symbol_id=="0")?"ALL":$row->symbol; ?></td>
				<td ondblclick="Edit(<?php echo $row->expiries_id; ?>)"><?php  echo  $row->ex_date; ?></td>
				
				<?php 
					$defaultStatus="";
					if($row->status=="0")
					{
						$defaultStatus='onclick="Edit_is_Default('.$row->expiries_id.')"';
					?>	
							
						<td ondblclick="Edit(<?php echo $row->expiries_id; ?>)"><span class="badge bg-success">Active</span></td>
					<?php 	
					}
					else // 1
					{
					?>
						<td ondblclick="Edit(<?php echo $row->expiries_id; ?>)"><span class="badge bg-danger">Deactive</span></td>
					<?php 	
					}	
			    ?>
					
					
					<?php 
					if($row->is_Default=="1")
					{
					?>	
						<td <?=$defaultStatus;?>><span class="badge bg-success">Yes</span></td>
					<?php 	
					}
					else 
					{
					?>
						<td <?=$defaultStatus;?>><span class="badge bg-danger">No</span></td>
					<?php 	
					}	
					?>
					


					<td >
					<button style="<?php echo $DeletebtnStyle;?>" type="button"  class="btn btn-sm btn-default" data-toggle="tooltip"  data-placement="left" title="Delete" data-original-title="Delete"  onclick="Delete(<?php echo $row->expiries_id; ?>)">
					<i class="fa fa-trash"></i>
					</button>
					</td>
			</tr>
			<?php  } ?>
		</tbody>

	</table>
					
					
<script src="<?php echo base_url(); ?>assets/customjs/multi_chkbox.js"></script>


