<?php

$Exchange_Name = [
    'type' => 'text',
    "name" => "name",
    'class' => 'form-control',
	'id' => "name",
	'placeholder' => "Enter Exchange Name",
];


$Rate_Decimal_Point = [
    'type' => 'text',
    "name" => "decimal_points",
    'class' => 'form-control',
	'id' => "decimal_points",
	'placeholder' => "Enter Rate Decimal Point",

];

$Exchange_id = [
    'type' => 'hidden',
    "name" => "exchange_id",
    'id' => "exchange_id",
    'value' => "",
	
];
 
?>


	<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1><?php echo $this->lang->line("Exchange_Header"); ?></h1>
			</div>
		</div>
		</div>
		</section>
	
			<!--
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
					
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Exchange_Title"); ?> </h3>
				</div>

					<div class="card-body">
					<form id="FormSubmit" autocomplete="off">   <?php //echo form_open(); ?>
					 <?php  echo form_input($Exchange_id); ?>
					
						
						<div class="row">
						
							<div class="col-sm-2">
							<div class="form-group">
							<label><?php echo $this->lang->line("Exchange_Name"); ?></label>  
							<?php  echo form_input($Exchange_Name); ?>
							</div>
							 </div>
							 
							<div class="col-sm-3">
							<div class="form-group">
							<label><?php echo $this->lang->line("Rate_Decimal_Point"); ?></label>
							<?php  echo form_input($Rate_Decimal_Point); ?>
							</div>
							</div>
						
						</div>
							
						<div class="card-footer" style="background-color: #FFF">
						<button id="Submit" type="submit" class="btn btn-primary">Submit</button>
						</div>
							
					</form>  <?php // echo form_close(); ?>
					</div>
					
			</div>
			</div>
			-->


	


		<section class="content">
		<div class="container-fluid">
		<div class="row">
		<div class="col-12">

				<div class="card card-primary card-outline">
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Exchange_Title"); ?></h3>

				<!--
				<div class="card-tools">
				<button style="" class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->lang->line("Action"); ?>
				<span class="caret"></span></button>

					<ul class="dropdown-menu" style="cursor: pointer;">
					<li align="center" ><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"> <?php echo $this->lang->line("Delete_Bulk"); ?></a></li>
					</ul>
				</div>
				-->
				
				</div>
					<div class="card-body" id="Response"></div> <!-- Load AJAX Table -->
				</div>

		</div>
		</div>
		</div>
		</section>
	
	</div>  <!-- content-wrapper End -->

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/exchange.js"></script>
  
 







		  


  