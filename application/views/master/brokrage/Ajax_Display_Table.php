	
	<table id="example2" class="table table-bordered table-hover MyTable">

		<thead>
			<tr>
			<th><input type="checkbox" id="select_all" value=""/></th> 
			<th>Exchange</th>
			<th><?=$this->lang->line("Brokrage_Symbol"); ?></th>
			<th><?=$this->lang->line("Brokrage_Type"); ?></th>
			<th><?=$this->lang->line("Brokrage_Base_On"); ?></th>
			<th><?=$this->lang->line("Brokrage_Intra_Day");?></th>
			<th><?=$this->lang->line("Brokrage_Side") ?></th>
			<th><?=$this->lang->line("Brokrage_Next_Day"); ?></th>
			<th><?=$this->lang->line("Brokrage_Minimum"); ?></th>
			<th><?=$this->lang->line("Action") ?></th>
			</tr>
		</thead>

		<tbody>
			<?php foreach($Brokrage as $row) { ?>

			<tr> 
				<td><input type="checkbox"  name="checked_id[]" class="checkbox" value="<?php echo  $row->bid; ?>"/></td>
				
				<?php /*
				<td><?php  echo  $row->symbol; //name; //symbol_id; ?></td>
				*/
				?>
				
				<td><?php echo strtoupper($row->name); ?></td>
				
				<td><?php echo($row->symbol_id=="0")?"ALL":$row->symbol; ?></td>


				<?php 
					if($row->brokrage_type_id=="1")
					{
					?>	
						<td>Flat</td>
					<?php 	
					}
					else // 2
					{
					?>
						<td>SQUAREUP</td> <!-- Both -->
					<?php 	
					}	
				?>	


				
				
				<?php 
					if($row->base_on_id=="1")
					{
					?>	
						<td>Amount</td>
					<?php 	
					}
					else if($row->base_on_id=="2")// 2
					{
					?>
						<td>Lot</td>
					<?php 	
					}
					else
					{
						?>
						<td>Rate</td>
						<?php 
					}			
				?>


				<td><?php  echo  $row->intraday; ?></td>



				<?php 
					if($row->side_id=="1")
					{
					?>	
						<td>Both Side</td>
					<?php 	
					}
					else // 2
					{
					?>
						<td>High Side</td>
					<?php 	
					}	
				?>


				<td><?php  echo  $row->nxt_day; ?></td>



				<?php 
					if($row->minimum_id=="1")
					{
					?>	
						<td>Yes</td>
					<?php 	
					}
					else // 2
					{
					?>
						<td>No</td>
					<?php 	
					}	
				?>

		


				<td>
					<button  type="button"  class="btn btn-sm btn-default" data-toggle="tooltip"  data-placement="left" title="Delete" data-original-title="Delete"  onclick="Delete(<?php echo $row->bid; ?>)">
					<i class="fa fa-trash"></i>
					</button>
				</td>
			</tr>
			<?php  } ?>
		</tbody>

	</table>
					
<script src="<?php echo base_url(); ?>assets/customjs/multi_chkbox.js"></script>



