<?php



$Name = [
    'type' => 'text',
    "name" => "name",
    'class' => 'form-control',
	'id' => "name",
	'placeholder' => "Enter Name",
];


$Code = [
    'type' => 'text',
    "name" => "code",
    'class' => 'code form-control',
	'id' => "code",
	'placeholder' => "Enter Account Code",

];


?>




	<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1><?php echo $this->lang->line("Brokrage_Header"); ?></h1>
			</div>
		</div>
		</div>
		</section>
	
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
					
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Brokrage_Title"); ?> </h3>
				</div>

					<div class="card-body"> <!-- card-body stat -->
					<form id="FormSubmit" autocomplete="off">   
					
					 
					 <input type="hidden" name="bid" id="bid" placeholder="bid" value="">
					 <input type="hidden" name="aid" id="aid" placeholder="aid" value="">
					
						
						
						<div class="row">  <!-- row stat -->
						
							<div class="col-sm-3">
							<div class="form-group">
							<label><?php echo $this->lang->line("Brokrage_Account_Code"); ?></label>  
							<?php  echo form_input($Code); ?>
							</div>
							 </div>
							 
							<div class="col-sm-3">
							<div class="form-group">
							<label><?php echo $this->lang->line("Brokrage_Name"); ?></label>
							<?php  echo form_input($Name); ?>
							</div>
							</div>
							 
							<div class="col-sm-3">
							<div class="form-group">
								<label><?php echo $this->lang->line("Brokrage_Exchange"); ?></label>
								<select class="form-control select2" style="width: 100%;" name="exchange_id" id="exchange_id">
								
								<option selected="selected" value="">Select Exchange</option>
									<?php 
									foreach ($Exchange as $row)
									{
									?>
										<option value="<?php echo $row->exchange_id; ?>"><?php echo strtoupper($row->name);?></option>
									<?php 
									}
									?>
								</select>
							</div>
							</div>
							
							
							
							
							
							<div class="col-sm-2">
							<div class="form-group">
								<label>Symbol</label>
									<select class="form-control" id="symbol_id" name="symbol_id" required>
										<option>Select First Exchange</option>
									</select>
								</div>
							</div>
							
							
							<div class="col-sm-1">		
							<div class="form-group">
							<label>&nbsp;</label>
							<button id="ok" name="ok"  class="btn btn-info btn-sm form-control">OK</button>
							</div>
						</div>
						
					</div>  <!-- row end -->
					
					
						
						 
						<div class="row" id="row2">   <!-- row 2 stat -->
						
						
							<div class="col-sm-3">
							<div class="form-group">
								<label>Brokrage Type</label>

								<select class="form-control" style="width: 100%;" name="brokrage_type_id" id="brokrage_type_id">
								
								<option  value="">Select Brokrage Type</option>
								<option value="1">Flat</option>
								<option value="2"> SQUAREUP</option>
								
								</select>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label>Base on</label>
								<select class="form-control " style="width: 100%;" name="base_on_id" id="base_on_id">
								
										<option value="">Select Base on</option>
										<option value="1">Amount</option>
										<option value="2">Lot</option>
										<option value="3">Rate</option>
								</select>
							</div>
							</div>
							
							<div class="col-sm-2">
							<div class="form-group">
							<label>Intra Day % </label>
							<input type="text" name="intraday" id="intraday" class="form-control" >
							</div>
							</div>
							
							<div class="col-sm-2">
							<div class="form-group">
								<label>Side</label>
								<select class="form-control" style="width: 100%;" name="side_id" id="side_id">
								<option selected="selected" value=""> Select Side</option>
								<option value="1">Both Side</option>
								<option value="2">High Side</option>
								</select>
							</div>
							</div>

						
							<div class="col-sm-2">
							<div class="form-group">
							<label>Next Day % </label>
							<input type="text" name="nxt_day" id="nxt_day" class="form-control" placeholder="Next Day" >
							</div>
							</div>
								
								<div class="col-sm-2">
								<label>Minimum</label>
								<select class="form-control" style="width: 100%;" name="minimum_id" id="minimum_id">
								<option selected="selected" value=""> Select Minimum</option>
								<option value="1">Yes</option>
								<option value="2">No</option>
								</select>
								</div>
						</div> <!-- row 2 end -->
						
						
						
						
							
				</div>	 <!-- card-body end --> 			
						
							
				<div class="card-footer" style="background-color: #FFF">
				<button id="Submit" type="submit" class="btn btn-primary">Submit</button>
				</div>
							
			</form>  
		</div> 
		</div>
			


	


		<section class="content">
		<div class="container-fluid">
		<div class="row">
		<div class="col-12">

				<div class="card card-primary card-outline">
				<div class="card-header">
				<h3 class="card-title"><?php echo $this->lang->line("Brokrage_Title"); ?></h3>

				<div class="card-tools">
				<button style="" class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->lang->line("Action"); ?>
				<span class="caret"></span></button>

					<ul class="dropdown-menu" style="cursor: pointer;">
					<li align="center" ><a name="bulk_delete_submit" id="bulk_delete_submit" class="delete_all"> <?php echo $this->lang->line("Delete_Bulk"); ?></a></li>
					</ul>
				</div>
				
				</div>
					<div class="card-body" id="Response"></div> <!-- Load AJAX Table -->
				</div>

		</div>
		</div>
		</div>
		</section>
	
	</div>  <!-- content-wrapper End -->
	
	
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/brokrage.js"></script>


<script>
 


/*
$(document).ready(function()
{
		$('#row2').hide();	
		 $(document).on('click', '#ok', function() {
		// $('#ok').click(function () {
		//$("button[name=ok]").click(function()  {
		
			var code = $("#code").val();
			var name = $("#name").val();
			var exchange_id = $("#exchange_id").val();
			var symbol = $("#symbol").val();
			
			if(code == "")
			{
				$('#row2').hide();
			}
			else if(name == "")
			{
				$('#row2').hide();
			}
			else if(exchange_id == "")
			{
				$('#row2').hide();
			}
			else if(symbol == "")
			{
				$('#row2').hide();
			}
			else
			{
				$('#row2').show();	
				return false;  // return false is Stop is form submit
			}
		});
});
*/
$(document).ready(function()
{
	$('#row2').hide();	
	$(document).on('click', '#ok', function() {
		
		var code = $("#code").val();
		var name = $("#name").val();
		var exchange_id = $("#exchange_id").val();
		var symbol_id = $("#symbol_id").val();
		
		// console.log(code);
		// console.log(name);
		// console.log(exchange_id);
		// console.log(symbol);
		
		if(code == "")
		{
			$('#row2').hide();
			//alert("me");
		}
		else if(name == "")
		{
			$('#row2').hide(); 
			//alert("me");
		}
		else if(exchange_id == "")
		{
			$('#row2').hide();
			alert("Select Exchange");
		}
		else if(symbol_id == "")
		{
			$('#row2').hide();
			//alert("me");
		}
		else
		{
			$('#row2').show();	
			
			 // html += '<option value='+data[i].cid+'>'+data[i].city_name;+'</option>';
			 
			 /*
				html += '<div class="col-sm-4">';
				html += '<div class="blog-article">';
				html += '<div class="blog-article-thumbnail">';
				// <img src="images/<?php echo $row2['photos'];?>" alt="">';
				html += '<img src="'+data[i].photos+'" alt="">';
				// html += '<p>city:: '+data[i].cid;+'</p>';							
				html += '<p>city:: '+data[i].city_name;+'</p>';							
				html += '</div>';
				// html += '<h4>'+data[i].name;+'</h4>';
				html += '</div>';
				html += '</div>';
			*/
			// $('#exchange_id').on('change', function(event){ });
		
			
			// var exchange= $('#exchange_id').val();
			// var name= $('#name').val();
			// var code= $('#code').val();
			
				$.ajax({
					url: base_url+"master/brokrage/Edit_Record/",
					method : "POST",
					data: {code: code , name: name , exchange_id: exchange_id , symbol_id:symbol_id },
					async : true,
					dataType : 'json',
					success:function(data)
					{
						
						$('#exchange_id-error').hide();
						
						// alert("success");
						
						// var htmlA = '';
						var htmlB = '';
						var i;
						for(i=0; i<data.length; i++)
						{
							$('#bid').val(data[i].bid);
							
							$('#aid').val(data[i].aid);

							$('#brokrage_type_id').find("option").removeAttr("selected");
							$('#brokrage_type_id').find("option[value='"+data[i].brokrage_type_id+"']").attr("selected","selected");

							$('#base_on_id').find("option").removeAttr("selected");
							$('#base_on_id').find("option[value='"+data[i].base_on_id+"']").attr("selected","selected");

							$('#intraday').val(data[i].intraday);

							$('#side_id').find("option").removeAttr("selected");
							$('#side_id').find("option[value='"+data[i].side_id+"']").attr("selected","selected");

							$('#nxt_day').val(data[i].nxt_day);


							$('#minimum_id').find("option").removeAttr("selected");
							$('#minimum_id').find("option[value='"+data[i].minimum_id+"']").attr("selected","selected");
								
						 }
						// $("#row2").html(htmlB);
						// $("#brokrage_type_id").html(htmlB);
					}
				});
			
			 }
			return false;  // return false is Stop is form submit
		//}
	});
});


</script>


<script>
	// Autocomplete Start For Account-Name Searching 
	  $(document).ready(function()
	  {
			$("#name").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"master/brokrage/Search_Account_Name/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							// If Wrong Data Type In Text Box Will null 
							$('#uid').val(""); 
							$('#code').val("");
							$('#name').val("");
							toastr.error('Please Enter Correct Acccount Name')
					
						}
						else
						{
							$("#bid").val(""); // Hidden Id Clear
							$("#aid").val(""); // Hidden Id Clear
							
							// load_data();
							$('#code-error').hide(); // label Hide
							$('#code').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
							response(data); // This is Auto complete code  only if part is added
						}
					}
				});
			},
			select: function (event, ui) 
			{
				$('#name').val(ui.item.label);
				$('#uid').val(ui.item.value);
				$('#code').val(ui.item.code);
				return false;
			}
		});
	});
	// Autocomplete End For Account-Name Searching 
  
</script>

<script>
// Autocomplete Start For Account-Code Searching 
	  $(document).ready(function()
	  {
			$("#code").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"master/brokrage/Search_Account_Code/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							// If Wrong Data Type In Text Box Will null
							$('#uid').val("");  
							$('#code').val("");
							$('#name').val("");
							toastr.error('Please Enter Correct Acccount Code')
					
						}
						else
						{
							$("#bid").val(""); // Hidden Id Clear
							$("#aid").val(""); // Hidden Id Clear
							
							// load_data();
							$('#name-error').hide(); // label Hide
							$('#name').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
							response(data); // This is Auto complete code  only if part is added
						}
					}
				});
			},
			select: function (event, ui) 
			{
				$('#code').val(ui.item.label);
				$('#uid').val(ui.item.value);
				$('#name').val(ui.item.name);
				// $('#aid').val(ui.item.aid);
				return false;
			}
		});
	});
	// Autocomplete End For Account-Code Searching 
  
</script>

<script type="text/javascript">
	/*
	$(document).ready(function(){
		
			// Start Code Using search Code And 
			$('#code').on('change', function(event){ // change
		
			var inputVal = $(this).val();
			// var resultTBL= $(this).siblings("#Response");
				if(inputVal.length){
			  
						$.ajax({
							url: base_url+"master/brokrage/Search_Accounts_C/",
							type: 'post',
							data: {code: inputVal},
							dataType: "json",
							success:function(data)
							{
								// $('#aid').val(data[0].aid);
								
								console.log(data);
								//alert("ale");
							}
						// load_data();
						});
					} 	
				});
		});
		*/
		
</script>	




<script type="text/javascript">
//***************** Display Table Using Search ***********/
	/*
	$(document).ready(function(){
		
		
		
			// Start Code Using search Code And 
			$('#code').on('change', function(event){ // change
			// $('#exchange_id').on('change', function(event){ // change // exchange_id
		
			//var inputVal=$('#code').val();
			
			var inputVal = $(this).val();
			var resultTBL= $(this).siblings("#Response");
			if(inputVal.length){
			  
				$.ajax({
				url: base_url+"master/brokrage/searchTable_Code/",
				type: 'post',
				data: {term: inputVal},
				success:function(data)
				{
				
					//$('#aid').val();
					
					$('#Response').html(data);
					$('.MyTable').DataTable({
						"paging": true,
						"lengthChange": true,
						"searching": true,
						"ordering": true,
						"info": true,
						"autoWidth": false,
						"responsive": true,
						"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
						"iDisplayLength": 5
					});
				}
				// load_data();
				});
			} 
			else
			{
				resultTBL.empty();
			}
		});
		// End Code Using search Code 
		
			// Start Code Using search name 
			$('#name').on('change', function(event){
			var inputVal = $(this).val();
			var resultTBL= $(this).siblings("#Response");
			if(inputVal.length){
			  
				$.ajax({
				url: base_url+"master/brokrage/searchTable_NAME/",
				type: 'post',
				data: {term: inputVal},
				
				success:function(data)
				{
					$('#Response').html(data);
					$('.MyTable').DataTable({
						"paging": true,
						"lengthChange": true,
						"searching": true,
						"ordering": true,
						"info": true,
						"autoWidth": false,
						"responsive": true,
						"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
						"iDisplayLength": 5
					});
				}
				// load_data();
				});
			} 
			else
			{
				resultTBL.empty();
			}
		});
		// End Code Using search name 
		
	});

	*/
	//******************************** Display Table Using Search END*******************************/
	
		
		
		
		
			/*
			$('#exchange_id').on('change', function(event){ // change // exchange_id
		
			// var inputVal=$('#exchange_id').val(); // or var inputVal = $(this).val();
			var exchange= $('#exchange_id').val();
			var name= $('#name').val();
			var code= $('#code').val();
			
			
			
			// var resultTBL= $(this).siblings("#Response");
			// if(exchange.length){
			  
				$.ajax({
				url: base_url+"master/brokrage/searchTable/",
				type: 'post',
				data: {term1: exchange , term2: name , term3: code},
				success:function(data)
				{
				
					//$('#aid').val();
					
					$('#Response').html(data);
					$('.MyTable').DataTable({
						"paging": true,
						"lengthChange": true,
						"searching": true,
						"ordering": true,
						"info": true,
						"autoWidth": false,
						"responsive": true,
						"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
						"iDisplayLength": 5
					});
				}
				// load_data();
				});
			// } 
			// else
			// {
				// resultTBL.empty();
			// }
		});
		// });
	*/
</script>





<script type="text/javascript">
// searchTable_NAME
	// dataType: "json",
/*
	$(document).on('keyup','#code',function( e ) {    

		var code = $('#code').val();
         

          $.ajax({
            
           
			url: base_url+"master/brokrage/Search_Account_Code_keyup/",
            type: 'post',
            dataType: "json",
            data: {code: code},
				success: function(data)  // , response
				{
					
					if (!$.trim(data))
					{   
						$('#uid').val("");
						$('#name').val("");
						
					}
					else
					{
						//alert(data.name);
						//alert($('#name').val(data[0].name));
						$('#uid').val(data[0].uid);
						$('#name').val(data[0].name);						
					}
				}
          });
          return false;
      }); 
  
  */
  
  
  
  // $('#code input[type="text"]').on("keyup input", function(){
  	  // alert("code");
	  
	  /*
    // Set search input value on click of result item
    $(document).on("click", "#Response", function(){ // p
        // $(this).parents(".search-box").find('input[type="text"]').val($(this).text());
        $(this).parent("#Response").empty();
    });
	
	  // $.get("backend-search.php", {term: inputVal}).done(function(data){
			// $.get(base_url+"master/brokrage/searchTable_Code/",{term: inputVal}).done(function(data){
			// url: base_url+"master/brokrage/Search_Account_Code/",	
			//resultTBL.html(data);
	
	
	*/
  </script>


		  


  