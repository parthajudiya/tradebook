
<div class="content-wrapper">
	
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Import Form Excel - Mt</h1>
				</div>
			</div>
		</div>
	</section>
	
	
	<div class="col-md-12">
		<div class="card card-primary card-outline"> 
			
			<div class="card-body">
				
				<form id="FormSubmit"  autocomplete="off" method="post" enctype="multipart/form-data"> 
					
					<div class="row">
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>Exchange</label>
								
								<select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
									<?php 
										
										foreach ($Exchange as $row_B)
										{
										?>	
										<option value="<?php echo $row_B->exchange_id;?>"> <?php echo strtoupper($row_B->name); ?> </option>
										<?php
										}
									?>
								</select>
							</div>
						</div>
						
						
						<div class="col-sm-3">
							<div class="form-group">
								<label>Setlment</label>
								<select class="form-control " style="width: 100%;" name="setlement_id" id="setlement_id">
								</select>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label>Upload</label><br/>
								<input type="file" id="FileData" name="FileData">
							</div>
						</div>
						
						<div class="col-sm-1">
							<div class="form-group"><br/>
								<label>FW</label>
								<input type="checkbox" id="type" name="type" value="2">
							</div>
						</div>
						
						
						<div class="col-sm-1">
							<label>&nbsp;</label><br>
							<button id="Submit" type="submit" class="btn btn-primary">Get Date</button>
						</div>
						
					</div>
					
				</form> 
			</div>
			
		</div>
	</div>
	
	
	
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					
					
					
					<div class="card card-primary card-outline">
						
						<div class="card-header">
							<h3 class="card-title"><?php echo $this->lang->line("Account_Title") ?></h3>
							<div class="card-tools">
								<button id="SubmitTble" type="submit" class="btn btn-primary" style="background-color: #000000;border-color: #000000;">Submit</button>
							</div>
						</div>
							
							
							
							<div class="row" style="margin-left:15px;">
								
								
								<div class="col-sm-2">
									<div class="form-group">
										<label>Excel Symbol :</label>
										<input type="text" class="form-control"  id="OldSymbol" name="OldSymbol">
									</div>
								</div>
								
								<div class="col-sm-2">
									<div class="form-group">
										<label>Actual Symbol :</label>
										<select class="form-control select2"  style="width: 100%;" id="symbol_id" name="symbol_id" >
										</select>
									</div>
								</div>
								
								<div class="col-sm-2">
									<div class="form-group">
										<label>Expirey Date :</label>
										<select class="form-control "  style="width: 100%;" id="ex_date" name="ex_date" >
										</select>
									</div>
								</div>
								
								<div class="col-sm-2">
									<div class="form-group">
										<label>Excel Party Code :</label>
										<input type="text" class="form-control"  id="Old_party_code" name="Old_party_code">
									</div>
								</div>
								
								<div class="col-sm-2">
									<div class="form-group">
										<label>Actual Party Code :</label>
										<input type="text" class="form-control"  id="party_code" name="party_code">
										<input type="hidden" placeholder="pid"  id="pid" name="pid">
										<input type="hidden" placeholder="party_name"  id="party_name" name="party_name">
									</div>
								</div>
								
								<div class="col-sm-2">
									<label>&nbsp;</label><br/>
									<input type="submit" class="btn btn-primary" name="update" id="update" value="Update">
									
								</div>
								
							</div>
							
							
							
							<div class="row" style="margin-left:15px;">
								<div class="col-sm-2">
									<div class="form-group">
										<label>Expirey  M20</label>
										<select type="text" class="form-control"  id="M20" name="M20" >
										</select>
									</div>
								</div>
								
								
								<div class="col-sm-2">
									<div class="form-group">
										<label>Expirey Date :</label>
										<select class="form-control "  style="width: 100%;" id="ex_date_m20" name="ex_date_m20" >
										</select>
									</div>
								</div>
								
								<div class="col-sm-2">
									<label>&nbsp;</label><br/>
									<input type="submit" class="btn btn-primary" name="update2" id="update2" value="Update">
								</div>
								
								
								
							</div>
							
							
						
						<div class="card-body" id="Response"></div> <!-- Load AJAX Table -->
						</div>
						
					</div>
					
				</div>
				
			</div>
			
		
		
		
	</section>
	
</div>  <!-- content-wrapper End -->

<script>
	var url = url;
	var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/import_form_excel.js"></script>





<script>
	var symbol_id_global;
	var setlement_id_global;
	var instument=[];
	var ex_date_global = [];
	
	$(document).ready(function(){
		symbol_function(); 
	});
	
	$(document).ready(function(){
		
		$('#exchange_id').change(function(){
			symbol_function(); 
		}); 
	});
	
	$(document).ready(function(){
		
		$('#symbol_id').change(function(){
			$(document).on('change',  '#symbol_id', function() { 
				expirey_function(); 
			}); 
		});
	});
	
	function symbol_function()
	{
		
		var id=$("#exchange_id").val();
		$.ajax({
			url: base_url+"transaction/Buy_sell/Symbol_Search",
			method : "POST",
			data : {id: id},
			async : true,
			dataType : 'json',
			success: function(data)
			{
				
				var html = '';
				var i;
				
				for(i=0; i<data.length; i++)
				{
					html += '<option value='+data[i].symbol_id+'>'+data[i].symbol+'</option>';
				}
				$('#symbol_id').html(html);
				expirey_function(); 						
				
			}
		});
		return false;
	}
	function expirey_function()
	{
		var id=$("#symbol_id").val();
		var exchange_id=$("#exchange_id").val();
		
		$.ajax({
			url: base_url+"transaction/Buy_sell/Symbol_OnChange",
			method : "POST",
			data : {id: id,exchange_id:exchange_id},
			async : true,
			dataType : 'json',
			success: function(data)
			{
				var html = '';
				var i;
				for(i=0; i<data.length; i++)
				{
		
					html += '<option value='+data[i].ex_date+' '+((ex_date_global==data[i].ex_date)?"selected":"")+'>'+data[i].ex_date+'</option>';
				}
				$('#ex_date').html(html);  // first row exp
				
				$('#ex_date_m20').html(html); // m20 second exp 
			}
		});
		return false;
	}
	
	
	
	$(document).ready(function()
	{
		$("#party_code").autocomplete({
			source: function( request, response )
			{
				$.ajax({
					url: base_url+"transaction/Buy_sell/Search_Account_Code/",
					type: 'post',
					dataType: "json",
					data: {search: request.term},
					success: function( data ) 
					{
						if (!$.trim(data))
						{   
							// If Wrong Data Type In Text Box Will null
							$('#pid').val("");  
							$('#party_code').val("");
							$('#party_name').val("");
							toastr.error('Please Enter Correct Acccount Code')
							
						}
						else
						{
							//$('#name-error').hide(); // label Hide
							//$('#name').attr({class: 'form-control ui-autocomplete-input'}); // Tetx Color red Hide
							response(data); // This is Auto complete code  only if part is added
						}
					}
				});
			},
			select: function (event, ui) 
			{
				$('#pid').val(ui.item.value);
				$('#party_code').val(ui.item.label);
				$('#party_name').val(ui.item.party_name);
				return false;
			}
		});
	});
</script>



<script>
	
	var tableData;
	$(document).ready(function (){
		$(document).on('submit', '#FormSubmit', function(){ 	
			$.ajax({
				url: base_url+"process/Import_excel_mt/UploadData",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					$('#Response').html(data);
					
					// OPTION DROUP DOWN
					var option={};
					$(".M20_TR").each(function(){
						option[$(this).text()]=$(this).text();
					});
					
					tableData=$('.MyTable').DataTable({
					"paging": false,
					"lengthChange": true,
					"searching": true,
					"ordering": true,
					"info": true,
					"autoWidth": false,
					"responsive": false,
					"scrollY": 1000,
					"scrollX": 100,
					
				});
				
					// Loop In Data M
					for(i in option){
					 $("#M20").append("<option value='"+option[i]+"'>"+option[i]+"</option>");
					}
				}
			});
		return false;
		});
	});
</script>

<script>
	// start codeing update
	
	$(document).ready(function()
	{
		$(document).on('click', '#update', function()  
		{ 
			
			var Old_party_code = $("#Old_party_code").val();
			
			var OldSymbol = $("#OldSymbol").val();
			
			var NewSymbol_ID = $("#symbol_id").val();
			var NewSymbol_Name = $("#symbol_id option:selected").text();
			
			var ex_date = $("#ex_date option:selected").text();
			
			var pid = $("#pid").val();
			var party_code = $("#party_code").val();
			var party_name = $("#party_name").val();
			
			$("#example2").find("tr").each(function()
			{
				if(($(this).find(".old_symble_td").text()).toLowerCase()==OldSymbol.toLowerCase() && ($(this).find(".ex_date option:selected").text()).toLowerCase()==ex_date.toLowerCase() && ($(this).find(".Old_party_code_td").text()).toLowerCase()==Old_party_code.toLowerCase())
				{
					$(this).find(".symbol_id").val(NewSymbol_ID);
					$(this).find(".symbol_name").text(NewSymbol_Name);
					
					$(this).find("select.ex_date").val(ex_date);
					
					if(party_code!="" && pid!="" && party_name!=""){
						$(this).find(".party_code_new").text(party_code);
						$(this).find(".party_id_new").val(pid);
						$(this).find(".party_name_new").val(party_name);
					}
				}
			});
		});
	});	
	
	 
</script>
<script>
		// update2
		$(document).ready(function()
		{
			$(document).on('click', '#update2', function()  
			{ 
			
			// console.log("a");
				var ex_date_m20 = $("#ex_date_m20 option:selected").text();
				var M20 = $("#M20").val();
				
				$("#example2").find("tr").each(function()
				{
				
					if($(this).find(".M20_TR").text() == M20)
					{
						$(this).find("select.ex_date").val(ex_date_m20);
					}	
				});
			
			});
		});
		
</script>


<script>
	
	
	// Submit Table
	$(document).ready(function (){
		
		$(document).on('click', '#SubmitTble', function() {
			
			var Exchange = $("#exchange_id").val();
			var Setlment = $("#setlement_id").val();
			if($("#type").prop("checked") == true){
				var Type ="2";
				}else{
				var Type ="1";
				
			}
			
			// First is Controller variable And Second is JS Variable
			
			$("#example2").find("tr").each(function(){
				var row=this;
				var Tr_date = $(this).find(".tr_date").text();
				var Symbol_id = $(this).find(".symbol_id").val();
				// var instument = $(this).find(".instument").text();
				var ex_date = $(this).find(".ex_date").val(); // expirey_date_span
				var qty1 = $(this).find(".Qty").text();
				var rate1 = $(this).find(".rate1").text();
				
				var pid = $(this).find(".party_id_new").val();
				var party_code = $(this).find(".party_code_new").text();
				var party_name_new = $(this).find(".party_name_new").val();
				
				var buy_sell_id = $(this).find("#buy_sell_id").val();
				
				// , instument:instument
				
				$.ajax({  
					type: "POST",  
					url: base_url+"process/Import_excel_mt/SubmitData",	
					data: {exchange:Exchange,setlment:Setlment,type:Type,Tr_date:Tr_date,Symbol_id:Symbol_id ,ex_date:ex_date,qty1:qty1,rate1:rate1,pid:pid,party_code:party_code , party_name_new:party_name_new , buy_sell_id:buy_sell_id},
					success: function(data) {   
						if(data=="1"){
							row.remove();
							tableData.row( row ).remove().draw();
						}
					}
				});
			});				
			
		});
	});
	
</script>
