<?php

$Start_Date  = [
    'type' => 'text',
    "name" => "start_date",
    'id' => "start_date",
	'class'=>"form-control",
	'placeholder'=>"Start Date",
];


$End_Date  = [
    'type' => 'text',
    "name" => "end_date",
    'id' => "end_date",
	'class'=>"form-control",
	'placeholder'=>"End Date",
];

$Next_Open_Date  = [
    'type' => 'text',
    "name" => "next_open_date",
    'id' => "next_open_date",
	'class'=>"form-control",
	'placeholder'=>"Next Open Date",
	'value'  => "".date("d-m-Y")."",
];

 
$ex_date  = [
    'type' => 'text',
    "name" => "ex_date",
    'id' => "ex_date",
	'class'=>"form-control",
	'placeholder'=>"",
];

 
?>

	<div class="content-wrapper">  <!-- content-wrapper Start -->
	<div class="col-md-12">
	<div class="card card-primary card-outline"> 
			
		<div class="card-header">
		<h3 class="card-title">Closing Forward </h3>
		<div class="card-tools">
		<button id="Forward_Remove" type="submit" class="btn btn-sm btn-primary">Forward Remove</button>
		</div>
		</div>
		
				
		<div class="card-body">
		<form id="FormSubmit" name="FormSubmit" autocomplete="off">
			
		<div class="row">
						
			<div class="col-sm-1">
			<div class="form-group">
			<label>Exchange</label>
			<select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
			<!-- <option value="" >Select Exchange</option>-->
			<?php 
			// select2
			foreach ($Exchange as $row_B)
			{
				?>	
				<option value="<?php echo $row_B->exchange_id; ?>" ><?php echo strtoupper($row_B->name);?></option>
				<?php
			}
			?>
			</select>
			</div>
			</div>
						
						
			<div class="col-sm-3">
			<div class="form-group">
			<label><?php echo $this->lang->line("LBL_Closing_Setlmennt"); ?></label>
			<select class="form-control " style="width: 100%;" name="closing_setlment_id" id="closing_setlment_id">
			<!--
			<option value="">Closing Setlment</option>
			-->
			</select>
			</div>
			</div>
		
			<div class="col-sm-1">
			<div class="form-group">
			<label><?php echo $this->lang->line("LBL_Start_Date"); ?></label>
			<?php  echo form_input($Start_Date); ?>
			</div>
			</div>

			<div class="col-sm-1">
			<div class="form-group">
			<label><?php echo $this->lang->line("LBL_End_Date"); ?></label>
			<?php  echo form_input($End_Date); ?>
			</div>
			</div>
						
			<div class="col-sm-3">
			<div class="form-group">
			<label><?php echo $this->lang->line("LBL_Opening_Setlmennt"); ?></label>
			<select class="form-control " style="width: 100%;" name="opening_setlment_id" id="opening_setlment_id">
			<!--
			<option value="">Opening Setlment</option>
			-->
			</select>
			</div>
			</div>
						
			<div class="col-sm-1">
			<div class="form-group">
			<label>Next Op DT</label>
			<?php   echo form_input($Next_Open_Date); ?>
			</div>
			</div>

			<div class="col-sm-1">
			<div class="form-group">
			<label>Closing Rate</label>
			<select class="form-control " style="width: 100%;" name="closing_rate_type_id" id="closing_rate_type_id">
			<!--
			<option value="">Closing Rate Type</option>
			-->
			<option value="1">Ltp</option>
			<option value="2">BHAVCOPY</option>
			</select>
			</div>
			</div>

			<div class="col-sm-1">
			<div class="form-group">
			<label>&nbsp;</label>
			<button id="SubmitC" type="submit" class="btn btn-primary form-control">Data</button>
			</div>
			</div>
							
							
		</div>
		</form>  
		</div>
					
	</div>
	</div>

		<section class="content">
		<div class="container-fluid">
		<div class="row">
		
		
		<div class="col-7 MyDiV">
		<form id="FormResponse" name="FormResponse" autocomplete="off">	
		<div class="card card-primary card-outline">
		<div class="card-body" id="Response"></div> <!-- Load AJAX Table 1 -->
		</div>

			<div class="card-footer" style="background:white">
			<div class="card-tools">
			<div class="row"> 
			<input type="hidden" class="exchange_id" name="exchange_id">
			
			
			<div class="col-3"> 
			<div class="form-group">
			<label>Expiry Date</label>
			<select class="form-control " style="width: 100%;" name="expiries_id" id="expiries_id">
			<?php 
			foreach($Expiries as $row_exp)
			{
				?>	
				<option value="<?php echo $row_exp->expiries_id; ?>" ><?php echo $row_exp->ex_date;?></option>
				<?php
			}
			?>
			</select>
			</div>
			</div>
			
			
			
			<div class="col-2">
			<label>&nbsp;</label>
			<button type="submit" class="form-control btn btn-primary">Process</button>
			</div>
			
			</div>
			</div>
			</div>

		</div>
		</form>	
		
			<div class="col-5 MyDiV">
				<div class="card card-primary card-outline"> 
				<div class="card-body" id="Response2"></div> <!-- Load AJAX Table -->
				</div>
				
				
				<div class="card-footer" style="background:#FFF">

				<ul class="nav nav-tabs">
				<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#home">
				Get Data Online
				</a>
				</li>
				
				<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#menu2">  
				Get Data On Excel
				</a>
				</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				
				<div class="tab-pane container active" id="home"> 
				<br/>
				<div class="row">
				
						<div class="col-3"> 
						<input type="submit" id="OnlineID" name="OnlineID" class="btn btn-sm btn-primary" value="Online Data">
						</div>
						
						<div class="col-3">
						<button type="button" id="ApplyOnline" name="ApplyOnline" class="btn btn-sm btn-info">Apply Ltp </button>
						</div>
					</div>
				
				</div>
				
			
				<div class="tab-pane container " id="menu2"><br>
					<form id="FormFileData" enctype="multipart/form-data" >
					<div class="row">
						<div class="col-5">
						<div class="custom-file">
						<input type="file" id="FileData" name="FileData">
						</div>
						</div>
						
						<div class="col-2">
						<input type="submit" id="submitD" name="submit" class="btn btn-sm btn-primary" value="Upload File">
						</div>
						
						<div class="col-3">
						<button type="button" id="Apply" name="Apply" class="btn btn-sm btn-info">Apply All</button>
						</div>
						
						
					</div>
					</form>
					<a target="_blank" href="https://www1.nseindia.com/products/content/derivatives/equities/archieve_fo.htm">Link To Bhavcopy </a>
					Select Daily Setlment Price File
				
				</div>
				</div>
				</div>
			</div>
		</div>
		</div>
		</section>
	
	</div>  <!-- content-wrapper End -->

<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/closing_forward.js"></script>

<script>
	/* Global */
	var ins_glob = [];
	var exdt_glob = [];
	var syname_glob = [];
	var syname = [];
	var exdt = [];
	var insname = [];
	/* Global*/
			
			// Get Lat Price On Money Controller
			$(document).ready(function (){
			$(document).on('click', '#OnlineID', function(){
					
					syname_glob.length = 0; // Clear  Data Of APPEND
					ins_glob.length = 0; // Clear Data Of APPEND
					exdt_glob.length = 0; // Clear Data Of APPEND
					
					$("#example2 tbody").find("tr").each(function()
					{
						 // syname = $(this).find(".syname").val();
						 syname = $(this).find(".sycode").val();
						 insname = $(this).find(".insname").text();
						 exdt = $(this).find(".exdt").text();
						 
						syname_glob.push(syname);
						ins_glob.push(insname);
						exdt_glob.push(exdt);
					});
				
				
					
					$.ajax({
						url: base_url+"Process/Closing_forward/GetData_Online",
						method : "POST",
						data : {syname_glob:syname_glob , ins_glob:ins_glob , exdt_glob:exdt_glob},
						// async : false,
						dataType : 'JSON',
						success: function(data){
						
						for(i=0; i<data.length; i++)
						 {
							 
							$("#example2").find("tr").each(function()
							{
							
							
								var exdt = $(this).closest("tr").find(".exdt").text(); 
								var syname = $(this).closest("tr").find(".sycode").val(); 
								var insname = $(this).closest("tr").find(".insname").text();  
								if(data[i].expiry_date == exdt &&  data[i].Passing_Symbol_Code == syname && data[i].instrument == insname) 
								{
										$(this).find(".rate").val(data[i].lastprice);
								} 
							});
						}
					}
					});
					return false;
				});
			});
</script>
<script>

		// One Table Value To Another Value Go using online ID
		$(document).ready(function()
		{
			$(document).on('click', '#ApplyOnline', function()  
			{ 
				$("#example2 tbody").find("tr").each(function()
				{
					var id=$(this).find(".rate").attr("id");
					var data=$(this).find(".rate").val();
					var rate = $("."+id).val(data);
					
				});
			});
		});
		
				// var data=$(this).find("input").val();
				// var id=$(this).find("input").attr("id");
				// console.log(data);
</script>
<script>

			
		//Code working excel upload 
		$(document).ready(function (){
		 $(document).on('submit', '#FormFileData', function(){ 
		 
			$.ajax({
					url: base_url+"process/closing_forward/UploadDataExcel",
					method:"POST", 
					data: new FormData(this),
					contentType: false,
					//cache: false,
					processData:false,
					dataType: "json",
					success: function(data)
					{
					   for(i=0; i<data.length; i++)
						 {
							 
							$("#example2 tbody").find("tr").each(function()
							{
								
								var exdt = $(this).closest("tr").find(".Ex_Date_Excel").val(); 
								var syname = $(this).closest("tr").find(".nsc_id").val(); 
								var insname = $(this).closest("tr").find(".insname").text();
								 
								if(data[i].symbol == syname && data[i].Exdt == exdt && data[i].instrument == insname)
								{
									// console.log("body");
									$(this).find(".rate").val(data[i].closingrate);
								 } 
							});
						}	 
					}
				});
				return false;
			});
		});		
</script>
<script>
		// One Table Value To Another Value Go
		$(document).ready(function()
		{
			$(document).on('click', '#Apply', function()  
			{ 
				$("#example2 tbody").find("tr").each(function()
				{
					var id=$(this).find(".rate").attr("id");
					var data=$(this).find(".rate").val();
					var rate = $("."+id).val(data);
				});
			});
		});	
		
/*
var id=$(this).find("input").attr("id");
var data=$(this).find("input").val();
var rate = $("."+id).val(data);
*/
</script>



<script>
var opening_setlment_id;
var closing_setlment_id;

var next_open_date;


		// Display First And Second Table
		$(document).ready(function()
		{
			$(".MyDiV").hide();
			$(document).on('submit', '#FormSubmit', function()  // // $(document).on('click', '#Submit', function() 
			{ 
			$(".MyDiV").show();
			$('#Response').html("");
			
				var exchange_id = $("#exchange_id").val();
				var closing_setlment_id = $("#closing_setlment_id").val();
				var start_date = $("#start_date").val();
				var end_date = $("#end_date").val();
				var closing_rate_type_id = $("#closing_rate_type_id").val();
				var opening_setlment_id = $("#opening_setlment_id").val();
				var next_open_date = $("#next_open_date").val();
				
				$.ajax({
					url: base_url+"process/closing_forward/Check_Closing_Forward_Base_On_Buy_Sell",
                    method : "POST",
                    data : {exchange_id: exchange_id , closing_setlment_id: closing_setlment_id , start_date:start_date , end_date:end_date , closing_rate_type_id:closing_rate_type_id , opening_setlment_id:opening_setlment_id , next_open_date:next_open_date  }, 
                    async : false,
                    dataType : 'JSON',
                    success: function(data){
						
					var data=eval(data);
					
					//$('#Response').html('');
					
					
					/* First Table */
					var table="";
			
					// Class 
					table+='<table id="example1" class="table table-striped MyTable">'+
					'<thead>'+
					'<tr>'+
					//'<th>No</th>'+
					'<th>Code</th>'+
					'<th>Name</th>'+
					'<th>Symbol</th>'+
					'<th>Instrument</th>'+
					'<th>Expiry</th>'+
					'<th>Stock</th>'+
					'<th>Closing_Rate</th>'+
					'</tr>'+
					'</thead>'+
					'<tbody id="Response">';
			
					
					for(var i=0;i<data.length;i++) 
					{
						table+='<tr>'+
						'<td>'+data[i].code+'</td>'+   //  party_code 27-dec
						'<td>'+data[i].username+'</td>'+	 // party_name	 27-dec
						'<td data-symbol="'+data[i].symbol_id+'">'+data[i].symbol+'</td>'+
						'<td>'+data[i].instument+'</td>'+
						'<td>'+data[i].ex_date_value+'</td>'+
						// '<td>'+data[i].ex_date+'</td>'+
						'<td>'+data[i].qty1+'</td>'+
						
						 
						
						'<input type="hidden" id="next_open_date"  name="next_open_date" value="'+next_open_date+'" >'+
						
						'<input type="hidden" id="opening_setlment_id"  name="opening_setlment_id" value="'+opening_setlment_id+'" >'+
						'<input type="hidden" id="buysell_ids"  name="buysell_ids" value="'+data[i].buysell_id+'">'+
						'<input type="hidden"  name="qty1['+data[i].buysell_id+']" value="'+data[i].qty1+'">'+
						// '<td><input type="number" step="any"  name="rate1['+data[i].buysell_id+']"  class="form-control rate1 rate1_'+data[i].symbol_id+'_'+data[i].ex_date+'"></td>'+
						//'<td><input type="number" step="any"  name="rate1['+data[i].buysell_id+']"  class="form-control rate1 rate1_'+data[i].symbol_id+'_'+data[i].ex_date_value+'"></td>'+
						'<td><input type="number" step="any"  name="rate1['+data[i].buysell_id+']"  class="form-control rate1 rate1_'+data[i].sy_code+'_'+data[i].ex_date_value+'"></td>'+
						'</tr>' ;
					}
					'</tbody>'+        		
					'</table>';	
					
					$('#Response').append(table);
			
					$('.MyTable').DataTable({
						"paging": true,
						"lengthChange": true,
						"searching": true,
						"ordering": true,
						"info": true,
						"autoWidth": false,
						"responsive": true,
						//"pageLength": 5,
						//"sLengthMenu": "Display _MENU_ records",
						"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
						"iDisplayLength": 5
					});
					
					load_secondtable();
				}
                });
                return false;
			});
		});
		
		// 
</script>
<script>
	// Load  Second Table
	function load_secondtable()
	{
		$('#Response2').html("");
				var exchange_id = $("#exchange_id").val();
				var closing_setlment_id = $("#closing_setlment_id").val();
				var start_date = $("#start_date").val();
				var end_date = $("#end_date").val();
				var closing_rate_type_id = $("#closing_rate_type_id").val();
				var opening_setlment_id = $("#opening_setlment_id").val();
				var next_open_date = $("#next_open_date").val();
				var ex_date = $("#ex_date").val();
				
				// alert(exchange_id);
				
				
			$.ajax({
				url: base_url+"process/Closing_forward/Second_table",
				method : "POST",
				
				data : {exchange_id: exchange_id , closing_setlment_id: closing_setlment_id , start_date:start_date , end_date:end_date , closing_rate_type_id:closing_rate_type_id , opening_setlment_id:opening_setlment_id , next_open_date:next_open_date , ex_date:ex_date},
				
				
				success: function(data){
				$('#Response2').html(data);
				
					$('.MyTable2').DataTable({
						"paging": true,
						"lengthChange": true,
						"searching": true,
						"ordering": true,
						"info": true,
						"autoWidth": false,
						"responsive": true,
						"aLengthMenu": [[5 , 15 , 25, 50, 75, -1], [5 , 15 , 25, 50, 75, "All"]],
						"iDisplayLength": 5
					});
				}
			});
		return false;
	}	
</script>




<script>
		
		$(document).ready(function()
		{
			$(document).on('submit', '#FormResponse', function(e)  // // $(document).on('click', '#Submit', function() 
			{
				 e.preventDefault();
				 var form = $(this);
				 
				$.ajax({
						type: 'POST', 
						url: base_url+"process/closing_forward/Closing_Forward_Key_Chk",
					
						data: form.serialize(), 
						success: function(data)
						{
						
							// alert("Success Closing Forward Process ");
							// location.reload();
							
						}
					});
					return false;
				});
		  });
</script>
<script>

		$(document).ready(function()
		{
		
			
			$(document).on('click', '#Forward_Remove', function() 
			{
					
				var next_open_date = $("#next_open_date").val();
				// alert(opening_setlment_id);
				$.ajax({
					url: base_url+"process/closing_forward/Forward_Remove",
                    method : "POST",
                    data : {next_open_date:next_open_date},
                    async : true,
                    dataType : 'json',
                    success: function(data)
					{
						alert("Remove Forward");			
					}
                });
                return false;
				
			});
		});


// (window).on("load", function () {
		// $("#Response").hide();
		// $("#ViewTable").css("display","none");
	// });
</script>




	<script>
	$(document).ready(function(){
	// var  ex_date = [];
            $('#exchange_id').change(function(){  
                var id=$(this).val();
                $.ajax({
					url: base_url+"process/closing_forward/expiries_date",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data)
					{
						if (data.length == 0) 
						{
							$('#ex_date').val("");		
						}
						else
						{
							$('#ex_date').val(data[0].ex_date);		
							// console.log(data[0].ex_date);
						}			
					}
                });
                return false;
            }); 
             
        });
    </script>


