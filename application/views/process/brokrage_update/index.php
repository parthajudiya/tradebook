

<?php



$Start_Date  = [
    'type' => 'text',
    "name" => "start_date",
    'id' => "start_date",
	'class'=>"form-control",
	'placeholder'=>"From Date",
];


$End_Date  = [
    'type' => 'text',
    "name" => "end_date",
    'id' => "end_date",
	'class'=>"form-control",
	'placeholder'=>"To Date",
];

$Group  = [
    'type' => 'text',
    "name" => "group",
    'id' => "group",
	'class'=>"form-control",
	'placeholder'=>"Group",
];

$Code  = [
    'type' => 'text',
    "name" => "party_code",
    'id' => "party_code",
	'class'=>"form-control",
	'placeholder'=>"Account	Code",
];

$Name  = [
    'type' => 'text',
    "name" => "party_name",
    'id' => "party_name",
	'class'=>"form-control",
	'placeholder'=>"Account Name",
];
 

 
?>


<div class="content-wrapper">  <!-- content-wrapper Start -->

		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
			<h1>Brokrage Update</h1>
			</div>
		</div>
		</div>
		</section>
		
		
			<div class="col-md-12">
			<div class="card card-primary card-outline"> 
					
				
				
					<div class="card-body">
					<form id="FormSubmit" name="FormSubmit" autocomplete="off">
					
					
						
						<div class="row">
						
						
						<div class="col-sm-2">
							<div class="form-group">
								<label><?php echo $this->lang->line("Brokrage_Exchange"); ?></label>
								
								<select class="form-control " style="width: 100%;" name="exchange_id" id="exchange_id">
								
								<?php 
							
									foreach ($Exchange as $row_B)
									{
									?>	
										<option value="<?php echo $row_B->exchange_id; ?>" ><?php echo strtoupper($row_B->name); ?></option>
									<?php
									}
								?>
								</select>
							</div>
						</div>
						
						
						<div class="col-sm-4">
							<div class="form-group">
							
							
								<label>Setlment</label>
								
								<select class="form-control " style="width: 100%;" name="setlement_id" id="setlement_id">
								<!--
								<option value="">Setlment</option>
								-->
								</select>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label>From Date</label>
								<?php  echo form_input($Start_Date); ?>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label>To Date</label>
									<?php  echo form_input($End_Date); ?>
							</div>
						</div>
						
						
					</div>	
						
					<div class="row">	
						
						
						<div class="col-sm-2">
							<div class="form-group">
								<label>A/c Code</label>
									<?php  echo form_input($Code); ?>
							</div>
						</div>
						
						
						<div class="col-sm-5">
							<div class="form-group">
								<label>&nbsp;</label>
									<?php  echo form_input($Name); ?>
							</div>
						</div>
						
						
						<div class="col-2">
							<label>&nbsp;</label><br>
							<button id="Submit" type="submit" class="btn btn-primary">Brokrage Update</button>
						
					</div>
				</div>
			</form> 
		</div>
					
		</div>
		</div>

		
	</div>  <!-- content-wrapper End -->
	
<script>
var url = url;
var base_url="<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/customjs/brokrage_update.js"></script>
<script src="<?php echo base_url(); ?>assets/customjs/global_exchange_to_settlement_to_date.js"></script>

<script>
	$(document).ready(function()
     {
		$(document).on('submit', '#FormSubmit', function() 
		{ 
				var exchange_id = $("#exchange_id").val();
				var setlement_id = $("#setlement_id").val();
				var start_date = $("#start_date").val();
				var end_date = $("#end_date").val();
				var party_code = $("#party_code").val();
				$.ajax({
					type:"POST", 
					url: base_url+"process/Brokrage_update/Update",
					data: {exchange_id:exchange_id , setlement_id:setlement_id , start_date:start_date , end_date:end_date, party_code:party_code },
					success: function(data , result)
					{ 
						//console.log(data);
						toastr.success('Brokrage Update Sucessfully');
					}
				});
				return false;
		});
	});
</script>


