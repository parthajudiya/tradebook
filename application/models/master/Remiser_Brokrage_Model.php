<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Remiser_brokrage_model extends CI_Model {

    # VIEW
    public function view()
	{
		$this->db->select('*');
		$this->db->from('remiser_brokarge');
		//$this->db->join('exchange', 'symbol.exchange_id = exchange.exchange_id');
		$query = $this->db->get();
		return	$query->result();
	}
	
	# Delete
	public function delete($id)
	{
		$this->db->where('remiser_id', $id);
		$result = $this->db->delete('remiser_brokarge');
		return $result;
	}
	
	# Delete Bulk
	public function delete_bulk($id)
	{
		if(is_array($id))
		{
			$this->db->where_in('remiser_id', $id);
			
		}
		else
		{
			$this->db->where('remiser_id', $id);
		}
		
		$result = $this->db->delete('remiser_brokarge');
		return $result;
		#print_r($this->db->last_query());
	}

}


