<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model {
	

	
	
	
	public function view()
	{
			
    
        $query = $this->db->get('account');
        return $query->result();
		
		
			/*
			$this->db->select('*');
			$this->db->from('account');
			$this->db->join('users', 'account.uid = users.id');
			$query = $this->db->get();
			return	$query->result();
			*/
	}

	public function add($data)
	{
		$result = $this->db->insert('account', $data );
		return $result;
	}
	
	public function delete($id)
	{
		$this->db->where('aid', $id);
		$result = $this->db->delete('account');
	
		return $result;
	}
	
	
	
	public function delete_bulk($id)
	{
		if(is_array($id))
		{
			$this->db->where_in('aid', $id);
			
		}
		else
		{
		$this->db->where('aid', $id);
		}
		
		$result = $this->db->delete('account');
			return $result;
		 #print_r($this->db->last_query());
	}
	
	public function Edit($id)
	{
		/*
		if($id) {
			$this->db->where('id', $id);
			$query =  $this->db->get("account");
			return $query->row_array();
			print_r($this->db->last_query()); 
		}
		*/
	
	
			$this->db->select('*');
			$this->db->from('account');
			
			/*
			$this->db->join('users', 'account.uid = users.id'); */
			$this->db->where('aid', $id ); 
			
			
			$query = $this->db->get();
			return $query->row_array();
			//print_r($this->db->last_query()); 
			 
	}
	
	public function Update($data , $aid)
	{
		$this->db->where('aid', $aid);
		$result = $this->db->update('account', $data);
		#print_r($this->db->last_query()); 
		
		# return ($update == true) ? true : false;
	}	
	
	// For Droup Down
	public function Search_Account($postData)
	{
		$response = array();

		if(isset($postData['search']) )
		{
		  // Select record
		  $this->db->select('*');
		  $this->db->where("username like '%".$postData['search']."%' ");
   
		  $records = $this->db->get('users')->result();
   
		  foreach($records as $row )
		  {
			 	$response[] = array(
				 "value"=>$row->id,
				"label"=>$row->username,
				"email"=>$row->email
				// "Address"=>$row->Cus_Address,
				// "Mobile"=>$row->Cus_Mobile
				);
		  }
   
		}
   
		return $response;
	}
	
	
	
	/*
    // For Multiple Condition
    public function view_where($where = null,$select="*") {
        $this->db->trans_start();
        if (!is_null($where)) {
            $this->db->where($where);
        }
        $this->db->select($select);
        $this->db->order_by("name", "asc");
        $query = $this->db->get('account');
        $this->db->trans_complete();
        return $query->result();
    }
	

   
	*/
	
	
	

	

}
