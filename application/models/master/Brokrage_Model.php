<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brokrage_model extends CI_Model {
	
	
	public function Select_Exchange()
	{
		$query = $this->db->get('exchange');
        return $query->result();
	}
	

  # VIEW
    public function view() // $postData
	{
		
		// print_r ($postData);
		// echo " rr";
		// die();
		// $query = $this->db->get('brokrage');
        // return $query->result();
		
		/*
		$this->db->select('*');
		$this->db->from('brokrage');
		$this->db->join('symbol', 'brokrage.symbol_id = symbol.symbol_id');
		$this->db->group_by('brokrage.symbol_id'); // Added
		$query = $this->db->get();
		return	$query->result();
		*/
		
		$this->db->select('brokrage.*,exchange.*,symbol.symbol,symbol.symbol_id as sy_id');
		$this->db->from('brokrage');
		$this->db->join('exchange', 'brokrage.exchange_id = exchange.exchange_id');
		$this->db->join('symbol', 'brokrage.exchange_id = symbol.exchange_id AND brokrage.symbol_id = symbol.symbol_id','left');
		
		// add qry for search
		//$this->db->where("code like '%".$postData['search']."%' ");
		
		$query = $this->db->get();
		return	$query->result();
		print_r($this->db->last_query()); 
		
		
		
		
	}
	
	# ADD
	public function add($data)
	{
		# Add Brokrage And Seaech And Check code And name
		# After Find aid in account in select Query
		# And Store aid in array And Marge Both Arry And 
		# INSERT	
		
		$code = $this->input->post("code");
		$name = $this->input->post("name");
		
		$this->db->select('*');
		$this->db->where("code = '".$code."' AND username = '".$name."' ");	
		$records = $this->db->get('account')->result();
		
		foreach($records as $row )
		{
			$aid = $row->aid;
		}
		
		$data1 = array(
		"aid" => $aid
		);
			
		$newarry = array_merge($data ,$data1);
		# print_r($newarry);
		
		$result = $this->db->insert('brokrage', $newarry );
		return $result;
		
	
	}
	
		public function Upadte($data ,$bid)
		{
			$this->db->where('bid', $bid);
			$result = $this->db->update('brokrage', $data);
			return $result;
			// print_r($this->db->last_query()); 
		}
	
	
	
	
	public function Search_Account_Name($postData)
	{
		$response = array();
		if(isset($postData['search']) )
		{
			$this->db->select('*');
			$this->db->where("username like '%".$postData['search']."%' ");
			$records = $this->db->get('account')->result();
   
		  foreach($records as $row )
		  {
			 	$response[] = array(
				 "value"=>$row->uid,
				"label"=>$row->username,
				"code"=>$row->code
				// "Address"=>$row->Cus_Address,
				// "Mobile"=>$row->Cus_Mobile
				);
		  }
   
		}
		return $response;
	}
	
					//"uid"=>$row->uid,
					//"name"=>$row->username
	
	public function Search_Account_Code($postData)  //code
	{
		
			$response = array();
			if(isset($postData['search']) )
			{
				$this->db->select('*');
				$this->db->where("code like '%".$postData['search']."%' ");
				$records = $this->db->get('account')->result();
	   
			  foreach($records as $row )
			  {
					$response[] = array(
					 "value"=>$row->uid,
					 "label"=>$row->code,
					 "name"=>$row->username
					 // "aid"=>$row->aid
					);
			  }
	   
			}
			return $response;
	}
	
	/*
	public function Search_Accounts_C($code)
	{
		// print_r($code);
		// die();
			$response = array();
			$this->db->select('*');
			$this->db->where("code like '".$code['code']."' ");
			$records =  $this->db->get('brokrage')->result();
			// print_r($this->db->last_query());  
   
			
			foreach($records as $row )
			{
				$response[] = array(
				"aid"=>$row->aid,
				"code"=>$row->code,
				"exchange_id"=>$row->exchange_id,
				"symbol_id"=>$row->symbol_id
				//"p_tax"=>$row->p_tax
				);
			}
			return $response;
		
		/*
			$response = array();
			if(isset($postData['search']) )
			{
				$this->db->select('*');
				// $this->db->where("code like '%".$postData['search']."%' ");
				$this->db->where("code like '".$postData['search']."'");
				$records = $this->db->get('brokrage')->result();
	   
			  foreach($records as $row )
			  {
					$response[] = array(
					 "value"=>$row->code,
					 "label"=>$row->exchange_id,
					 "symbol_id"=>$row->symbol_id
					 // "aid"=>$row->aid
					);
			  }
	   
			}
			return $response;
			
	}
	*/
	
	
	
	
	public function Search_Account_Code_keyup($postData) 
	{
		
			$response = array();
			$this->db->select('*');
			$this->db->where("code like '".$postData['code']."' ");
			$records = $this->db->get('account')->result();
			#print_r($this->db->last_query());  
   
			foreach($records as $row )
			{
				$response[] = array(
				"uid"=>$row->uid,
				"name"=>$row->username
				//"p_tax"=>$row->p_tax
				);
			}
			return $response;
	}
	
	
	public function Edit_Record($code , $name , $exchange_id , $symbol_id)
	{
		$response = array();
		$this->db->select('*');
		$this->db->where("brokrage.code = '".$code."' AND brokrage.name = '".$name."' AND brokrage.exchange_id = '".$exchange_id."' AND brokrage.symbol_id = '".$symbol_id."' ");
		
		$records = $this->db->get('brokrage')->result();
		#print_r($this->db->last_query());  
		
		foreach($records as $row )
		{
			$response[] = array(
			"bid"=>$row->bid,
			"aid"=>$row->aid,
			"brokrage_type_id"=>$row->brokrage_type_id,
			"base_on_id"=>$row->base_on_id,
			"intraday"=>$row->intraday,
			"side_id"=>$row->side_id,
			"nxt_day"=>$row->nxt_day,
			"minimum_id"=>$row->minimum_id
			);
		}
		return $response;
		
	}
	
	
	
	public function searchTable($exchange, $name ,$code)
	{
		//$this->db->select('*');
			
		$response = array();
		$this->db->select('brokrage.*,exchange.*,symbol.symbol,symbol.symbol_id as sy_id');
	
		$this->db->where("brokrage.exchange_id = '".$exchange."' AND brokrage.name = '".$name."' AND brokrage.code = '".$code."' ");
		$this->db->from('brokrage');
		$this->db->join('exchange', 'brokrage.exchange_id = exchange.exchange_id');
		$this->db->join('symbol', 'brokrage.exchange_id = symbol.exchange_id AND brokrage.symbol_id = symbol.symbol_id','left');
		
		$query = $this->db->get();
		return	$query->result();
		
		/*
		$records = $this->db->get()->result();
		foreach($records as $row )
		{
			$response[] = array(
			"name"=>$row->name,
			"exchange"=>$row->exchange_id,
			"code"=>$row->code
			);
			return $response;
		}
		*/
			
		
		
		// $query = $this->db->get();
		// return	$query->result();
	}
	
	
	/*
	public function  searchTable_Code($CODE)
	{
		//$this->db->select('*');
			
		
		$this->db->select('brokrage.*,exchange.*,symbol.symbol,symbol.symbol_id as sy_id');
		//add 
		// $this->db->where("code like '%".$CODE['search']."%' ");
		// $this->db->where("code like '%".$CODE."%'");
		$this->db->where("code like '".$CODE."'");
		$this->db->from('brokrage');
		$this->db->join('exchange', 'brokrage.exchange_id = exchange.exchange_id');
		$this->db->join('symbol', 'brokrage.exchange_id = symbol.exchange_id AND brokrage.symbol_id = symbol.symbol_id','left');
		
		$query = $this->db->get();
		return	$query->result();
	}
	
	public function  searchTable_NAME($NAME)
	{
		//$this->db->select('*');
			
		
		$this->db->select('brokrage.*,exchange.*,symbol.symbol,symbol.symbol_id as sy_id');
		//add 
		// .name as name_acc 
		// $this->db->where("code like '%".$CODE['search']."%' ");
		// $this->db->where("code like '%".$CODE."%'");
		$this->db->where("brokrage.name like '".$NAME."'");
		$this->db->from('brokrage');
		$this->db->join('exchange', 'brokrage.exchange_id = exchange.exchange_id');
		$this->db->join('symbol', 'brokrage.exchange_id = symbol.exchange_id AND brokrage.symbol_id = symbol.symbol_id','left');
		
		$query = $this->db->get();
		return	$query->result();
	}
	*/
	
	
	/*
	# EDIT
	public function edit($id)
	{
		$this->db->select('*');
		$this->db->from('brokrage');
		$this->db->where('exchange_id', $id ); 
		$query = $this->db->get();
		return $query->row_array();
	}
	
	# UPDATE
	public function update($data , $id)
	{
		$this->db->where('exchange_id', $id);
		$result = $this->db->update('brokrage', $data);
		#print_r($this->db->last_query()); 
	}	
		*/
	
	# Delete
	public function delete($id)
	{
		$this->db->where('bid', $id);
		$result = $this->db->delete('brokrage');
		return $result;
	}
	
	# DELETE BULK
	public function delete_bulk($id)
	{
		if(is_array($id))
		{
			$this->db->where_in('bid', $id);
		}
		else
		{
			$this->db->where('bid', $id);
		}
		$result = $this->db->delete('brokrage');
		return $result;
	}


}

