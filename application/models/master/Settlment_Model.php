<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settlment_model extends CI_Model {
	
	
	 public function Count_Settlment()
	 {
		// $this->db->select('COUNT(*) as count_row')->from('setlement');
		// select count(*), max(id) from mytab where category = 1
		
		$this->db->select('MAX(setlement_id)+ 1 as count_row')->from('setlement');
		$query = $this->db->get('exchange');
		return $query->row_array();
	 }

    # Exchange Select 
	 public function Select_Exchange()
	 {
		$query = $this->db->get('exchange');
		return $query->result();
	 }

    # VIEW
    public function view()
	{
		$this->db->select('*');
		$this->db->from('setlement');
		$this->db->join('exchange', 'setlement.exchange_id = exchange.exchange_id');
		$query = $this->db->get();
		return	$query->result();
	}
	
	# ADD
	public function add($data)
	{
		$result = $this->db->insert('setlement', $data );
		return $result;
	}
	
	public function edit($id)
	{
		$this->db->select('*');
		$this->db->from('setlement');
		$this->db->join('exchange', 'setlement.exchange_id = exchange.exchange_id');
		$this->db->where('setlement_id', $id ); 
		$query = $this->db->get();
		return $query->row_array();
		//print_r($this->db->last_query()); 
	}
	
	
	public function Update($data , $setlement_id)
	{
		$this->db->where('setlement_id', $setlement_id);
		$result = $this->db->update('setlement', $data);
		print_r($this->db->last_query()); 
		# return ($update == true) ? true : false;
	}
	
	# Delete
	public function delete($id)
	{
		$this->db->where('setlement_id', $id);
		$result = $this->db->delete('setlement');
		return $result;
	}
	
	# Delete Bulk
	public function delete_bulk($id)
	{
		if(is_array($id))
		{
			$this->db->where_in('setlement_id', $id);
			
		}
		else
		{
			$this->db->where('setlement_id', $id);
		}
		
		$result = $this->db->delete('setlement');
		return $result;
	}
	
	public function Bulk_Ledger_Yes($data , $id)
	{
		if(is_array($id))
		{
			$this->db->where_in('setlement_id', $id);
		}
		else
		{
			$this->db->where('setlement_id', $id);
		}
		
		
		
		$result = $this->db->update('setlement', $data);
		return $result;
	}
	
	public function Bulk_Ledger_No($data , $id)
	{
		if(is_array($id))
		{
			$this->db->where_in('setlement_id', $id);
		}
		else
		{
			$this->db->where('setlement_id', $id);
		}
		$result = $this->db->update('setlement', $data);
		return $result;
	}


		
	
	

}
