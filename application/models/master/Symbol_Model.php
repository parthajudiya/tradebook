<?php
# 	if ($query->num_rows() > 0)
#   return ($row->count > 0) ? FALSE : TRUE;
# print_r($this->db->last_query()); 		
defined('BASEPATH') OR exit('No direct script access allowed');

class Symbol_model extends CI_Model {
	
	public function AddSymbol($data)
	{
		$result = $this->db->insert_batch('symbol', $data);
		return $result;
	}
	
	public function Validate_Symbol($d)
	{
		if($d['symbol']!="")
		{
			 $this->db->where('symbol', $d['symbol']); 
			 $this->db->where('instument', $d['instument']);  
			
			$this->db->from('symbol');
			$query=$this->db->get();
		
		
			# Same symbol same ex ma no jaay validate
			if ($query->num_rows() == 0)
			{	
				return true;
			}	
			else
			{
				return false;
			}	
		}
	}
	
	# Import / Insert Data Form Csv  AddSymbol
	/*public function AddSymbol($d)
	{
		$validate = $this->Validate_Symbol($d);
		if($validate == "1")
		{
			$datas = $this->db->insert('symbol', $d);
			return $datas;	
		}
		else
		{
			$datas = "2";
			return $datas;		
		}
	}
	*/
	/*
	# ADD
	public function add($data)
	{
		$result = $this->db->insert('symbol', $data );
		return $result;
	}
	*/
	
	# Exchange Select 
	 public function Select_Exchange()
	 {
		$query = $this->db->get('exchange');
		return $query->result();
		
	 }

    # VIEW
    public function view()
	{
		$this->db->select('*');
		$this->db->from('symbol');
		$this->db->join('exchange', 'symbol.exchange_id = exchange.exchange_id');
		$query = $this->db->get();
		return	$query->result();
	}
	
	
	
	public function edit($id)
	{
		$this->db->select('*');
		$this->db->from('symbol');
		/*	$this->db->join('users', 'account.uid = users.id'); */
		$this->db->join('exchange', 'symbol.exchange_id = exchange.exchange_id');
		$this->db->where('symbol_id', $id ); 
		$query = $this->db->get();
		return $query->row_array();
		//print_r($this->db->last_query()); 
	}
	
	
	public function Update($data , $symbol_id)
	{
		$this->db->where('symbol_id', $symbol_id);
		$result = $this->db->update('symbol', $data);
		print_r($this->db->last_query()); 
		# return ($update == true) ? true : false;
	}
	
	# Delete
	public function delete($id)
	{
		$this->db->where('symbol_id', $id);
		$result = $this->db->delete('symbol');
		return $result;
	}
	
	# Delete Bulk
	public function delete_bulk($id)
	{
		if(is_array($id))
		{
			$this->db->where_in('symbol_id', $id);
			
		}
		else
		{
			$this->db->where('symbol_id', $id);
		}
		
		$result = $this->db->delete('symbol');
		return $result;
		#print_r($this->db->last_query());
	}
	

}
