<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Exchange_model extends CI_Model {

	# VIEW
    public function view()
	{
		$query = $this->db->get('exchange');
        return $query->result();
	}
	
	# ADD
	public function add($data)
	{
		$result = $this->db->insert('exchange', $data );
		return $result;
	}
	
	# EDIT
	public function edit($id)
	{
		$this->db->select('*');
		$this->db->from('exchange');
		$this->db->where('exchange_id', $id ); 
		$query = $this->db->get();
		return $query->row_array();
	}
	
	# UPDATE
	public function update($data , $id)
	{
		$this->db->where('exchange_id', $id);
		$result = $this->db->update('exchange', $data);
		#print_r($this->db->last_query()); 
	}	
	
	# Delete
	public function delete($id)
	{
		$this->db->where('exchange_id', $id);
		$result = $this->db->delete('exchange');
		return $result;
	}
	
	# DELETE BULK
	public function delete_bulk($id)
	{
		if(is_array($id))
		{
			$this->db->where_in('exchange_id', $id);
		}
		else
		{
			$this->db->where('exchange_id', $id);
		}
		$result = $this->db->delete('exchange');
		return $result;
	}
	

}
