<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Expiry_model extends CI_Model {
	
	public function Validate_Data($d)
	{
		$this->db->where('symbol_id', $d['symbol_id']);
		$this->db->where('ex_date', $d['ex_date']);
		$this->db->from('expiries');
		$query=$this->db->get();
		if ($query->num_rows() == 0)
			return true;
		else
			return false;
		
	}
	
	public function adddata($data)
	{

		
		$datas = $this->db->insert_batch('expiries', $data);
		return $datas;
	}
	
	
	
	/*
	public function adddata($d) // $Symbol_Name ,
	{
		$validate = $this->Validate_Data($d);
		
		if($validate == "1")
		{
			 
			 $datas = $this->db->insert('expiries', $d);
			//$datas = $this->db->insert_batch('expiries', $d);
			return $datas;
			print_r($this->db->last_query());
		}
		else
		{
				
			$datas = "2";
				
			return $datas;		
		}
		
	}
	*/
	

	
	
	/*
	public function adddata($Symbol_Name )
	{
		$data = $this->db->insert_batch('expiries', $Symbol_Name);
		print_r($this->db->last_query());
		// return $data;
	}
	
	*/
			

	# Join Second Droup Down
	 public function Select_Symbol($id)
	 {
		 
		$query = $this->db->get_where('symbol', array('exchange_id' => $id));
        return $query;
	}

    # Exchange Select 
	 public function Select_Exchange()
	 {
		$query = $this->db->get('exchange');
		return $query->result();
	 }

    # VIEW
    public function view()
	{
		$this->db->select('expiries.*,exchange.*,symbol.symbol,symbol.symbol_id as sy_id');
		$this->db->from('expiries');
		$this->db->join('exchange', 'expiries.exchange_id = exchange.exchange_id');
		$this->db->join('symbol', 'expiries.exchange_id = symbol.exchange_id AND expiries.symbol_id = symbol.symbol_id','left');
		//$this->db->where('expiries.symbol_id =0 ');
	//	$this->db->group_by('expiries.symbol_id');
		$query = $this->db->get();
		return	$query->result();
		print_r($this->db->last_query()); 
		
		
		
		//  where, like, group_by, having, order_by
		// $this->db->();
		// GROUP BY
	}
	
	# ADD
	public function add($data)
	{
		$result = $this->db->insert('expiries', $data );
		return $result;
	}
	
	
	
	public function edit($id)
	{
		$this->db->select('*');
		$this->db->from('expiries');
		$this->db->join('exchange', 'expiries.exchange_id = exchange.exchange_id');
		$this->db->where('expiries_id', $id ); 
		$query = $this->db->get();
		return $query->row_array();
		
	}

	
	
	public function Update($data , $expiries_id)
	{
		$this->db->where('expiries_id', $expiries_id);
		$result = $this->db->update('expiries', $data);
		print_r($this->db->last_query()); 
		# return ($update == true) ? true : false;
	}
	
	# Delete
	public function delete($id)
	{
		$this->db->where('expiries_id', $id);
		$result = $this->db->delete('expiries');
		return $result;
	}
	
	
	# Delete Bulk
	public function delete_bulk($id)
	{
		if(is_array($id))
		{
			$this->db->where_in('expiries_id', $id);
		}
		else
		{
			$this->db->where('expiries_id', $id);
		}
		$result = $this->db->delete('expiries');
		return $result;
		#print_r($this->db->last_query());
	}
	
	
	
	# is_Default
	public function is_Default($data , $id = null)
	{
		if(!is_null($id))
		{
			$this->db->where('expiries_id', $id);
		}	
		
		$result = $this->db->update('expiries', $data);
		return $result;
		}
	
	
	// simple symbol qry
	 public function Sel_Symbol()
	{
		$this->db->select('*');
		$this->db->from('symbol');
		
		$query = $this->db->get();
		//print_r($this->db->last_query());
		return	$query->result_array();
		
	}
	
	//$this->db->where_in('symbol',$symbol); 
	// public function se_sy($s)
	// {
		// $this->db->select('*');
		// $this->db->from('symbol');
		// $this->db->where_in('symbol.symbol_id',$s); # Passing Arry Multiple select Query
		// $query = $this->db->get();
		// print_r($this->db->last_query());
		// return	$query->result_array();
	// }
	
	
	

}
