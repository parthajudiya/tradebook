<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_remove_model extends CI_Model {

    
	
	public function Exchange()
	{
		$query = $this->db->get('exchange');
        return $query->result(); 
		
	}
	
	public function Search_Data($start_date , $end_date , $party_code=null , $party_name=null , $exchange_id ,  $setlement_id , $type)
	{
		$this->db->select('*');
		$this->db->from('buy_sell');
		
		if($party_code && $party_name)
		{			
			$this->db->where('buy_sell.party_code', $party_code ); 
			$this->db->where('buy_sell.party_name', $party_name ); 
		} 
		$this->db->where('buy_sell.exchange_id', $exchange_id ); 
		$this->db->where('buy_sell.setlement_id', $setlement_id ); 
		$this->db->where('buy_sell.type', $type ); 
		
		$this->db->where('date1 BETWEEN "'. $start_date . '" AND "'.$end_date.'"');
		$query = $this->db->get();
		#print_r($this->db->last_query()); 
		return $query->result_array();
	}
	public function Remove_buy_sell_data($id)
	{
		$this->db->where_in('buysell_id', $id);
        $query = $this->db->delete('buy_sell');
		return $query;
	}
	

}
