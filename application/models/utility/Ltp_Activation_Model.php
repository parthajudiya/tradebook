<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ltp_activation
 *
 * @author Parth
 */
class Ltp_activation_model extends CI_Model {

    public function view($where = null,$select="*") {
        $this->db->trans_start();
        if (!is_null($where)) {
            $this->db->where("id", $where);
        }
        $this->db->select($select);
        $this->db->order_by("name", "asc");
        $query = $this->db->get('ltp_activation');
        $this->db->trans_complete();
        return $query->result();
    }

    // For Multiple Condition
    public function view_where($where = null,$select="*") {
        $this->db->trans_start();
        if (!is_null($where)) {
            $this->db->where($where);
        }
        $this->db->select($select);
        $this->db->order_by("name", "asc");
        $query = $this->db->get('ltp_activation');
        $this->db->trans_complete();
        return $query->result();
    }

    public function findname($where = null,$old=null) {
        $this->db->trans_start();
        if (!is_null($where)) {
            $this->db->where("name", $where);
        }
        if (!is_null($old)) {
            $this->db->where("name !=", $old);
        }
        $this->db->order_by("name", "asc");
        $query = $this->db->get('ltp_activation');
        $this->db->trans_complete();
        return $query->num_rows();
    }

    public function add($array) {
        $this->db->trans_start();
        $this->db->insert("ltp_activation", $array);
        $data = $this->db->insert_id();
        $this->db->trans_complete();
        return $data;
    }

    public function delete($id) {
        $this->db->trans_start();
            $this->db->where('id', $id);
            $data = $this->db->delete('ltp_activation');
            $this->db->trans_complete();
            return $data;
    }

    public function edit($array, $id) {
        $this->db->trans_start();
        $this->db->set($array);
        $this->db->where('id', $id);
        $data = $this->db->update('ltp_activation');
        $this->db->trans_complete();
        return $data;
    }

}
