<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brokrage_update_model extends CI_Model {

    function __construct() 
	{
		parent::__construct();
       
	}
	
	# Exchange Select 
	 public function Select_Exchange()
	 {
		$query = $this->db->get('exchange');
		return $query->result();
	}
	
	public function SelBsBro($exchange_id , $setlement_id , $start_date , $end_date,$party_code=null)
	{
		$this->db->select('*');
		$this->db->from('buy_sell');
		//$this->db->join('brokrage', 'buy_sell.exchange_id = brokrage.exchange_id');  
		$this->db->join('brokrage as brokragetable', 'buy_sell.brokrage_id = brokragetable.bid');   
		if($party_code)
		{			
			$this->db->where('buy_sell.party_code', $party_code ); 
		} 
		$this->db->where('buy_sell.exchange_id', $exchange_id ); 
		$this->db->where('buy_sell.setlement_id', $setlement_id ); 
		$this->db->where('date1 BETWEEN "'. $start_date . '" AND "'.$end_date.'"');
		$query = $this->db->get();
		// print_r($this->db->last_query()); 
		// die();
		return $query->result_array();
	}
	
	public function update($myarry , $buysell_id , $party_code = null)
	{
		
		if(is_array($buysell_id))
		{
			// echo "y";
			$this->db->where_in('buysell_id', $buysell_id);
		}
		else
		{
			//	 echo "n";
			$this->db->where('buysell_id', $buysell_id); 
		}
		if(is_array($party_code))
		{
			$this->db->where('party_code', $party_code);
		}	
		$result = $this->db->update('buy_sell', $myarry);
		// print_r($this->db->last_query()); 
		return $result;
		
		
	}
	

}
