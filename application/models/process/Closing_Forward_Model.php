<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Closing_forward_model extends CI_Model {
	
	
	function __construct() 
	{
		parent::__construct();
       
	}
	
	
	# select first table 
	  public function Select_Buy_Sell($exchange_id , $closing_setlment_id  ,  $start_date , $end_date ) // $ex_date
	 {
		# Buy Total 
		$this->db->select('*');
		$this->db->from('buy_sell');
		
		
		$this->db->join('account', 'buy_sell.pid = account.aid'); // 27-dec
		
		$this->db->join('setlement', 'buy_sell.setlement_id = setlement.setlement_id');
		$this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');  
		$this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');
		
		
		$this->db->group_by('buy_sell.party_code'); 
		$this->db->group_by('buy_sell.symbol_id');  
		$this->db->group_by('buy_sell.expiries_id');  
		
		$this->db->where('buy_sell.date1 BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
		$this->db->where('buy_sell.exchange_id', $exchange_id);
		$this->db->where('buy_sell.setlement_id', $closing_setlment_id);
		
		$this->db->select_sum('buy_sell.qty1'); 
		$query = $this->db->get();
		return  $query->result_array();
		
	}
		
		#$this->db->where('buy_sell.type',1); // ONLY FW TYPE IS SHOW
		
		 #  $data =  $query->result();
		#return	$query->result();
		# $this->db->where('buy_sell.qty1 != ', '0'); 
		// $this->db->select('buy_sell.*,  setlement.*');
		// $this->db->join('expiries', 'buy_sell.ex_date = expiries.ex_date');


	/////////////////////// END ///////////////////////////////
	
	# select second table 
	public function Second_table($exchange_id, $closing_setlment_id, $start_date, $end_date )
	{
		$this->db->select('*');
		$this->db->from('buy_sell');
		
		$this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');  
		$this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id'); 
		
		$this->db->where('buy_sell.exchange_id', $exchange_id);
		$this->db->where('buy_sell.setlement_id', $closing_setlment_id);	
		$this->db->where('buy_sell.date1 BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
		
		$this->db->group_by('buy_sell.symbol_id');  
		$this->db->group_by('buy_sell.expiries_id');  
		$query = $this->db->get();
		  
		return   $query->result();
		
		
		#$this->db->where('buy_sell.type',1); // ONLY FW TYPE IS SHOW
		#print_r($this->db->last_query());
		//	$this->db->join('expiries', 'buy_sell.ex_date = expiries.ex_date');
		// return  $query->result_array();
		// $this->db->where('buy_sell.setlement_id', $closing_setlment_id);
	}
	
	
	
	public function Get_Symbol_Code($symbol)
	{
		$this->db->select('*');
		$this->db->from('symbol');
		$this->db->where('symbol', $symbol);
		$query = $this->db->get();
		return   $query->result();
	}
	
	
	public function Forward_Remove($next_open_date)
	{
		$this->db->where('date1', $next_open_date);
		// $this->db->where_in('type', 2);
		$this->db->where_in('type', ['2','3']);
		$data = $this->db->delete('buy_sell');
		# print_r($this->db->last_query());  
		return $data;
	}
		
	



	

	

	
	public function keychk($key) 
	{
		
		# left join for set brokrage == 0
		
		$this->db->select('*');
		$this->db->from('buy_sell');
		
		$this->db->where('buy_sell.buysell_id', $key);
		$this->db->group_by('buy_sell.symbol_id');  
		$this->db->group_by('buy_sell.party_code'); 
		
		$this->db->select_sum('buy_sell.qty1'); 
		
		$query = $this->db->get();
		#print_r($this->db->last_query());  
		return  $query->result_array();
		
	}
	
	
	
			//$data = array();
		//  , $exchange_id , $closing_setlment_id
		// $this->db->join('brokrage', 'buy_sell.nxt_day = brokrage.nxt_day');  
		// $this->db->group_by('brokrage.exchange_id'); 
		// $this->db->where('brokrage.exchange_id',$exchange_id );
		
		// $this->db->where('brokrage.nxt_day'); // brokrage
		// $this->db->where('buy_sell.setlement_id', $closing_setlment_id);
		 
	   
		// $datakey = 	$query->result_array();
		
		
			/*
		  foreach($datakey as $row) 
		  {
			   // echo $row['buysell_id'];
				//echo "<br>";
		  }
		  */
		
		
		# die();
	
		/*	
	# Not Need		
	# insert
	public function Closing_Forward_Insert_Process($exchange_id , $closing_setlment_id , $ex_date , $rate1_new , $total_new)
	{
		$data = array();
		$this->db->select('*');
		$this->db->from('buy_sell');
		$this->db->join('expiries', 'buy_sell.ex_date = expiries.ex_date');
		$this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');  
		$this->db->group_by('buy_sell.party_code'); 
		$this->db->group_by('buy_sell.symbol_id');  
		$this->db->where('buy_sell.exchange_id', $exchange_id);
		$this->db->where('buy_sell.setlement_id', $closing_setlment_id);
		$this->db->select_sum('buy_sell.qty1'); 
		 # $this->db->where('buy_sell.qty1 != ', '0'); 
		$query = $this->db->get();
		 #  $data =  $query->result();
		$data =  $query->result_array();
		
		
		  
		  foreach($data as $row) 
		  {
			if($row['qty1']!="0") 
			{
			
			# Selling	
			$data = array( 
				 array(
					"exchange_id" =>  $row['exchange_id'],
					"setlement_id" =>  $row['setlement_id'],
					"tr_no" => $row['tr_no'],
					"type" => 3, // CF
					"date1" => $row['date1'],
					"buy_sell_id" => 2, 
					"symbol_id" => $row['symbol_id'],
					"instument" => $row['instument'],
					"ex_date" => $row['ex_date'],
					"qty1" => ($row['qty1']*-1),
					"rate1" => $rate1_new,
					// "total_amt1" => ($row['total_amt1']*-1),
					"total_amt1" =>  ($total_new*-1),
					"pid" => $row['pid'],
					"party_code" => $row['party_code'],
					"party_name" => $row['party_name'],
					"bid" => $row['bid'],
					"brokrage_code" => $row['brokrage_code'],
					"brokrage_name" => $row['brokrage_name'],
					"re_no" => $row['re_no'],
					"remark" => $row['remark'],
					"curr_time" => date("Y-m-d H:i:s"),
					"base_on_id" => $row['base_on_id'],
					"brokrage" => $row['brokrage'],
					"nxt_day" => $row['nxt_day'],
					"side_id" => $row['side_id'],
				), 
				
				# Buying
				array(
					"exchange_id" =>  $row['exchange_id'],
					"setlement_id" =>  $row['setlement_id'],
					"tr_no" => $row['tr_no'],
					"type" => 2, // fw
					"date1" => $row['date1'],
					"buy_sell_id" => $row['buy_sell_id'], // Buy
					"symbol_id" => $row['symbol_id'],
					"instument" => $row['instument'],
					"ex_date" => $row['ex_date'],
					"qty1" => $row['qty1'],
					"rate1" => $rate1_new,
					"total_amt1" => $total_new,
					"pid" => $row['pid'],
					"party_code" => $row['party_code'],
					"party_name" => $row['party_name'],
					"bid" => $row['bid'],
					"brokrage_code" => $row['brokrage_code'],
					"brokrage_name" => $row['brokrage_name'],
					"re_no" => $row['re_no'],
					"remark" => $row['remark'],
					"curr_time" => date("Y-m-d H:i:s"),
					"base_on_id" => $row['base_on_id'],
					"brokrage" => $row['brokrage'],
					"nxt_day" => $row['nxt_day'],
					"side_id" => $row['side_id'],
				), 
			);	
				
				#echo "a";
				#  print_r($data);
				#  die();
				
		
			 
			 $query_B = $this->db->insert_batch('buy_sell', $data );
			#  echo "<pre>";
			#  print_r($data); 
			#  echo "</pre>";
			 
			# return	$query_B->result();
			# return	$query->result();
			}
		}
		
	 }
	*/




	// added gloable
	public function Select_Exchange_On_Change($id)
	{
		$query = $this->db->get_where('setlement', array('exchange_id' => $id ));
		return $query;
	}

	public function expiries_date($id)
	{
		$query = $this->db->get_where('expiries', array('exchange_id' => $id , 'symbol_id' =>"0" ));
		// print_r($this->db->last_query()); 
		return $query;
	}
	
	
	# Exchange Select 
	 public function Select_Exchange()
	 {
		$query = $this->db->get('exchange');
		return $query->result();
		
	 }
	 
	
	# Select Expires
	public function Select_Expiries()
	{
		$query = $this->db->get('expiries');
		return $query->result();
	}
	
}
	
	
	
	


