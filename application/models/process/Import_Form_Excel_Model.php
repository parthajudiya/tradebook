<?php

defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_vars','2000');
class Import_form_excel_model extends CI_Model {


    function __construct() 
	{
		parent::__construct();
       
	}
	
	# Exchange Select 
	 public function Select_Exchange()
	 {
		$query = $this->db->get('exchange');
		return $query->result();
	}
	
	public function insert_excel($Insert)
	{
		$result = $this->db->insert('buy_sell', $Insert );
		return $result;
	}
	
	
}
