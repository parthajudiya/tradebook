<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Eod_high_low_check_model extends CI_Model {

   # Exchange Select 
	 public function Select_Exchange()
	 {
		$query = $this->db->get('exchange');
		return $query->result();
	 }
	 
	 # Join Second Droup Down ==> setlement
	 public function Select_Setlement($id)
	 {
		$query = $this->db->get_where('setlement', array('exchange_id' => $id)); 
		# print_r($this->db->last_query()); 
		return $query;
	 }
	 
	 
	 public function Buy_Sell_Chk_Fo($sid_HL_Form,$AsOnDate1)
	 {
		$this->db->select('*');
		$this->db->from('buy_sell');
		$this->db->join('exchange', 'buy_sell.exchange_id = exchange.exchange_id'); 
		# $this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');   // expiries expiries_id
		$this->db->join('expiries', 'buy_sell.ex_date = expiries.ex_date');  
		$this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');  
		$this->db->where('buy_sell.setlement_id', $sid_HL_Form);
		$this->db->where('buy_sell.date1', $AsOnDate1);
		$query = $this->db->get();
		# print_r($this->db->last_query()); 
		# die();
		return	$query->result();
	 }
	 


}
