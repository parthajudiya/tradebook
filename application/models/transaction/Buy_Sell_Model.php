<?php

//SELECT * FROM `brokrage`
//// echo $qry = "SELECT * FROM `brokrage` WHERE  code ='".$party_code."' AND (`symbol_id`='".$symbol_id."' OR `symbol_id`='0')";
defined('BASEPATH') OR exit('No direct script access allowed');

class Buy_sell_model extends CI_Model {

    /**     * ********************* ******************** */
    # VIEW

    public function view() {
        //r $date = new DateTime("now");
        //r $curr_date = $date->format('Y-m-d');

        $this->db->select('*');

        $this->db->select('party.code as party_code, party.username as party_name');
        $this->db->select('broker.code as brokrage_code, broker.username as brokrage_name');
        $this->db->from('buy_sell');

        # This Join Not Working Alis Same Table Name Join Error
        // $this->db->join('account', 'buy_sell.pid = account.aid' ); // 27-dec
        // $this->db->join('account', 'buy_sell.bid = account.aid'); // 27-dec
        #This Join Working
        $this->db->join('account AS party', 'buy_sell.pid = party.aid'); // 27-dec
        $this->db->join('account AS broker', 'buy_sell.bid = broker.aid'); // 27-dec

        $this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');
        $this->db->join('exchange', 'buy_sell.exchange_id = exchange.exchange_id');
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');

        #$this->db->where('type !=', 3);
        #$this->db->where('type !=', 4);
        // r	 $this->db->where('date1',$curr_date);//use date function
        $query = $this->db->get();
        # print_r($this->db->last_query()); 
        return $query->result();
    }

    ###################################################
    # Brokrage

    /*
      public function Chk_Qry_Brokrage($party_code, $party_name ,$symbol_id)
      {

      // echo $symbol_id;
      // echo	 $query = "SELECT * FROM `brokrage` WHERE  code ='".$party_code."' AND name ='".$party_name."' AND (`symbol_id`='".$symbol_id."' OR `symbol_id`='0')";
      // $this->db->where("`code`=$party_code AND `name`=$party_name AND(symbol_id=$symbol_id  OR symbol_id='0')");
      //  OR 'symbol_id' => '0'
      // $array_where = array('code' => $party_code , 'name' => $party_name , ('symbol_id' => $symbol_id OR  'symbol_id' => 0 ));
      // $this->db->where($array_where) ;
      //	print_r($this->db->last_query());
      //	die();
      //$query = $this->db->get('brokrage');
      //print_r($this->db->last_query());
      // return $query->result();
      $this->db->select('*');
      $this->db->from('brokrage');

      $this->db->where('code',$party_code);
      $this->db->where('name',$party_name);
      $this->db->where('symbol_id',$symbol_id ); // OR 'symbol_id' , '0' //  OR 'symbol_id', 0
      $query = $this->db->get();

      return	$query->result();
      }

     */

    # Exchange Ni Upper Brokrage LAAGE	

    public function Chk_Qry_Brokrage($party_code, $party_name, $symbol_id, $exchange_id) {
        $this->db->select('*');
        $this->db->from('brokrage');
        $this->db->where('code', $party_code);
        $this->db->where('name', $party_name);
        $this->db->where('exchange_id', $exchange_id);

        //$this->db->where('symbol_id',$symbol_id );
        //$this->db->join('symbol', 'brokrage.exchange_id = symbol.exchange_id AND brokrage.symbol_id = symbol.symbol_id','left');
        $query = $this->db->get();
        // print_r($this->db->last_query()); 
        return $query->result_array();
    }

    /*     * ********************** ******************** */

    /*
      public function Insert_Forward($party_code, $party_name ,$symbol_id , $instument , $buy_sell_id , $expiries_id , $qty1 , $buy_sell_type , $rate1 , $total_amt1)
      {
      $data = array(

      'party_code' =>$party_code,
      'party_name' =>$party_name,
      'symbol_id' =>$symbol_id,
      'instument' =>$instument,
      'buy_sell_id' =>$buy_sell_id,
      'expiries_id' =>$expiries_id,
      'qty1' =>$qty1,
      'buy_sell_type' =>$buy_sell_type,
      'rate1' =>$rate1,
      'total_amt1' =>$total_amt1,
      );
      $query = $this->db->insert('forward', $data );
      return $query;
      }

     */

    /*     * ********************** ******************** */

    # Tr No Count Row Qry

    public function countRow() {
        // $this->db->select('COUNT(*) as count_rows')->from('buy_sell'); 
        /*
          $this->db->select('MAX(buysell_id)+ 1 as count_rows')->from('buy_sell');
          $query = $this->db->get();
          return  $query->row_array();
         */

        $date = new DateTime("now");
        $curr_date = $date->format('Y-m-d');
        $this->db->where('date1', $curr_date); //use date function

        $this->db->select('MAX(buysell_id)+ 1 as count_rows')->from('buy_sell');
        $query = $this->db->get();
        return $query->row_array();
    }

    /*     * ********************** ******************** */

    # Exchange Select 

    public function Select_Exchange() {
        $query = $this->db->get('exchange');
        return $query->result();
    }

    /* DT 05-09-2020 */

    public function Sel_Settlement() {
        $query = $this->db->get('setlement');
        return $query->result();
    }

    public function Sel_Symbol() {
        $this->db->order_by("symbol", "ASC");
        $query = $this->db->get('symbol');
        return $query->result();
    }

    public function Sel_Exp() {

        $this->db->select('*');
        $this->db->from('expiries');
        $this->db->group_by('ex_date');
        $query = $this->db->get();
        return $query->result();
    }

    /* DT 05-09-2020 */

    /*     * ********************** ******************** */

    # Join Second Droup Down ==> setlement

    public function Select_Setlement($id) {

        $query = $this->db->get_where('setlement', array('exchange_id' => $id));
        // print_r($this->db->last_query()); 
        return $query;
    }

    /*     * ********************** ******************** */

    # Symbol Search base on Exchange id 

    public function Select_Symbol($id) {
        $this->db->order_by("symbol", "ASC");
        $query = $this->db->get_where('symbol', array('exchange_id' => $id));
        return $query;
    }

    /*     * ********************** ******************** */

    # Symbol Search  Base on On change Expiry Date

    public function Symbol_OnChange($symbol_id = null, $exchange_id = null) {
        // $this->db->order_by("symbol", "ASC"); 
        if (!is_null($exchange_id)) {
            # IS_Defalut SELECTED = 1 In First In Expiry Date 
            $this->db->order_by("is_Default", "DESC");
            $this->db->where(array('exchange_id' => $exchange_id));
        }
        if (!is_null($symbol_id)) {
            $this->db->where(array('symbol_id' => $symbol_id));
        }
        $this->db->group_by('expiries_id');
        // $this->db->group_by('ex_date');
        $query = $this->db->get('expiries');
        //print_r($this->db->last_query()); 
        return $query;
    }

    /*     * ********************** ******************** */

    # With Add Brokrage in Buy And Sell 

    public function add($data, $data_3) {
        $qty1 = $this->input->post("qty1");
        $rate1 = $this->input->post("rate1");
        $total_amt1 = $qty1 * $rate1;
        /* */
        $data2 = array("total_amt1" => abs($total_amt1));
        $newarry = array_merge($data, $data2);

        # die();

        $newarry_2 = array_merge($newarry, $data_3);
        $result = $this->db->insert('buy_sell', $newarry_2);

        # print_r($this->db->last_query()); 
        return $result;
    }

    # With Out Add Brokrage In Buy Sell 

    public function add_without_brokrage($data, $data_3) {

        $qty1 = $this->input->post("qty1");
        $rate1 = $this->input->post("rate1");
        $total_amt1 = $qty1 * $rate1;
        # die();

        /* */
        $data2 = array("total_amt1" => abs($total_amt1));
        $newarry = array_merge($data, $data2);

        $newarry_2 = array_merge($newarry, $data_3);
        $result = $this->db->insert('buy_sell', $newarry_2);
        return $result;
    }

    public function update($data, $data_3, $buysell_id) {
        $qty1 = $this->input->post("qty1");
        $rate1 = $this->input->post("rate1");
        $total_amt1 = $qty1 * $rate1;

        $data2 = array("total_amt1" => abs($total_amt1));
        $newarry = array_merge($data, $data2);
        $newarry_2 = array_merge($newarry, $data_3);

        $this->db->where('buysell_id', $buysell_id);
        $result = $this->db->update('buy_sell', $newarry_2);
        //print_r($this->db->last_query()); 
    }

    # Update  With Out Brokrage

    public function update_without_brokrage($data, $data_3, $buysell_id) {
        $qty1 = $this->input->post("qty1");
        $rate1 = $this->input->post("rate1");
        $total_amt1 = $qty1 * $rate1;

        $data2 = array("total_amt1" => abs($total_amt1));
        $newarry = array_merge($data, $data2);
        $newarry_2 = array_merge($newarry, $data_3);

        $this->db->where('buysell_id', $buysell_id);
        $result = $this->db->update('buy_sell', $newarry_2);
        //print_r($this->db->last_query()); 
    }

    public function search_record($From_Date = null, $To_Date = null, $Type_Search = null, $Client_Code_Search = null) {

        $this->db->select('*');
        $this->db->from('buy_sell');

        $this->db->join('exchange', 'buy_sell.exchange_id = exchange.exchange_id');
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');

        if ($From_Date && $To_Date && $Type_Search && $Client_Code_Search) { //
            $this->db->where('date1 >=', $From_Date);
            $this->db->where('date1 <=', $To_Date);

            $this->db->where('type', $Type_Search);
            $this->db->where('pid', $Client_Code_Search);
        }

        $query = $this->db->get();
        // print_r($this->db->last_query());
        // die();
        return $query->result();
    }

    # Delete

    public function delete($id) {
        $this->db->where('buysell_id', $id);
        $result = $this->db->delete('buy_sell');
        return $result;
    }

    /*     * ********************** ******************** */
    # DELETE BULK

    public function delete_bulk($id) {
        if (is_array($id)) {
            $this->db->where_in('buysell_id', $id);
        } else {
            $this->db->where('buysell_id', $id);
        }
        $result = $this->db->delete('buy_sell');
        return $result;
    }

    /*     * ********************** ******************** */

    // Party Name
    public function Search_Account_Name($postData) {
        $response = array();
        if (isset($postData['search'])) {
            $this->db->select('*');
            $this->db->where("username like '%" . $postData['search'] . "%' ");
            //$this->db->where("username = '".$postData['search']."'");
            // $this->db->where('ac_type', 3);

            $records = $this->db->get('account')->result();

            foreach ($records as $row) {
                $response[] = array(
                    "value" => $row->aid,
                    "label" => $row->username,
                    "party_code" => $row->code
                        // "Address"=>$row->Cus_Address,
                        // "Mobile"=>$row->Cus_Mobile
                );
            }
        }
        return $response;
    }

    /*     * ********************** ******************** */

    // Party Code
    public function Search_Account_Code($postData) {  //code
        $response = array();
        if (isset($postData['search'])) {
            $this->db->select('*');
            $this->db->where("code like '%" . $postData['search'] . "%' ");
            //$this->db->where('ac_type', 3);
            $records = $this->db->get('account')->result();

            foreach ($records as $row) {
                $response[] = array(
                    // "value"=>$row->uid,
                    "value" => $row->aid,
                    "label" => $row->code,
                    "party_name" => $row->username
                );
            }
        }
        return $response;
    }

    /*     * ********************** ******************** */

    /*     * ********************** ******************** */

    public function Search_Broker_Name($postData) {
        $response = array();
        if (isset($postData['search'])) {
            $this->db->select('*');
            $this->db->where("username like '%" . $postData['search'] . "%' ");
            // $this->db->group_start()->where('ac_type','1')->or_where('ac_type','2')->group_end();
            $records = $this->db->get('account')->result();
            //print_r($this->db->last_query()); 

            foreach ($records as $row) {
                $response[] = array(
                    "value" => $row->aid,
                    "label" => $row->username,
                    "brokrage_code" => $row->code
                );
            }
        }
        return $response;
    }

    /*     * ********************** ******************** */

    public function Search_Brokrage_Code($postData) {  //code
        // uid
        $response = array();
        if (isset($postData['search'])) {
            $this->db->select('*');
            $this->db->where("code like '%" . $postData['search'] . "%' ");
            // $this->db->group_start()->where('ac_type','1')->or_where('ac_type','2')->group_end();
            $records = $this->db->get('account')->result();

            foreach ($records as $row) {
                $response[] = array(
                    "value" => $row->aid,
                    "label" => $row->code,
                    "brokrage_name" => $row->username
                );
            }
        }
        return $response;
    }

    /*     * ********************** Edit ******************** */

    // public function Edit2($id)
    // {
    // $this->db->select('*');
    // $this->db->from('buy_sell');
    // /* $this->db->join('users', 'account.uid = users.id'); */
    // $this->db->where('buysell_id', $id ); 
    // $query = $this->db->get();
    // return $query->row_array();
    // }

    public function Edit($id) {
        $this->db->select('*');
        $this->db->from('buy_sell');
        /* $this->db->join('users', 'account.uid = users.id'); */
        $this->db->where('buysell_id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    /*     * ********************** Edit ******************** */

    public function singlerecord() {

        $this->db->select('*');
        $this->db->from('buy_sell');
        $this->db->order_by("buysell_id", "DESC");
        $this->db->limit(1);
        $query = $this->db->get();
        // print_r($this->db->last_query()); 
        return $query->row_array();
        // $this->db->where('buysell_id', MAX(buysell_id) ); 
    }

    /* same query stat  data come from account table */

    public function party_code_search($code) {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('code', $code);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function brokrage_code_search($code) {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('code', $code);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function party_id_search($pid) {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('aid', $pid);
        $query = $this->db->get();
        #print_r($this->db->last_query()); 
        return $query->row_array();
        // return	$query->result();
    }

    public function broker_id_search($bid) {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('aid', $bid);
        $query = $this->db->get();
        return $query->row_array();
    }

    /* same query end  */

    public function select_brocode() {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('ac_type', 2);
        $this->db->where('disp_order', 0);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function chk_one($id, $chk_one) {
        $id = $_REQUEST["id"];
        $chk_one = $_REQUEST["chk_one"];

        $this->db->set('chk_one', $chk_one);
        $this->db->where('buysell_id', $id);
        $result = $this->db->update('buy_sell');
        print_r($this->db->last_query());
    }

    public function chk_two($id, $chk_two) {
        $id = $_REQUEST["id"];
        $chk_two = $_REQUEST["chk_two"];

        $this->db->set('chk_two', $chk_two);
        $this->db->where('buysell_id', $id);
        $result = $this->db->update('buy_sell');
        print_r($this->db->last_query());
    }

}

// $this->db->select('*');
		//$this->db->select('MAX(buysell_id)');
//return $query;
//return	$query->result();
//$this->db->join('exchange', 'buy_sell.exchange_id = exchange.exchange_id');
		//$this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');