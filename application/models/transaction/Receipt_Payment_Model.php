<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment_Model
 *
 * @author Parth
 */
class Receipt_payment_model extends CI_Model {

    public function view($where = null,$select="*") {
        $this->db->trans_start();
        if (!is_null($where)) {
            $this->db->where("id", $where);
        }
        $this->db->select($select);
        $this->db->order_by("name", "asc");
        $query = $this->db->get('payment');
        $this->db->trans_complete();
        return $query->result();
    }

    // For Multiple Condition
    public function view_where($where = null,$select="*") {
        $this->db->trans_start();
        if (!is_null($where)) {
            $this->db->where($where);
        }
        $this->db->select($select);
        $this->db->order_by("name", "asc");
        $query = $this->db->get('payment');
        $this->db->trans_complete();
        return $query->result();
    }

    public function findname($where = null,$old=null) {
        $this->db->trans_start();
        if (!is_null($where)) {
            $this->db->where("name", $where);
        }
        if (!is_null($old)) {
            $this->db->where("name !=", $old);
        }
        $this->db->order_by("name", "asc");
        $query = $this->db->get('payment');
        $this->db->trans_complete();
        return $query->num_rows();
    }

    public function add($array) {
        $this->db->trans_start();
        $this->db->insert("payment", $array);
        $data = $this->db->insert_id();
        $this->db->trans_complete();
        return $data;
    }

    public function delete($id) {
        $this->db->trans_start();
            $this->db->where('id', $id);
            $data = $this->db->delete('payment');
            $this->db->trans_complete();
            return $data;
    }

    public function edit($array, $id) {
        $this->db->trans_start();
        $this->db->set($array);
        $this->db->where('id', $id);
        $data = $this->db->update('payment');
        $this->db->trans_complete();
        return $data;
    }

}
