<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Global_function_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function Get_Start_Date($id) {
        $this->db->select('*');
        $this->db->from('setlement');
        $this->db->where('setlement_id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function Select_Exchange_On_Change($id) {
        $this->db->order_by('setlement_id', 'DESC');
        $query = $this->db->get_where('setlement', array('exchange_id' => $id));
        return $query;
    }

    public function party_code_search($code) {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('code', $code);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function Search_Account_Code($postData) {  //code
        $response = array();
        if (isset($postData['search'])) {
            $this->db->select('*');
            $this->db->where("code like '%" . $postData['search'] . "%' ");
            //$this->db->where('ac_type', 3);
            $records = $this->db->get('account')->result();

            foreach ($records as $row) {
                $response[] = array(
                    // "value"=>$row->uid,
                    "value" => $row->aid,
                    "label" => $row->code,
                    "party_name" => $row->username
                );
            }
        }
        return $response;
    }

    public function Sel_Symbol() {
        $this->db->select('*');
        $this->db->from('symbol');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    public function Sel_Users($aid=null) {
        $this->db->select('*');
        $this->db->from('account');
        if($aid){
            $this->db->where('aid' , $aid);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function Select_Expirey() {
        $this->db->select('*');
        $this->db->from('expiries');
        $query = $this->db->get();
        return $query->result_array();
    }

}
