<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_position_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function Print_Table_2_Excel($arr1DtA, $arr1DtB, $exchange_id, $setlement_id) {

        $this->db->select('*,SUM(buy_sell.brokrage) AS `brokrage`');
        $this->db->select('SUM(buy_sell.qty1) AS `stock`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=1,buy_sell.total_amt1,0)) AS `TotalBuyRs`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=2,buy_sell.total_amt1,0)) AS `TotalSellRs`');

        #$this->db->where('buy_sell.party_code',$id); 
        $this->db->where('buy_sell.exchange_id', $exchange_id);
        $this->db->where('buy_sell.setlement_id', $setlement_id);
        $this->db->where('buy_sell.date1 >=', $arr1DtA);
        $this->db->where('buy_sell.date1 <=', $arr1DtB);

        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');

        $this->db->group_by('buy_sell.symbol_id,buy_sell.ex_date');
        $this->db->group_by('buy_sell.pid');

        $query = $this->db->get('buy_sell');
        $result = $query->result();
        return $result;

        /*
          $this->db->select('*');
          $this->db->from('buy_sell');
          $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');
          $this->db->where('buy_sell.exchange_id',$exchange_id);
          $this->db->where('buy_sell.setlement_id',$setlement_id);
          $this->db->where('buy_sell.date1 >=',$arr1DtA);
          $this->db->where('buy_sell.date1 <=',$arr1DtB);
          $query = $this->db->get();
          $result =$query->result();
          return $result;
         */
    }

    # Exchange Select 

    public function Select_Exchange() {
        $query = $this->db->get('exchange');
        return $query->result();
    }

    // glob added
    public function Select_Exchange_On_Change($id) {
        $this->db->order_by('setlement_id', 'DESC');
        $query = $this->db->get_where('setlement', array('exchange_id' => $id));
        return $query;
    }

    /*
      public function party_code_search($code)
      {
      $this->db->select('*');
      $this->db->from('account');
      $this->db->where('code', $code );
      $query = $this->db->get();
      return $query->row_array();
      }

      public function Search_Account_Code($postData)  //code
      {
      $response = array();
      if(isset($postData['search']) )
      {
      $this->db->select('*');
      $this->db->where("code like '%".$postData['search']."%' ");
      //$this->db->where('ac_type', 3);
      $records = $this->db->get('account')->result();

      foreach($records as $row )
      {
      $response[] = array(
      // "value"=>$row->uid,
      "value"=>$row->aid,
      "label"=>$row->code,
      "party_name"=>$row->username
      );
      }

      }
      return $response;
      }
     */

    public function load_first($exchange_id, $setlement_id, $arr1Dt, $arr1Dt2, $aid = null) { // party_code
        # PARTY
        $data = array();
        $this->db->select('*,SUM(buy_sell.brokrage) AS `brokrage`');
        $this->db->select('SUM(buy_sell.qty1) AS `stock`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=1,buy_sell.total_amt1,0)) AS `TotalBuyRs`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=2,buy_sell.total_amt1,0)) AS `TotalSellRs`');
        $this->db->from('buy_sell');

        // $this->db->join('account', 'buy_sell.party_code = account.code',"right");  // 28-dec
        // $this->db->join('account', 'buy_sell.pid = account.aid',"right");  // 28-dec

        $this->db->join('account AS party', 'buy_sell.pid = party.aid'); // 27-dec
        // if (!empty($party_code))	
        if (!empty($aid)) {
            // $this->db->where('buy_sell.party_code', $party_code);
            $this->db->where('buy_sell.pid', $aid);
            // $this->db->or_where('buy_sell.brokrage_code', $party_code);
            $this->db->or_where('buy_sell.bid', $aid);
        }
        $this->db->where('date1 BETWEEN "' . $arr1Dt . '" AND "' . $arr1Dt2 . '"');
        $this->db->where('buy_sell.exchange_id', $exchange_id);
        $this->db->where('buy_sell.setlement_id', $setlement_id);
        // $this->db->group_by('buy_sell.party_code');  
        $this->db->group_by('buy_sell.pid');
        $query = $this->db->get();
        $data = $query->result();

        # Broker
        $this->db->select('*,SUM(buy_sell.brokrage) AS `brokrage`');

        // $this->db->select('buy_sell.brokrage_code as  party_code,buy_sell.brokrage_name as party_name');
        $this->db->select('broker.code as brokrage_code, broker.username as brokrage_name');
        $this->db->select('SUM(buy_sell.qty1*-1) AS `stock`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=1,buy_sell.total_amt1,0))  AS `TotalBuyRs`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=2,buy_sell.total_amt1,0))  AS `TotalSellRs`');
        $this->db->from('buy_sell');
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');
        $this->db->join('account AS broker', 'buy_sell.bid = broker.aid'); // 28-dec 
        $this->db->where('date1 BETWEEN "' . $arr1Dt . '" AND "' . $arr1Dt2 . '"');
        $this->db->where('buy_sell.exchange_id', $exchange_id);
        $this->db->where('buy_sell.setlement_id', $setlement_id);
        // $this->db->group_by('buy_sell.brokrage_code'); // 28-dec 
        $this->db->group_by('buy_sell.bid');
        $query = $this->db->get();
        $data = array_merge($data, $query->result());
        #print_r($this->db->last_query());
        return $data;
    }

    /*
      public function load_second()
      {
      $this->db->select('*');
      $this->db->from('buy_sell');
      $this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');   // add 17-dec
      $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');
      $this->db->group_by('buy_sell.symbol_id');
      $query = $this->db->get();
      return   $query->result();
      }
     */

    # On Change Table DBL CLICK  SECOND TABLE  stock

    public function onchange_second($id) {
        # Client; 

        $this->db->select('*,SUM(buy_sell.brokrage) AS `brokrage`');
        $this->db->select('SUM(buy_sell.qty1) AS `stock`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=1,buy_sell.total_amt1,0)) AS `TotalBuyRs`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=2,buy_sell.total_amt1,0)) AS `TotalSellRs`');

        $this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');   // add 17-dec
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');

        // $this->db->where('buy_sell.party_code',$id); // 28-dec
        $this->db->where('buy_sell.pid', $id);  // 28-dec
        // $this->db->group_by('buy_sell.symbol_id,buy_sell.ex_date');  //  17 dec
        $this->db->group_by('buy_sell.symbol_id,buy_sell.expiries_id');
        $query = $this->db->get('buy_sell');

        #print_r($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {

            // $this->db->select('*,buy_sell.brokrage_code as  party_code,buy_sell.brokrage_name as party_name,sum(buy_sell.total_amt1-(-brokrage))*-1 AS `remains_value`,sum(buy_sell.total_amt1)*-1 AS `total_amt1`');
            # Broker
            // 10 oct	
            // $this->db->select('*,sum(buy_sell.qty1) as stock,sum(buy_sell.total_amt1-(-brokrage)) AS `remains_value`,sum(buy_sell.total_amt1) AS `total_amt1`');
            // $this->db->select('*,sum(buy_sell.qty1*-1) as stock,sum(buy_sell.total_amt1-(-brokrage))*-1 AS `remains_value`,sum(buy_sell.total_amt1)*-1 AS `total_amt1`');
            $this->db->select('*,SUM(buy_sell.brokrage) AS `brokrage`'); // 29 dec
            // $this->db->select('buy_sell.brokrage_code as  party_code,buy_sell.brokrage_name as party_name'); //   29 dec
            $this->db->select('SUM(buy_sell.qty1*-1) AS `stock`');
            $this->db->select('SUM(IF(buy_sell.buy_sell_id=1,buy_sell.total_amt1,0))*-1  AS `TotalBuyRs`');
            $this->db->select('SUM(IF(buy_sell.buy_sell_id=2,buy_sell.total_amt1,0))*-1  AS `TotalSellRs`');
            $this->db->from('buy_sell');
            $this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');   // add 17-dec
            $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');

            // $this->db->where('buy_sell.brokrage_code',$id);
            $this->db->where('buy_sell.bid', $id);  // 28-dec
            $this->db->group_by('buy_sell.symbol_id,buy_sell.expiries_id');   // 17 dec
            // $this->db->group_by('buy_sell.symbol_id,buy_sell.ex_date');  // 17 dec
            $query = $this->db->get();
            return $query->result();
        }
    }

    # Three Table

    public function load_three($exchange_id, $setlement_id, $arr1Dt, $arr1Dt2) {
        // exit;
        $this->db->select('*');
        $this->db->from('buy_sell');
        
       
        
        $this->db->where('buy_sell.date1 BETWEEN "' . $arr1Dt . '" AND "' . $arr1Dt2 . '"');
        
        $this->db->where('buy_sell.setlement_id', $setlement_id);
        $this->db->where('buy_sell.exchange_id', $exchange_id);
        
        
         $this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');
        
        // $this->db->group_by('buy_sell.symbol_id');
        $this->db->group_by('buy_sell.symbol_id,buy_sell.expiries_id');   //  dec
        $query = $this->db->get();
        
        return $query->result();
    }

    /*
      public function load_three_code($exchange_id , $setlement_id , $arr1Dt, $arr1Dt2 , $party_code)
      {
      $this->db->select('*');
      $this->db->from('buy_sell');
      $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');

      $this->db->where('date1 BETWEEN "'. $arr1Dt . '" AND "'.$arr1Dt2.'"');
      $this->db->where('buy_sell.exchange_id',$exchange_id);
      $this->db->where('buy_sell.setlement_id',$setlement_id);

      $this->db->where('buy_sell.party_code', $party_code);
      $this->db->or_where('buy_sell.brokrage_code', $party_code);
      $this->db->group_by('buy_sell.symbol_id');
      $query = $this->db->get();
      return   $query->result();
      }
     */

    # NOt Need Four 

    public function load_four() {
        $this->db->select('*');
        $this->db->from('buy_sell');
        $this->db->join('account', 'buy_sell.party_code = account.code');
        $this->db->group_by('buy_sell.pid');
        $query = $this->db->get();
        #print_r($this->db->last_query()); 
        return $query->result();
    }

    # NOt Need Four 

    public function onchange_four($id) {

        $this->db->select('*,SUM(buy_sell.brokrage) AS `brokrage`');
        $this->db->select('SUM(buy_sell.qty1) AS `stock`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=1,buy_sell.total_amt1,0)) AS `TotalBuyRs`');
        $this->db->select('SUM(IF(buy_sell.buy_sell_id=2,buy_sell.total_amt1,0)) AS `TotalSellRs`');

        // $this->db->select('*,sum(buy_sell.total_amt1-(-brokrage)) AS `remains_value`,sum(buy_sell.total_amt1) AS `total_amt1`');

        $this->db->from('buy_sell');
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');
        $this->db->where('buy_sell.symbol_id', $id);
        $this->db->group_by('buy_sell.symbol_id');
        $this->db->group_by('buy_sell.pid');
        $query = $this->db->get();
        #print_r($this->db->last_query()); 
        return $query->result();
    }

    # PopupTbl2

    public function PopupTbl2($id) {
        $this->db->select('*');
        $this->db->select('party.code as party_code, party.username as party_name');
        $this->db->select('broker.code as brokrage_code, broker.username as brokrage_name');
        $this->db->from('buy_sell');
        $this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');
        $this->db->join('account AS party', 'buy_sell.pid = party.aid'); // 30-dec
        $this->db->join('account AS broker', 'buy_sell.bid = broker.aid'); // 30-dec
        $this->db->where('buy_sell.symbol_id', $id);
        $this->db->group_by('buy_sell.expiries_id');  // 30 dec
        $query = $this->db->get();
        return $query->result();
    }

    # Print Summary 

    public function Summary() {
        $this->db->select('*,sum(buy_sell.total_amt1)*-1 AS `Total_PL`');
        $this->db->from('buy_sell');
        $this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');
        // $this->db->group_by('buy_sell.symbol_id,buy_sell.ex_date');   // 17 dec
        $this->db->group_by('buy_sell.symbol_id,buy_sell.expiries_id');  // 17 dec
        $query = $this->db->get();
        return $query->result();
    }

    # Insert Ltp

    public function insert_ltp($d) {

        $this->db->where('buy_sell.symbol_id', $d['symbol_id_glob']);
        $this->db->where('buy_sell.setlement_id', $d['setlement_id']);
        $this->db->where('buy_sell.exchange_id', $d['exchange_id']);
        $result = $this->db->update('buy_sell', ["buy_sell.ltp" => $d['ltp']]);
        #print_r($this->db->last_query()); 
        return $result;
    }

}
