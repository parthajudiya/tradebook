<?php
//	print_r($this->db->last_query()); 
defined('BASEPATH') OR exit('No direct script access allowed');

class Bill_model extends CI_Model {

    public function Select_Exchange()
	{
		$query = $this->db->get('exchange');
		return $query->result();
	}
	
	public function Select_Exchange_On_Change($id)
	{
		$query = $this->db->get_where('setlement', array('exchange_id' => $id ));
		return $query;
	}
	
	public function party_code_search($code)
	{
		$this->db->select('*');
		$this->db->from('account');
		$this->db->where('code', $code ); 
		$query = $this->db->get();
		return $query->row_array();
	}
	
	public function Search_Account_Code($postData)  //code
	{
		$response = array();
		if(isset($postData['search']) )
		{
			$this->db->select('*');
			$this->db->where("code like '%".$postData['search']."%' ");
			//$this->db->where('ac_type', 3);
			$records = $this->db->get('account')->result();
   
		  foreach($records as $row )
		  {
				$response[] = array(
				 // "value"=>$row->uid,
				 "value"=>$row->aid,
				 "label"=>$row->code,
				 "party_name"=>$row->username
				);
		  }
   
		}
		return $response;
	}
	/////////////////// Code Start ///////////////////////////////////////
	
	public function load_first($exchange_id , $setlement_id , $arr1Dt, $arr1Dt2 , $party_code=null)
	{ 
		$this->db->select('*');
		$this->db->from('buy_sell');
		$this->db->join('account', 'buy_sell.party_code = account.code'); 
		$this->db->group_by('account.code');  
		$this->db->where('date1 BETWEEN "'. $arr1Dt . '" AND "'.$arr1Dt2.'"');
		$this->db->where('buy_sell.exchange_id',$exchange_id);
		$this->db->where('buy_sell.setlement_id',$setlement_id);
		
		//if($party_code){
		if (!empty($party_code)){	
			$this->db->where('account.code', $party_code);
			}
		
		$query = $this->db->get();
		$data = $query->result();
		return   $data; 
	}
	

	
	
	public function load_second($id)
	{
		/*
		$this->db->select('*,SUM(buy_sell.brokrage) AS `brokrage`');
		$this->db->select('SUM(buy_sell.qty1) AS `stock`');
		$this->db->select('SUM(IF(buy_sell.buy_sell_id=1,buy_sell.total_amt1,0)) AS `TotalBuyRs`');
		$this->db->select('SUM(IF(buy_sell.buy_sell_id=2,buy_sell.total_amt1,0)) AS `TotalSellRs`');
		$this->db->join('account', 'buy_sell.party_code = account.code'); 
		$this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id');   
		$this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id');  
		$this->db->where('account.code',$id); 
		$this->db->group_by('buy_sell.symbol_id,buy_sell.expiries_id');  
		$query = $this->db->get('buy_sell');
		$data2 = $query->result();
		return   $data2; 
	*/
	
		
		$this->db->select('*');
		$this->db->from('buy_sell');
		$this->db->join('account', 'buy_sell.party_code = account.code'); 
		$this->db->join('symbol', 'buy_sell.symbol_id = symbol.symbol_id'); 
		$this->db->join('expiries', 'buy_sell.expiries_id = expiries.expiries_id'); 
		//$this->db->group_by('buy_sell.buy_sell_id'); 
		$this->db->where('account.code', $id);
		$this->db->order_by('buy_sell.symbol_id' , "ASC"); 
		$query = $this->db->get();
		$data2 = $query->result();
		return   $data2; 
		
	}
	

}
