<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Party_ledger_model extends CI_Model {

    # Exchange Select
    public function Select_Exchange()
    {
      $query = $this->db->get('exchange');
      return $query->result();
    }
	
	public function ResultTable($exchange_id , $arr1Dt=null , $arr1Dt2=null , $party_code=null , $party_name=null)
	{
		
		// $this->db->select('*');
		// $this->db->select('*,sum(buy_sell.total_amt1-(-brokrage))*-1 AS `remains_value`,sum(buy_sell.total_amt1)*-1 AS `total_amt1`');
		$this->db->select('*,sum(buy_sell.total_amt1)*-(-1) AS `remains_value` , sum(buy_sell.total_amt1)*-1 AS `total_amt1` ');
		// $this->db->select('*,sum(buy_sell.total_amt1)*-1 AS `remains_value`,sum(buy_sell.total_amt1)*-1 AS `total_amt1`');
		$this->db->from('buy_sell');
		$this->db->join('exchange', 'buy_sell.exchange_id = exchange.exchange_id'); 
		$this->db->join('setlement', 'buy_sell.setlement_id = setlement.setlement_id'); 
		//$this->db->join('account', 'buy_sell.party_code = account.code'); 
		
		$this->db->where('buy_sell.party_code',$party_code);
		$this->db->where('buy_sell.party_name',$party_name);
		if(!is_null($arr1Dt) && !is_null($arr1Dt2)){
			$this->db->where('date1 BETWEEN "'. $arr1Dt . '" AND "'.$arr1Dt2.'"');
			$this->db->group_by('setlement.setlement_id');  
		}else{
			$this->db->where('date1 <',$arr1Dt);
		}
		
		$this->db->where('buy_sell.exchange_id',$exchange_id);
		$query = $this->db->get();
		$data = $query->result();
		return   $data; 
		
		/*
		$this->db->select('*,sum(buy_sell.total_amt1-(-brokrage))*-1 AS `remains_value`,sum(buy_sell.total_amt1)*-1 AS `total_amt1`');
		$this->db->from('buy_sell');
		$this->db->join('account', 'buy_sell.party_code = account.code'); 
		$this->db->where('date1 BETWEEN "'. $arr1Dt . '" AND "'.$arr1Dt2.'"');
		$this->db->where('buy_sell.exchange_id',$exchange_id);
		$this->db->where('buy_sell.setlement_id',$setlement_id);
		$this->db->where('buy_sell.party_code', $party_code);
		$this->db->or_where('buy_sell.brokrage_code', $party_code);
		$this->db->group_by('buy_sell.party_code');  
		$query = $this->db->get();
		$data=$query->result();
		*/
		
		
		
		
	}
}
