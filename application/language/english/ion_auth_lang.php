<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
* Description:  English language file for Ion Auth messages and errors
*/


// Th Lable And Droup Down
$lang['Action']  = 'Action';
$lang['Delete_Bulk']  = 'Delete Bulk';







//Master - Account

$lang['Account_Header']  = 'Account Master';
$lang['Account_Title']  = 'Account';
$lang['Account_Code']  = 'A/c Code';
$lang['Account_Name']  = 'A/c Name';
$lang['Account_Type']  = 'A/c Type';
$lang['Mobile_No']  = 'Mobile No';
$lang['Email_id']  = 'Email ID';
$lang['Account_Group']  = 'Group';
$lang['Disp_Order']  = 'Disp Order';
$lang['Closing_Rate_Type']  = 'Closing Rate Type';
$lang['Account_Code_F']  = 'Account Code';
$lang['Account_Name_F']  = 'Name';
$lang['Ac_Type_F']  = 'Ac Type';
$lang['Mobile_F']  = 'Mobile';
$lang['Email_F']  = 'Email';
$lang['Disp_Ord_F']  = 'Disp Ord';

//Master - Exchange

$lang['Exchange_Header']  = 'Exchange Master';
$lang['Exchange_Title']  = 'Exchange';
$lang['Exchange_Name']  = 'Exchange Name';
$lang['Rate_Decimal_Point']  = 'Rate Decimal Point';


//Master - Brokrage
$lang['Brokrage_Header']  = 'Brokrage Master';
$lang['Brokrage_Title']  = 'Brokrage';

$lang['Brokrage_Account_Code']  = 'Account Code';
$lang['Brokrage_Name']  = 'Name';
$lang['Brokrage_Symbol']  = 'Symbol';
$lang['Brokrage_Exchange']  = 'Exchange';

$lang['Brokrage_Type']  = 'Type';
$lang['Brokrage_Base_On']  = 'Base On';
$lang['Brokrage_Intra_Day']  = 'Intra Day';
$lang['Brokrage_Side']  = 'Side';
$lang['Brokrage_Next_Day']  = 'Next Day';
$lang['Brokrage_Minimum']  = 'Minimum';


//Master - Symbol

$lang['Symbol_Header']  = 'Symbol Master';
$lang['Symbol_Title']  = 'Symbol';
$lang['Symbol_Instument']  = 'Instument';
$lang['Symbol']  = 'Symbol';
$lang['Symbol_Lot_size']  = 'Lot size';
$lang['Symbol_Exchange']  = 'Exchange';


//Master - Settlment
$lang['Settlment_Header']  = 'Settlment Master';
$lang['Settlment_Title']  = 'Settlment';
$lang['Settlment_No']  = 'Settlment No';
$lang['Settlment_Start_Date']  = 'Start Date';
$lang['Settlment_End_Date']  = 'End Date';
$lang['Settlment_Description']  = 'Description';
$lang['Settlment_Ledger']  = 'Ledger';
$lang['Settlment_Exchange']  = 'Exchange';


//Master - Expiry
$lang['Expiry_Header']  = 'Expiry Master';
$lang['Expiry_Title']  = 'Expiry';
$lang['Expiry_Exchange']  = 'Exchange';
$lang['Expiry_ex_date']  = 'Expiry Date';
$lang['Expiry_Exchange']  = 'Exchange';



// Master - Remiser Brokrage

$lang['Remiser_Header']  = 'Remiser Brokrage';
$lang['Remiser_Title']  = 'Remiser';
$lang['Remise_Srno']  = 'Sr.no';
$lang['Remise_Exchange']  = 'Exchange';
$lang['Remise_Main_Ac_code']  = 'Main A/c_code';
$lang['Remise_Main_Ac_Name']  = 'Name';
$lang['Remise_Ac_code']  = 'R A/c_code';
$lang['Remise_Ac_Name']  = 'Name';
$lang['Remiser']  = 'Remiser %';




//transaction - Buy sell
$lang['BuySell_Title']  = 'Buy / Sell';
$lang['BuySell_Header']  = 'Buy / Sell Master';
$lang['Remiser_Title']  = 'Remiser';
$lang['label_Exchange']  = 'Exchange';
$lang['label_settlment']  = 'Settlment';
$lang['label_Tr_No']  = 'Tr.No';
$lang['label_BuySell']  = 'Buy/Sell';
$lang['label_Symbol']  = 'Symbol';
$lang['label_Instument']  = 'Instument';
$lang['label_Expiry']  = 'Expiry';
$lang['label_Type']  = 'Type';
$lang['label_Date']  = 'Date';


// transaction  eod high low check

$lang['Eod_High_Low_Heeader']  = 'Eod High / Low Check';
$lang['Eod_High_Low_Check_Title']  = 'Eod High / Low Check';

// transaction  receipt_payment

$lang['Receipt_Payment_Heeader']  = 'Receipt / Payment';
$lang['Receipt_Payment_Title']  = 'Receipt / Payment';


// process closing_forward

$lang['closing_forward_Header'] = "Closing Forward";
$lang['closing_forward_Title'] = "Closing Forward";
$lang['LBL_Closing_Setlmennt'] = "Closing Setlmennt";
$lang['LBL_Opening_Setlmennt'] = "Opening Setlmennt";
$lang['LBL_Start_Date'] = "Start Date";
$lang['LBL_End_Date'] = "End Date";
$lang['LBL_Closing_Rate_Type'] = "Closing Rate Type";

$lang['LBL_Next_Open_Date'] = "Next Open Date";

$lang['LBL_Code'] = "Code";
$lang['LBL_Name'] = "Name";
$lang['LBL_Symbol'] = "Symbol";
$lang['LBL_Instrument'] = "Instrument";
$lang['LBL_Expiry'] = "Expiry";
$lang['LBL_Stock'] = "Stock";
$lang['LBL_Closing_Rate'] = "Closing Rate";

















// $lang['Send_SMS']  = 'Send SMS';



// Account Creation
$lang['account_creation_successful']            = 'Account Successfully Created';
$lang['account_creation_unsuccessful']          = 'Unable to Create Account';
$lang['account_creation_duplicate_email']       = 'Email Already Used or Invalid';
$lang['account_creation_duplicate_identity']    = 'Identity Already Used or Invalid';
$lang['account_creation_missing_default_group'] = 'Default group is not set';
$lang['account_creation_invalid_default_group'] = 'Invalid default group name set';


// Password
$lang['password_change_successful']          = 'Password Successfully Changed';
$lang['password_change_unsuccessful']        = 'Unable to Change Password';
$lang['forgot_password_successful']          = 'Password Reset Email Sent';
$lang['forgot_password_unsuccessful']        = 'Unable to email the Reset Password link';

// Activation
$lang['activate_successful']                 = 'Account Activated';
$lang['activate_unsuccessful']               = 'Unable to Activate Account';
$lang['deactivate_successful']               = 'Account De-Activated';
$lang['deactivate_unsuccessful']             = 'Unable to De-Activate Account';
$lang['activation_email_successful']         = 'Activation Email Sent. Please check your inbox or spam';
$lang['activation_email_unsuccessful']       = 'Unable to Send Activation Email';
$lang['deactivate_current_user_unsuccessful']= 'You cannot De-Activate your self.';

// Login / Logout
$lang['login_successful']                    = 'Logged In Successfully';
$lang['login_unsuccessful']                  = 'Incorrect Login';
$lang['login_unsuccessful_not_active']       = 'Account is inactive';
$lang['login_timeout']                       = 'Temporarily Locked Out.  Try again later.';
$lang['logout_successful']                   = 'Logged Out Successfully';

// Account Changes
$lang['update_successful']                   = 'Account Information Successfully Updated';
$lang['update_unsuccessful']                 = 'Unable to Update Account Information';
$lang['delete_successful']                   = 'User Deleted';
$lang['delete_unsuccessful']                 = 'Unable to Delete User';

// Groups
$lang['group_creation_successful']           = 'Group created Successfully';
$lang['group_already_exists']                = 'Group name already taken';
$lang['group_update_successful']             = 'Group details updated';
$lang['group_delete_successful']             = 'Group deleted';
$lang['group_delete_unsuccessful']           = 'Unable to delete group';
$lang['group_delete_notallowed']             = 'Can\'t delete the administrators\' group';
$lang['group_name_required']                 = 'Group name is a required field';
$lang['group_name_admin_not_alter']          = 'Admin group name can not be changed';

// Activation Email
$lang['email_activation_subject']            = 'Account Activation';
$lang['email_activate_heading']              = 'Activate account for %s';
$lang['email_activate_subheading']           = 'Please click this link to %s.';
$lang['email_activate_link']                 = 'Activate Your Account';

// Forgot Password Email
$lang['email_forgotten_password_subject']    = 'Forgotten Password Verification';
$lang['email_forgot_password_heading']       = 'Reset Password for %s';
$lang['email_forgot_password_subheading']    = 'Please click this link to %s.';
$lang['email_forgot_password_link']          = 'Reset Your Password';

